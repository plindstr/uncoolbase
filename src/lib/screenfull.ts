/**
 * Clean version of Screenfull.js
 */

// Find function to use
let fn: { [name: string]: string } = (() => {

    const fnMap: string[][] = [
        [
            'requestFullscreen',
            'exitFullscreen',
            'fullscreenElement',
            'fullscreenEnabled',
            'fullscreenchange',
            'fullscreenerror'
        ],
        // new WebKit
        [
            'webkitRequestFullscreen',
            'webkitExitFullscreen',
            'webkitFullscreenElement',
            'webkitFullscreenEnabled',
            'webkitfullscreenchange',
            'webkitfullscreenerror'

        ],
        // old WebKit (Safari 5.1)
        [
            'webkitRequestFullScreen',
            'webkitCancelFullScreen',
            'webkitCurrentFullScreenElement',
            'webkitCancelFullScreen',
            'webkitfullscreenchange',
            'webkitfullscreenerror'

        ],
        [
            'mozRequestFullScreen',
            'mozCancelFullScreen',
            'mozFullScreenElement',
            'mozFullScreenEnabled',
            'mozfullscreenchange',
            'mozfullscreenerror'
        ],
        [
            'msRequestFullscreen',
            'msExitFullscreen',
            'msFullscreenElement',
            'msFullscreenEnabled',
            'MSFullscreenChange',
            'MSFullscreenError'
        ]
    ];

    let ret: { [name: string]: string } = {};

    for(let i = 0; i < fnMap.length; ++i) {
        let val = fnMap[i];
        if(val && val[1] in document) {
            for(let j = 0; j < val.length; ++j) {
                ret[fnMap[0][i]] = val[i];
            }
        }
    }

    return ret;

})();

export function request(elem: HTMLElement) {
    let request = fn['requestFullscreen'];
    elem = elem || document.documentElement;
    (<any>elem)[request]();
};

export function exit() {
    (<any>document)[fn['exitFullscreen']]();
};

export function toggle(elem: HTMLElement) {
    if (isFullscreen) {
        exit();
    } else {
        request(elem);
    }
};

export function isFullscreen(): boolean {
    return !!(<any>document)[fn['fullscreenElement']];
}

export function getElement(): Element {
    return (<Element>(<any>document)[fn['fullscreenElement']]);
}

export function isEnabled(): boolean {
    return (<any>document)[fn['fullscreenEnabled']];
}
