/**
 * Uncool Vibes - vibration API wrapper.
 */

/**
 * Stop current vibration
 */
export function killVibe() {
    window.navigator && window.navigator.vibrate && (window.navigator.vibrate(0));
}

/**
 * Try to vibrate for a set duration.
 *
 * @param duration duration in milliseconds, or alternating on/off pattern duration array
 */
export function vibrate(duration: number | number[]): void {
    window.navigator && window.navigator.vibrate && (window.navigator.vibrate(duration));
}
