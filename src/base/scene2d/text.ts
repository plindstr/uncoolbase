import { Node2D, PivotPosition } from "./node";
import { AABB, Matrix } from "../math";
import { StaticPool } from "../mem";
import { Font, Surface, Renderer2D } from "../graphics";

interface RenderCoord {

    // Source image coordinates
    sx: number;
    sy: number;
    sw: number;
    sh: number;

    // Destination offsets
    dx: number;
    dy: number;
    dw: number;
    dh: number;
}

export class Text2D extends Node2D {

    private _text_font: Font;
    private _text_text: string;
    private _text_bounds: AABB;
    private _text_aabb: AABB;
    private _text_pivot: PivotPosition = null;
    private _text_render: Surface = null;
    private _text_prerender: boolean = false;
    private _text_renderCoords: StaticPool<RenderCoord>;

    constructor(font: Font, text: string = "") {
        super();
        this._text_font = font;
        this._text_text = text;
        this._text_renderCoords = new StaticPool<RenderCoord>(() => {
            return {
                sx: 0,
                sy: 0,
                sw: 1,
                sh: 1,

                // Destination offsets
                dx: 0,
                dy: 0,
                dw: 1,
                dh: 1
            }
        });
        this._text_bounds = new AABB();
        this._text_aabb = new AABB();
        this._repaint();

        this.markAsDrawable();
    }

    public setPrerender(enable: boolean): Text2D {
        if(enable == this._text_prerender) return;

        this._text_prerender = enable;
        if(!(enable || this._text_text.length < 16) && this._text_render) {
            this._text_render = null;
        } else {
            this._repaint();
        }

        return this;
    }

    public isPrerenderEnabled(): boolean {
        return this._text_prerender;
    }

    public setPivotPosition(p: PivotPosition): Node2D {
        this._text_pivot = p;
        return super.setPivotPosition(p);
    }

    public setPivotXY(x: number, y: number): Node2D {
        this._text_pivot = null;
        return super.setPivotXY(x,y);
    }

    public setFont(f: Font): Text2D {
        this._text_font = f;
        this._repaint();
        return this;
    }

    public setText(str: string): Text2D {
        if(this._text_text != str) {
            this._text_text = str;
            this._repaint();
        }
        return this;
    }

    public getBounds(): AABB {
        return this._text_bounds;
    }

    public getText(): string {
        return this._text_text;
    }

    public getAABB(): AABB {
        let aabb = this._text_aabb;
        let bounds = this._text_bounds;
        let pivot = this.getPivot();
        let mtx = this.getLocalToGlobalMatrix();
        let p00 = mtx.projectXY(bounds.getX0() - pivot.x, bounds.getY0() - pivot.y);
        let p01 = mtx.projectXY(bounds.getX0() - pivot.x, bounds.getY1() - pivot.y);
        let p10 = mtx.projectXY(bounds.getX1() - pivot.x, bounds.getY0() - pivot.y);
        let p11 = mtx.projectXY(bounds.getX1() - pivot.x, bounds.getY1() - pivot.y);

        aabb.set(p00);
        aabb.add(p01);
        aabb.add(p10);
        aabb.add(p11);

        return aabb;
    }

    private _repaint(): void {
        let s = this._text_text;
        let l = s.length;
        let rc = this._text_renderCoords;
        let bb = this._text_bounds;
        let f = this._text_font;
        let pp = this._text_pivot;
        let pr = this._text_prerender;
        let rr = this._text_render;
        rc.clear();

        let first = true;
        let yy = 0;
        let xx = 0;
        for(let i = 0; i < l; ++i) {
            let c = s.charAt(i);
            let g = f.getGlyph(c);
            if(g && g.w > 0 && g.h > 0) {
                let r = rc.alloc();
                let k = 0;
                if(i < l - 1) {
                    k = f.getKerning(c, s.charAt(i + 1));
                }
                r.sx = g.x;
                r.sy = g.y;
                r.sw = g.w;
                r.sh = g.h;
                r.dx = xx + g.xoffset;
                r.dy = yy + g.yoffset;
                r.dw = g.w;
                r.dh = g.h;

                xx += g.advance + k;

                if(first) {
                    bb.setXY(r.dx, r.dy);
                    first = false;
                } else {
                    bb.addXY(r.dx, r.dy);
                }

                bb.addXY(r.dx + r.dw, r.dy);
                bb.addXY(r.dx + r.dw, r.dy + r.dh);
                bb.addXY(r.dx, r.dy + r.dh);

            } else if(g) {

                xx += g.advance;

                if(first) {
                    bb.setXY(xx, 0);
                    first = false;
                } else {
                    bb.addXY(xx, 0);
                }
            }
        }

        this.setSizeXY(bb.getWidth(), f.getLineHeight());

        if(pp) {
            super.setPivotPosition(pp);
        }

        if(pr && s.length >= 16) {
            if(!rr) { this._text_render = new Surface(); }
            this.renderOffscreen();
        }
    }

    private renderOffscreen(): void {
        let bb = this._text_bounds;
        let rr = this._text_render;
        let f = this._text_font;
        let rc = this._text_renderCoords;
        let w = bb.getWidth();
        let h = bb.getHeight();
        if(rr.getWidth() < w || rr.getHeight() < h) {
            rr.setSize(w,h);
        }

        let e = f.getImage().getElement();
        let offsX = -bb.getX0();
        let offsY = -bb.getY0();
        let ctx = rr.getContext2D();  // we're ALWAYS working with Context2D here
        ctx.clearRect(0, 0, w + 10, h + 10);
        for(let i = 0, l = rc.size(); i < l; ++i) {
            let g = rc.get(i);
            ctx.drawImage(e, g.sx, g.sy, g.sw, g.sh, g.dx + offsX, g.dy + offsY, g.dw, g.dh);
        }
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {
        let rc = this._text_renderCoords;
        let f = this._text_font;
        let p = this.getPivot();
        if(this._text_prerender && this._text_text.length >= 16) {
            // Draw single pre-rendered image
            let bb = this._text_bounds;
            let x = bb.getX0();
            let y = bb.getY0();
            let w = bb.getWidth();
            let h = bb.getHeight();
            r.beginImages(this._text_render, 1);
            r.addSubImage(x - p.x, y - p.y, 1, 1, 0, 0, w, h, 1, 0, 0, 1);
        } else {
            // Draw every character individually (slow, but uses less memory)
            r.beginImages(f.getImage(), rc.size());
            for(let i = 0, l = rc.size(); i < l; ++i) {
                let g = rc.get(i);
                r.addSubImage(g.dx - p.x, g.dy - p.y, 1, 1, g.sx, g.sy, g.sw, g.sh, 1, 0, 0, 1);
            }
        }

        r.setMatrix(mtx);
        r.setBlendMode(this.getBlendMode());
        r.setAlpha(alpha);
        r.draw();
    }

}
