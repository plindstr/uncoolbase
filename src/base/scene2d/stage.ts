import { Node2D } from "./node";
import { Matrix, Vec2, AABB } from "../math";
import { Camera } from "./camera";
import { StaticPool } from "../mem";
import { getMouse, getTouch, MouseButton, getLastInputMethod, InputMethod } from "../input";
import { getScreenScaling } from "../core";
import { Button2D } from "./button";
import { Surface } from "../graphics/surface";
import { getScreen } from "../core/screen";
import { clamp } from "../math/coremath";

class DrawNode {
    public node: Node2D = null;
    public matrix: Matrix = null;
    public alpha: number = 1;
}

const tvec = new Vec2();
const tvec2 = new Vec2();
const aabb = new AABB();

/**
 * Base container for 2D graphics. Handles recursive
 * drawing of Nodes of all types.
 */
export class Stage2D {

    private _stage_output: Surface;
    private _stage_root: Node2D = new Node2D();
    private _stage_camera: Camera = new Camera(this);
    private _stage_drawOrder = new StaticPool<DrawNode>(() => new DrawNode());
    private _stage_matrices = new StaticPool<Matrix>(() => new Matrix());
    private _stage_matrix: Matrix = new Matrix();
    private _stage_invMatrix: Matrix = new Matrix();
    private _stage_tempvector: Vec2 = new Vec2();
    private _stage_visrect: AABB = new AABB();
    private _stage_dirty: boolean = true;
    private _stage_alpha: number = 1.0;

    public constructor(output: Surface = null) {
        if(output === null) {
            output = getScreen();
        }
        this._stage_output = output;
        this._stage_root.setStage(this);
    }

    public markAsDirty(): Stage2D {
        this._stage_dirty = true;
        return this;
    }

    public isDirty(): boolean {
        return this._stage_dirty;
    }

    public getOutput(): Surface {
        return this._stage_output;
    }

    public setOutput(out: Surface): Stage2D {
        this._stage_output = (out == null ? getScreen() : out);
        return this;
    }

    public getCamera(): Camera {
        return this._stage_camera;
    }

    public addChild(node: Node2D): Stage2D {
        this._stage_root.addChild(node);
        return this;
    }

    public addChildren(nodes: Node2D[]): Stage2D {
        this._stage_root.addChildren(nodes);
        return this;
    }

    public clearChildren(): Stage2D {
        this._stage_root.clearChildren();
        return this;
    }

    public getChildCount(): number {
        return this._stage_root.getChildCount();
    }

    public getChildCountDeep(): number {
        return this._stage_root.getChildCountDeep();
    }

    public getMatrix(): Matrix {
        this.updateMatrix();
        return this._stage_matrix;
    }

    public setAlpha(alpha: number): Stage2D {
        this._stage_alpha = clamp(alpha, 0, 1);
        return this;
    }

    public getAlpha(): number {
        return this._stage_alpha;
    }

    private getDrawables(node: Node2D, mtx: Matrix, alpha: number) {
        const visrect = this._stage_visrect;
        let n = node.getFirstChild();
        while(n != null) {
            if(n.isVisible()) {
                let a = alpha * n.getAlpha();
                let m = this._stage_matrices.alloc();
                m.set(mtx).multiply(n.getMatrix());

                if(n.isDrawable()) {
                    n.calcAABB(aabb, m);
                    let shouldDraw = n.isSticky() || aabb.isTouching(visrect);

                    if(shouldDraw) {
                        let d = this._stage_drawOrder.alloc();
                        d.node = n;
                        d.matrix = m;
                        d.alpha = a;
                    }
                }

                this.getDrawables(n, m, a);
            }
            n = n.getNextNode();
        }
    }

    public draw(): void {
        // Don't do any drawing if there's nothing to draw
        if(this._stage_root.getChildCount() === 0) return;

        this.updateMatrix();
        this._stage_drawOrder.clear();
        this._stage_matrices.clear();

        let r = this._stage_output.getRenderer();
        r.init();

        this.getDrawables(this._stage_root, Matrix.IDENTITY, this._stage_alpha);

        let sm = this._stage_matrix;
        for(let i = 0, l = this._stage_drawOrder.size(); i < l; ++i) {
            let d = this._stage_drawOrder.get(i);
            d.node.draw(r, d.matrix.prepend(sm), d.alpha);
        }
        this._stage_dirty = true;

        r.end();
    }

    private updateMatrix() {
        if(this._stage_dirty) {
            let ow = this._stage_output.getWidth();
            let oh = this._stage_output.getHeight();

            this._stage_matrix
                .identity()
                .translateXY(ow * .5, oh * .5)
                .multiply(this._stage_camera.getMatrix());
            this._stage_invMatrix.set(this._stage_matrix).invert();
            this._stage_dirty = false;

            // Calculate visible world rect
            this._stage_invMatrix.projectXY(0, 0, tvec2);
            this._stage_visrect.set(tvec2);
            this._stage_invMatrix.projectXY(ow, oh, tvec2);
            this._stage_visrect.add(tvec2);
        }
    }

    //
    // Coordinate conversion
    //

    /**
     * Return the world-space bounding box of the area visible on screen
     */
    public getVisibleRect(): AABB {
        this.updateMatrix();
        return this._stage_visrect;
    }

    /**
     * Translate coordinates from screen-space (top-left 0,0) to world-space
     *
     * @param coordinates screen-space coordinates
     * @param target [optional] vector to store result in
     * @returns target vector or internal temporary
     */
    public screenToWorld(coordinates: Vec2, target: Vec2 = this._stage_tempvector): Vec2 {
        this.updateMatrix();
        return this._stage_invMatrix.project(coordinates,target);
    }

    /**
     * Translate coordinates from world-space to screen-space
     *
     * @param coordinates world-space coordinates
     * @param target [optional] vector to store result in
     * @returns target vector or internal temporary
     */
    public worldToScreen(coordinates: Vec2, target: Vec2 = this._stage_tempvector): Vec2 {
        this.updateMatrix();
        return this._stage_matrix.project(coordinates,target);
    }

    /**
     * Update all Button2Ds attached to this Stage
     */
    public updateUI(): void {
        let uiobjects: Node2D[] = [];

        // XXX: Do not assume that all UI Objects are Button2Ds
        let findUIObjects = (node: Node2D) => {
            var n = node.getFirstChild();
            while(n != null) {
                if(n.isVisible()) {
                    if(n.isUIObject()) {
                        if((<Button2D>n).isEnabled()) {
                            uiobjects.push((<Button2D>n));
                        }
                    } else {
                        findUIObjects(n);
                    }
                }
                n = n.getNextNode();
            }
        };
        findUIObjects(this._stage_root);

        if(!uiobjects.length) return;
        uiobjects = uiobjects.reverse();

        this.updateMatrix();        // Update matrix to avoid collision with tvec later on

        let mouse = getMouse();
        let touch = getTouch();
        let scale = 1.0 / getScreenScaling();

        let mpressed = mouse.isButtonPressed(MouseButton.LEFT);
        let mdown = mouse.isButtonDown(MouseButton.LEFT);
        let mreleased = mouse.isButtonReleased(MouseButton.LEFT);

        let mpos = mouse.getPosition();
        let mworld = this.screenToWorld(tvec.set(mpos).multiplyXY(scale), tvec);

        let activeTouchCount = touch.getActiveTouchCount();
        let endedTouchCount = touch.getEndedTouchCount();
        let clicked = false;

        let method = getLastInputMethod();
        if(method === InputMethod.TOUCH) {
            if(activeTouchCount === 0 && endedTouchCount === 0) {
                for(let b of uiobjects) {
                    b.resetUILogic();
                }
            } else {

                if(touch.getStartedTouchCount()) {
                    let started = touch.getStartedTouches();
                    for(let t of started) {
                        let tworld = this.screenToWorld(tvec.set(t.getPosition()).multiplyXY(scale), tvec);
                        for(let b of uiobjects) {
                            clicked = b.updateUILogic(tworld.x, tworld.y, t.getRadius(), true, true, false, clicked);
                            if(clicked) t.cancel();
                        }
                    }
                }

                if(touch.getActiveTouchCount()) {
                    let active = touch.getActiveTouches();
                    for(let t of active) {
                        let tworld = this.screenToWorld(tvec.set(t.getPosition()).multiplyXY(scale), tvec);
                        for(let b of uiobjects) {
                            b.updateUILogic(tworld.x, tworld.y, t.getRadius(), false, true, false, clicked);
                        }
                    }
                }

                if(touch.getEndedTouchCount()) {
                    let ended = touch.getEndedTouches();
                    for(let t of ended) {
                        let tworld = this.screenToWorld(tvec.set(t.getPosition()).multiplyXY(scale), tvec);
                        for(let b of uiobjects) {
                            clicked = b.updateUILogic(tworld.x, tworld.y, t.getRadius(), false, false, true, clicked);
                            if(clicked) t.cancel();
                        }
                    }
                }
            }

        } else {

            // Update mouse logic
            for(let b of uiobjects) {
                clicked = b.updateUILogic(mworld.x, mworld.y, 1.0, mpressed, mdown, mreleased, clicked);
            }

        }

    }

}
