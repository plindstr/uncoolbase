import { Matrix } from "../math";
import { Node2D } from "./node";
import { Color, Renderer2D } from "../graphics";

/**
 * Draws a colored rectangle with the specified draw mode
 */
export class ColorRect2D extends Node2D {

    private _cr_color: Color;

    constructor(c: Color = Color.WHITE, width: number = 100, height: number = 100) {
        super();
        this._cr_color = new Color();
        this._cr_color.set(c);
        this.setSizeXY(width, height);
        this.markAsDrawable();
    }

    public setColor(c: Color): ColorRect2D {
        this._cr_color.set(c);
        return this;
    }

    public getColor(): Color {
        return this._cr_color;
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {
        let p = this.getPivot();
        r.setMatrix(mtx);
        r.setAlpha(alpha);
        r.setBlendMode(this.getBlendMode());
        r.beginRectsFilled(1);
        r.addRect(-p.x, -p.y, this.getWidth(), this.getHeight(), this._cr_color);
        r.draw();
    }

}
