import { Matrix } from "../math";
import { Node2D } from "./node";
import { Color, Renderer2D } from "../graphics";

export class ColorLayer2D extends Node2D {

    private _cl_color: Color;

    constructor(c: Color = Color.WHITE) {
        super();
        this._cl_color = new Color();
        this._cl_color.set(c);
        this.markAsDrawable();
    }

    public setColor(c: Color): ColorLayer2D {
        this._cl_color.set(c);
        return this;
    }

    public getcolor(): Color {
        return this._cl_color;
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {
        r.setMatrix(Matrix.IDENTITY);
        r.setAlpha(alpha);
        r.setBlendMode(this.getBlendMode());
        r.beginRectsFilled(1);
        r.addRect(0,0,r.getWidth(), r.getHeight(), this._cl_color);
        r.draw();
    }

}
