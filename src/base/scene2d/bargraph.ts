import { Node2D } from "./node";
import { Color, Renderer2D } from "../graphics";
import { clamp, Matrix } from "../math";

/*
 * Simple solid color bar graph
 * Useful for health counters etc..
 */

export class BarGraph2D extends Node2D {

    public static VERTICAL:   number = 0;
    public static HORIZONTAL: number = 1;

    private _bg_orientation: number = BarGraph2D.HORIZONTAL;
    private _bg_outline: Color = Color.RED;
    private _bg_fill: Color = Color.RED;
    private _bg_outlineWidth: number = 1.0;
    private _bg_rangeFrom: number = 0.0;
    private _bg_rangeTo: number = 1.0;
    private _bg_value: number = 0.5;

    constructor(orientation: number, width: number, height: number) {
        super();
        this._bg_orientation = (orientation | 0);
        this.setSizeXY(width, height);
        this.markAsDrawable();
    }

    public setOutlineWidth(w: number): BarGraph2D {
        this._bg_outlineWidth = Math.max(1, w);
        return this;
    }

    public getOutlineWidth(): number {
        return this._bg_outlineWidth;
    }

    public setOutlineColor(c: Color): BarGraph2D {
        this._bg_outline = c;
        return this;
    }

    public getOutlineColor(): Color {
        return this._bg_outline;
    }

    public setFillColor(c: Color): BarGraph2D {
        this._bg_fill = c;
        return this;
    }

    public getFillColor(): Color {
        return this._bg_fill;
    }

    public setRange(min: number, max: number): BarGraph2D {
        this._bg_rangeFrom = Math.min(min,max);
        this._bg_rangeTo = Math.max(min,max);
        if(this._bg_rangeFrom === this._bg_rangeTo) this._bg_rangeTo += 0.0001;
        return this;
    }

    public getRangeMin(): number {
        return this._bg_rangeFrom;
    }

    public getRangeMax(): number {
        return this._bg_rangeTo;
    }

    public setValue(v: number): BarGraph2D {
        this._bg_value = clamp(v, this._bg_rangeFrom, this._bg_rangeTo);
        return this;
    }

    public getValue(): number {
        return this._bg_value;
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {
        const fillsize = ((this._bg_value - this._bg_rangeFrom) / (this._bg_rangeTo - this._bg_rangeFrom));

        r.setAlpha(alpha);
        r.setMatrix(mtx);
        r.setLineWidth(this._bg_outlineWidth);

        r.beginRectsFilled(2);
        if(this._bg_orientation == BarGraph2D.VERTICAL) {
            r.addRect(0,0, this.getWidth(), fillsize * this.getHeight(), this._bg_fill);
        } else {
            r.addRect(0,0, fillsize * this.getWidth(), this.getHeight(), this._bg_fill);
        }
        r.draw();

        r.beginRectsOutlined(2);
        if(this._bg_orientation == BarGraph2D.VERTICAL) {
            r.addRect(0,0, this.getWidth(), this.getHeight(), this._bg_outline);
        } else {
            r.addRect(0,0, this.getWidth(), this.getHeight(), this._bg_outline);
        }
        r.draw();

    }

}
