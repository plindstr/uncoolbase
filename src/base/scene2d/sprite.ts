import { Node2D, PivotPosition } from "./node";
import { Vec4, wrap_pi2, clamp, Matrix } from "../math";
import { ImageSource, Renderer2D } from "../graphics";
import { max } from "../math/coremath";

export class Sprite2D extends Node2D {

    private _sprite_image: ImageSource;
    private _sprite_tint: Vec4;

    constructor(src: ImageSource) {
        super();
        this.setImageSource(src);
        this._sprite_tint = new Vec4(0,0,1,1);

        this.markAsDrawable();
    }

    public setTintHue(hue: number): Sprite2D {
        this._sprite_tint.x = wrap_pi2(hue);
        return this;
    }

    public setTintSaturation(sat: number): Sprite2D {
        this._sprite_tint.y = clamp(sat, -1, 1);
        return this;
    }

    public setTintValue(val: number): Sprite2D {
        this._sprite_tint.z = max(val, 0);
        return this;
    }

    public setTintAlpha(alpha: number): Sprite2D {
        this._sprite_tint.w = clamp(alpha, 0, 1);
        return this;
    }

    public getTintHue(): number {
        return this._sprite_tint.x
    }

    public getTintSaturation(): number {
        return this._sprite_tint.y;
    }

    public getTintValue(): number {
        return this._sprite_tint.z;
    }

    public getTintAlpha(): number {
        return this._sprite_tint.w;
    }

    public setTint(hue: number, saturation: number, value: number, alpha: number): Sprite2D {
        hue = wrap_pi2(hue);
        saturation = clamp(saturation, -1, 1);
        value = clamp(value, 0, 1);
        alpha = clamp(alpha, 0, 1);
        this._sprite_tint.setXYZW(hue, saturation, value, alpha);
        return this;
    }

    public setImageSource(src: ImageSource): Sprite2D {
        this._sprite_image = src;
        this.setSizeXY(src.getWidth(), src.getHeight());
        this.setPivotPosition(PivotPosition.CENTER);
        return this;
    }

    public getImageSource(): ImageSource {
        return this._sprite_image;
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {
        let t = this._sprite_tint;
        let hue = t.x;
        let sat = t.y;
        let val = t.z;
        let a = t.w;
        let p = this.getPivot();
        r.setBlendMode(this.getBlendMode());
        r.setAlpha(alpha);
        r.setMatrix(mtx);
        r.beginImages(this._sprite_image, 1);
        r.addImage(-p.x, -p.y, 1, 1, a, hue, sat, val);
        r.draw();
    }

}
