import { array_clear, array_access } from "../mem/array";
import { Node2D } from "./node";
import { AABB } from "../math/aabb";
import { Matrix } from "../math/matrix";
import { max } from "../math/coremath";
import { Size } from "../math/size";
import { Vec2 } from "../math/vec";
import { debugErrorNamed } from "../core/logging";
import { Screen } from "../core/screen";

/**
 * Enumeration of possible alignemnts for components inside a Layout
 */
export enum LayoutAlignment {
    TOP_LEFT,
    TOP,
    TOP_RIGHT,

    LEFT,
    CENTER,
    RIGHT,

    BOTTOM_LEFT,
    BOTTOM,
    BOTTOM_RIGHT
}

/**
 * Layout container base class. Positions its direct children according
 * to layouting rules (vertical / horizontal / grid / custom).
 */
export abstract class Layout2D extends Node2D {

    private _spacing = new Vec2();
    private _normalize_x: boolean = false;
    private _normalize_y: boolean = false;
    private _components: Node2D[] = [];
    private _bounds: AABB[] = [];
    private _slots: Size[] = [];
    private _size: number = 0;
    private _compAlignment = LayoutAlignment.CENTER;
    private _slotAlignment = LayoutAlignment.CENTER;
    private _contentBB = new AABB();

    constructor() {
        super();
    }

    /**
     * (Internal) get access to a shared content BB. This should
     * be set to the correct size of (not absolute position) of
     * the slots inside the layout.
     */
    protected getContentBB(): AABB {
        return this._contentBB;
    }

    /**
     * Get the horizontal offset that should be
     * applied to component placement so that the
     * slot alignment promise holds.
     * Requires contentBB set to the correct size.
     */
    public getContentOffsetX(): number {
        let cwidth = this._contentBB.getWidth();

        // Find the X offset for the content
        let x0 = 0;
        switch(this._slotAlignment) {
            case LayoutAlignment.CENTER:
            case LayoutAlignment.TOP:
            case LayoutAlignment.BOTTOM:
                x0 = cwidth * -.5;
                break;
            case LayoutAlignment.LEFT:
            case LayoutAlignment.TOP_LEFT:
            case LayoutAlignment.BOTTOM_LEFT:
                x0 = 0;
                break;
            case LayoutAlignment.TOP_RIGHT:
            case LayoutAlignment.RIGHT:
            case LayoutAlignment.BOTTOM_RIGHT:
                x0 = -cwidth;
                break;
        }
        return x0;
    }

    /**
     * Get the horizontal offset that should be
     * applied to component placement so that the
     * slot alignment promise holds.
     * Requires contentBB set to the correct size.
     */
    public getContentOffsetY(): number {
        let cheight = this._contentBB.getHeight();

        // Find the Y offset for the content
        let y0 = 0;
        switch(this._slotAlignment) {
            case LayoutAlignment.CENTER:
            case LayoutAlignment.LEFT:
            case LayoutAlignment.RIGHT:
                y0 = cheight * -.5;
                break;
            case LayoutAlignment.TOP:
            case LayoutAlignment.TOP_LEFT:
            case LayoutAlignment.TOP_RIGHT:
                y0 = 0;
                break;
            case LayoutAlignment.BOTTOM:
            case LayoutAlignment.BOTTOM_LEFT:
            case LayoutAlignment.BOTTOM_RIGHT:
                y0 = -cheight;
                break;
        }
        return y0;
    }

    /**
     * Get the width of the content as calculated by the
     * content bounding box
     */
    public getContentWidth(): number {
        return this._contentBB.getWidth();
    }

    /**
     * Get the height of the content as calculated by the
     * content bounding box.
     */
    public getContentHeight(): number {
        return this._contentBB.getHeight();
    }

    /**
     * Override child list modification callback to be able to respond
     * to components being added or removed.
     */
    protected onChildListModified(): void {
        super.onChildListModified();
        this.refresh();
    }

    /**
     * Called by Layout to perform actual positioning of children.
     * Child bounds have been precalculated to be normalized in width/height
     * if needed. Children need to be placed using placeComponent(idx, x, y);
     */
    protected abstract doLayout(): void;

    /**
     * Refresh the layout. This function is called automatically when
     * an item is added or removed, but can be called manually to redo
     * the layouting of child nodes (e.g. some node has changed shape
     * in a way that should affect the layout). Be aware that layouting
     * is slow and generated garbage on the heap; this function should
     *  _not_ be called in a regular update loop.
     */
    public refresh(): Layout2D {

        // Reset supporting data
        array_clear(this._components);
        this._size = 0;

        // Find components, create bounds and slots objects as needed
        for(let c = this.getFirstChild(); !!c; c = c.getNextNode()) {
            this._components.push(c);
            if(this._bounds.length <= this._size) {
                this._bounds.push(new AABB());
            }
            if(this._slots.length <= this._size) {
                this._slots.push(new Size());
            }
            ++this._size;
        }

        // Find component bounds
        let idx = 0;
        let maxWidth = 0, maxHeight = 0;
        for(let c of this._components) {
            let bb = this._bounds[idx];
            c.getAABBCumulative(bb, Matrix.IDENTITY);
            maxWidth  = max(bb.getWidth(), maxWidth);
            maxHeight = max(bb.getHeight(), maxHeight);
            ++idx;
        }

        // Find slot sizes
        for(let i = 0; i < this._size; ++i) {
            let s = this._slots[i];
            let b = this._bounds[i];

            s.width = this._normalize_x ? maxWidth : b.getWidth();
            s.height = this._normalize_y ? maxHeight : b.getHeight();
        }

        // Perform layouting
        this.doLayout();
        return this;
    }

    /**
     * Get the number of components added to this Layout
     */
    public getComponentCount(): number {
        return this._size;
    }

    /**
     * (Internal) find the current bounds for a component by index
     *
     * @param idx index of component
     */
    protected getComponentBounds(idx: number): AABB {
        return array_access(this._bounds, idx);
    }

    /**
     * (Internal) get the size of the layout slot for a component by index
     *
     * @param idx index of component
     */
    protected getComponentSlotSize(idx: number): Size {
        return array_access(this._slots, idx);
    }

    /**
     * Calculate the correct X offset relative to the slot for a component
     * by index
     *
     * @param idx component index
     */
    protected getComponentOffsetX(idx: number): number {
        let bb = this.getComponentBounds(idx);
        let sz = this.getComponentSlotSize(idx);
        let szw = sz.width;
        let bbw = bb.getWidth();
        let xx = 0;

        // Find X coordinate
        switch(this._compAlignment) {
            case LayoutAlignment.CENTER:
            case LayoutAlignment.TOP:
            case LayoutAlignment.BOTTOM:
                xx = (szw - bbw) * .5;
                break;
            case LayoutAlignment.TOP_RIGHT:
            case LayoutAlignment.RIGHT:
            case LayoutAlignment.BOTTOM_RIGHT:
                xx = szw - bbw;
                break;
        }
        return xx;
    }

    /**
     * Calculate the correct Y offset relative to the slot for a component
     * by index
     *
     * @param idx component index
     */
    protected getComponentOffsetY(idx: number): number {
        let bb = this.getComponentBounds(idx);
        let sz = this.getComponentSlotSize(idx);
        let szh = sz.height;
        let bbh = bb.getHeight();
        let yy = 0;

        // Find Y coordinate
        switch(this._compAlignment) {
            case LayoutAlignment.LEFT:
            case LayoutAlignment.CENTER:
            case LayoutAlignment.RIGHT:
                yy = (szh - bbh) * .5;
                break;
            case LayoutAlignment.BOTTOM_LEFT:
            case LayoutAlignment.BOTTOM:
            case LayoutAlignment.BOTTOM_RIGHT:
                yy = (szh - bbh);
                break;
        }
        return yy;
    }

    /**
     * (Internal) set the position of a component by index
     *
     * @param idx index of component
     * @param x X coordinate of component
     * @param y Y coordinate of component
     */
    protected placeComponent(idx: number, x: number, y: number): void {
        let c  = array_access(this._components, idx);
        let bb = array_access(this._bounds, idx);
        let w  = bb.getWidth();
        let h  = bb.getHeight();

        // Adjust X/Y coordinates of component according to
        // coordinate/bb discrepancy (we want to place the _bb_
        // of the component, but we can only affect the component's
        // position)
        let xoffset = c.getX() - bb.min.x;
        let yoffset = c.getY() - bb.min.y;

        bb.min.x = x;
        bb.min.y = y;
        bb.max.x = x + w;
        bb.max.y = y + h;

        c.setPositionXY(x + xoffset, y + yoffset);
    }

    /**
     * Set spacing between components. This value is added to
     * the position of each component added on top of the usual
     * slot size.
     *
     * @param spacing_x horizontal separation
     * @param spacing_y [optional] vertical separation, default to horizontal separation value
     */
    public setComponentSpacing(spacing_x: number, spacing_y: number = spacing_x): Layout2D {
        this._spacing.setXY(+spacing_x, +spacing_y);
        return this;
    }

    /**
     * Set whether or not slot sizes should be normalized.
     * Defaults depend on the type of layout used.
     *
     * @param horizontal if true, width of each slot is determined by the widest component
     * @param vertical If true, height of each slot is determined by the highest component. Defaults to value of horizontal param.
     */
    public setNormalized(horizontal: boolean, vertical: boolean = horizontal): Layout2D {
        this._normalize_x = !!horizontal;
        this._normalize_y = !!vertical;
        return this;
    }

    /**
     * Get the horizontal spacing, in points, between components.
     */
    public getComponentSpacingHorizontal(): number {
        return this._spacing.x;
    }

    /**
     * Get the vertical spacing, in points, between components.
     */
    public getComponentSpacingVertical(): number {
        return this._spacing.y;
    }

    /**
     * Return true if the width of slots is to be determined by
     * the widest added component. Default: false.
     */
    public isNormalizedHorizontal(): boolean {
        return this._normalize_x;
    }

    /**
     * Return true if the height of slots is to be determined by
     * the hightest added component. Default: false.
     */
    public isNormalizedVertical(): boolean {
        return this._normalize_y;
    }

    /**
     * Set the alignment of individual items. This is especially useful
     * with normalized layouts.
     *
     * Items should be positioned in their slots such that their position
     * conforms to the alignment setting, i.e. when laying out boxes in
     * a grid, a box too small to fit into its slot should be placed such
     * that its bottom-right corner touches the bottom-right corner of its
     * slot, if alignment is set to LayoutAlignment.BOTTOM_RIGHT.
     *
     * @param align a value form the LayoutAlignemnt enum
     */
    public setComponentAlignment(align: LayoutAlignment): Layout2D {
        this._compAlignment = align;
        return this;
    }

    /**
     * Get the alignment for individual items. See setAlignment(align).
     */
    public getComponentAlignment(): LayoutAlignment {
        return this._compAlignment;
    }

    /**
     * Set the alignment of the content. The default is CENTER,
     * meaning that the center of the content will be at local
     * coordinates (0,0) of the Layout.
     *
     * @param align a LayoutAlignment value
     */
    public setContentAlignment(align: LayoutAlignment): Layout2D {
        this._slotAlignment = align;
        return this;
    }

    /**
     * Get the content alignment mode.
     */
    public getContentAlignment(): LayoutAlignment {
        return this._slotAlignment;
    }
}

/**
 * Arranges items vertically (top to bottom).
 */
export class VerticalLayout2D extends Layout2D {

    constructor() {
        super();
        this.setNormalized(true, false);
    }

    protected measureContent(): void {
        let bb = this.getContentBB();
        bb.setXY(0,0);

        // Find start position for slots
        let space = this.getComponentSpacingVertical();
        let y = 0;
        for(let i = 0, l = this.getComponentCount(); i < l; ++i) {
            let s = this.getComponentSlotSize(i);
            y += s.height + space;
            bb.addXY(s.width, y);
        }
    }

    protected doLayout(): void {
        let space = this.getComponentSpacingVertical();

        // Find the size of the content as we understand it
        this.measureContent();
        let x0 = this.getContentOffsetX();
        let y0 = this.getContentOffsetY();

        // Place components inside layout
        let y = 0;
        for(let i = 0, l = this.getComponentCount(); i < l; ++i) {
            let sz = this.getComponentSlotSize(i);
            let xx = this.getComponentOffsetX(i);
            let yy = this.getComponentOffsetY(i);

            this.placeComponent(i, x0 + xx, y0 + y + yy);
            y += sz.height + space;
        }
    }
}

/**
 * Arranges items horizontally (left to right).
 */
export class HorizontalLayout2D extends Layout2D {

    constructor() {
        super();
        this.setNormalized(false, true);
    }

    protected measureContent(): void {
        let bb = this.getContentBB();
        bb.setXY(0,0);

        // Find start position for slots
        let space = this.getComponentSpacingHorizontal();
        let x = 0;
        for(let i = 0, l = this.getComponentCount(); i < l; ++i) {
            let s = this.getComponentSlotSize(i);
            x += s.width + space;
            bb.addXY(x, s.height);
        }
    }

    protected doLayout(): void {
        let space = this.getComponentSpacingHorizontal();

        // Find the size of the content as we understand it
        this.measureContent();
        let x0 = this.getContentOffsetX();
        let y0 = this.getContentOffsetY();

        // Place components inside layout
        let x = 0;
        for(let i = 0, l = this.getComponentCount(); i < l; ++i) {
            let sz = this.getComponentSlotSize(i);
            let xx = this.getComponentOffsetX(i);
            let yy = this.getComponentOffsetY(i);

            this.placeComponent(i, x0 + x + xx, y0 + yy);
            x += sz.width + space;
        }
    }
}

/**
 * Arranges items in a grid. You must specify the
 * number of columns; rows will be added automatically.
 */
export class GridLayout2D extends Layout2D {

    private _columns: number;

    constructor(columns: number) {
        super();
        this.setColumnCount(columns);
        this.setNormalized(true, true);
    }

    public setColumnCount(columns: number): GridLayout2D {
        this._columns = max(1, columns | 0);
        this.refresh();
        return this;
    }

    public getColumnCount(): number {
        return this._columns;
    }

    protected measureContent(): void {
        let bb = this.getContentBB();
        bb.setXY(0,0);

        // Find start position for slots
        let space_x = this.getComponentSpacingHorizontal();
        let space_y = this.getComponentSpacingVertical();
        let y = 0;
        for(let i = 0, l = this.getComponentCount(); i < l;) {
            let maxh = 0;
            let x = 0;
            for(let c = 0; c < this._columns && i < l; ++c, ++i) {
                let s = this.getComponentSlotSize(i);
                x += s.width + space_x;
                maxh = max(s.height, maxh);
                bb.addXY(x, y + s.height);
            }
            y += maxh + space_y;
        }
    }

    protected doLayout(): void {
        let space_x = this.getComponentSpacingHorizontal();
        let space_y = this.getComponentSpacingVertical();

        // Find the size of the content as we understand it
        this.measureContent();
        let x0 = this.getContentOffsetX();
        let y0 = this.getContentOffsetY();

        // Place components inside layout
        let y = 0;
        for(let i = 0, l = this.getComponentCount(); i < l;) {
            let maxh = 0;
            let x = 0;
            for(let c = 0; c < this._columns && i < l; ++c, ++i) {
                let sz = this.getComponentSlotSize(i);
                let xx = this.getComponentOffsetX(i);
                let yy = this.getComponentOffsetY(i);

                this.placeComponent(i, x0 + x + xx, y0 + y + yy);
                maxh = max(sz.height, maxh);
                x += sz.width + space_x;
            }
            y += maxh + space_y;
        }
    }
}

/**
 * Screen anchor points for edge layouts
 */
export enum Edge2D {
    TOP_LEFT,
    TOP,
    TOP_RIGHT,

    LEFT,
    CENTER,
    RIGHT,

    BOTTOM_LEFT,
    BOTTOM,
    BOTTOM_RIGHT
}

/**
 * Edge layout: allows placing objects at screen anchor points.
 * All component slots are VerticalLayouts.
 */
export class EdgeLayout2D extends Layout2D {

    private _anchors: VerticalLayout2D[] = [
        null, null, null,
        null, null, null,
        null, null, null
    ];

    private _margin = new Vec2();

    constructor() {
        super();
    }

    /**
     * Set distance to screen edge for all anchor points.
     * Positive distance is inward to screen center, negative
     * is outward from screen center.
     *
     * @param x horizontal margin
     * @param y [options] vertical margin, defaults to horizontal parameter value.
     */
    public setMargin(x: number, y: number = x): EdgeLayout2D {
        this._margin.setXY(x, y);
        return this;
    }

    private getAnchor(anchor: Edge2D) {
        let a = this._anchors[anchor];
        if(a) return a;

        // Create new
        a = new VerticalLayout2D();

        switch(anchor) {
            case Edge2D.TOP_LEFT:
                a.setComponentAlignment(LayoutAlignment.TOP_LEFT);
                a.setContentAlignment(LayoutAlignment.TOP_LEFT);
                break;

            case Edge2D.TOP:
                a.setComponentAlignment(LayoutAlignment.TOP);
                a.setContentAlignment(LayoutAlignment.TOP);
                break;

            case Edge2D.TOP_RIGHT:
                a.setComponentAlignment(LayoutAlignment.TOP_RIGHT);
                a.setContentAlignment(LayoutAlignment.TOP_RIGHT);
                break;

            case Edge2D.LEFT:
                a.setComponentAlignment(LayoutAlignment.LEFT);
                a.setContentAlignment(LayoutAlignment.LEFT);
                break;

            case Edge2D.CENTER:
                a.setComponentAlignment(LayoutAlignment.CENTER);
                a.setContentAlignment(LayoutAlignment.CENTER);
                break;

            case Edge2D.RIGHT:
                a.setComponentAlignment(LayoutAlignment.RIGHT);
                a.setContentAlignment(LayoutAlignment.RIGHT);
                break;

            case Edge2D.BOTTOM_LEFT:
                a.setComponentAlignment(LayoutAlignment.BOTTOM_LEFT);
                a.setContentAlignment(LayoutAlignment.BOTTOM_LEFT);
                break;

            case Edge2D.BOTTOM:
                a.setComponentAlignment(LayoutAlignment.BOTTOM);
                a.setContentAlignment(LayoutAlignment.BOTTOM);
                break;

            case Edge2D.BOTTOM_RIGHT:
                a.setComponentAlignment(LayoutAlignment.BOTTOM_RIGHT);
                a.setContentAlignment(LayoutAlignment.BOTTOM_RIGHT);
                break;
        }

        this._anchors[anchor] = a;
        super.addChild(a);
        return a;
    }

    /**
     * Add a component to the AnchorLayout at the specified anchor point
     *
     * @param position
     * @param component
     */
    public addComponent(anchor: Edge2D, component: Node2D) {
        this.getAnchor(anchor).addChild(component);
    }

    public addChild(child: Node2D): Node2D {
        debugErrorNamed(this, "Cannot add child directly! Use addComponent() instead");
        return null;
    }

    public addChildren(children: Node2D[]): Node2D {
        debugErrorNamed(this, "Cannot add children directly! Use addComponent() instead");
        return null;
    }

    public refresh(): EdgeLayout2D {
        for(let a of this._anchors) {
            if(!a) continue;

            a.setComponentSpacing(
                this.getComponentSpacingHorizontal(),
                this.getComponentSpacingVertical()
            );
            a.setNormalized(
                this.isNormalizedHorizontal(),
                this.isNormalizedVertical()
            );
            a.refresh();
        }
        this.doLayout();
        return this;
    }

    protected doLayout(): void {

        let mx = this._margin.x;
        let my = this._margin.y;
        let sx = Screen.HALF_WIDTH;
        let sy = Screen.HALF_HEIGHT;

        for(let i = 0; i < 9; ++i) {
            let a = this._anchors[i];
            let px = 0, py = 0;

            if(!a) continue;

            switch(i) {
                case Edge2D.TOP_LEFT:
                    px = mx - sx, py = my - sy;
                    break;

                case Edge2D.TOP:
                    py = my - sy;
                    break;

                case Edge2D.TOP_RIGHT:
                    px = sx - mx, py = my - sy;
                    break;

                case Edge2D.LEFT:
                    px = mx - sx;
                    break;

                case Edge2D.RIGHT:
                    px = sx - mx;
                    break;

                case Edge2D.BOTTOM_LEFT:
                    px = mx - sx, py = sy - my;
                    break;

                case Edge2D.BOTTOM:
                    py = sy - my;
                    break;

                case Edge2D.BOTTOM_RIGHT:
                    px = sx - mx, py = sy - my;
                    break;
            }

            a.setPositionXY(px, py);

        }

    }

}
