import { Vec4, Matrix, wrap_pi2, clamp } from "../math";
import { Renderer2D, Surface, Color } from "../graphics";
import { Node2D, PivotPosition } from "./node";

const mmax = (a: number, b: number) => a > b ? a : b;

/**
 * An object like Text2D that renders into its own
 * Canvas using WebFonts and Canvas text rendering.
 */
export class NativeText2D extends Node2D {

    private _nt_render: Surface;      // Canvas that NativeText2D draws to

    private _nt_fontName: string;     // webfont name
    private _nt_fontStyle: string;    // webfont style
    private _fontSize: number;     // webfont size
    private _nt_strokeStyle: string;
    private _nt_strokeWidth: number;
    private _nt_fillStyle: string;
    private _nt_shadowX: number;
    private _nt_shadowY: number;
    private _nt_shadowBlur: number;
    private _nt_shadowColor: string;
    private _nt_maxWidth: number;
    private _nt_text: string;
    private _nt_tint: Vec4;
    private _nt_dirty: boolean;
    private _nt_pivot: PivotPosition;

    constructor(text: string = '') {
        super();
        this._nt_render = new Surface();
        this._nt_render.setSize(1024, 64);
        this._nt_fontName = 'arial';
        this._fontSize = 16;
        this._nt_fontStyle = 'normal';
        this._nt_strokeStyle = Color.TRANSPARENT.asString();
        this._nt_fillStyle = Color.BLACK.asString();
        this._nt_strokeWidth = 1;
        this._nt_tint = new Vec4(0,0,1,1);
        this._nt_shadowBlur = 0;
        this._nt_shadowX = 0;
        this._nt_shadowY = 0;
        this._nt_maxWidth = 1024;
        this._nt_text = text;
        this._nt_dirty = true;
        this.markAsDrawable();
    }

    public setPivotPosition(p: PivotPosition): Node2D {
        this._nt_pivot = p;
        return super.setPivotPosition(p);
    }

    public setPivotXY(x: number, y: number): Node2D {
        this._nt_pivot = null;
        return super.setPivotXY(x,y);
    }

    public setTint(hue: number, sat: number, value: number, alpha: number = 1): NativeText2D {
        this._nt_tint.x = wrap_pi2(hue);
        this._nt_tint.y = clamp(sat, -1, 1);
        this._nt_tint.z = clamp(value, 0, 1);
        this._nt_tint.w = clamp(alpha, 0, 1);
        return this;
    }

    public setMaxWidth(w: number): NativeText2D {
        this._nt_maxWidth = w | 0;
        this._nt_dirty = true;
        return this;
    }

    public getMaxWidth(): number {
        return this._nt_maxWidth;
    }

    public setFont(font: string): NativeText2D {
        this._nt_fontName = font;
        this._nt_dirty = true;
        return this;
    }

    public setFontSize(size: number): NativeText2D {
        this._fontSize = size | 0;
        this._nt_dirty = true;
        return this;
    }

    public getFontSize(): number {
        return this._fontSize;
    }

    public setShadow(x: number, y: number, blur: number, color: Color = Color.BLACK): NativeText2D {
        this._nt_shadowX = x;
        this._nt_shadowY = y;
        this._nt_shadowBlur = blur;
        this._nt_shadowColor = color.asString();
        this._nt_dirty = true;
        return this;
    }

    public setFillStyle(color: Color): NativeText2D {
        if(color) {
            this._nt_fillStyle = color.asString();
        }
        this._nt_dirty = true;
        return this;
    }

    public setStrokeStyle(color: Color, width: number = 1): NativeText2D {
        if(color) {
            this._nt_strokeStyle = color.asString();
        }
        this._nt_strokeWidth = width;
        this._nt_dirty = true;
        return this;
    }

    public getStrokeWidth(): number {
        this._nt_dirty = true;
        return this._nt_strokeWidth;
    }

    public getFont(): string {
        return this._nt_fontName;
    }

    public setText(text: string): NativeText2D {
        this._nt_text = text ? '' + text : '';
        this._nt_dirty = true;
        return this;
    }

    public getText(): string {
        return this._nt_text;
    }

    private _repaint(): void {
        let ctx = this._nt_render.getContext2D();

        let fontdef = this._fontSize + 'px ' + this._nt_fontName;
        if(this._nt_fontStyle) {
            fontdef = this._nt_fontStyle + ' ' + fontdef;
        }

        const setparams = () => {
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.font = fontdef;
            ctx.strokeStyle = this._nt_strokeStyle;
            ctx.lineWidth = this._nt_strokeWidth;
            ctx.fillStyle = this._nt_fillStyle;
            ctx.shadowColor = this._nt_shadowColor;
            ctx.shadowOffsetX = this._nt_shadowX;
            ctx.shadowOffsetY = this._nt_shadowY;
            ctx.shadowBlur = this._nt_shadowBlur;
        };
        setparams();

        let sb = mmax(this._nt_shadowBlur, this._nt_strokeWidth);
        let metrics = ctx.measureText(this._nt_text);
        let width = (metrics.width + sb + sb) || 128;
        let height = (this._fontSize + sb + sb) || 32;

        this.setSizeXY(width, height);
        this._nt_render.setSize(width, height);
        ctx.clearRect(0, 0, this._nt_render.getWidth(), this._nt_render.getHeight());
        setparams();

        let w = width * .5;
        let h = height * .5;
        ctx.strokeText(this._nt_text, w, h, this._nt_maxWidth);
        ctx.fillText(this._nt_text, w, h, this._nt_maxWidth);

        if(this._nt_pivot) {
            this.setPivotPosition(this._nt_pivot);
        }

        this._nt_render.setVolatile(true);
    }

    public draw(rr: Renderer2D, mtx: Matrix, alpha: number): void {

        if(this._nt_dirty) {
            this._repaint();
        }

        let t = this._nt_tint;
        let hue = t.x;
        let sat = t.y;
        let val = t.z;
        let a = t.w;
        let p = this.getPivot();

        rr.setMatrix(mtx);
        rr.setAlpha(alpha);
        rr.setBlendMode(this.getBlendMode());
        rr.beginImages(this._nt_render, 1);
        rr.addImage(-p.x, -p.y, 1, 1, a, hue, sat, val);
        rr.draw();

        if(this._nt_dirty) {
            // Repaint complete
            this._nt_render.setVolatile(false);
            this._nt_dirty = false;
        }
    }

}
