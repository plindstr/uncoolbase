import { Matrix, Vec4, Vec2, toDegree, clamp, wrap_pi2, toRadian } from "../math";
import { Node2D, PivotPosition } from "./node";
import { ImageSource, Renderer2D, Animation } from "../graphics";

const VISIBLE_BIT = 1 << 0;
const DIRTY_BIT   = 1 << 1;

/**
 * Batch Sprite. Can be added to a SpriteBatch.
 *
 * Has configurable position, scale and rotation.
 * Inherits size, image and pivot from owning
 * SpriteBatch.
 */
export class BSprite2D {
    private _bs_owner: SpriteBatch2D;
    private _bs_prev: BSprite2D;
    private _bs_next: BSprite2D;
    private _bs_tint: Vec4;

    private _bs_flags: number;
    private _bs_position: Vec2;
    private _bs_scale: Vec2;
    private _bs_rotation: number;
    private _bs_frame: number;
    private _bs_matrix: Matrix;

    constructor() {
        this._bs_owner = null;
        this._bs_prev = null;
        this._bs_next = null;
        this._bs_tint = new Vec4(0,0,1,1);
        this._bs_flags = VISIBLE_BIT;
        this._bs_position = new Vec2(0,0);
        this._bs_scale = new Vec2(1,1);
        this._bs_rotation = 0;
        this._bs_frame = 0;
        this._bs_matrix = new Matrix();
    }

    public isVisible(): boolean {
        return !!(this._bs_flags & VISIBLE_BIT);
    }

    public setVisible(b: boolean): BSprite2D {
        if(b) {
            this._bs_flags |= VISIBLE_BIT;
        } else {
            this._bs_flags &= ~VISIBLE_BIT;
        }
        return this;
    }

    private _bsprite_addto(batch: SpriteBatch2D): BSprite2D {
        this.removeFromParent();

        this._bs_owner = batch;
        this._bs_next = null;
        this._bs_prev = null;

        let last = batch.getLastSprite();
        if(!!last) {
            this._bs_prev = last;
            last._bs_next = this;
        }

        return this;
    }

    private _bsprite_unlink(): BSprite2D {
        if(!!this._bs_prev) this._bs_prev._bs_next = this._bs_next;
        if(!!this._bs_next) this._bs_next._bs_prev = this._bs_prev;
        this._bs_owner = null;
        return this;
    }

    public setFrame(idx: number): BSprite2D {
        this._bs_frame = idx | 0;
        return this;
    }

    public getFrame(): number {
        return this._bs_frame;
    }

    public removeFromParent(): BSprite2D {
        if(this._bs_owner) {
            this._bs_owner.remove(this);
            this._bs_owner = null;
        }
        return this;
    }

    public getAlpha(): number {
        return this._bs_tint.w;
    }

    public getTintHue(): number {
        return this._bs_tint.x;
    }

    public getTintSaturation(): number {
        return this._bs_tint.y;
    }

    public getTintValue(): number {
        return this._bs_tint.z;
    }

    public getNextSprite(): BSprite2D {
        return this._bs_next
    }

    public getPrevSprite(): BSprite2D {
        return this._bs_prev;
    }

    public bringToFront(): BSprite2D {
        if(!this._bs_owner) return;
        if(this._bs_owner.getNumSprites() < 2) return;
        if(this._bs_owner.getLastSprite() === this) return;

        if(this._bs_owner.getFirstSprite() === this) this._bs_owner['_sb_head'] = this._bs_next;
        if(this._bs_prev) this._bs_prev._bs_next = this._bs_next;
        if(this._bs_next) this._bs_next._bs_prev = this._bs_prev;


        this._bs_prev = this._bs_owner.getLastSprite();
        this._bs_prev._bs_next = this;
        this._bs_next = null;

        this._bs_owner['_sb_tail'] = this;
    }

    public sendToBack(): BSprite2D {
        if(!this._bs_owner) return;
        if(this._bs_owner.getNumSprites() < 2) return;
        if(this._bs_owner.getFirstSprite() === this) return;

        if(this._bs_owner.getLastSprite() === this) this._bs_owner['_sb_tail'] = this._bs_prev;
        if(this._bs_prev) this._bs_prev._bs_next = this._bs_next;
        if(this._bs_next) this._bs_next._bs_prev = this._bs_prev;

        this._bs_next = this._bs_owner.getFirstSprite();
        this._bs_next._bs_prev = this;
        this._bs_prev = null;

        this._bs_owner['_sb_head'] = this;
    }

    public getImageSource(): ImageSource {
        if(this._bs_owner) {
            return this._bs_owner.getImageSource();
        }
        return null;
    }

    public getWidth(): number {
        let o = this._bs_owner;
        if(o) {
            let anim = o.getAnimation();
            if(anim) {
                return anim.getFrameWidth();
            }

            let src = o.getImageSource();
            if(src) {
                return src.getWidth();
            }
        }
        return 0;
    }

    public getHeight(): number {
        let o = this._bs_owner;
        if(o) {
            let anim = o.getAnimation();
            if(anim) {
                return anim.getFrameWidth();
            }

            let src = o.getImageSource();
            if(src) {
                return src.getHeight();
            }
        }
        return 0;
    }

    public getPosition(): Vec2 {
        return this._bs_position;
    }

    public getX(): number {
        return this._bs_position.x;
    }

    public getY(): number {
        return this._bs_position.y;
    }

    public getScale(): Vec2 {
        return this._bs_scale;
    }

    public getRotation(): number {
        return toDegree(this._bs_rotation);
    }

    public getRotationRad(): number {
        return this._bs_rotation;
    }

    public setAlpha(a: number): BSprite2D {
        this._bs_tint.w = clamp(a, 0, 1);
        return this;
    }

    public setTint(hue: number, sat: number, value: number): BSprite2D {
        this._bs_tint.x = wrap_pi2(hue);
        this._bs_tint.y = clamp(sat, -1, 1);
        this._bs_tint.z = clamp(value, 0, 1);
        return this;
    }

    public setPosition(p: Vec2): BSprite2D {
        this._bs_position.set(p);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public setPositionXY(x: number, y: number): BSprite2D {
        this._bs_position.setXY(x,y);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public setPositionX(x: number): BSprite2D {
        this._bs_position.x = x;
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public setPositionY(y: number): BSprite2D {
        this._bs_position.y = y;
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public move(d: Vec2): BSprite2D {
        this._bs_position.add(d);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public moveXY(x: number, y: number): BSprite2D {
        this._bs_position.addXY(x,y);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public setScale(s: Vec2): BSprite2D {
        this._bs_scale.set(s);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public setScaleXY(x: number, y: number = x): BSprite2D {
        this._bs_scale.setXY(x,y);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public scale(s: Vec2): BSprite2D {
        this._bs_scale.multiply(s);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public scaleXY(x: number, y: number = x): BSprite2D {
        this._bs_scale.multiplyXY(x,y);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public setRotation(d: number): BSprite2D {
        this._bs_rotation = wrap_pi2(toRadian(d));
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public setRotationRad(dr: number): BSprite2D {
        this._bs_rotation = wrap_pi2(dr);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public rotate(dr: number): BSprite2D {
        this._bs_rotation = wrap_pi2(this._bs_rotation + toRadian(dr));
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public rotateRad(dr: number): BSprite2D {
        this._bs_rotation = wrap_pi2(this._bs_rotation + dr);
        this._bs_flags |= DIRTY_BIT;
        return this;
    }

    public getMatrix(): Matrix {
        if(this._bs_flags & DIRTY_BIT) {
            let m = this._bs_matrix;
            let p = this._bs_position;
            let s = this._bs_scale;
            m.buildSpriteMatrix(p.x, p.y, s.x, s.y, this._bs_rotation);
            this._bs_flags &= ~DIRTY_BIT;
        }
        return this._bs_matrix;
    }

}

/**
 * Sprite Batch. Allows drawing a large number
 * of sprites at once. Each sprite can be scaled,
 * rotated and positioned, but they share the
 * same input imagesource and size.
 */
export class SpriteBatch2D extends Node2D {

    private _sb_head: BSprite2D;
    private _sb_tail: BSprite2D;
    private _sb_pivot: Vec2;
    private _sb_size: Vec2;
    private _sb_anim: Animation;
    private _sb_image: ImageSource;
    private _sb_num: number;

    constructor(src: ImageSource | Animation) {
        super();

        if(src instanceof Animation) {
            this._sb_anim = src;
            this._sb_image = (<Animation>src).getSource();
            this._sb_size = new Vec2(src.getFrameWidth(), src.getFrameHeight());
        } else {
            this._sb_anim = null;
            this._sb_image = <ImageSource>src;
            this._sb_size = new Vec2(src.getWidth(), src.getHeight());
        }
        this._sb_head = null;
        this._sb_tail = null;
        this._sb_pivot = new Vec2(0,0);
        this._sb_num = 0;
        this.setSpritePivotPosition(PivotPosition.CENTER);
        this.markAsDrawable();
        this.markAsSticky();
    }

    public getAnimation(): Animation {
        return this._sb_anim;
    }

    public add(sprite: BSprite2D): SpriteBatch2D {
        sprite.removeFromParent();
        sprite['_bsprite_addto'](this);
        if(!this._sb_head) this._sb_head = sprite;
        this._sb_tail = sprite;
        this._sb_num++;
        return this;
    }

    public remove(sprite: BSprite2D): SpriteBatch2D {
        if(this._sb_head === sprite) {
            this._sb_head = sprite.getNextSprite();
        }
        if(this._sb_tail === sprite) {
            this._sb_tail = sprite.getPrevSprite();
        }
        sprite['_bsprite_unlink']();
        this._sb_num--;
        return this;
    }

    public clear(): SpriteBatch2D {
        let s = this._sb_head;
        while(s) {
            let next = s.getNextSprite();
            s['_bsprite_unlink']();
            s = next;
        }
        this._sb_head = null;
        this._sb_tail = null;
        return this;
    }

    public getNumSprites(): number {
        return this._sb_num;
    }

    public getFirstSprite(): BSprite2D {
        return this._sb_head;
    }

    public getLastSprite(): BSprite2D {
        return this._sb_tail;
    }

    public setImageSource(src: ImageSource): SpriteBatch2D {
        let me = this;
        me._sb_image = src;
        me.setSizeXY(src.getWidth(), src.getHeight());
        me.setSpritePivotPosition(PivotPosition.CENTER);
        return this;
    }

    public getImageSource(): ImageSource {
        return this._sb_image;
    }

    public setSpritePivot(x: number, y: number): SpriteBatch2D {
        this._sb_pivot.setXY(x,y);
        return this;
    }

    public setSpritePivotPosition(p: PivotPosition): SpriteBatch2D {
        let me = this;
        switch(p) {
            case PivotPosition.TOP_LEFT:
                me._sb_pivot.setXY(0,0);
            break;
            case PivotPosition.TOP_CENTER:
                me._sb_pivot.setXY(me._sb_size.x * .5,0);
            break;
            case PivotPosition.TOP_RIGHT:
                me._sb_pivot.setXY(me._sb_size.x,0);
            break;
            case PivotPosition.CENTER_LEFT:
                me._sb_pivot.setXY(0,me._sb_size.y * .5);
            break;
            case PivotPosition.CENTER:
                me._sb_pivot.setXY(me._sb_size.x * .5,me._sb_size.y * .5);
            break;
            case PivotPosition.CENTER_RIGHT:
                me._sb_pivot.setXY(me._sb_size.x,me._sb_size.y * .5);
            break;
            case PivotPosition.BOTTOM_LEFT:
                me._sb_pivot.setXY(0,me._sb_size.y);
            break;
            case PivotPosition.BOTTOM_CENTER:
                me._sb_pivot.setXY(me._sb_size.x * .5,me._sb_size.y);
            break;
            case PivotPosition.BOTTOM_RIGHT:
                me._sb_pivot.setXY(me._sb_size.x,me._sb_size.y);
            break;
        }
        return this;
    }

    public getSpritePivot(): Vec2 {
        return this._sb_pivot;
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {
        let px = this._sb_pivot.x;
        let py = this._sb_pivot.y;
        let anim = false;

        r.setBlendMode(this.getBlendMode());
        r.setAlpha(alpha);
        r.setMatrix(mtx);
        if(this._sb_anim) {
            r.beginSpritesAnimated(this._sb_anim, this._sb_num);
            anim = true;
        } else {
            r.beginSprites(this._sb_image, this._sb_num);
        }

        let s = this._sb_head;
        while(s) {
            let pos   = s.getPosition();
            let scale = s.getScale();
            let rot   = s.getRotationRad();
            let hue   = s.getTintHue();
            let sat   = s.getTintSaturation();
            let val   = s.getTintValue();
            let a     = s.getAlpha();

            let shouldDraw = s.isVisible();

            if(shouldDraw) {
                if(anim) {
                    let f = s.getFrame();
                    r.addSpriteAnimated(f, pos.x, pos.y, px, py, scale.x, scale.y, rot, a, hue, sat, val);
                } else {
                    r.addSprite(pos.x, pos.y, px, py, scale.x, scale.y, rot, a, hue, sat, val);
                }
            }

            s = s.getNextSprite();
        }
        r.draw();

    }

}
