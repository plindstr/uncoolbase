import { Vec2, AABB, Vec4, toDegree, clamp, wrap_pi2, toRadian, Matrix } from "../math";
import { StaticPool } from "../mem";
import { Node2D, PivotPosition } from "./node";
import { Renderer2D, Font } from "../graphics";

interface RenderCoord {

    // Source image coordinates
    sx: number;
    sy: number;
    sw: number;
    sh: number;

    // Destination offsets
    dx: number;
    dy: number;
    dw: number;
    dh: number;
}

export class BText2D {

    private _owner: TextBatch2D = null;
    private _next: BText2D = null;
    private _prev: BText2D = null;

    private _position: Vec2 = new Vec2();
    private _scale: Vec2 = new Vec2();
    private _rotation: number = 0;
    private _text: string = "";
    private _bounds: AABB = new AABB();
    private _pivotposition: PivotPosition = PivotPosition.TOP_LEFT;
    private _pivot: Vec2 = new Vec2();
    private _tint: Vec4 = new Vec4(0, 0, 1, 1);

    private _dirty: boolean = true;
    private _renderCoords: StaticPool<RenderCoord>;

    constructor(text: string = "") {
        this._text = text;
        this._renderCoords = new StaticPool<RenderCoord>(() => {
            return {
                sx: 0,
                sy: 0,
                sw: 1,
                sh: 1,
                dx: 0,
                dy: 0,
                dw: 1,
                dh: 1
            }
        });
    }

    private _btext_addto(batch: TextBatch2D): BText2D {
        this.removeFromParent();

        this._owner = batch;
        this._next = null;
        this._prev = null;

        let last = batch.getLastText();
        if(!!last) {
            this._prev = last;
            last._next = this;
        }

        return this;
    }

    private _btext_unlink(): BText2D {
        if(!!this._prev) this._prev._next = this._next;
        if(!!this._next) this._next._prev = this._prev;
        this._owner = null;
        return this;
    }

    public removeFromParent(): BText2D {
        if(this._owner) {
            this._owner.remove(this);
            this._owner = null;
        }
        return this;
    }

    public bringToFront(): BText2D {
        if(!this._owner) return;
        if(this._owner.getNumTexts() < 2) return;
        if(this._owner.getLastText() === this) return;

        if(this._prev) this._prev._next = this._next;
        if(this._next) this._next._prev = this._prev;
        this._next = null;
        this._prev = this._owner.getLastText();

        this._owner['_tail'] = this;
    }

    public sendToBack(): BText2D {
        if(!this._owner) return;
        if(this._owner.getNumTexts() < 2) return;
        if(this._owner.getLastText() === this) return;

        if(this._prev) this._prev._next = this._next;
        if(this._next) this._next._prev = this._prev;
        this._next = this._owner.getFirstText();
        this._prev = null;

        this._owner['_head'] = this;
    }

    public getNextText(): BText2D {
        return this._next;
    }

    public getPrevText(): BText2D {
        return this._prev;
    }

    public getFont(): Font {
        return !!this._owner ? this._owner.getFont() : null;
    }

    public setText(text: string): BText2D {
        this._text = "" + text;
        this._dirty = true;
        return this;
    }

    public getText(): string {
        return this._text;
    }

    public getXOffset(): number {
        return this._bounds.getX0();
    }

    public getYOffset(): number {
        return this._bounds.getY0();
    }

    public getWidth(): number {
        return this._bounds.getWidth();
    }

    public getHeight(): number {
        return this._bounds.getHeight();
    }

    public getPosition(): Vec2 {
        return this._position;
    }

    public getX(): number {
        return this._position.x;
    }

    public getY(): number {
        return this._position.y;
    }

    public getScale(): Vec2 {
        return this._scale;
    }

    public getRotation(): number {
        return toDegree(this._rotation);
    }

    public getRotationRad(): number {
        return this._rotation;
    }

    public setAlpha(a: number): BText2D {
        this._tint.w = clamp(a, 0, 1);
        return this;
    }

    public setTint(hue: number, sat: number, val: number): BText2D {
        this._tint.x = wrap_pi2(hue);
        this._tint.y = clamp(sat, -1, 1);
        this._tint.z = clamp(val, 0, 1);
        return this;
    }

    public setPosition(p: Vec2): BText2D {
        this._position.set(p);
        return this;
    }

    public setPositionXY(x: number, y: number): BText2D {
        this._position.setXY(x,y);
        return this;
    }

    public setPositionX(x: number): BText2D {
        this._position.x = x;
        return this;
    }

    public setPositionY(y: number): BText2D {
        this._position.y = y;
        return this;
    }

    public move(d: Vec2): BText2D {
        this._position.add(d);
        return this;
    }

    public moveXY(x: number, y: number): BText2D {
        this._position.addXY(x,y);
        return this;
    }

    public setScale(s: Vec2): BText2D {
        this._scale.set(s);
        return this;
    }

    public setScaleXY(x: number, y: number = x): BText2D {
        this._scale.setXY(x,y);
        return this;
    }

    public scale(s: Vec2): BText2D {
        this._scale.multiply(s);
        return this;
    }

    public scaleXY(x: number, y: number = x): BText2D {
        this._scale.multiplyXY(x,y);
        return this;
    }

    public setRotation(d: number): BText2D {
        this._rotation = wrap_pi2(toRadian(d));
        return this;
    }

    public setRotationRad(dr: number): BText2D {
        this._rotation = wrap_pi2(dr);
        return this;
    }

    public rotate(dr: number): BText2D {
        this._rotation = wrap_pi2(this._rotation + toRadian(dr));
        return this;
    }

    public rotateRad(dr: number): BText2D {
        this._rotation = wrap_pi2(this._rotation + dr);
        return this;
    }

    public getPivot(): Vec2 {
        return this._pivot;
    }

    public getPivotPosition(): PivotPosition {
        return this._pivotposition;
    }

    private _isDirty(): boolean {
        return this._dirty;
    }

    private _repaint() {
        let f = this.getFont();
        if(!f) return;

        let s = this._text;
        let l = s.length;
        let bb = this._bounds;
        let rc = this._renderCoords;
        rc.clear();

        let first = true;
        let yy = 0;
        let xx = 0;
        for(let i = 0; i < l; ++i) {
            let c = s.charAt(i);
            let g = f.getGlyph(c);
            if(g && g.w > 0 && g.h > 0) {
                let r = rc.alloc();
                let k = 0;
                if(i < l - 1) {
                    k = f.getKerning(c, s.charAt(i + 1));
                }
                r.sx = g.x;
                r.sy = g.y;
                r.sw = g.w;
                r.sh = g.h;
                r.dx = xx + g.xoffset;
                r.dy = yy + g.yoffset;
                r.dw = g.w;
                r.dh = g.h;

                xx += g.advance + k;

                if(first) {
                    bb.setXY(r.dx, r.dy);
                    first = false;
                } else {
                    bb.addXY(r.dx, r.dy);
                }

                bb.addXY(r.dx + r.dw, r.dy);
                bb.addXY(r.dx + r.dw, r.dy + r.dh);
                bb.addXY(r.dx, r.dy + r.dh);

            } else if(g) {

                xx += g.advance;

                if(first) {
                    bb.setXY(xx, 0);
                    first = false;
                } else {
                    bb.addXY(xx, 0);
                }
            }
        }

        // TODO: set pivot position

        this._dirty = false;
    }
}

export class TextBatch2D extends Node2D {

    private _head: BText2D = null;
    private _tail: BText2D = null;
    private _num: number = 0;
    private _font: Font = null;

    constructor(font: Font = null) {
        super();

        this._font = font;

        this.markAsDrawable();
        this.markAsSticky();
    }

    public setFont(font: Font): TextBatch2D {
        this._font = font;
        return this;
    }

    public getFont(): Font {
        return this._font;
    }

    public getNumTexts(): number {
        return this._num;
    }

    public getFirstText(): BText2D {
        return this._head;
    }

    public getLastText(): BText2D {
        return this._tail;
    }

    public add(text: BText2D): TextBatch2D {
        text.removeFromParent();
        text['_btext_addto'](this);
        if(!this._head) this._head = text;
        this._tail = text;
        this._num++;

        return this;
    }

    public remove(text: BText2D): TextBatch2D {
        if(this._head === text) {
            this._head = text.getNextText();
        }
        if(this._tail === text) {
            this._tail = text.getPrevText();
        }
        text['_btext_unlink']();
        this._num--;

        return this;
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {

        if(!this._num || !this._font) {
            return;
        }

        r.setAlpha(alpha);
        r.setMatrix(mtx);

        let t = this._head;
        while(t !== null) {

            // TODO: IMPLEMENT!
            // I had no idea this was still a work in progress!

            // handle 't'

            t = t.getNextText();
        }

    }

}
