import { Vec2, Matrix, toDegree, toRadian, wrap, wrap_pi2 } from "../math";
import { getScreenScaling } from "../core";
import { Stage2D } from "./stage";

const mmax = (a: number, b: number) => a > b ? a : b;
const tvec = new Vec2();

export class Camera {

    private _camera_position: Vec2;
    private _camera_scaling:  Vec2;
    private _camera_rotation: number;
    private _camera_matrix:   Matrix;
    private _camera_stage:    Stage2D;
    private _camera_zoom:     number;
    private _camera_resScale: number;
    private _camera_dirty:    boolean;

    constructor(stage: Stage2D) {
        this._camera_position = new Vec2();
        this._camera_resScale = 1.0;
        this._camera_scaling = new Vec2(1.0 / getScreenScaling());
        this._camera_rotation = 0;
        this._camera_matrix = new Matrix();
        this._camera_stage = stage;
        this._camera_zoom = 0;
        this._camera_dirty = true;
    }

    public isDirty(): boolean {
        return this._camera_dirty;
    }

    public markAsDirty(): Camera {
        this._camera_dirty = true;
        return this;
    }

    public setResolutionScale(scale: number): Camera {
        scale = mmax(scale, 0.000001);
        this._camera_resScale = scale;
        this._camera_scaling.setXY(scale / getScreenScaling());
        this._camera_dirty = true;
        return this;
    }

    public getResolutionScale(): number {
        return this._camera_resScale;
    }

    public getX(): number {
        return this._camera_position.x;
    }

    public getY(): number {
        return this._camera_position.y;
    }

    public setPosition(p: Vec2): Camera {
        this._camera_position.set(p);
        this._camera_dirty = true;
        this._camera_stage.markAsDirty();
        return this;
    }

    public setPositionXY(px: number, py: number): Camera {
        this._camera_position.setXY(px, py);
        this._camera_dirty = true;
        this._camera_stage.markAsDirty();
        return this;
    }

    public move(d: Vec2): Camera {
        this._camera_position.add(d);
        this._camera_dirty = true;
        this._camera_stage.markAsDirty();
        return this;
    }

    public moveXY(dx: number, dy: number): Camera {
        this._camera_position.addXY(dx, dy);
        this._camera_dirty = true;
        this._camera_stage.markAsDirty();
        return this;
    }

    public getZoom(): number {
        return this._camera_zoom;
    }

    public setZoom(z: number): Camera {
        this._camera_zoom = z;
        this._camera_dirty = true;
        this._camera_stage.markAsDirty();
        return this;
    }

    public getRotation(): number {
        return toDegree(this._camera_rotation);
    }

    public getRotationRad(): number {
        return this._camera_rotation;
    }

    public setRotation(deg: number): Camera {
        this._camera_rotation = toRadian(wrap(deg, 0, 360));
        this._camera_stage.markAsDirty();
        this._camera_dirty = true;
        return this;
    }

    public setRotationRad(rad: number): Camera {
        this._camera_rotation = wrap_pi2(rad);
        this._camera_stage.markAsDirty();
        this._camera_dirty = true;
        return this;
    }

    public rotate(deg: number): Camera {
        this._camera_rotation = toRadian(wrap(toDegree(this._camera_rotation) + deg, 0, 360));
        this._camera_stage.markAsDirty();
        this._camera_dirty = true;
        return this;
    }

    public rotateRad(rad: number): Camera {
        this._camera_rotation = wrap_pi2(rad + this._camera_rotation);
        this._camera_stage.markAsDirty();
        this._camera_dirty = true;
        return this;
    }

    public getMatrix(): Matrix {
        if(this._camera_dirty) {
            let rs = this._camera_resScale;
            let s = 1.0 / (getScreenScaling() * rs);
            let z = mmax(s + (this._camera_zoom / s), 0.00001) * rs;
            this._camera_matrix.buildNodeMatrix(
                tvec.set(this._camera_position).multiplyXY(-z),
                this._camera_scaling.setXY(z),
                -this._camera_rotation);

            this._camera_dirty = false;
        }
        return this._camera_matrix;
    }

}
