import { Matrix, AABB, Vec2, clamp, toDegree, toRadian, wrap, wrap_pi2 } from "../math";
import { Stage2D } from "./stage";
import { BlendMode, Renderer2D } from "../graphics";
import { debugLogNamed } from "../core/logging";

export enum PivotPosition {
    TOP_LEFT,
    TOP_CENTER,
    TOP_RIGHT,
    CENTER_LEFT,
    CENTER,
    CENTER_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_CENTER,
    BOTTOM_RIGHT
}

const aabb_matrix = new Matrix();

const DIRTY_BIT       = 1 << 1;
const VISIBLE_BIT     = 1 << 2;
const STICKY_BIT      = 1 << 3;
const DRAWABLE_BIT    = 1 << 4;
const UIOBJECT_BIT    = 1 << 5;

const mmin4 = (a: number, b: number, c: number, d: number) => {
    let n = a < b ? a : b;
        n = n < c ? n : c;
        n = n < d ? n : d;
    return n;
}

const mmax4 = (a: number, b: number, c: number, d: number) => {
    let n = a > b ? a : b;
        n = n > c ? n : c;
        n = n > d ? n : d;
    return n;
}

export class Node2D {

    public static calculateAABB(target: AABB, mtx: Matrix, pivot: Vec2, size: Vec2): AABB {
        let tmin = target.min;
        let tmax = target.max;
        let px = pivot.x;
        let py = pivot.y;
        let sx = size.x;
        let sy = size.y;

        // let p00 = matrix.projectXY(-px, -py);
        let xx = -px, yy = -py;
        let p00x = xx * mtx.a + yy * mtx.c + mtx.tx;
        let p00y = xx * mtx.b + yy * mtx.d + mtx.ty;

        // let p01 = matrix.projectXY(-px, -py + sy);
        xx = -px, yy = sy - py;
        let p01x = xx * mtx.a + yy * mtx.c + mtx.tx;
        let p01y = xx * mtx.b + yy * mtx.d + mtx.ty;

        // let p10 = matrix.projectXY(-px + sx, -py);
        xx = sx - px, yy = -py;
        let p10x = xx * mtx.a + yy * mtx.c + mtx.tx;
        let p10y = xx * mtx.b + yy * mtx.d + mtx.ty;

        // let p11 = matrix.projectXY(-px + sx, -py + sy);
        xx = sx - px, yy = sy - py;
        let p11x = xx * mtx.a + yy * mtx.c + mtx.tx;
        let p11y = xx * mtx.b + yy * mtx.d + mtx.ty;

        // Set AABB
        tmin.x = mmin4(p00x, p01x, p10x, p11x);
        tmin.y = mmin4(p00y, p01y, p10y, p11y);
        tmax.x = mmax4(p00x, p01x, p10x, p11x);
        tmax.y = mmax4(p00y, p01y, p10y, p11y);

        return target;
    }

    // Linkage
    private _node_parentNode: Node2D = null;
    private _node_firstChild: Node2D = null;
    private _node_lastChild: Node2D = null;
    private _node_nextNode: Node2D = null;
    private _node_prevNode: Node2D = null;
    private _node_childCount: number = 0;
    private _node_stage: Stage2D = null;

    // Properties
    private _node_position: Vec2 = new Vec2();
    private _node_scale: Vec2 = new Vec2(1,1);
    private _node_rotation: number = 0;
    private _node_size: Vec2 = new Vec2(0,0);
    private _node_pivot: Vec2 = new Vec2(0,0);
    private _node_matrix: Matrix = new Matrix();
    private _node_blendmode: BlendMode = BlendMode.ALPHA;
    private _node_alpha: number = 1;
    private _node_bits: number = DIRTY_BIT | VISIBLE_BIT;
    private _node_userdata: any = null;

    // Methods
    constructor() {
    }

    //
    // Flags
    //

    public isUIObject(): boolean {
        return !!(this._node_bits & UIOBJECT_BIT);
    }

    protected markAsUIObject(): Node2D {
        this._node_bits |= UIOBJECT_BIT;
        return this;
    }

    public isDrawable(): boolean {
        return !!(this._node_bits & DRAWABLE_BIT);
    }

    protected markAsDrawable(): Node2D {
        this._node_bits |= DRAWABLE_BIT;
        return this;
    }

    public isDirty(): boolean {
        return !!(this._node_bits & DIRTY_BIT);
    }

    protected markAsDirty(): Node2D {
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public isSticky(): boolean {
        return !!(this._node_bits & STICKY_BIT);
    }

    protected markAsSticky(): Node2D {
        this._node_bits |= STICKY_BIT;
        return this;
    }

    public isVisible(): boolean {
        return !!(this._node_bits & VISIBLE_BIT)
    }

    public setVisible(b: boolean): Node2D {
        if(b) {
            this._node_bits |= VISIBLE_BIT;
        } else {
            this._node_bits &= ~VISIBLE_BIT;
        }
        return this;
    }

    //
    // Linkage
    //

    public getParent(): Node2D {
        return this._node_parentNode;
    }

    public getNextNode(): Node2D {
        return this._node_nextNode;
    }

    public getPrevNode(): Node2D {
        return this._node_prevNode;
    }

    public getFirstChild(): Node2D {
        return this._node_firstChild;
    }

    public getLastChild(): Node2D {
        return this._node_lastChild;
    }

    public getChildCount(): number {
        return this._node_childCount;
    }

    public getChildCountDeep(): number {
        let sum = this._node_childCount;
        let n = this._node_firstChild;
        while(n != null) {
            sum += n.getChildCountDeep();
            n = n.getNextNode();
        }
        return sum;
    }

    public removeFromParent(): Node2D {
        let p = this._node_parentNode;
        if(p != null) {
            if(p._node_firstChild == this) p._node_firstChild = this._node_nextNode;
            if(p._node_lastChild == this) p._node_lastChild = this._node_prevNode;
            if(this._node_nextNode != null) this._node_nextNode._node_prevNode = this._node_prevNode;
            if(this._node_prevNode != null) this._node_prevNode._node_nextNode = this._node_nextNode;
            p._node_childCount--;
            this._node_parentNode = null;
            this._node_nextNode = null;
            this._node_prevNode = null;
            this.setStage(null);
            p.onChildListModified();
        }
        return this;
    }

    public addChild(child: Node2D): Node2D {
        child.removeFromParent();
        child._node_parentNode = this;
        if(this._node_childCount == 0) {
            this._node_firstChild = child;
            this._node_lastChild = child;
            this._node_childCount = 1;
        } else {
            child._node_prevNode = this._node_lastChild;
            child._node_nextNode = null;
            this._node_lastChild._node_nextNode = child;
            this._node_lastChild = child;
            this._node_childCount++;
        }
        child.setStage(this._node_stage);
        this.onChildListModified();
        return this;
    }

    public addChildren(children: Node2D[]): Node2D {
        for(let c of children) {
            c.removeFromParent();
            c._node_parentNode = this;
            if(this._node_childCount == 0) {
                this._node_firstChild = c;
                this._node_lastChild = c;
                this._node_childCount = 1;
            } else {
                c._node_prevNode = this._node_lastChild;
                c._node_nextNode = null;
                this._node_lastChild._node_nextNode = c;
                this._node_lastChild = c;
                this._node_childCount++;
            }
            c.setStage(this._node_stage);
        }
        this.onChildListModified();
        return this;
    }

    public clearChildren(): Node2D {
        while(this._node_firstChild != null) {
            this._node_firstChild.removeFromParent();
        }
        this.onChildListModified();
        return this;
    }

    /**
     * This function gets called when this node's child list is
     * modified. By default, this does nothing. Extending classes
     * may want to override this method (though remember to call
     * the super implementation) if they need to update state
     * based on the child element list.
     */
    protected onChildListModified(): void {
        // NOP
    }

    /**
     * Recursively set stage for this node and its children
     */
    public setStage(s: Stage2D): Node2D {
        if(this._node_stage != s) {
            this._node_stage = s;
            let c = this._node_firstChild;
            while(c != null) {
                c.setStage(s);
                c = c._node_nextNode;
            }
        }
        return this;
    }

    public getStage(): Stage2D {
        return this._node_stage;
    }

    public bringToFront(): Node2D {
        let p = this._node_parentNode;
        if(p) {
            this.removeFromParent();
            p.addChild(this);
        }
        return this;
    }

    public sendToBack(): Node2D {
        let p = this._node_parentNode;
        if(p && p._node_firstChild != this) {
            if(this._node_nextNode) { this._node_nextNode._node_prevNode = this._node_prevNode; }
            if(this._node_prevNode) { this._node_prevNode._node_nextNode = this._node_nextNode; }
            if(p._node_lastChild == this) p._node_lastChild = this._node_prevNode;

            this._node_nextNode = null;
            this._node_prevNode = null;

            this._node_nextNode = p._node_firstChild;
            p._node_firstChild._node_prevNode = this;
            p._node_firstChild = this;
        }
        return this;
    }

    //
    // Graphics node intrinsics
    //

    public getAlpha(): number {
        return this._node_alpha;
    }

    public setAlpha(a: number): Node2D {
        this._node_alpha = clamp(a,0,1);
        return this;
    }

    public getBlendMode(): BlendMode {
        return this._node_blendmode;
    }

    public setBlendMode(mode: BlendMode): Node2D {
        this._node_blendmode = mode;
        return this;
    }

    public setSizeXY(w: number, h: number): Node2D {
        this._node_size.setXY(w,h);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public setSize(sz: Vec2): Node2D {
        this._node_size.set(sz);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public getWidth(): number {
        return this._node_size.x;
    }

    public getHeight(): number {
        return this._node_size.y;
    }

    public getSize(): Vec2 {
        return this._node_size;
    }

    public getPosition(): Vec2 {
        return this._node_position;
    }

    public getX(): number {
        return this._node_position.x;
    }

    public getY(): number {
        return this._node_position.y;
    }

    public setPosition(p: Vec2): Node2D {
        this._node_position.set(p);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public setPositionX(x: number): Node2D {
        this._node_position.x = x;
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public setPositionY(y: number): Node2D {
        this._node_position.y = y;
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public setPositionXY(x: number, y: number): Node2D {
        this._node_position.setXY(x,y);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public move(d: Vec2): Node2D {
        this._node_position.add(d);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public moveXY(dx: number, dy: number): Node2D {
        this._node_position.addXY(dx,dy);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public getPivot(): Vec2 {
        return this._node_pivot;
    }

    public setPivotPosition(p: PivotPosition): Node2D {
        let np = this._node_pivot;
        switch(p) {
            case PivotPosition.TOP_LEFT:
                np.setXY(0,0);
            break;
            case PivotPosition.TOP_CENTER:
                np.setXY(this._node_size.x * .5,0);
            break;
            case PivotPosition.TOP_RIGHT:
                np.setXY(this._node_size.x,0);
            break;
            case PivotPosition.CENTER_LEFT:
                np.setXY(0,this._node_size.y * .5);
            break;
            case PivotPosition.CENTER:
                np.setXY(this._node_size.x * .5,this._node_size.y * .5);
            break;
            case PivotPosition.CENTER_RIGHT:
                np.setXY(this._node_size.x,this._node_size.y * .5);
            break;
            case PivotPosition.BOTTOM_LEFT:
                np.setXY(0,this._node_size.y);
            break;
            case PivotPosition.BOTTOM_CENTER:
                np.setXY(this._node_size.x * .5,this._node_size.y);
            break;
            case PivotPosition.BOTTOM_RIGHT:
                this._node_pivot.setXY(this._node_size.x,this._node_size.y);
            break;
        }
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public setPivot(p: Vec2): Node2D {
        this._node_pivot.set(p);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public setPivotXY(x: number, y: number): Node2D {
        this._node_pivot.setXY(x,y);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public getScale(): Vec2 {
        return this._node_scale;
    }

    public setScale(s: Vec2): Node2D {
        this._node_scale.set(s);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public setScaleXY(x: number, y: number = x): Node2D {
        this._node_scale.setXY(x,y);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public scale(s: Vec2): Node2D {
        this._node_scale.multiply(s);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public scaleXY(sx: number, sy: number = sx): Node2D {
        this._node_scale.multiplyXY(sx,sy);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public getRotation(): number {
        return toDegree(this._node_rotation);
    }

    public getRotationRad(): number {
        return this._node_rotation;
    }

    public setRotation(deg: number): Node2D {
        this._node_rotation = toRadian(wrap(deg,0,360));
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public setRotationRad(rad: number): Node2D {
        this._node_rotation = wrap_pi2(rad);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public rotate(deg: number): Node2D {
        this._node_rotation = toRadian(wrap(toDegree(this._node_rotation) + deg,0,360));
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public rotateRad(rad: number): Node2D {
        this._node_rotation = wrap_pi2(this._node_rotation + rad);
        this._node_bits |= DIRTY_BIT;
        return this;
    }

    public getMatrix(): Matrix {
        if(!!(this._node_bits & DIRTY_BIT)) {
            this._node_matrix.buildNodeMatrix(this._node_position, this._node_scale, this._node_rotation);
            this._node_bits &= ~DIRTY_BIT; // Clear dirty flag
        }
        return this._node_matrix;
    }

    public getGlobalToLocalMatrix(matrix: Matrix = new Matrix()): Matrix {
        let p = this._node_parentNode;
        let mtx = matrix.set(this.getMatrix());
        while(p != null) {
            mtx.multiply(p.getMatrix());
            p = p._node_parentNode;
        }
        return mtx;
    }

    public getLocalToGlobalMatrix(matrix: Matrix = new Matrix()): Matrix {
        let p = this._node_parentNode;
        let mtx = matrix.set(this.getMatrix());
        while(p != null) {
            mtx.prepend(p.getMatrix());
            p = p._node_parentNode;
        }
        return mtx;
    }

    /**
     * Find the world-space AABB for this Node (children not included)
     *
     * @param target AABB object to assign result to
     */
    public getAABB(target: AABB): AABB {
        return Node2D.calculateAABB(target, this.getLocalToGlobalMatrix(aabb_matrix), this._node_pivot, this._node_size);
    }

    /**
     * Calculate the AABB for this Node using a specific reference matrix
     *
     * @param target AABB object to assign result to
     * @param matrix Matrix to use as base world transform
     */
    public calcAABB(target: AABB, matrix: Matrix): AABB {
        return Node2D.calculateAABB(target, matrix, this._node_pivot, this._node_size);
    }

    /**
     * Calculate the cumulative AABB of this node and all its children.
     * This is a slow and unoptimized feature, and should not be used
     * as the basis for a render loop (but initialization time calculations
     * should be fine).
     *
     * @param target AABB objec to assign result to
     * @param matrix Matrix to use as base world transform
     */
    public getAABBCumulative(target: AABB, matrix: Matrix): AABB {
        const recurse = (target: AABB, node: Node2D, mtx: Matrix): void => {
            // XXX; TODO: do not spam objects
            let m = mtx.clone().multiply(node.getMatrix());
            let bb = new AABB();

            // Zero-size elements should not be counted
            // (They're either not visible or pure)
            node.calcAABB(bb, m);

            for(let c = node.getFirstChild(); !!c; c = c.getNextNode()) {
                recurse(bb, c, m);
            }

            // Only merge nonzero BBs
            // (BBs of zero size are usually pure container)
            if(bb.getWidth() > 0 && bb.getHeight() > 0) {
                if(target.getWidth() == 0 && target.getHeight() == 0) {
                    target.copy(bb);
                } else {
                    target.merge(bb);
                }
            }
        };

        // Reset AABB
        target.setXY(0,0);

        // Recurse from current node onward
        recurse(target, this, matrix);
        return target;
    }

    public setUserData(data: any): Node2D {
        this._node_userdata = data;
        return this;
    }

    public getUserData(): any {
        return this._node_userdata;
    }

    //
    // Virtuals
    //

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {
        // NOP
    }

    /**
     * Reset UI logic to initial state. Used when input method is TOUCH
     * and no touches are active to avoid having buttons become sticky.
     *
     * Normally a NOP.
     */
    public resetUILogic(): void {
        // NOP
    }

    /**
     * Logic for UI nodes. Return a boolean value indicating whether
     * or not a click or touch event has been eaten by this node.
     *
     * @param px
     * @param py
     * @param radius
     * @param ppressed
     * @param pdown
     * @param preleased
     * @param alreadyclicked
     */
    public updateUILogic(
        px: number, py: number,
        radius: number,
        ppressed: boolean,
        pdown: boolean,
        preleased: boolean,
        alreadyclicked: boolean): boolean {

        return false;
    }

}

/**
 * Show a group of nodes.
 * Convenience function that calls setVisible(true) on all nodes passed as parameters.
 *
 * @param node a Node2D instance
 * @param more [optional], [variable] more Node2D instances
 */
export function show(node: Node2D, ...more: Node2D[]) {
    if(node) node.setVisible(true);
    for(let n of more) {
        if(n) n.setVisible(true);
    }
}

/**
 * Hide a group of nodes.
 * Convenience function that calls setVisible(false) on all nodes passed as parameters.
 *
 * @param node a Node2D instance
 * @param more [optional], [variable] more Node2D instances
 */
export function hide(node: Node2D, ...more: Node2D[]) {
    if(node) node.setVisible(false);
    for(let n of more) {
        if(n) n.setVisible(false);
    }
}
