import { Vec2, Matrix, clamp, toRadian, fast_cos, fast_sin, toDegree, Random, wrap_pi2 } from "../math";
import { Node2D } from "./node";
import { Renderer2D, ImageSource } from "../graphics";
import { getTimeDelta } from "../core";

const mabs = (a: number) => a < 0 ? -a : a;
const mmin = (a: number, b: number) => a < b ? a : b;
const mmax = (a: number, b: number) => a > b ? a : b;
const lerp = (bias: number, a: number, b: number) => (b - a) * bias + a;
const toZero = (value: number, amount: number) =>
    (mabs(value) - amount < 0) ? 0 : (value > 0 ? value - amount : value + amount);
const para = (bias: number, a: number, b: number) => {
    let c = b - a;
    let s = -4.0 * c;
    let x = bias - 0.5;
    return (s * (x * x) + c) + a;
}

/**
 * Spawn parameters; ParticleSystem2D uses these values as
 * input for spawning particles.
 *
 * NOTE: value names are prefixed with underscore in order to
 * allow a packer to minify the actual value names, to achieve
 * higher compression in the packed JavaScript output.
 */
export class Particle2DSpawnParameters {

    /**
     * Normalized direction value
     */
    public _direction: Vec2 = new Vec2(0, -1);

    /**
     * Spread angle in degrees offset from direction vector
     */
    public _spread: number = 10;

    /**
     * Minimum movement speed of particles (points per second)
     */
    public _speed_min: number = 100;

    /**
     * Maximum movement speed of particles (points per second)
     */
    public _speed_max: number = 250;

    /**
     * Minimum life time of particles, in seconds
     */
    public _life_min: number = 1.0;

    /**
     * Maximum life time of particles, in seconds
     */
    public _life_max: number = 1.5;

    /**
     * Initial common scaling factor
     */
    public _scale_initial: number = 1.0;

    /**
     * Minimum change in scale per second
     */
    public _scale_delta_min: number = -0.5;

    /**
     * Maximum change in scale per second
     */
    public _scale_delta_max: number = 0.5;

    /**
     * Initial common rotation, in degrees
     */
    public _rotation: number = 0;

    /**
     * Minimum rotation, degrees per second
     */
    public _rotation_delta_min: number = -10;

    /**
     * Maximum rotation, degrees per seconds
     */
    public _rotation_delta_max: number = 10;

    /**
     * Hue shift amount, low value (webgl only)
     */
    public _hue_min: number = 0;

    /**
     * Hue shift amount, high value (webgl only)
     */
    public _hue_max: number = 0;

    /**
     * Saturation adjust amount, low value
     */
    public _saturation_min: number = 0;

    /**
     * Saturation adjust amount, high value
     */
    public _saturation_max: number = 0;

    /**
     * Color value, low value
     */
    public _value_min: number = 1;

    /**
     * Color value, high value
     */
    public _value_max: number = 1;
}

export class ParticleEmitter2D extends Node2D {
    private _pe_systems: ParticleSystem2D[];
    private _pe_params: Particle2DSpawnParameters;
    private _pe_projected_position: Vec2;
    private _pe_projected_direction: Vec2;

    private _pe_manual: boolean;
    private _pe_tm_elapsed: number;
    private _pe_tm_interval: number;
    private _pe_matrix: Matrix;

    constructor() {
        super();

        this._pe_systems = [];
        this._pe_params = new Particle2DSpawnParameters();
        this._pe_params._direction.setXY(1,0);
        this._pe_projected_position = new Vec2();
        this._pe_projected_direction = new Vec2();
        this._pe_manual = false;
        this._pe_tm_interval = 0.1;
        this._pe_tm_elapsed = 0;
        this._pe_matrix = new Matrix();

        this.setPivotXY(512, 512);
        this.setSizeXY(1024, 1024);

        this.markAsDrawable();
    }

    /**
     * Set radius (or, rather, half edge) of the bounding box that
     * is used to activate this particle emitter. The larger this
     * value is, the further away from the screen edge this emitter
     * is triggered.
     *
     * @param r a positive value
     */
    public setRadius(r: number): ParticleEmitter2D {
        r = mabs(r);
        let r2 = r * 2;
        this.setPivotXY(r, r);
        this.setSizeXY(r2, r2);
        return this;
    }

    /**
     * Reset timings for this particle emitter
     */
    public reset(): ParticleEmitter2D {
        this._pe_tm_elapsed = 0;
        return this;
    }

    /**
     * Set manual update method. If enabled, update(delta) needs to be called once
     * a frame. Otherwise, the update method is being called by the draw() method
     * during rendering and does not need to be touched manually.
     * By default, manual update is DISABLED.
     *
     * @param enable boolean, enable or disable manual update method
     */
    public setManualUpdate(enable: boolean): ParticleEmitter2D {
        this._pe_manual = !!enable;
        return this;
    }

    /**
     * Return true if manual update method is in use
     */
    public isManualUpdate(): boolean {
        return this._pe_manual;
    }

    /**
     * Make the emitter sticky. Cannot be unset.
     *
     * @param enabled
     */
    public setSticky(enabled: boolean): void {
        if(enabled) {
            this.markAsSticky();
        }
    }

    /**
     * Set emission rate, particles per second
     */
    public setRate(pps: number): ParticleEmitter2D {
        pps = Math.max(1,pps);
        this._pe_tm_interval = 1.0 / pps;
        return this;
    }

    public getRate(): number {
        return 1.0 / this._pe_tm_interval;
    }

    /**
     * Set speed interval of emitted particles
     *
     * @param speed_min minimum movement speed, in points per second
     * @param speed_max maximum movement speed, in points per second
     */
    public setParticleSpeed(speed_min: number, speed_max: number): ParticleEmitter2D {
        this._pe_params._speed_min = speed_min;
        this._pe_params._speed_max = speed_max;
        return this;
    }

    /**
     * Set life time interval of emitted particles
     *
     * @param min minimum time to live, in milliseconds
     * @param max maximum time to live, in milliseconds
     */
    public setParticleLife(min: number, max: number): ParticleEmitter2D {
        min *= 0.001;
        max *= 0.001;
        this._pe_params._life_min = mmin(min, max);
        this._pe_params._life_max = mmax(min, max);
        return this;
    }

    /**
     * Set scaling of emitted particles
     *
     * @param initial initial scale, common to all particles
     * @param min minimum amount of change per second
     * @param max maximum amount of change per second
     */
    public setParticleScale(initial: number, min: number, max: number): ParticleEmitter2D {
        this._pe_params._scale_initial = initial;
        this._pe_params._scale_delta_min = mmin(min, max);
        this._pe_params._scale_delta_max = mmax(min, max);
        return this;
    }

    /**
     * Set the hue modifier of emitted particles. This sets
     * the random sampling value; the resulting value for each
     * particle is wrapped to between 0 and 2 * PI.
     *
     * @param min low value of hue modifier
     * @param max high value of hue modifier
     */
    public setParticleHue(min: number, max: number): ParticleEmitter2D {
        this._pe_params._hue_min = min;
        this._pe_params._hue_max = max;
        return this;
    }

    /**
     * Set the saturation modifier of emitted particles. This
     * sets the random sampling range; the resulting value
     * for each particle is clamped to between -1 and 1.
     *
     * @param min low value of saturation modifier
     * @param max high value of saturation modifier
     */
    public setParticleSaturation(min: number, max: number): ParticleEmitter2D {
        this._pe_params._saturation_min = min;
        this._pe_params._saturation_max = max;
        return this;
    }

    /**
     * Sets the color value modifier of emitted particles. This
     * sets the random sampling range; the resulting value
     * for each particle is clamped to between 0 and 1.
     *
     * @param min low value of value modifier
     * @param max high value of value modifier
     */
    public setParticleValue(min: number, max: number): ParticleEmitter2D {
        this._pe_params._value_min = min;
        this._pe_params._value_max = max;
        return this;
    }

    /**
     * Set rotation of emitted particles
     *
     * @param initial initial common rotation value, in degrees
     * @param min minimum rotation value, in degrees per second
     * @param max maximum rotation value, in degrees per second
     */
    public setParticleRotation(initial: number, min: number, max: number): ParticleEmitter2D {
        this._pe_params._rotation = initial;
        this._pe_params._rotation_delta_min = mmin(min, max);
        this._pe_params._rotation_delta_max = mmax(min, max);
        return this;
    }

    /**
     * Set spread of emitted particles, as centered delta relative to emission angle
     *
     * @param deg spread, in degrees
     */
    public setParticleSpread(deg: number): ParticleEmitter2D {
        this._pe_params._spread = clamp(deg, 0, 360);
        return this;
    }

    /**
     * Set particle emission angle (local to the emitter node)
     *
     * @param deg angle, in degrees
     */
    public setParticleAngle(deg: number): ParticleEmitter2D {
        let r = toRadian(deg);
        this._pe_params._direction.x = fast_cos(r);
        this._pe_params._direction.y = fast_sin(r);
        return this;
    }

    public getParticleAngle(): number {
        return toDegree(Math.atan2(this._pe_params._direction.y, this._pe_params._direction.x));
    }

    /**
     * Add a particle system to this emitter - when the emitter is drawn, it spawns
     * particles from systems attached to it.
     *
     * @param system a ParticleSystem2D instance
     */
    public addSystem(system: ParticleSystem2D): ParticleEmitter2D {
        if(this._pe_systems.indexOf(system) < 0) {
            this._pe_systems.push(system);
        }
        return this;
    }

    /**
     * Remove a particle system from this emitter, if added.
     *
     * @param system a ParticleSystem2D instance
     */
    public removeSystem(system: ParticleSystem2D): ParticleEmitter2D {
        let idx = this._pe_systems.indexOf(system);
        if(idx >= 0) {
            this._pe_systems.splice(idx,1);
        }
        return this;
    }

    /**
     * Update the emitter's state. Should only be called once a frame.
     * Unless the emitter has been set to manual update mode, this function
     * is called automatically by the draw() routine as part of standard
     * scene rendering.
     *
     * @param delta time between frames, in seconds.
     */
    public update(delta: number): void {

        let stage = this.getStage();
        if(!stage) return;

        let mtx = this._pe_matrix;
        let t = delta;
        let e = this._pe_tm_elapsed + t;
        let c = 0;
        let interval = this._pe_tm_interval;
        let worldpos = mtx.project(Vec2.ZERO, this._pe_projected_position);
        stage.screenToWorld(worldpos, worldpos);
        let direction = mtx.project(this._pe_params._direction, this._pe_projected_direction);
        stage.screenToWorld(direction, direction);
        direction.subtract(worldpos).normalize();

        while(e > interval) {
            e -= interval;
            ++c;
        }
        this._pe_tm_elapsed = e;

        if(c > 0) {
            let systems = this._pe_systems;
            let params = this._pe_params;
            for(let i = 0, l = systems.length; i < l; ++i) {
                let s = systems[i];
                if(s.isEnabled()) {
                    s.spawn(c, worldpos, direction, params);
                }
            }
        }
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {
        this._pe_matrix.set(mtx);
        if(!this._pe_manual) {
            this.update(getTimeDelta());
        }
    }
}

const tpos = new Vec2();

export class ParticleSystem2D extends Node2D {

    private _ps_image: ImageSource;
    private _ps_data: Float32Array
    private _ps_size: number;
    private _ps_used: number;

    private _ps_gravity: Vec2;
    private _ps_scroll: Vec2;
    private _ps_drag: number;
    private _ps_random: Random;
    private _ps_inverse: Matrix;
    private _ps_manualUpdate: boolean;
    private _ps_enabled: boolean;
    private _ps_parabolic_fade: boolean;

    constructor(img: ImageSource, count: number = 256) {
        super();

        this._ps_image = img;
        this._ps_data = new Float32Array(count *  16);
        this._ps_used = 0;
        this._ps_size = count;
        this._ps_gravity = new Vec2();
        this._ps_scroll = new Vec2();
        this._ps_random = new Random(4096);
        this._ps_inverse = new Matrix();
        this._ps_drag = 0;
        this._ps_manualUpdate = false;
        this._ps_enabled = true;
        this._ps_parabolic_fade = false;

        this.markAsSticky();
        this.markAsDrawable();
    }

    public setParabolicFade(enabled: boolean): ParticleSystem2D {
        this._ps_parabolic_fade = !!enabled;
        return this;
    }

    public isParabolicFade(): boolean {
        return this._ps_parabolic_fade;
    }

    public setManualUpdate(enabled: boolean): ParticleSystem2D {
        this._ps_manualUpdate = !!enabled;
        return this;
    }

    public isManualUpdate(): boolean {
        return this._ps_manualUpdate;
    }

    public setEnabled(enabled: boolean): ParticleSystem2D {
        this._ps_enabled = !!enabled;
        return this;
    }

    public isEnabled(): boolean {
        return this._ps_enabled;
    }

    private sanitize(params: Particle2DSpawnParameters): void {
        params._life_min = Math.max(params._life_min, 0.01);
        params._life_max = Math.max(params._life_max, 0.015);
    }

    // TODO: position is WORLD POSITION
    public spawn(num: number, position: Vec2, direction: Vec2, params: Particle2DSpawnParameters, inertia?: Vec2): ParticleSystem2D {
        this.sanitize(params);
        num |= 0;

        // XXX: slow...
        let inverse = this._ps_inverse;
        inverse.set(this.getMatrix());

        let n: Node2D = this.getParent();
        while(n != null) {
            inverse.prepend(n.getMatrix());
            n = n.getParent();
        }
        inverse.invert().project(position, tpos);

        let base_angle = toDegree(Math.atan2(direction.y,direction.x));
        let half_spread = params._spread * .5;

        let ixadd = 0;
        let iyadd = 0;
        if(inertia) {
            ixadd = inertia.x;
            iyadd = inertia.y;
        }

        let rnd = this._ps_random;
        params._direction.normalize();

        let roto = params._rotation;

        const size = this._ps_size;
        const data = this._ps_data;
        let used = this._ps_used;
        num = mmin(num, size - used);

        for(let i = 0; i < num; ++i) {

            let p = (used++) << 4;  // particle index

            let speed = rnd.nextInRange(params._speed_min,params._speed_max);
            let life = rnd.nextInRange(params._life_min,params._life_max);
            let scale = rnd.nextInRange(params._scale_delta_min,params._scale_delta_max);
            let angle = rnd.nextInRange(base_angle - half_spread,base_angle + half_spread);
            let roto_d = toRadian(rnd.nextInRange(params._rotation_delta_min, params._rotation_delta_max));
            let hue = wrap_pi2(rnd.nextInRange(params._hue_min, params._hue_max));
            let sat = clamp(rnd.nextInRange(params._saturation_min, params._saturation_max), -1, 1);
            let val = clamp(rnd.nextInRange(params._value_min, params._value_max), 0, 1);
            angle = toRadian(angle);

            // interface IParticle {
            //     x: number;
            //     y: number;
            //     s: number;
            //     a: number;      // alpha value
            //     l: number;      // life time
            //     r: number;

            //     dx: number;
            //     dy: number;
            //     ds: number;
            //     dr: number;
            //     dl: number;     // delta life
            //     l0: number;     // reciprocal of initial life time

            //     hue: number;
            //     sat: number;
            //     val: number
            // }
            // 15 fields; add padding, get 16; shift by 4

            data[p + 0] = tpos.x;                           // x
            data[p + 1] = tpos.y;                           // y
            data[p + 2] = params._scale_initial;            // s
            data[p + 3] = 0;                                // a
            data[p + 4] = life;                             // l
            data[p + 5] = roto;                             // r

            data[p + 6] = fast_cos(angle) * speed + ixadd;  // dx
            data[p + 7] = fast_sin(angle) * speed + iyadd;  // dy
            data[p + 8] = scale;                            // ds
            data[p + 9] = roto_d;                           // dr
            data[p + 10] = -1.0 / life;                     // dl
            data[p + 11] = -data[p + 10];                   // l0

            data[p + 12] = hue;                             // hue
            data[p + 13] = sat;                             // saturation
            data[p + 14] = val;                             // value
        }

        this._ps_used = used;
        return this;
    }

    public setDrag(drag: number): ParticleSystem2D {
        this._ps_drag = drag;
        return this;
    }

    public getDrag(): number {
        return this._ps_drag;
    }

    public getGravity(): Vec2 {
        return this._ps_gravity;
    }

    public setGravity(g: Vec2): ParticleSystem2D {
        this._ps_gravity.set(g);
        return this;
    }

    public getScroll(): Vec2 {
        return this._ps_scroll;
    }

    public setScroll(s: Vec2): ParticleSystem2D {
        this._ps_scroll.set(s);
        return this;
    }

    public clear(): ParticleSystem2D {
        this._ps_used = 0;
        return this;
    }

    public update(delta: number): ParticleSystem2D {
        let g = this._ps_gravity;
        let s = this._ps_scroll;
        let d = this._ps_drag * delta;
        let gxadd = g.x * delta;
        let gyadd = g.y * delta;
        let sxadd = s.x * delta;
        let syadd = s.y * delta;
        let data = this._ps_data;
        let used = this._ps_used;

        if(this._ps_parabolic_fade) {
            for(let i = 0; i < used; ++i) {
                let p = i << 4;

                data[p + 0] += data[p + 6] * delta + sxadd; // x + dx
                data[p + 1] += data[p + 7] * delta + syadd; // y + dy
                data[p + 2] += data[p + 8] * delta;         // s + ds
                data[p + 4] += data[p + 10] * delta;        // l + dl
                data[p + 5] += data[p + 9] * delta;         // r + dr

                data[p + 3]  = para(data[p + 4] * data[p + 11], 0, 1);
                data[p + 6]  = toZero(data[p + 6] + gxadd, d);
                data[p + 7]  = toZero(data[p + 7] + gyadd, d);

                // Remove particle
                if(data[p + 4] < 0) {
                    let from = (--used) << 4;
                    for(let j = 0; j < 16; ++j) {
                        data[p + j] = data[from + j];
                    }
                }
            }
        } else {
            for(let i = 0; i < used; ++i) {
                let p = i << 4;

                data[p + 0] += data[p + 6] * delta + sxadd; // x + dx
                data[p + 1] += data[p + 7] * delta + syadd; // y + dy
                data[p + 2] += data[p + 8] * delta;         // s + ds
                data[p + 4] += data[p + 10] * delta;        // l + dl
                data[p + 5] += data[p + 9] * delta;         // r + dr

                data[p + 3]  = lerp(data[p + 4] * data[p + 11], 0, 1);
                data[p + 6]  = toZero(data[p + 6] + gxadd, d);
                data[p + 7]  = toZero(data[p + 7] + gyadd, d);

                // Remove particle
                if(data[p + 4] < 0) {
                    let from = (--used) << 4;
                    for(let j = 0; j < 16; ++j) {
                        data[p + j] = data[from + j];
                    }
                }
            }
        }

        this._ps_used = used;

        return this;
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number) {

        if(!this._ps_manualUpdate) {
            this.update(getTimeDelta());
        }

        let pw = this._ps_image.getWidth() * .5;
        let ph = this._ps_image.getHeight() * .5;

        let data = this._ps_data;
        let used = this._ps_used;

        r.setMatrix(mtx);
        r.setAlpha(alpha);
        r.setBlendMode(this.getBlendMode());
        r.beginSprites(this._ps_image, used);

        for(let i = 0; i < used; ++i) {
            let p = i << 4;
            let x = data[p + 0];
            let y = data[p + 1];
            let s = data[p + 2];
            let a = data[p + 3];
            let roto = data[p + 5];
            let hue = data[p + 12];
            let sat = data[p + 13];
            let val = data[p + 14];
            r.addSprite(x, y, pw, ph, s, s, roto, a, hue, sat, val);
        }

        r.draw();

    }

}
