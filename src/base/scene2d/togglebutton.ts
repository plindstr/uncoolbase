import { Node2D } from "./node";
import { ButtonState, Button2D } from "./button";
import { wrap } from "../math";
import { CallbackList } from "../mem/callbacklist";
import { assert } from "../core/assert";

export class ToggleButtonState extends Node2D {

    private _tbstate_containers: Node2D[] = [];

    public hasContainer(state: ButtonState): boolean {
        return !!this._tbstate_containers[state];
    }

    public getContainer(state: ButtonState): Node2D {
        let me = this;
        let c = me._tbstate_containers[state];
        if(c) return c;

        c = new Node2D();
        me._tbstate_containers[state] = c;

        return c;
    }

    public setActiveState(state: ButtonState): ToggleButtonState {
        this.clearChildren();
        this.addChild(this.getContainer(state));
        return this;
    }

}

export class ToggleButton2D extends Button2D {

    private _togglebutton_state: ToggleButtonState[] = [];
    private _togglebutton_currentstate: ToggleButtonState;
    private _togglebutton_stateidx: number = 0;
    private _togglebutton_handlers = new CallbackList<(state: number) => void>();

    constructor() {
        super();

        super.addClickHandler(() => {
            this._togglebutton_stateidx = wrap(this._togglebutton_stateidx + 1, 0, this._togglebutton_state.length);
            this.setCurrentStateIndex(this._togglebutton_stateidx);
            this._togglebutton_handlers.run(this._togglebutton_stateidx);
        });

        // Add initial state
        this._togglebutton_currentstate = this.addState();
    }

    private _setActiveState(stateidx: number): void {
        if(this._togglebutton_currentstate) {
            this._togglebutton_currentstate.removeFromParent();
        }
        this._togglebutton_stateidx = stateidx;
        this._togglebutton_currentstate = this.getCurrentState();
        this.addChild(this._togglebutton_currentstate, false);
        this._togglebutton_currentstate.setActiveState(this.getButtonState());
    }

    public addState(): ToggleButtonState {
        let s = new ToggleButtonState();
        this._togglebutton_state.push(s);
        this._setActiveState(this._togglebutton_state.length - 1);
        return s;
    }

    public getStateContainer(state: ButtonState): Node2D {
        return this._togglebutton_currentstate.getContainer(state);
    }

    public getCurrentStateIndex(): number {
        return this._togglebutton_stateidx;
    }

    public getCurrentState(): ToggleButtonState {
        return this._togglebutton_state[this._togglebutton_stateidx];
    }

    public setButtonState(state: ButtonState): ToggleButton2D {
        this._togglebutton_currentstate.setActiveState(state);
        return this;
    }

    public hasButtonState(state: ButtonState): boolean {
        return !!this._togglebutton_currentstate.hasContainer(state);
    }

    public makeActive(state: ToggleButtonState): ToggleButton2D {
        if(!state) {
            console.error("State is null");
            throw 0;
        }

        let idx = this._togglebutton_state.indexOf(state);
        assert(idx >= 0, "Provided state is not part of this ToggleButton");
        this._setActiveState(idx);
        return this;
    }

    public setCurrentStateIndex(stateidx: number): ToggleButton2D {
        stateidx |= 0;
        assert(stateidx >= 0 && stateidx < this._togglebutton_state.length,
            "State index " + stateidx + " is out of bounds; acceptable range between 0 and " + this._togglebutton_state.length + ", exclusive");
        this._setActiveState(stateidx);
        return this;
    }

    public addStateChangeHandler(handler: (newstate: number) => void): ToggleButton2D {
        this._togglebutton_handlers.add(handler);
        return this;
    }

    public removeStateChangeHandler(handler: (newstate: number) => void): ToggleButton2D {
        this._togglebutton_handlers.remove(handler);
        return this;
    }

    public clearStateChangeHandlers(): ToggleButton2D {
        this._togglebutton_handlers.clear();
        return this;
    }

}
