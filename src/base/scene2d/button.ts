import { Node2D } from "./node";
import { AABB, Matrix, Vec2 } from "../math";
import { KeySym, getKeyboard } from "../input";
import { CallbackList } from "../mem/callbacklist";

// Temporary objects for AABB calculation
const t_aabb   = new AABB();
const t_aabb2  = new AABB();
const t_matrix = new Matrix();
const t_pivot  = new Vec2();
const t_size   = new Vec2();

/**
 * Possible states for Button2D
 */
export enum ButtonState {
    DISABLED,
    ENABLED,
    HOVERED,
    PRESSED
}

/**
 * Button class - provides an on-screen object that
 * can be clicked with a pointing device. Use
 * stage.updateUI() to run the default update routine
 * on all visible and enabled Button objects, whereby
 * they will update their state (hovered/pressed) and
 * test whether or not they've been clicked.
 *
 * Buttons can also have keyboard shortcuts, which
 * will be tested for if the buttons are visible and
 * enabled.
 */
export class Button2D extends Node2D {

    private _button_shortcut: KeySym;
    private _button_state: ButtonState;
    private _button_enabled: boolean;
    private _button_engaged: boolean;
    private _button_bounds: AABB;
    private _button_stateContainers: Node2D[] = [];
    private _button_clickhandlers = new CallbackList<()=>void>();
    private _button_stateHandlers = new CallbackList<(state: ButtonState)=>void>();

    constructor() {
        super();

        let me = this;
        me._button_state = ButtonState.ENABLED;
        me._button_enabled = true;
        me._button_engaged = false;
        me._button_shortcut = null;
        me._button_bounds = new AABB();

        this.markAsUIObject();
    }

    protected setStateContainer(state: ButtonState, container: Node2D): Button2D {
        if(state === this._button_state) {
            this._button_stateContainers[state].removeFromParent();
        }
        this._button_stateContainers[state] = container;
        return this;
    }

    public addClickHandler(fn: () => void): Button2D {
        this._button_clickhandlers.add(fn);
        return this;
    }

    public removeClickHandler(fn: () => void): Button2D {
        this._button_clickhandlers.remove(fn);
        return this;
    }

    public clearClickHandlers(): Button2D {
        this._button_clickhandlers.clear();
        return this;
    }

    public addStateChangeHandler(fn: (state: ButtonState) => void): Button2D {
        this._button_stateHandlers.add(fn);
        return this;
    }

    public removeStateChangeHandler(fn: (state: ButtonState) => void): Button2D {
        this._button_stateHandlers.remove(fn);
        return this;
    }

    public clearStateChangeHandler(): Button2D {
        this._button_stateHandlers.clear();
        return this;
    }

    public addChild(child: Node2D, addToBounds: boolean = true): Button2D {
        if(addToBounds) {
            let b = this._button_bounds;
            let s = child.getAABB(t_aabb);

            if(b.min.equals(b.max)) {
                b.set(s.min);
            } else {
                b.add(s.min);
            }
            b.add(s.max);
        }

        super.addChild(child);

        return this;
    }

    /**
     * Manually set bounds for this button. Bounds are still subject to
     * regular node scaling, rotation, etc.
     *
     * @param bb an AABB instance
     */
    public setBounds(bb: AABB): Button2D {
        let me = this;
        me._button_bounds.set(bb.min);
        me._button_bounds.add(bb.max);
        return this;
    }

    /**
     * Check if a state container has been added for the given button state
     *
     * @param state a ButtonState
     */
    public hasStateContainer(state: ButtonState): boolean {
        return !!this._button_stateContainers[state];
    }

    /**
     * The state container is visible when the button is in a given state
     *
     * @param state a ButtonState
     */
    public getStateContainer(state: ButtonState): Node2D {
        let me = this;
        let c = me._button_stateContainers[state];
        if(c) return c;

        c = new Node2D();
        me._button_stateContainers[state] = c;

        if(state === this._button_state) {
            this.addChild(c);
        }

        return c;
    }

    /**
     * Set button enabled state. If button is disabled, it will
     * be left permanently in DISABLED ButtonState; all setState
     * commands will be effectively ignored. If the button is
     * disabled, it will not respond to clicks or shortcuts.
     *
     * @param enabled true or false
     */
    public setEnabled(enabled: boolean): Button2D {
        let me = this;
        me._button_enabled = enabled;
        me.setButtonState(ButtonState.ENABLED);   // see logic in setState
        return this;
    }

    public isEnabled(): boolean {
        return this._button_enabled;
    }

    public setShortcutKey(key: KeySym | null): Button2D {
        this._button_shortcut = key;
        return this;
    }

    public getShortcutKey(): KeySym {
        return this._button_shortcut;
    }

    public hasButtonState(state: ButtonState): boolean {
        return !!this._button_stateContainers[state];
    }

    public setButtonState(state: ButtonState): Button2D {
        let me = this;
        if(!me._button_enabled) state = ButtonState.DISABLED;

        let c = me._button_stateContainers[me._button_state];
        if(c) c.removeFromParent();

        me._button_state = state;
        c = me._button_stateContainers[state];
        if(c) super.addChild(c);

        this._button_stateHandlers.run(state);

        return this;
    }

    public getButtonState(): ButtonState {
        return this._button_state;
    }

    public getAABB(target: AABB): AABB {
        let b = this._button_bounds;
        t_pivot.setXY(-b.min.x, -b.min.y);
        t_size.setXY(b.getWidth(), b.getHeight());
        return Node2D.calculateAABB(target, this.getLocalToGlobalMatrix(t_matrix), t_pivot, t_size);
    }

    public calcAABB(target: AABB, matrix: Matrix): AABB {
        let b = this._button_bounds;
        t_pivot.setXY(-b.min.x, -b.min.y);
        t_size.setXY(b.getWidth(), b.getHeight());
        return Node2D.calculateAABB(target, matrix, t_pivot, t_size);
    }

    /**
     * Call this when input is touch and no touches have been received to reset internal state
     * tracking to something more useful
     */
    public resetUILogic(): void {
        let me = this;
        if(!me._button_enabled) return;
        if(!me.isVisible()) return;

        me._button_engaged = false;
        me.setButtonState(ButtonState.ENABLED);
    }

    /**
     * Simulate a click
     */
    public click(): void {
        this._button_clickhandlers.run();
    }

    public updateUILogic(
        px: number,
        py: number,
        radius: number,
        ppressed: boolean,
        pdown: boolean,
        preleased: boolean,
        alreadyclicked: boolean): boolean {

        let me = this;
        if(!me._button_enabled) return;
        if(!me.isVisible()) return;

        let bb = me.getAABB(t_aabb);
        let pbb = t_aabb2.setXY(px - radius, py - radius).addXY(px + radius, py + radius);
        let over = bb.isTouching(pbb);
        let click = false;

        const callHandlers = () => {
            this._button_clickhandlers.run();
        }

        if(over) {

            if(ppressed) {
                me._button_engaged = true;
            }

            if(!alreadyclicked && pdown && ppressed) {
                if(me.hasButtonState(ButtonState.PRESSED)) {
                    me.setButtonState(ButtonState.PRESSED);
                }
            } else {
                if(me.hasButtonState(ButtonState.HOVERED)) {
                    me.setButtonState(ButtonState.HOVERED);
                }
            }

            if(!alreadyclicked && preleased && me._button_engaged) {
                callHandlers();
                click = true;
            }

        } else {

            me.setButtonState(ButtonState.ENABLED);
            me._button_engaged = false;

        }

        if(me._button_shortcut) {

            if(getKeyboard().isKeyPressed(me._button_shortcut)) {
                callHandlers();
            }

        }

        return click || alreadyclicked;
    }

}
