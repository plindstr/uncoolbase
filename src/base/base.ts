//
//      The Uncool Base
// Game development framework
//
// Licence: MIT
//

import { AssetList } from "./loader/assetlist";
import { getContainer, debugLog, setDebugVisible } from "./core";
import { array_add_unique } from "./mem";
import { updateTime, getTimeDelta } from "./core/timer";
import { getKeyboard, KeySym, getTouch, getMouse, resetInputTouchedThisFrame } from "./input";
import { updateTimers } from "./timer";
import { updateTweens } from "./tween";
import { updateAnimators } from "./graphics/animationmanager";
import { updateLoops } from "./core";
import { getMixer } from "./sound";
import { getRuntimeProvider, getDeploymentProvider, initializePlatformProviders } from "./platform";
import { initialize } from "./init";
import { debug } from "./core/debug";
import { updateTextureCaches } from "./graphics/texturecache";


// Re-export all core functionality
export * from "./mem"
export * from "./core"
export * from "./math"
export * from "./input"
export * from "./graphics"
export * from "./collision"
export * from "./sound"
export * from "./timer"
export * from "./tween"
export * from "./util"
export * from "./loader"
export * from "./platform"
export * from "./scene2d"
export * from "./events"


let backPressed = false;
let forcedFocus = true;

/**
 * Enable or disable the forced focus mode
 *
 * @param enable boolean value
 */
export function setForcedFocus(enable: boolean): void {
    forcedFocus = !!enable;
}

export function isForcedFocusEnabled(): boolean {
    return !!forcedFocus;
}

/**
 * Mark that the universal 'back' button has been pressed
 */
export function markBackPressed(): void {
    backPressed = true;
}

/**
 * Test if back button was pressed this frame. The back button
 * is a platform-dependent 'escape' key or similar. By default,
 * 'backPressed' returns true if ESC has been pressed.
 *
 * Call markBackPressed to also trigger this value in other situations
 * (like as a result of the 'backbutton' event on Android).
 */
export function isBackPressed(): boolean {
    return backPressed;
}


let nextFrame: (() => void)[] = [];

/**
 * Run a function at the start of the next frame, after timers
 * have updated but before input state is updated or any
 * processing is performed.
 *
 * @param fn a callback function that takes no parameters
 */
export function runNextFrame(fn: () => void): void {
    array_add_unique(nextFrame, fn);
}

let shouldRunMainLoop = false;
function mainLoop(time: number): void {

    if(forcedFocus) {
        /// XXX: Facebook's ad system makes the game lose focus
        window.focus(); // force refocus after ad to make keyboard work...
    }

    updateTime(time);
    let delta = getTimeDelta();

    // Update any function scheduled for next frame
    while(nextFrame.length) {
        let fn = nextFrame.shift();
        fn.call(null);
    }

    resetInputTouchedThisFrame();
    getKeyboard().update();
    getMouse().update();

    backPressed = backPressed || getKeyboard().isKeyPressed(KeySym.ESC);

    updateTimers(delta);
    updateTweens(delta);
    updateAnimators(delta);
    updateLoops(delta);

    // Update sound system
    getMixer().update();

    // Update touch here since updating it resets the tracked state
    getTouch().update();

    // Update all texture caches to flush out unused temporary textures
    updateTextureCaches();

    // Reset back button flag
    backPressed = false;

    if(shouldRunMainLoop) {
        requestAnimationFrame(mainLoop);
    }
}

export function setMainLoopActive(enable: boolean): void {
    if((shouldRunMainLoop = !!enable)) {
        requestAnimationFrame(mainLoop);
    } else {
        debugLog("Base main loop halted");
    }
}

export function addSuspendHandler(fn: () => void): void {
    getRuntimeProvider().addSuspendHandler(fn);
}

export function removeSuspendHandler(fn: () => void): void {
    getRuntimeProvider().removeSuspendHandler(fn);
}

export function addResumeHandler(fn: () => void): void {
    getRuntimeProvider().addResumeHandler(fn);
}

export function removeResumeHandler(fn: () => void): void {
    getRuntimeProvider().removeResumeHandler(fn);
}

export async function start(assetList: AssetList = null, autostart: boolean = true): Promise<void> {

    // Set body style
    debugLog("Set body style");
    let s = document.body.style;
    s.position = 'fixed';
    s.width = '100%';
    s.height = '100%';
    s.overflow = 'hidden';
    s.background = '#000';
    s.color = 'rgb(200,200,200)';
    s.margin = '0px';
    s.padding = '0px';
    s.border = '0px';

    // Run core initialization
    initialize();

    // Make debug visible from the outset if possible
    debug(() => setDebugVisible(true));

    // Initialize any and all platform providers here
    debugLog("Initialize platform providers");
    await initializePlatformProviders();

    // Load the application if we must
    if(!!assetList) {
        debugLog('Starting load of ' + assetList.size() + ' items');
        await getDeploymentProvider().loadApplication(assetList);
    } else {
        debugLog('Skipping initial load due to lack of asset list');
    }

    // Start the application main loop
    debugLog("Start main loop");
    setMainLoopActive(true);

    if(autostart) {
        // Wait for acknowledgement of startup from the deployment platform provider
        debugLog("Wait for deployment provider application start");
        await getDeploymentProvider().startApplication();
        debugLog("Deployment provider start complete");
    }

    debugLog("Set game visible");
    setGameVisible(true);

    // Set debug invisible after startup is complete
    debugLog("Hide debug console");
    setDebugVisible(false);

    debugLog("Game started");
}

/**
 * Toggle visibility of the actual game content
 *
 * @param visible true to show, false to hide
 */
export function setGameVisible(visible: boolean): void {
    getContainer().style.visibility = visible ? 'visible' : 'hidden';
}


