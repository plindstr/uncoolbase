import { ImageSource } from "./imagesource";
import { Image } from "./image";
import { Rect } from "../math/rect";
import { assert } from "../core/assert";

export class SubImage implements ImageSource {

    private _si_image: Image = null;
    private _si_rect: Rect = null;
    private _si_xoffset: number = 0;
    private _si_yoffset: number = 0;
    private _si_real_width: number = 0;
    private _si_real_height: number = 0;

    constructor(image: Image, x: number, y: number, w: number, h: number, xoffset: number = 0, yoffset: number = 0, real_w: number = w, real_h: number = h) {
        assert(x >= 0 && x < image.getWidth(), "Invalid X coordinate");
        assert(y >= 0 && y < image.getHeight(), "Invalid Y coordinate");
        assert(w > 0 && x + w <= image.getWidth(), "Invalid width");
        assert(h > 0 && y + h <= image.getHeight(), "Invalid height");
        assert(real_w > 0, "Real width cannot be <= 0");
        assert(real_h > 0, "Real height cannot be <= 0");
        assert(real_w >= w, "Real width cannot be smaller than sample width");
        assert(real_h >= h, "Real height cannot be smaller than sample height");

        this._si_image = image;
        this._si_rect = new Rect(x, y, w, h);
        this._si_real_width = real_w;
        this._si_real_height = real_h;
        this._si_xoffset = xoffset;
        this._si_yoffset = yoffset;
    }

    public getElement(): HTMLImageElement {
        return this._si_image.getElement();
    }

    public getElementWidth(): number {
        return this._si_image.getElementWidth();
    }

    public getElementHeight(): number {
        return this._si_image.getElementHeight();
    }

    public getSampleRect(): Rect {
        return this._si_rect;
    }

    public getX(): number {
        return this._si_xoffset;
    }

    public getY(): number {
        return this._si_yoffset;
    }

    public getWidth(): number {
        return this._si_real_width;
    }

    public getHeight(): number {
        return this._si_real_height;
    }

    public setResident(enable: boolean): void {
        if(enable && !this._si_image.isResident()) {
            this._si_image.setResident(true);
        }
    }

    public isResident(): boolean {
        return this._si_image.isResident();
    }

}
