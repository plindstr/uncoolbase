//
// blendmode.ts
//
// A list of supported blend modes
//

export enum BlendMode {

    /**
     * Standard alpha blending
     *
     * result = source * source_alpha + destination * (1.0 - source_alpha)
     */
    ALPHA,

    /**
     * Standard additive blending
     *
     * result = source * source_alpha + destination
     */
    ADDITIVE,

    /**
     * Standard multiplicative blending
     *
     * result = source * source_alpha * destination
     */
    MULTIPLY
}
