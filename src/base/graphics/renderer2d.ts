import { ImageSource } from "./imagesource";
import { BlendMode } from "./blendmode";
import { Matrix } from "../math";
import { Color } from "./color";
import { Animation } from "./animation";
import { Surface } from "./surface";

export interface Renderer2D {

    /**
     * Return true if this is a WebGL renderer
     */
    isGL(): boolean;

    /**
     * Run initialization routine before starting render of a new frame
     */
    init(): void;

    /**
     * Signal end of frame, clean up renderer state
     */
    end(): void;

    /**
     * Do what you need to make sure the provided image is
     * renderable as soon as possible.
     *
     * @param image an image source
     */
    precache(image: ImageSource): void;

    /**
     * Clear texture cache (DANGEROUS, WEBGL ONLY)
     */
    clearCache(): void;

    /**
     * Enable or disable persistent texture caching mode (DANGEROUS, WEBGL ONLY)
     * @param enable true to enable persistent caching and _DISABLE_ dynamic caching (default: false)
     */
    setCachePersistent(enable: boolean): void

    /**
     * Get the surface this Renderer is attached to
     */
    getSurface(): Surface;

    /**
     * Get the width of the surface this Renderer is attached to
     */
    getWidth(): number;

    /**
     * Get the height of the surface this Renderer is attached to
     */
    getHeight(): number;

    /**
     * Set up the context such that its internal state matches
     * the state of the renderer
     */
    restore(): void;

    /**
     * Set the current blend mode
     *
     * @param mode see values in BlendMode enum
     */
    setBlendMode(mode: BlendMode): void;

    /**
     * Set the global alpha
     *
     * @param alpha value between 0 and 1
     */
    setAlpha(alpha: number): void;

    /**
     * Set the current world matrix
     * @param matrix
     */
    setMatrix(matrix: Matrix): void;

    /**
     * Get the current world matrix
     */
    getMatrix(): Matrix;

    /**
     * Set the line width to use for all line drawing routines
     * @param width
     */
    setLineWidth(width: number): void;

    /**
     * Get the currently set line width
     */
    getLineWidth(): number;

    /**
     * Begin drawing individual lines. Each pair of
     * two points added will form a single line from
     * point A to point B
     */
    beginLines(color: Color, expectedVertices: number): void;

    /**
     * Begin drawing a line strip.
     */
    beginLineStrip(color: Color, expectedVertices: number): void;

    /**
     * Begin drawing a line loop. Like line strip,
     * but when draw() is called, a last point is added
     * which is equal to the first, so that the list
     * of added points forms a closed loop.
     */
    beginLineLoop(color: Color, expectedVertices: number): void;

    /**
     * Add a point to use to form lines
     *
     * @param x
     * @param y
     */
    addPoint(x: number, y: number, color: Color): void;

    /**
     * Begin rectangle outline drawing
     */
    beginRectsOutlined(expectedRects: number): void;

    /**
     * Begin filled rectangle drawing
     */
    beginRectsFilled(expectedRects: number): void;

    /**
     * Add a ractangle definition
     *
     * @param x X coordinate
     * @param y Y coordinate
     * @param width Width in local space
     * @param height Height in local space
     */
    addRect(x: number, y: number, width: number, height: number, color: Color): void;

    /**
     * Begin drawing outlined triangles
     *
     * @param expectedTris expected number of traingles
     */
    beginTrianglesOutlined(expectedTris: number): void;

    /**
     * Begin drawing filled triangles
     *
     * @param expectedTris expected number of traingles
     */
    beginTrianglesFilled(expectedTris: number): void;

    /**
     * Add a solid color triangle
     *
     * @param x0 first vertex x coordinate
     * @param y0 first vertexy coordinate
     * @param x1 second vertex x coordinate
     * @param y1 second vertex y coordinate
     * @param x2 third vertex x coordinate
     * @param y2 third vertex y coordinate
     * @param color color to use for triangle or first vertex if c1 and/or c2 are defined
     * @param c1 color to use for second vertex
     * @param c2 color to use for third vertex
     */
    addTriangle(x0: number, y0: number, x1: number, y1: number, x2: number, y2: number, color: Color, c1: Color, c2: Color): void;

    /**
     * Begin drawing a batch of images
     *
     * @param image image to use as source
     */
    beginImages(image: ImageSource, expectedNumber: number): void;

    /**
     * Add an image to an image batch
     *
     * @param x X target coordinate
     * @param y Y target coordinate
     * @param sx X scaling
     * @param sy Y scaling
     * @param alpha alpha multiplier
     * @param hue [optional] hue modifier, default to 0 (WebGL only)
     * @param sat [optional] saturation modifier, default to 0 (WebGL only)
     * @param val [optional] value modifier, default to 1 (WebGL only)
     */
    addImage(x: number, y: number, sx: number, sy: number, alpha: number, hue: number, sat: number, val: number): void;

    /**
     * Add an image subregion to an image batch
     *
     * @param x X target coordinate
     * @param y Y target coordinate
     * @param sx X scaling
     * @param sy Y scaling
     * @param ix image source rect X coordinate
     * @param iy image source rect Y coordinate
     * @param iw image source rect width
     * @param ih image source rect height
     * @param alpha alpha multiplier
     * @param hue [optional] hue modifier, default to 0 (WebGL only)
     * @param sat [optional] saturation modifier, default to 0 (WebGL only)
     * @param val [optional] value modifier, default to 1 (WebGL only)
     */
    addSubImage(x: number, y: number, sx: number, sy: number,
        ix: number, iy: number, iw: number, ih: number, alpha: number, hue: number, sat: number, val: number): void;

    /**
     * Start drawing sprites
     *
     * @param image image source to use for sprites
     * @param expectedNumber expected number of sprites to draw
     */
    beginSprites(image: ImageSource, expectedNumber: number): void;

    /**
     * Add a sprite to the draw list. Uses the image
     * specified as parameter to beginSprites for rendering
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param px pivot x coordinate
     * @param py pivot y coordinate
     * @param sx X scaling factor
     * @param sy Y scaling factor
     * @param rotation rotation in radians
     * @param alpha alpha multiplier
     * @param hue [optional] hue modifier, default to 0 (WebGL only)
     * @param sat [optional] saturation modifier, default to 0 (WebGL only)
     * @param val [optional] value modifier, default to 1 (WebGL only)
     */
    addSprite(x: number, y: number, px: number, py: number, sx: number, sy: number, rotation: number, alpha: number, hue: number, sat: number, val: number): void;

    /**
     * Start drawing animated sprites (i.e. multiple instances of a single animation)
     *
     * @param anim animation to use as source
     * @param expectedNumber expected number of sprites to draw
     */
    beginSpritesAnimated(anim: Animation, expectedNumber: number): void;

    /**
     * Add an animated sprite to the draw list. Can only be used if beginSpritesAnimated was called first.
     * Calls getFrame() on the provided animation to find UV sampling coordinates.
     *
     * @param frame frame number to use
     * @param x x coordinate
     * @param y y coordinate
     * @param px pivot x coordinate
     * @param py pivot y coordinate
     * @param sx X scaling factor
     * @param sy Y scaling factor
     * @param rotation rotation in radians
     * @param alpha alpha multiplier
     * @param hue [optional] hue modifier, default to 0 (WebGL only)
     * @param sat [optional] saturation modifier, default to 0 (WebGL only)
     * @param val [optional] value modifier, default to 1 (WebGL only)
     */
    addSpriteAnimated(frame: number, x: number, y: number, px: number, py: number, sx: number, sy: number, rotation: number, alpha: number, hue: number, sat: number, val: number): void;

    /**
     * Draw data pushed in current mode and set render mode to NONE
     */
    draw(): void;

}
