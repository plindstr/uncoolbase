import { hasUrlParam } from "../core/urlparam";
import { GLTexture, isNPOTTexturesEnabled } from "./gl/gltexture";
import { isPow2, nearestPow2 } from "../math/coremath";
import { debug } from "../core/debug";
import { createLogger } from "../core/logging";
import { ImageSource } from "./imagesource";
import { BiMap } from "../mem/bimap";
import { array_rotate } from "../mem/array";
import { isCanvasVolatile } from "./surfaceutil";
import { gl_getMemoryUsage } from "./gl/glbase";
import { assertFail } from "../core/assert";
import { getTimeCurrent } from "../core/timer";

const logger = createLogger('GLTextureCache');

const LARGE_MEM_THRESHOLD = 512 * 1024;          // 512k mem usage
const IDLE_DESTROY_THRESHOLD_LARGE = 10;         // go 10 cycles (about 10 seconds) before freeing up unused textures >= 256k
const IDLE_DESTROY_THRESHOLD_SMALL = 5;          // go about 5 seconds before freeing up textures smaller than 256k
const IDLE_REUSE_THRESHOLD_SMALL = 3;            // go 3 cycles without being used; after that reuse freely
const IDLE_REUSE_THRESHOLD_LARGE = 6;            // go 6 cycles without being used; after that reuse freely
const MAX_DESTROY_PER_FRAME = 1;

const tempCanvas = document.createElement('canvas');
const cacheDebug = hasUrlParam('tcdebug');
let create_count = 0;
let realloc_count = 0;

let gui: any = null;

const guiProps = {
    GLMem_kB: 0,
    CacheMem_kB: 0,
    CacheMem_MB: 0,
};

class GLTextureUsage {
    _tex: GLTexture;
    _idle: number = 0;
    _size: number = 0;

    constructor(t: GLTexture = null) {
        this._tex = t;
    }
}

const fillGLTexture = (t: GLTexture, src: ImageSource, e: (HTMLImageElement | HTMLCanvasElement)): GLTexture => {
    let npot = isNPOTTexturesEnabled();

    if(npot || (isPow2(src.getWidth()) && isPow2(src.getHeight()))) {
        t.setData(e, 0);
    } else {
        let w0 = src.getWidth();
        let h0 = src.getHeight();
        let w = nearestPow2(w0);
        let h = nearestPow2(h0);

        // Print debug memory stats
        debug(() => {
            if(!cacheDebug) return;

            let orig_size = w0 * h0 * 4;
            let new_size = w * h * 4;
            let overhead = 0;
            let overhead_pct = 0;
            if(orig_size > new_size) {
                overhead = ((orig_size - new_size) / 1024.0) | 0;
                overhead_pct = (((orig_size / new_size) - 1.0) * 100) | 0;
                logger.debugLog(`Converting surface of size ${w0},${h0} to pow2 texture ${w},${h} (shrunk ${overhead} kb, ${overhead_pct}%)`);
            } else {
                overhead = ((new_size - orig_size) / 1024.0) | 0;
                overhead_pct = (((new_size / orig_size) - 1.0) * 100) | 0;
                logger.debugLog(`Converting surface of size ${w0},${h0} to pow2 texture ${w},${h} (overhead ${overhead} kb, ${overhead_pct}%)`);
            }
        });
        tempCanvas.width = w | 0;
        tempCanvas.height = h | 0;
        let ctx = tempCanvas.getContext('2d');
        ctx.drawImage(e, 0, 0, w, h);
        t.setData(tempCanvas, 0);
    }
    return t;
}

const updateIfNeeded = (tex: GLTexture, e: HTMLCanvasElement): boolean => {
    if(isCanvasVolatile(e)) {
        tex.setData(e);
        return true;
    }
    return false;
}

const toString = (u: GLTextureUsage) => {
    let large = u._size >= LARGE_MEM_THRESHOLD;
    return `${(u._size / 1024) | 0} kb ${large ? '[LARGE]' : '[SMALL]'} ${u._tex.getWidth()}x${u._tex.getHeight()}`;
}

const report = (u: GLTextureUsage, count: number, op: string) => {
    if(cacheDebug) { logger.debugLog(`[${count}] ${op} ${toString(u)}`); }
};

let useTexCache = true;

/**
 * Enable or disable the use of dynamic texture caching globally.
 * When texture caching is enabled, any textures that go unused for a long time
 * in a GL context will be marked as eligible for reuse, and later automatically
 * destroyed to conserve GPU memory. This can be disabled on an individual cache
 * level.
 *
 * @param enabled a boolean value
 */
export const setTextureCacheEnabled = (enabled: boolean) => useTexCache = !!enabled;

/**
 * Check if the use of dynamic texture cache is enabled globally.
 */
export const isTextureCacheEnabled = () => !!useTexCache;

const textureCaches = new Set<TextureCache>();
let lastClean = 0;

export const updateTextureCaches = () => {
    // Don't clean more than once per second
    let t = getTimeCurrent();
    if(t - lastClean > 1000) {
        textureCaches.forEach(c => c.update());
        textureCaches.clear();
        lastClean = t;
    }
}

/**
 * Self-pruning GLTextureCache. One is automatically created by WebGLRenderer2D.
 * Attempts to reuse and minimize unnecessary use of GL texture resources.
 */
export class TextureCache {

    private _context: WebGLRenderingContext;
    private _imgMap:  BiMap<HTMLImageElement, GLTexture>;
    private _cvsMap:  BiMap<HTMLCanvasElement, GLTexture>;
    private _usageMap: Map<GLTexture, GLTextureUsage>;
    private _usage: GLTextureUsage[];
    private _memusage: number;
    private _persistent: boolean;

    constructor(context: WebGLRenderingContext) {
        this._context = context;
        this._imgMap  = new BiMap<HTMLImageElement, GLTexture>();
        this._cvsMap  = new BiMap<HTMLCanvasElement, GLTexture>();
        this._usageMap = new Map<GLTexture, GLTextureUsage>();
        this._usage = [];
        this._memusage = 0;
        this._persistent = false;

        debug(() => {
            if(hasUrlParam('glstat') || cacheDebug) {
                this.addStatGUI();
            }
        });
    }

    /**
     * Turn persistent mode on or off. This should
     * not be left on, as it removes the dynamic cache
     * behavior, meaning that every single texture that
     * is requested through GLTextureCache will stay in memory.
     *
     * @param enable true to enable, false to disable
     */
    public setPersistent(enable: boolean): void {
        this._persistent = !!enable;
    }

    private _reuse(): GLTexture {
        // Persistent mode disallows reuse
        if(this._persistent) return null;

        if(isTextureCacheEnabled() && this._usage.length > 1) { // We want at least 2 textures in before we start reusing
            let u = this._usage[0];
            let large = u._size >= LARGE_MEM_THRESHOLD;
            if((large && u._idle > IDLE_REUSE_THRESHOLD_LARGE) || (u._idle > IDLE_REUSE_THRESHOLD_SMALL)) {
                let t = u._tex;
                this._imgMap.deleteByValue(t);
                this._cvsMap.deleteByValue(t);
                array_rotate(this._usage);
                return t;
            }
        }
        return null;
    }

    private _getFreshGLTexture(): GLTexture {
        let gl = this._context;
        let t: GLTexture = null;

        if(t = this._reuse()) {
            return t;
        }

        t = new GLTexture(gl);
        let u = new GLTextureUsage(t);
        this._usage.push(u);
        this._usageMap.set(t, u);
        return t;
    }

    public addStatGUI(): void {
        // Try to add a Dat.GUI resource meter
        try {
            // Pull dat GUI up - it should have been auto-injected
            if((<any>window).dat == undefined) return;

            if(!gui) {
                gui = new (<any>window).dat.GUI();
            }

            if(gui) {
                gui.add(guiProps, 'GLMem_kB').listen();
                gui.add(guiProps, 'CacheMem_kB').listen();
                gui.add(guiProps, 'CacheMem_MB').listen();
            }
        } catch(ignore) {}
    }

    public get(src: ImageSource): GLTexture {
        textureCaches.add(this);

        let e = src.getElement();
        let t: GLTexture;
        let ucount = this._usage.length;
        let setsize: boolean = false;
        let created: boolean = false;
        if(e instanceof HTMLImageElement) {
            t = this._imgMap.get(e);
            if(!t) {
                t = this._getFreshGLTexture();
                fillGLTexture(t, src, e);
                this._imgMap.set(e, t);
                setsize = true;
                created = this._usage.length !== ucount;
            }
        } else if(e instanceof HTMLCanvasElement) {
            t = this._cvsMap.get(e);
            if(!t) {
                t = this._getFreshGLTexture();
                fillGLTexture(t, src, e);
                this._cvsMap.set(e, t);
                setsize = true;
                created = this._usage.length !== ucount;
            }
            if(updateIfNeeded(t, e)) {
                setsize = true;
            }
        } else {
            assertFail("Invalid element type for texture");
        }

        let u = this._usageMap.get(t);
        u._idle = src.isResident() ? -1 : 0;
        if(setsize) {
            u._size = (t.getWidth() * t.getHeight() * 4) | 0;
            if(created) {
                report(u, ++create_count, "Create");
            } else {
                report(u, ++realloc_count, "Reallocate");
            }
        }
        return t;
    }

    public update(): void {
        if(this._usage.length < 1) return;

        for(let u of this._usage) {
            if(u._idle >= 0) u._idle++;
        }

        this._usage = this._usage.sort((a, b) => a._idle === b._idle ? b._size - a._size : b._idle - a._idle);

        if(useTexCache) {
            let n = 0;
            let i = 0;
            while(this._usage[i]._idle > IDLE_DESTROY_THRESHOLD_LARGE) {

                // Destroy textures that have lingered for too long
                let u = this._usage[i];
                let large = u._size >= LARGE_MEM_THRESHOLD;

                // If this is a large texture we're over the large destroy threshold
                // otherwise check if we're
                // 06/2019 - only destroy one texture per frame
                if(!this._persistent && n < MAX_DESTROY_PER_FRAME &&
                    (large || (u._idle >= IDLE_DESTROY_THRESHOLD_SMALL))) {
                    report(u, ++n, "Destroy");

                    u._tex.destroyGL();
                    this._imgMap.deleteByValue(u._tex);
                    this._cvsMap.deleteByValue(u._tex);
                    this._usageMap.delete(u._tex);
                    this._usage.splice(i, 1);

                } else {
                    // If we're small but not old enough, we'll need to move on to the next one
                    ++i;
                }
            }
        }

        // Calculate memory usage
        this._memusage = 0;
        for(let u of this._usage) {
            this._memusage += u._size;
        }
        guiProps.GLMem_kB = (gl_getMemoryUsage() / 1024) | 0;
        guiProps.CacheMem_kB = ((this._memusage / 1024) | 0);
        guiProps.CacheMem_MB = guiProps.CacheMem_kB / 1024;

        create_count = 0;
        realloc_count = 0;
    }

    public clear(): void {
        let count = 0;
        let size = 0;
        
        for(let t of this._imgMap.values()) {
            size += t.getWidth() * t.getHeight() * 4;
            t.destroyGL();
            ++count;
        }
        for(let t of this._cvsMap.values()) {
            size += t.getWidth() * t.getHeight() * 4;
            t.destroyGL();
            ++count;
        }

        logger.debugLog(`Clear [${count}] ${(size / 1024) | 0} kB`);

        this._imgMap.clear();
        this._cvsMap.clear();

        this._usage = [];
    }

    public getMemUsage(): number {
        return this._memusage;
    }

}
