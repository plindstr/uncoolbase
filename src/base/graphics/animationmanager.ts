import { SafeList } from "../mem/safelist";
import { Animator } from "./animation";

const animators = new SafeList<Animator>();

export const registerAnimator = (a: Animator) => animators.add(a);

export const unregisterAnimator = (a: Animator) => animators.remove(a);

export const clearAnimators = () => animators.clear();

export const updateAnimators = (delta: number) => {
    animators.forEach((a) => a.update(delta));
}
