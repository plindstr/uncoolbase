import { Rect } from "../math/rect";

export interface ImageSource {

    /**
     * Get the element to use for drawing
     */
    getElement(): HTMLImageElement | HTMLCanvasElement;

    /**
     * Get the width of the element used for drawing
     */
    getElementWidth(): number;

    /**
     * Get the height of the element used for drawing
     */
    getElementHeight(): number;

    /**
     * Get the actual area that should be used for drawing
     */
    getSampleRect(): Rect;

    /**
     * Get the X offset relative to the sample rect used for drawing
     */
    getX(): number;

    /**
     * Get the Y offset relative to the sample rect used for drawing
     */
    getY(): number;

    /**
     * Get the original width of the image to use (sample rect can be
     * smaller than actual width if it is part of an image atlas with
     * trimmed subimages)
     */
    getWidth(): number;

    /**
     * Get the original height of the image to use (sample rect can be
     * smaller than actual width if it is part of an image atlas with
     * trimmed subimages)
     */
    getHeight(): number;

    /**
     * Toggle the memory resident status of this image source. If
     * the source is set to resident, it will not be expunged from
     * a texture cache automatically (but will on clear()).
     * NOTE: SubImage objects will _only_ enable the resident flag
     * of their source image, _not_ disable it. That has to be done
     * through the actual Image serving as the Atlas.
     *
     * @param enable true to enable, false to disable.
     */
    setResident(enable: boolean): void;

    /**
     * Check if the ImageSource is memory-resident or not.
     * Memory resident ImageSources do not get expunged from
     * texture caches automatically, instead requiring explicit
     * removal.
     */
    isResident(): boolean;

}
