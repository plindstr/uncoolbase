import { SubImage } from "./subimage";
import { Image } from "./image";
import { assert } from "../core/assert";

/**
 * Texture atlas, used for storage and retrieval of images
 */
export class Atlas {

    private _source: Image = null;
    private _images: { [name: string]: SubImage } = {};
    private _count: number = 0;

    constructor(source: Image) {
        this._source = source;
    }

    public addImage(id: string, x: number, y: number, width: number, height: number, real_width: number, real_height: number, pivot_x: number, pivot_y: number): void {
        let si = new SubImage(this._source, x, y, width, height, real_width, real_height, pivot_x, pivot_y);
        assert(!this._images[id], "Attempted to overwrite existing subimage!");
        this._images[id] = si;
        ++this._count;
    }

    public getImageCount(): number {
        return this._count;
    }

    public getImage(id: string): SubImage | null {
        return this._images[id] || null;
    }

}

