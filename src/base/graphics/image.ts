import { Rect } from "../math/rect";
import { ImageSource } from "./imagesource";

/**
 * Wrapper around HTMLImageElement, used as draw source
 */
export class Image implements ImageSource {

    private _img_element: HTMLImageElement;
    private _img_rect: Rect;
    private _img_resident: boolean = false;

    constructor(elem: HTMLImageElement) {
        this._img_element = elem;
        this._img_rect = new Rect(0,0, elem.width | 0, elem.height | 0);
    }

    public getElement(): HTMLImageElement {
        return this._img_element;
    }

    public getElementWidth(): number {
        return this._img_element.width | 0;
    }

    public getElementHeight(): number {
        return this._img_element.height | 0;
    }

    public getX(): number {
        return 0;
    }

    public getY(): number {
        return 0;
    }

    public getWidth(): number {
        return this._img_rect.size.x;
    }

    public getHeight(): number {
        return this._img_rect.size.y;
    }

    public getSampleRect(): Rect {
        return this._img_rect;
    }

    public setResident(enable: boolean): void {
        this._img_resident = !!enable;
    }

    public isResident(): boolean {
        return this._img_resident;
    }

}
