import { ImageSource } from "./imagesource";
import { registerAnimator, unregisterAnimator } from "./animationmanager";
import { Rect } from "../math/rect";
import { wrap } from "../math/wrap";
import { clamp } from "../math/coremath";
import { CallbackList } from "../mem/callbacklist";

/**
 * Describes a single frame of animation
 */
export class AnimationFrame {
    x: number;
    y: number;
    w: number;
    h: number;
}

/**
 * 2D texture animation
 */
export class Animation {

    private _anim_source: ImageSource;
    private _anim_frames: AnimationFrame[];
    private _anim_frameWidth: number;
    private _anim_frameHeight: number;
    private _anim_frameCount: number;

    constructor(source: ImageSource, frameWidth: number, frameHeight: number) {
        this._anim_source = source;
        this._anim_frames = [];
        this._anim_frameWidth = frameWidth;
        this._anim_frameHeight = frameHeight;
        this._anim_frameCount = 0;
    }

    public getSource(): ImageSource {
        return this._anim_source;
    }

    public getFrameWidth(): number {
        return this._anim_frameWidth;
    }

    public getFrameHeight(): number {
        return this._anim_frameHeight;
    }

    public getFrameCount(): number {
        return this._anim_frameCount;
    }

    public getFrame(idx: number): AnimationFrame {
        return this._anim_frames[idx % this._anim_frameCount];
    }

    public addFrame(
        x: number,
        y: number,
        w: number = this._anim_frameWidth,
        h: number = this._anim_frameHeight): Animation {
            let f = new AnimationFrame();
            f.x = x + this._anim_source.getX();
            f.y = y + this._anim_source.getY();
            f.w = w;
            f.h = h;
            this._anim_frames.push(f);
            this._anim_frameCount++;
        return this;
    }

    public addFrameIndex(
        idx: number,
        frameW = this._anim_frameWidth,
        frameH = this._anim_frameHeight,
        startX: number = 0,
        startY: number = 0): Animation {

        let imageW = this._anim_source.getWidth();

        let x = (startX + (idx * frameW)) % imageW;
        let y = startY + ((startX + (idx * frameW)) / imageW) | 0;

        this.addFrame(x + this._anim_source.getX(), y + this._anim_source.getY(), frameW, frameH);

        return this;
    }

    public addFrameSequence(
        num: number,
        startX: number = 0,
        startY: number = 0,
        frameW: number = this._anim_frameWidth,
        frameH: number = this._anim_frameHeight): Animation {

        let imageW = this._anim_source.getWidth();
        let x = startX;
        let y = startY;
        for(let i = 0; i < num; ++i) {
            this.addFrame(x + this._anim_source.getX(), y + this._anim_source.getY(), frameW, frameH);
            x += frameW;
            if(x >= imageW) {
                x -= imageW;
                y += frameH;
            }
        }
        return this;
    }

}

/**
 * Animator can be used as an image source for
 * sprites, etc.
 */
export class Animator implements ImageSource {

    private _anim_animation: Animation;
    private _anim_currentFrame: number;
    private _anim_srect: Rect;
    private _anim_looping: boolean;
    private _anim_speed: number;
    private _anim_elapsed: number;
    private _anim_reverse: boolean;
    private _anim_userdata: any;
    private _anim_resident: boolean = false;
    private _anim_oncomplete = new CallbackList<() => void>();

    constructor(anim: Animation) {
        this._anim_animation = anim;
        this._anim_srect = new Rect();
        this._anim_currentFrame = 0;
        this._anim_looping = false;
        this._anim_speed = 16;
        this._anim_elapsed = 0;
        this._anim_reverse = false;
        this.setFrame(0);
    }

    public getAnimation(): Animation {
        return this._anim_animation;
    }

    public setUserData(data: any): void {
        this._anim_userdata = data;
    }

    public getUserData(): any {
        return this._anim_userdata;
    }

    public isLastFrame(): boolean {
        return (this._anim_currentFrame | 0) === ((this._anim_animation.getFrameCount() - 1) | 0);
    }

    public sync(anim: Animator): void {
        this._anim_elapsed = anim._anim_elapsed;
    }

    public getElement(): HTMLImageElement {
        return <HTMLImageElement>(this._anim_animation.getSource().getElement());
    }

    public getElementWidth(): number {
        return this._anim_animation.getSource().getElementWidth();
    }

    public getElementHeight(): number {
        return this._anim_animation.getSource().getElementHeight();
    }

    public getX(): number {
        return this._anim_srect.position.x;
    }

    public getY(): number {
        return this._anim_srect.position.y;
    }

    public getWidth(): number {
        return this._anim_srect.size.x;
    }

    public getHeight(): number {
        return this._anim_srect.size.y;
    }

    public getSampleRect(): Rect {
        return this._anim_srect;
    }

    public setFrame(idx: number): Animator {
        if(this._anim_looping) {
            this._anim_currentFrame = wrap(idx,0,this._anim_animation.getFrameCount()) | 0;
        } else {
            this._anim_currentFrame = clamp(idx,0,this._anim_animation.getFrameCount() - 1) | 0;
        }

        let f = this._anim_animation.getFrame(this._anim_currentFrame);
        this._anim_srect.position.setXY(f.x,f.y);
        this._anim_srect.size.setXY(f.w,f.h);

        return this;
    }

    public getFrame(): number {
        return this._anim_currentFrame;
    }

    public getFrameCount(): number {
        return this._anim_animation.getFrameCount();
    }

    public setLooping(b: boolean): Animator {
        this._anim_looping = b;
        return this;
    }

    public isLooping(): boolean {
        return this._anim_looping;
    }

    public setReverse(b: boolean): Animator {
        this._anim_reverse = b;
        return this;
    }

    public isReverse(): boolean {
        return this._anim_reverse;
    }

    public setAnimationSpeed(fps: number): Animator {
        this._anim_speed = fps;
        return this;
    }

    public getAnimationSpeed(): number {
        return this._anim_speed;
    }

    public start(): Animator {
        registerAnimator(this);
        return this;
    }

    public stop(): Animator {
        unregisterAnimator(this);
        return this;
    }

    public reset(): Animator {
        this.setFrame(0);
        return this;
    }

    public onComplete(callback: () => void): Animator {
        this._anim_oncomplete.add(callback);
        return this;
    }

    public clearOnComplete(): Animator {
        this._anim_oncomplete.clear();
        return this;
    }

    public update(delta: number): void {
        if(this._anim_speed == 0) return;

        let ftime = 1.0 / this._anim_speed;
        let e = this._anim_elapsed + delta;
        while(e >= ftime) {
            if(this._anim_looping && this._anim_reverse) {
                this.setFrame(this._anim_currentFrame - 1);
            } else {
                this.setFrame(this._anim_currentFrame + 1);
            }
            e -= ftime;
        }
        this._anim_elapsed = e;

        if(!this._anim_looping) {
            if(this._anim_currentFrame === this._anim_animation.getFrameCount() - 1) {
                this._anim_oncomplete.run();
                unregisterAnimator(this);
            }

        }

    }

    public setResident(enable: boolean): void {
        this._anim_resident = !!enable;
    }

    public isResident(): boolean {
        return this._anim_resident;
    }

}
