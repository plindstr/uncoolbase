import * as shaders from "./shaders/shaders_2d"

import { Renderer2D } from "./renderer2d";
import { Surface } from "./surface";
import { BlendMode } from "./blendmode";
import { Matrix, clamp } from "../math";
import { Color } from "./color";
import { ImageSource } from "./imagesource";
import { Animation } from "./animation";
import { TextureCache } from "./texturecache";
import { GLShader, combineShaders, GLShaderPrecision } from "./gl/glshader";
import { GLUniform } from "./gl/gluniform";
import { GLVertexBuffer, GLVBOField } from "./gl/glvertexbuffer";
import { GLIndexBuffer } from "./gl/glindexbuffer";
import { GLTexture } from "./gl/gltexture";
import { gl_makeActive, gl_init2D, gl_useBlendMode, gl_setEnabledAttribs, GL, gl_bindVertexArray } from "./gl/glbase";
import { GLVertexArray } from "./gl/glvertexarray";

enum DrawMode {
    NONE,

    LINES,
    LINE_STRIP,
    LINE_LOOP,

    RECT_OUTLINE,
    RECT_FILL,

    TRI_OUTLINE,
    TRI_FILL,

    IMAGES,

    SPRITES,
    SPRITES_ANIMATED
}

const hue_scale = 1.0 / (Math.PI * 2.0);

export class WebGLRenderer2D implements Renderer2D {

    private _target: Surface;
    private _context: WebGLRenderingContext;
    private _textureCache: TextureCache;
    private _currentImage: HTMLImageElement | HTMLCanvasElement;

    private _currentShader: GLShader;

    private _colorVAO: GLVertexArray;
    private _colorShader: GLShader;
    private _color_u_view: GLUniform;
    private _color_u_matrix: GLUniform;
    private _color_u_alpha: GLUniform;

    private _imageVAO: GLVertexArray;
    private _imageShader: GLShader;
    private _image_u_view: GLUniform;
    private _image_u_matrix: GLUniform;
    private _image_u_alpha: GLUniform;
    private _image_u_texture: GLUniform;

    private _spriteVAO: GLVertexArray;
    private _spriteShader: GLShader;
    private _sprite_u_view: GLUniform;
    private _sprite_u_matrix: GLUniform;
    private _sprite_u_alpha: GLUniform;
    private _sprite_u_texture: GLUniform;

    private _vertexData: GLVertexBuffer;   // a_vertex + a_color
    private _a_vertex: GLVBOField;
    private _a_color: GLVBOField;

    private _imageData: GLVertexBuffer;    // a_texcoord
    private _a_texcoord: GLVBOField;

    private _spriteData: GLVertexBuffer;   // a_rot + a_position + a_size + a_scale + a_pivot
    private _a_rot: GLVBOField;
    private _a_position: GLVBOField;
    private _a_scale: GLVBOField;
    private _a_pivot: GLVBOField;

    private _indexData: GLIndexBuffer;     // vertex index list

    private _drawmode: DrawMode;
    private _blendmode: BlendMode;

    private _alpha: number;
    private _view: Matrix;
    private _matrix: Matrix;
    private _linewidth: number;

    private _lineColor: Color;

    private _numPoints: number;
    private _numRects: number;
    private _numTris: number;
    private _numImages: number;
    private _numSprites: number;

    private _animation: Animation;
    private _image_u0: number;
    private _image_v0: number;
    private _image_u1: number;
    private _image_v1: number;
    private _image_uscale: number;
    private _image_vscale: number;

    private _image_w: number;
    private _image_h: number;
    private _image_iw: number;
    private _image_ih: number;

    private _image_xoffset: number;
    private _image_yoffset: number;

    private _texture: GLTexture;
    private _boundTexture: GLTexture;

    constructor(owner: Surface) {
        this._target = owner;
        this._context = owner.getContextGL();

        this._alpha = 1;
        this._view = new Matrix();
        this._matrix = new Matrix();

        this._linewidth = 1;
        this._lineColor = new Color();

        this._textureCache = owner.getTextureCache();
        this._currentImage = null;

        this._animation = null;
        this._image_iw = 0;
        this._image_ih = 0;
        this._image_u0 = 0;
        this._image_v0 = 0;
        this._image_u1 = 1;
        this._image_v1 = 1;
        this._image_uscale = 1;
        this._image_vscale = 1;
        this._image_w = 0;
        this._image_h = 0;

        this._image_xoffset = 0;
        this._image_yoffset = 0;

        this._texture = null;
        this._boundTexture = null;

        this._drawmode = DrawMode.NONE;
        this._numPoints = 0;
        this._numRects = 0;
        this._numTris = 0;
        this._numImages = 0;
        this._numRects = 0;


        //
        // Create shaders
        //

        let createShader = (vs_modules: string[], fs_modules: string[]) => {
            vs_modules.unshift(shaders.vertex2d.common);
            let vs = combineShaders(vs_modules);
            let fs = combineShaders(fs_modules);
            let s = new GLShader(this._context, GLShaderPrecision.HIGH);
            s.compile(vs, fs);
            return s;
        }

        this._currentShader = null;
        this._colorShader = createShader([ shaders.vertex2d.color ], [ shaders.fragment2d.color ]);
        this._imageShader = createShader([ shaders.vertex2d.color, shaders.vertex2d.image ], [ shaders.fragment2d.image ]);
        this._spriteShader = createShader([ shaders.vertex2d.color, shaders.vertex2d.image, shaders.vertex2d.sprite ], [ shaders.fragment2d.image ]);

        this._color_u_view = this._colorShader.getUniform('u_view');
        this._color_u_matrix = this._colorShader.getUniform('u_matrix');
        this._color_u_alpha = this._colorShader.getUniform('u_alpha');
        let color_a_vertex_loc = this._colorShader.getAttribLocation('a_vertex');
        let color_a_color_loc = this._colorShader.getAttribLocation('a_color');

        this._image_u_view = this._imageShader.getUniform('u_view');
        this._image_u_matrix = this._imageShader.getUniform('u_matrix');
        this._image_u_alpha = this._imageShader.getUniform('u_alpha');
        this._image_u_texture = this._imageShader.getUniform('u_texture');
        let image_a_vertex_loc = this._imageShader.getAttribLocation('a_vertex');
        let image_a_color_loc = this._imageShader.getAttribLocation('a_color');
        let image_a_texcoord_loc = this._imageShader.getAttribLocation('a_texcoord');

        this._sprite_u_view = this._spriteShader.getUniform('u_view');
        this._sprite_u_matrix = this._spriteShader.getUniform('u_matrix');
        this._sprite_u_alpha = this._spriteShader.getUniform('u_alpha');
        this._sprite_u_texture = this._spriteShader.getUniform('u_texture');
        let sprite_a_vertex_loc = this._spriteShader.getAttribLocation('a_vertex');
        let sprite_a_color_loc = this._spriteShader.getAttribLocation('a_color');
        let sprite_a_texcoord_loc = this._spriteShader.getAttribLocation('a_texcoord');
        let sprite_a_rot_loc = this._spriteShader.getAttribLocation('a_rot');
        let sprite_a_position_loc = this._spriteShader.getAttribLocation('a_position');
        let sprite_a_scale_loc = this._spriteShader.getAttribLocation('a_scale');
        let sprite_a_pivot_loc = this._spriteShader.getAttribLocation('a_pivot');


        //
        // Create vertex data buffers
        //

        this._colorVAO = new GLVertexArray(this._context);
        this._vertexData = new GLVertexBuffer(this._context);
        this._a_vertex = this._vertexData.addField('a_vertex', 2);
        this._a_color = this._vertexData.addField('a_color', 4);

        this._imageVAO = new GLVertexArray(this._context);
        this._imageData = new GLVertexBuffer(this._context);
        this._a_texcoord = this._imageData.addField('a_texcoord', 2);

        this._spriteVAO = new GLVertexArray(this._context);
        this._spriteData = new GLVertexBuffer(this._context);
        this._a_rot = this._spriteData.addField('a_rot', 1);
        this._a_position = this._spriteData.addField('a_position', 2);
        this._a_scale = this._spriteData.addField('a_scale', 2);
        this._a_pivot = this._spriteData.addField('a_pivot', 2);

        this._indexData = new GLIndexBuffer(this._context);

        // Bind color VAO
        gl_makeActive(this._context);

        this._colorVAO.bind();
        gl_setEnabledAttribs(
            color_a_vertex_loc,
            color_a_color_loc);
        this._vertexData.bindField(this._a_vertex, color_a_vertex_loc);
        this._vertexData.bindField(this._a_color, color_a_color_loc);
        this._indexData.bind();

        // Bind image VAO
        this._imageVAO.bind();
        gl_setEnabledAttribs(
            image_a_vertex_loc,
            image_a_color_loc,
            image_a_texcoord_loc
        );
        this._vertexData.bindField(this._a_vertex, image_a_vertex_loc);
        this._vertexData.bindField(this._a_color, image_a_color_loc);
        this._imageData.bindField(this._a_texcoord, image_a_texcoord_loc);
        this._indexData.bind();

        // Bind sprite VAO
        this._spriteVAO.bind();
        gl_setEnabledAttribs(
            sprite_a_vertex_loc,
            sprite_a_color_loc,
            sprite_a_texcoord_loc,
            sprite_a_rot_loc,
            sprite_a_position_loc,
            sprite_a_scale_loc,
            sprite_a_pivot_loc
        );
        this._vertexData.bindField(this._a_vertex, sprite_a_vertex_loc);
        this._vertexData.bindField(this._a_color, sprite_a_color_loc);
        this._imageData.bindField(this._a_texcoord, sprite_a_texcoord_loc);
        this._spriteData.bindField(this._a_rot, sprite_a_rot_loc);
        this._spriteData.bindField(this._a_position, sprite_a_position_loc);
        this._spriteData.bindField(this._a_scale, sprite_a_scale_loc);
        this._spriteData.bindField(this._a_pivot, sprite_a_pivot_loc);
        this._indexData.bind();

        // Don't bind vertex array anymore
        gl_bindVertexArray(null);        
    }

    public isGL(): boolean {
        return true;
    }

    public init(): void {
        gl_makeActive(this._context);
        gl_init2D(this._target.getWidth(), this._target.getHeight());
        this.updateViewMatrix();
        this.restore();
    }

    public end(): void {
        // Unbind the active VAO so that something else
        // that comes right after this doesn't pollute its state
        gl_bindVertexArray(null);
    }

    private updateViewMatrix(): void {
        let w = this._target.getWidth();
        let h = this._target.getHeight();
        this._view.a  =  2.0 / w;
        this._view.b  =  0.0;
        this._view.c  =  0.0;
        this._view.d  = -2.0 / h;
        this._view.tx = -1.0;
        this._view.ty =  1.0;
    }

    public getViewMatrix(): Matrix {
        return this._view;
    }

    public precache(image: ImageSource): void {
        this._textureCache.get(image);
    }

    public clearCache(): void {
        this._textureCache.clear();
    }

    public setCachePersistent(enable: boolean): void {
        this._textureCache.setPersistent(!!enable);
    }

    public getSurface(): Surface {
        return this._target;
    }

    public getTextureCache(): TextureCache {
        return this._textureCache;
    }

    public getWidth(): number {
        return this._target.getWidth();
    }

    public getHeight(): number {
        return this._target.getHeight();
    }

    public restore(): void {
        gl_makeActive(this._context);

        // Reset current shader ref so the correct shader
        // gets re-bound on next call to draw()
        this._currentShader = null;

        // Reset the current image so that the texture gets rebound
        this._currentImage = null;

        // Reset the currently bound texture
        this._boundTexture = null;

        // Reset the draw mode
        this._drawmode = DrawMode.NONE;
    }

    public setBlendMode(mode: BlendMode): void {
        this._blendmode = mode;
    }

    public setAlpha(alpha: number): void {
        this._alpha = clamp(alpha, 0, 1);
    }

    public setMatrix(matrix: Matrix): void {
        this._matrix.set(matrix);
    }

    public getMatrix(): Matrix {
        return this._matrix;
    }

    public setLineWidth(width: number): void {
        this._linewidth = Math.max(width, 0.001);
    }

    public getLineWidth(): number {
        return this._linewidth;
    }

    public beginLines(color: Color, numVertices: number): void {
        
        this._lineColor.set(color);
        this._drawmode = DrawMode.LINES;
        this._numPoints = 0;

        this._vertexData.clear().alloc(numVertices);
    }

    public beginLineStrip(color: Color, numVertices: number): void {
        
        this._lineColor.set(color);
        this._drawmode = DrawMode.LINE_STRIP;
        this._numPoints = 0;

        this._vertexData.clear().alloc(numVertices);
    }

    public beginLineLoop(color: Color, numVertices: number): void {
        
        this._lineColor.set(color);
        this._drawmode = DrawMode.LINE_LOOP;
        this._numPoints = 0;

        this._vertexData.clear().alloc(numVertices);
    }

    public addPoint(x: number, y: number, color: Color): void {
        
        let r = color.getRNorm();
        let g = color.getGNorm();
        let b = color.getBNorm();
        let a = color.getANorm();
        let a_vtx = this._a_vertex;
        let a_col = this._a_color;
        let vtx = this._vertexData;
        let pos = this._numPoints;
        vtx.setPosition(pos);
        vtx.write(a_vtx, x, y);
        vtx.setPosition(pos);
        vtx.write(a_col, r,g,b,a);

        this._numPoints++;
    }

    public beginRectsOutlined(numRects: number): void {

        this._drawmode = DrawMode.RECT_OUTLINE;
        this._numRects = 0;

        this._vertexData.clear().alloc(numRects << 2);
        this._indexData.clear().alloc(numRects << 3);
    }

    public beginRectsFilled(numRects: number): void {
        
        this._drawmode = DrawMode.RECT_FILL;
        this._numRects = 0;

        this._vertexData.clear().alloc(numRects << 2);
        this._indexData.clear().alloc(numRects * 6);
    }

    public addRect(x: number, y: number,
        width: number, height: number, color: Color): void {

        
        let r = color.getRNorm();
        let g = color.getGNorm();
        let b = color.getBNorm();
        let a = color.getANorm();

        let a_vtx = this._a_vertex;
        let a_col = this._a_color;
        let vtx = this._vertexData;
        let pos = this._numRects << 2;

        vtx.setPosition(pos);
        vtx.write(a_vtx, x, y);
        vtx.write(a_vtx, x + width, y);
        vtx.write(a_vtx, x + width, y + height);
        vtx.write(a_vtx, x, y + height);

        vtx.setPosition(pos);
        vtx.writeRepeat(a_col, 4, r,g,b,a);

        this._numRects++;
    }

    public beginTrianglesOutlined(expectedTris?: number): void {
        
        this._vertexData.clear().alloc(expectedTris * 3);
        this._indexData.clear().alloc(expectedTris * 6);
        this._drawmode = DrawMode.TRI_OUTLINE;
        this._numTris = 0;
    }

    public beginTrianglesFilled(expectedTris?: number): void {
        
        this._vertexData.clear().alloc(expectedTris * 3);
        this._indexData.clear();
        this._drawmode = DrawMode.TRI_FILL;
        this._numTris = 0;
    }

    public addTriangle(x0: number, y0: number, x1: number, y1: number, x2: number, y2: number, c0: Color, c1: Color, c2: Color): void {
        
        let r0 = c0.getRNorm();
        let g0 = c0.getGNorm();
        let b0 = c0.getBNorm();
        let a0 = c0.getANorm();
        let r1 = c1.getRNorm();
        let g1 = c1.getGNorm();
        let b1 = c1.getBNorm();
        let a1 = c1.getANorm();
        let r2 = c2.getRNorm();
        let g2 = c2.getGNorm();
        let b2 = c2.getBNorm();
        let a2 = c2.getANorm();

        let a_vtx = this._a_vertex;
        let a_col = this._a_color;
        let vtx = this._vertexData;
        let pos = this._numTris * 3;

        vtx.setPosition(pos);
        vtx.write(a_vtx, x0, y0);
        vtx.write(a_vtx, x1, y1);
        vtx.write(a_vtx, x2, y2);

        vtx.setPosition(pos);
        vtx.write(a_col, r0, g0, b0, a0);
        vtx.write(a_col, r1, g1, b1, a1);
        vtx.write(a_col, r2, g2, b2, a2);

        this._numTris++;
    }

    public beginImages(image: ImageSource, numImages: number): void {
        
        let w = image.getElementWidth();
        let h = image.getElementHeight();
        let src = image.getSampleRect();

        let iw = 1.0 / w;
        let ih = 1.0 / h;

        this._image_iw = iw;
        this._image_ih = ih;
        this._image_w = src.size.x;
        this._image_h = src.size.y;
        this._image_uscale = iw;
        this._image_vscale = ih;
        this._image_u0 = src.position.x * iw;
        this._image_v0 = src.position.y * ih;
        this._image_u1 = (src.position.x + src.size.x) * iw;
        this._image_v1 = (src.position.y + src.size.y) * ih;

        this._image_xoffset = image.getX();
        this._image_yoffset = image.getY();

        if(this._currentImage !== image.getElement()) {
            this._texture = this._textureCache.get(image);
            this._currentImage = image.getElement();
        }

        this._drawmode = DrawMode.IMAGES;
        this._numImages = 0;

        let nvertices = numImages << 2;
        this._vertexData.clear().alloc(nvertices);
        this._imageData.clear().alloc(nvertices);
        this._indexData.clear().alloc(numImages * 6);
    }

    public addImage(x: number, y: number, sx: number, sy: number, alpha: number, hue: number, sat: number, val: number): void {
        

        x += this._image_xoffset;
        y += this._image_yoffset;

        let a_vtx = this._a_vertex;
        let a_col = this._a_color;
        let a_tex = this._a_texcoord;
        let vtx = this._vertexData;
        let tex = this._imageData;
        let idx = this._indexData;
        let pos = this._numImages << 2;

        let u0 = this._image_u0;
        let v0 = this._image_v0;
        let u1 = this._image_u1;
        let v1 = this._image_v1;
        let w = this._image_w;
        let h = this._image_h;

        vtx.setPosition(pos);
        vtx.write(a_vtx, x, y);
        vtx.write(a_vtx, x + sx * w, y);
        vtx.write(a_vtx, x + sx * w, y + sy * h);
        vtx.write(a_vtx, x, y + sy * h);

        vtx.setPosition(pos);
        vtx.writeRepeat(a_col, 4, hue * hue_scale, sat, val, alpha);

        tex.setPosition(pos);
        tex.write(a_tex, u0, v0);
        tex.write(a_tex, u1, v0);
        tex.write(a_tex, u1, v1);
        tex.write(a_tex, u0, v1);

        idx.write(pos + 0);
        idx.write(pos + 1);
        idx.write(pos + 2);
        idx.write(pos + 0);
        idx.write(pos + 2);
        idx.write(pos + 3);

        this._numImages++;
    }

    public addSubImage(x: number, y: number, sx: number, sy: number,
        ix: number, iy: number, iw: number, ih: number, alpha: number,
        hue: number, sat: number, val: number): void {

        // TODO: verify fixed for atlas
        x += this._image_xoffset;
        y += this._image_yoffset;

        let a_vtx = this._a_vertex;
        let a_col = this._a_color;
        let a_tex = this._a_texcoord;
        let vtx = this._vertexData;
        let tex = this._imageData;
        let idx = this._indexData;
        let pos = this._numImages << 2;

        let uscale = this._image_uscale;
        let vscale = this._image_vscale;

        let u0 = ix * uscale;
        let v0 = iy * vscale;
        let u1 = u0 + iw * uscale;
        let v1 = v0 + ih * vscale;

        vtx.setPosition(pos);
        vtx.write(a_vtx, x, y);
        vtx.write(a_vtx, x + sx * iw, y);
        vtx.write(a_vtx, x + sx * iw, y + sy * ih);
        vtx.write(a_vtx, x, y + sy * ih);

        vtx.setPosition(pos);
        vtx.writeRepeat(a_col, 4, hue * hue_scale, sat, val, alpha);

        tex.setPosition(pos);
        tex.write(a_tex, u0, v0);
        tex.write(a_tex, u1, v0);
        tex.write(a_tex, u1, v1);
        tex.write(a_tex, u0, v1);

        idx.write(pos + 0);
        idx.write(pos + 1);
        idx.write(pos + 2);
        idx.write(pos + 0);
        idx.write(pos + 2);
        idx.write(pos + 3);

        this._numImages++;
    }

    public beginSprites(image: ImageSource, numSprites: number): void {
        
        // TODO: fix for atlas
        let w = image.getElementWidth();
        let h = image.getElementHeight();
        let src = image.getSampleRect();

        let iw = 1.0 / w;
        let ih = 1.0 / h;

        this._animation = null;
        this._image_iw = iw;
        this._image_ih = ih;
        this._image_w = src.size.x;
        this._image_h = src.size.y;
        this._image_uscale = iw;
        this._image_vscale = ih;
        this._image_u0 = src.position.x * iw;
        this._image_v0 = src.position.y * ih;
        this._image_u1 = (src.position.x + src.size.x) * iw;
        this._image_v1 = (src.position.y + src.size.y) * ih;

        this._image_xoffset = image.getX();
        this._image_yoffset = image.getY();

        if(this._currentImage !== image.getElement()) {
            this._texture = this._textureCache.get(image);
            this._currentImage = image.getElement();
        }

        this._drawmode = DrawMode.SPRITES;
        this._numSprites = 0;

        let nvertices = numSprites << 2;
        this._vertexData.clear().alloc(nvertices);
        this._imageData.clear().alloc(nvertices);
        this._spriteData.clear().alloc(nvertices);
        this._indexData.clear().alloc(numSprites * 6);
    }

    public addSprite(x: number, y: number, px: number, py: number, sx: number, sy: number,
        roto: number, alpha: number, hue: number, sat: number, val: number): void {

        // TODO: verify fixed for atlas
        x += this._image_xoffset;
        y += this._image_yoffset;

        let pos = this._numSprites << 2;

        let u0 = this._image_u0;
        let v0 = this._image_v0;
        let u1 = this._image_u1;
        let v1 = this._image_v1;

        let w = this._image_w;
        let h = this._image_h;

        let vtx = this._vertexData;
        let tex = this._imageData;
        let spr = this._spriteData;
        let idx = this._indexData;

        let a_vtx = this._a_vertex;
        let a_col = this._a_color;
        let a_tex = this._a_texcoord;
        let a_rot = this._a_rot;
        let a_pos = this._a_position;
        let a_sca = this._a_scale;
        let a_piv = this._a_pivot;

        vtx.setPosition(pos);
        vtx.write(a_vtx, 0, 0);
        vtx.write(a_vtx, w, 0);
        vtx.write(a_vtx, w, h);
        vtx.write(a_vtx, 0, h);

        vtx.setPosition(pos);
        vtx.writeRepeat(a_col, 4, hue * hue_scale, sat, val, alpha);

        tex.setPosition(pos);
        tex.write(a_tex, u0, v0);
        tex.write(a_tex, u1, v0);
        tex.write(a_tex, u1, v1);
        tex.write(a_tex, u0, v1);

        spr.setPosition(pos);
        spr.writeRepeat(a_rot, 4, roto);

        spr.setPosition(pos);
        spr.writeRepeat(a_pos, 4, x, y);

        spr.setPosition(pos);
        spr.writeRepeat(a_sca, 4, sx, sy);

        spr.setPosition(pos);
        spr.writeRepeat(a_piv, 4, px, py);

        idx.write(pos + 0);
        idx.write(pos + 1);
        idx.write(pos + 2);
        idx.write(pos + 0);
        idx.write(pos + 2);
        idx.write(pos + 3);

        this._numSprites++;
    }

    public beginSpritesAnimated(anim: Animation, numSprites: number): void {
        
        let image = anim.getSource();

        // TODO: fix for atlas

        let w = image.getElementWidth();
        let h = image.getElementHeight();
        let src = image.getSampleRect();

        let iw = 1.0 / w;
        let ih = 1.0 / h;

        this._animation = anim;
        this._image_w = src.size.x;
        this._image_h = src.size.y;
        this._image_iw = iw;
        this._image_ih = ih;
        this._image_uscale = iw;
        this._image_vscale = ih;
        this._image_u0 = src.position.x * iw;
        this._image_v0 = src.position.y * ih;
        this._image_u1 = (src.position.x + src.size.x) * iw;
        this._image_v1 = (src.position.y + src.size.y) * ih;

        this._image_xoffset = image.getX();
        this._image_yoffset = image.getY();

        if(this._currentImage !== image.getElement()) {
            this._texture = this._textureCache.get(image);
            this._currentImage = image.getElement();
        }
        this._drawmode = DrawMode.SPRITES_ANIMATED;
        this._numSprites = 0;

        let nvertices = numSprites << 2;
        this._vertexData.clear().alloc(nvertices);
        this._imageData.clear().alloc(nvertices);
        this._spriteData.clear().alloc(nvertices);
        this._indexData.clear().alloc(numSprites * 6);
    }

    public addSpriteAnimated(frame: number, x: number, y: number, px: number, py: number, sx: number, sy: number, roto: number, alpha: number, hue: number, sat: number, val: number): void {

        // TODO: verify fixed for atlas
        x += this._image_xoffset;
        y += this._image_yoffset;

        let pos = this._numSprites << 2;
        let a = this._animation;
        let f = a.getFrame(frame);
        let iw = this._image_iw;
        let ih = this._image_ih;
        let w = f.w;
        let h = f.h;

        let u0 = this._image_u0 + f.x * iw;
        let v0 = this._image_v0 + f.y * ih;
        let u1 = u0 + w * iw;
        let v1 = v0 + h * ih;

        let vtx = this._vertexData;
        let tex = this._imageData;
        let spr = this._spriteData;
        let idx = this._indexData;

        let a_vtx = this._a_vertex;
        let a_col = this._a_color;
        let a_tex = this._a_texcoord;
        let a_rot = this._a_rot;
        let a_pos = this._a_position;
        let a_sca = this._a_scale;
        let a_piv = this._a_pivot;

        vtx.setPosition(pos);
        vtx.write(a_vtx, 0, 0);
        vtx.write(a_vtx, w, 0);
        vtx.write(a_vtx, w, h);
        vtx.write(a_vtx, 0, h);

        vtx.setPosition(pos);
        vtx.writeRepeat(a_col, 4, hue * hue_scale, sat, val, alpha);

        tex.setPosition(pos);
        tex.write(a_tex, u0, v0);
        tex.write(a_tex, u1, v0);
        tex.write(a_tex, u1, v1);
        tex.write(a_tex, u0, v1);

        spr.setPosition(pos);
        spr.writeRepeat(a_rot, 4, roto);

        spr.setPosition(pos);
        spr.writeRepeat(a_pos, 4, x, y);

        spr.setPosition(pos);
        spr.writeRepeat(a_sca, 4, sx, sy);

        spr.setPosition(pos);
        spr.writeRepeat(a_piv, 4, px, py);

        idx.write(pos + 0);
        idx.write(pos + 1);
        idx.write(pos + 2);
        idx.write(pos + 0);
        idx.write(pos + 2);
        idx.write(pos + 3);

        this._numSprites++;
    }

    private drawLines(): void {
        let gl = this._context;

        this._color_u_view.setValue(this._view);
        this._color_u_matrix.setValue(this._matrix);
        this._color_u_alpha.setValue(this._alpha);

        if(this._currentShader !== this._colorShader) {
            this._colorShader.use();
            this._currentShader = this._colorShader;
        } else {
            this._color_u_alpha.update(gl);
            this._color_u_matrix.update(gl);
            this._color_u_alpha.update(gl);
        }

        gl_useBlendMode(this._blendmode);

        this._vertexData.update();

        this._colorVAO.bind();

        gl.lineWidth(this._linewidth);
        switch(this._drawmode) {
            case DrawMode.LINES:
            gl.drawArrays(GL.LINES, 0, this._numPoints);
            break;
            case DrawMode.LINE_STRIP:
            gl.drawArrays(GL.LINE_STRIP, 0, this._numPoints);
            break;
            case DrawMode.LINE_LOOP:
            gl.drawArrays(GL.LINE_LOOP, 0, this._numPoints);
            break;
        }
    }

    private drawRects(): void {
        
        let gl = this._context;

        this._color_u_view.setValue(this._view);
        this._color_u_matrix.setValue(this._matrix);
        this._color_u_alpha.setValue(this._alpha);

        if(this._currentShader !== this._colorShader) {
            this._colorShader.use();
            this._currentShader = this._colorShader;
        } else {
            this._color_u_alpha.update(gl);
            this._color_u_matrix.update(gl);
            this._color_u_alpha.update(gl);
        }

        gl_useBlendMode(this._blendmode);

        this._vertexData.update();

        let buf = this._indexData;
        buf.clear();
        switch(this._drawmode) {
            case DrawMode.RECT_FILL: {

                buf.alloc(this._numRects * 6);

                // Draw as a series of triangles; fill index buffer
                for(let i = 0; i < this._numRects; ++i) {
                    let offset = i << 2;
                    buf.write(offset + 0);
                    buf.write(offset + 1);
                    buf.write(offset + 2);
                    buf.write(offset + 0);
                    buf.write(offset + 2);
                    buf.write(offset + 3);
                }

                // buf.bind();
                buf.update();

                this._colorVAO.bind();

                gl.drawElements(GL.TRIANGLES, buf.getIndexCount(), GL.UNSIGNED_SHORT, 0);
            }
            break;
            case DrawMode.RECT_OUTLINE: {

                buf.alloc(this._numRects * 8);

                // Draw a series of lines
                for(let i = 0; i < this._numRects; ++i) {
                    let offset = i << 2;
                    buf.write(offset + 0);
                    buf.write(offset + 1);

                    buf.write(offset + 1);
                    buf.write(offset + 2);

                    buf.write(offset + 2);
                    buf.write(offset + 3);

                    buf.write(offset + 3);
                    buf.write(offset + 0);
                }

                // buf.bind();
                buf.update();
                gl.lineWidth(this._linewidth);

                this._colorVAO.bind();

                gl.drawElements(GL.LINES, buf.getIndexCount(), GL.UNSIGNED_SHORT, 0);
            }
            break;
        }

    }

    private drawTris(): void {
        let gl = this._context;

        this._color_u_view.setValue(this._view);
        this._color_u_matrix.setValue(this._matrix);
        this._color_u_alpha.setValue(this._alpha);

        if(this._currentShader !== this._colorShader) {
            this._colorShader.use();
            this._currentShader = this._colorShader;
        } else {
            this._color_u_alpha.update(gl);
            this._color_u_matrix.update(gl);
            this._color_u_alpha.update(gl);
        }

        gl_useBlendMode(this._blendmode);

        this._vertexData.update();
        
        switch(this._drawmode) {
            case DrawMode.TRI_FILL: {

                this._colorVAO.bind();

                gl.drawArrays(GL.TRIANGLES, 0, this._numTris * 3);
            }
            break;
            case DrawMode.TRI_OUTLINE: {
                let buf = this._indexData;
                buf.clear();
                buf.alloc(this._numTris * 6);

                for(let i = 0; i < this._numTris; ++i) {
                    let offset = i * 3;
                    buf.write(offset + 0);
                    buf.write(offset + 1);

                    buf.write(offset + 1);
                    buf.write(offset + 2);

                    buf.write(offset + 2);
                    buf.write(offset + 0);
                }

                buf.update();
                gl.lineWidth(this._linewidth);

                this._colorVAO.bind();

                gl.drawElements(GL.LINES, buf.getIndexCount(), GL.UNSIGNED_SHORT, 0);
            }
            break;
        }

    }

    private drawImages(): void {
        let gl = this._context;

        this._image_u_view.setValue(this._view);
        this._image_u_matrix.setValue(this._matrix);
        this._image_u_alpha.setValue(this._alpha);
        this._image_u_texture.setValue(0);

        if(this._currentShader !== this._imageShader) {
            this._currentShader = this._imageShader;
            this._imageShader.use();
        } else {
            this._image_u_view.update(gl);
            this._image_u_matrix.update(gl);
            this._image_u_alpha.update(gl);
            this._image_u_texture.update(gl);
        }

        gl_useBlendMode(this._blendmode);

        this._vertexData.update();
        this._imageData.update();
        this._indexData.update();

        if(this._boundTexture != this._texture) {
            this._boundTexture = this._texture;
            this._texture.bind(0);
        }

        this._imageVAO.bind();

        gl.drawElements(GL.TRIANGLES, this._indexData.getIndexCount(), GL.UNSIGNED_SHORT, 0);

    }

    private drawSprites(): void {
        let gl = this._context;

        this._sprite_u_view.setValue(this._view);
        this._sprite_u_matrix.setValue(this._matrix);
        this._sprite_u_alpha.setValue(this._alpha);
        this._sprite_u_texture.setValue(0);

        if(this._currentShader !== this._spriteShader) {
            this._currentShader = this._spriteShader;
            this._spriteShader.use();
        } else {
            this._sprite_u_view.update(gl);
            this._sprite_u_matrix.update(gl);
            this._sprite_u_alpha.update(gl);
            this._sprite_u_texture.update(gl);
        }

        gl_useBlendMode(this._blendmode);

        this._vertexData.update();
        this._imageData.update();
        this._spriteData.update();
        this._indexData.update();

        if(this._boundTexture != this._texture) {
            this._boundTexture = this._texture;
            this._texture.bind(0);
        }
        
        this._spriteVAO.bind();

        gl.drawElements(GL.TRIANGLES, this._indexData.getIndexCount(), GL.UNSIGNED_SHORT, 0);
    }

    public draw(): void {
        switch(this._drawmode) {
            case DrawMode.LINES: /* no break*/
            case DrawMode.LINE_LOOP: /* no break*/
            case DrawMode.LINE_STRIP:
            this.drawLines();
            break;

            case DrawMode.RECT_FILL: /* no break*/
            case DrawMode.RECT_OUTLINE:
            this.drawRects();
            break;

            case DrawMode.TRI_FILL: /* no break */
            case DrawMode.TRI_OUTLINE:
            this.drawTris();
            break;

            case DrawMode.IMAGES:
            this.drawImages();
            break;

            case DrawMode.SPRITES: /* no break */
            case DrawMode.SPRITES_ANIMATED:
            this.drawSprites();
            break;
        }

        // TODO, XXX, FIXME: this is a _HACK_ that should not be needed
        gl_bindVertexArray(null);

    }

}
