import { Renderer2D } from "./renderer2d";
import { Animation } from "./animation"
import { Surface } from "./surface";
import { BlendMode } from "./blendmode";
import { Color } from "./color";
import { ImageSource } from "./imagesource";
import { Matrix } from "../math/matrix";
import { clamp } from "../math/coremath";

interface SpriteDef {
    x: number;
    y: number;
    px: number;
    py: number;
    sx: number;
    sy: number;
    r: number;
    a: number;
}

interface ASpriteDef {
    f: number;
    x: number;
    y: number;
    px: number;
    py: number;
    sx: number;
    sy: number;
    r: number;
    a: number;
}

interface ImageDef {
    x: number;
    y: number;
    sx: number;
    sy: number;
    ix: number;
    iy: number;
    iw: number;
    ih: number;
    a: number;
}

interface RectDef {
    x: number;
    y: number;
    w: number;
    h: number;
    c: string;
}

interface PointDef {
    x: number;
    y: number;
}

interface TriDef {
    x0: number;
    y0: number;
    x1: number;
    y1: number;
    x2: number;
    y2: number;
    c:  string;
}

enum DrawMode {
    NONE,

    LINES,
    LINE_STRIP,
    LINE_LOOP,

    RECT_OUTLINE,
    RECT_FILL,

    TRI_OUTLINE,
    TRI_FILLED,

    IMAGES,

    SPRITES,
    SPRITES_ANIMATED
}

/**
 * 2D renderer object - maintains JS side state
 * to minimize native API interactions.
 */
export class CanvasRenderer2D implements Renderer2D {

    private _surface: Surface;
    private _context: CanvasRenderingContext2D;

    private _blendmode: BlendMode;
    private _matrix: Matrix;
    private _alpha: number;

    private _fillColor: Color;
    private _lineColor: Color;
    private _lineWidth: number;

    private _image: ImageSource;
    private _animation: Animation;
    private _drawmode: DrawMode;

    private _points: PointDef[];
    private _numPoints: number;

    private _triangles: TriDef[];
    private _numTris: number;

    private _rects: RectDef[];
    private _numRects: number;

    private _images: ImageDef[];
    private _numImages: number;

    private _sprites: SpriteDef[];
    private _numSprites: number;

    private _sprites_anim: ASpriteDef[];
    private _numASprites: number;

    private _spritemtx: Matrix;

    constructor(owner: Surface) {
        this._surface = owner;
        this._context = owner.getContext2D();

        this._blendmode = BlendMode.ALPHA;
        this._matrix = new Matrix();
        this._alpha = 1;

        this._fillColor = new Color(0,0,0,255);
        this._lineColor = new Color(255,255,255,255);
        this._lineWidth = 1;

        this._drawmode = DrawMode.NONE;
        this._points = [];
        this._numPoints = 0;
        this._triangles = [];
        this._numTris = 0;
        this._rects = [];
        this._numRects = 0;
        this._images = [];
        this._numImages = 0;
        this._sprites = [];
        this._numRects = 0;
        this._sprites_anim = [];
        this._numASprites = 0;

        this._spritemtx = new Matrix();

        this.restore();
    }

    public isGL(): boolean {
        return false;
    }

    public init(): void {
        // nop
    }

    public end(): void {
        // nop
    }

    public precache(image: ImageSource): void {
        // nop
    }

    public clearCache(): void {
        // nop
    }

    public setCachePersistent(enable: boolean): void {
        // nop
    }

    public getSurface(): Surface {
        return this._surface;
    }

    public getWidth(): number {
        return this._surface.getWidth();
    }

    public getHeight(): number {
        return this._surface.getHeight();
    }

    public restore(): void {
        var m = this._matrix;
        var ctx = this._context;
        this.setBlendMode(this._blendmode, true);
        ctx.setTransform(m.a, m.b, m.c, m.d, m.tx, m.ty);
        ctx.globalAlpha = this._alpha;
        ctx.fillStyle = this._fillColor.asString();
        ctx.strokeStyle = this._lineColor.asString();
        ctx.lineWidth = this._lineWidth;
    }

    public setBlendMode(mode: BlendMode, force: boolean = false): void {
        if(!force && mode === this._blendmode) return;
        let ctx = this._context;
        switch(mode) {
            case BlendMode.ADDITIVE:
                ctx.globalCompositeOperation = 'lighter';
            break;
            case BlendMode.MULTIPLY:
                ctx.globalCompositeOperation = 'multiply';
            break;
            default: // Also includes ALPHA
                ctx.globalCompositeOperation = 'source-over';
            break;
        }
        this._blendmode = mode;
    }

    public setAlpha(alpha: number): void {
        this._alpha = clamp(alpha, 0, 1);
    }

    public setMatrix(matrix: Matrix): void {
        this._matrix.set(matrix);
    }

    public getMatrix(): Matrix {
        return this._matrix;
    }

    public setLineWidth(width: number): void {
        this._lineWidth = Math.max(width, 0.0001);
    }

    public getLineWidth(): number {
        return this._lineWidth;
    }

    public beginLines(color: Color): void {
        this._lineColor.set(color);
        this._numPoints = 0;
        this._drawmode = DrawMode.LINES;
    }

    public beginLineStrip(color: Color): void {
        this._lineColor.set(color);
        this._numPoints = 0;
        this._drawmode = DrawMode.LINE_STRIP;
    }

    public beginLineLoop(color: Color): void {
        this._lineColor.set(color);
        this._numPoints = 0;
        this._drawmode = DrawMode.LINE_LOOP;
    }

    public addPoint(x: number, y: number): void {
        if(this._points.length === this._numPoints) {
            this._points.push({
                x: x, y: y
            });
        } else {
            let p = this._points[this._numPoints];
            p.x = x;
            p.y = y;
        }
        ++this._numPoints;
    }

    public beginRectsOutlined(): void {
        this._numRects = 0;
        this._drawmode = DrawMode.RECT_OUTLINE;
    }

    public beginRectsFilled(): void {
        this._numRects = 0;
        this._drawmode = DrawMode.RECT_FILL;
    }

    public addRect(x: number, y: number, width: number, height: number, color: Color): void {
        if(this._rects.length === this._numRects) {
            this._rects.push({
                x: x,
                y: y,
                w: width,
                h: height,
                c: color.asString()
            });
        } else {
            let r = this._rects[this._numRects];
            r.x = x;
            r.y = y;
            r.w = width;
            r.h = height;
            r.c = color.asString();
        }
        ++this._numRects;
    }

    public beginTrianglesOutlined(): void {
        this._numTris = 0;
        this._drawmode = DrawMode.TRI_OUTLINE;
    }

    public beginTrianglesFilled(): void {
        this._numTris = 0;
        this._drawmode = DrawMode.TRI_FILLED;
    }

    public addTriangle(x0: number, y0: number, x1: number, y1: number, x2: number, y2: number, color: Color): void {
        if(this._triangles.length === this._numTris) {
            this._triangles.push({
                x0: x0,
                y0: y0,
                x1: x1,
                y1: y1,
                x2: x2,
                y2: y2,
                c: color.asString()
            });
        } else {
            let t = this._triangles[this._numTris];
            t.x0 = x0;
            t.y0 = y0;
            t.x1 = x1;
            t.y1 = y1;
            t.x2 = x2;
            t.y2 = y2;
            t.c = color.asString();
        }
        ++this._numTris;
    }

    public beginImages(image: ImageSource): void {
        this._image = image;
        this._numImages = 0;
        this._drawmode = DrawMode.IMAGES;
    }

    public addImage(x: number, y: number, sx: number, sy: number, alpha: number): void {
        let rect = this._image.getSampleRect();
        let ix = rect.position.x;
        let iy = rect.position.y;
        let iw = rect.size.x;
        let ih = rect.size.y;
        if(this._images.length === this._numImages) {
            this._images.push({
                x: x,
                y: y,
                sx: sx,
                sy: sy,
                ix: ix,
                iy: iy,
                iw: iw,
                ih: ih,
                a: alpha
            });
        } else {
            let i = this._images[this._numImages];
            i.x = x;
            i.y = y;
            i.sx = sx;
            i.sy = sy;
            i.ix = ix;
            i.iy = iy;
            i.iw = iw;
            i.ih = ih;
            i.a = alpha;

        }
        ++this._numImages;
    }

    public addSubImage(
        x: number, y: number, sx: number, sy: number,
        ix: number, iy: number, iw: number, ih: number, alpha: number): void {
        if(this._images.length === this._numImages) {
            this._images.push({
                x: x,
                y: y,
                sx: sx,
                sy: sy,
                ix: ix,
                iy: iy,
                iw: iw,
                ih: ih,
                a: alpha
            });
        } else {
            let i = this._images[this._numImages];
            i.x = x;
            i.y = y;
            i.sx = sx;
            i.sy = sy;
            i.ix = ix;
            i.iy = iy;
            i.iw = iw;
            i.ih = ih;
            i.a = alpha;
        }
        ++this._numImages;
    }

    public beginSprites(image: ImageSource): void {
        this._image = image;
        this._numSprites = 0;
        this._drawmode = DrawMode.SPRITES;
    }

    public addSprite(x: number, y: number, px: number, py: number,
        sx: number, sy: number, roto: number, alpha: number): void {
        if(this._sprites.length === this._numSprites) {
            this._sprites.push({
                x: x,
                y: y,
                px: px,
                py: py,
                sx: sx,
                sy: sy,
                r: roto,
                a: alpha
            });
        } else {
            let s = this._sprites[this._numSprites];
            s.x = x;
            s.y = y;
            s.px = px;
            s.py = py;
            s.sx = sx;
            s.sy = sy;
            s.r = roto;
            s.a = alpha;
        }
        ++this._numSprites;
    }

    public beginSpritesAnimated(anim: Animation): void {
        this._drawmode = DrawMode.SPRITES_ANIMATED;
        this._animation = anim;
        this._image = anim.getSource();
        this._numASprites = 0;
    }

    public addSpriteAnimated(frame: number, x: number, y: number, px: number, py: number, sx: number, sy: number, rotation: number, alpha: number): void {
        if(this._sprites_anim.length === this._numASprites) {
            this._sprites_anim.push({
                f: frame,
                x: x,
                y: y,
                px: px,
                py: py,
                sx: sx,
                sy: sy,
                r: rotation,
                a: alpha
            });
        } else {
            let s = this._sprites_anim[this._numASprites];
            s.f = frame;
            s.x = x;
            s.y = y;
            s.px = px;
            s.py = py;
            s.sx = sx;
            s.sy = sy;
            s.r = rotation;
            s.a = alpha;
        }
        ++this._numASprites;
    }

    private drawLines(): void {
        if(this._numPoints === 0) return;

        let ctx = this._context;
        let m = this._matrix;
        let pts = this._points;

        ctx.globalAlpha = this._alpha;
        ctx.strokeStyle = this._lineColor.asString();
        ctx.lineWidth = this._lineWidth;
        ctx.setTransform(m.a, m.b, m.c, m.d, m.tx, m.ty);
        ctx.beginPath();
        if(this._drawmode == DrawMode.LINES) {
            for(let i = 0; i < this._numPoints; i += 2) {
                let p = pts[i];
                ctx.moveTo(p.x, p.y);
                p = pts[i + 1];
                ctx.lineTo(p.x, p.y);
            }
        } else {
            let p = pts[0];
            ctx.moveTo(p.x, p.y);
            for(let i = 1; i < this._numPoints; ++i) {
                p = pts[i];
                ctx.lineTo(p.x, p.y);
            }
            if(this._drawmode === DrawMode.LINE_LOOP) {
                p = pts[0];
                ctx.lineTo(p.x, p.y);
            }
        }
        ctx.stroke();
    }

    private drawTris(): void {
        if(this._numTris === 0) return;

        let ctx = this._context;
        let m = this._matrix;
        let trs = this._triangles;
        let n = this._numTris;

        ctx.globalAlpha = this._alpha;
        ctx.setTransform(m.a, m.b, m.c, m.d, m.tx, m.ty);

        if(this._drawmode === DrawMode.TRI_OUTLINE) {
            for(let i = 0; i < n; ++i) {
                let t = trs[i];
                ctx.strokeStyle = t.c;
                ctx.beginPath();
                ctx.moveTo(t.x0, t.y0);
                ctx.lineTo(t.x1, t.y1);
                ctx.lineTo(t.x2, t.y2);
                ctx.lineTo(t.x0, t.y0);
                ctx.stroke();
            }
        } else {
            for(let i = 0; i < n; ++i) {
                let t = trs[i];
                ctx.fillStyle = t.c;
                ctx.beginPath();
                ctx.moveTo(t.x0, t.y0);
                ctx.lineTo(t.x1, t.y1);
                ctx.lineTo(t.x2, t.y2);
                ctx.closePath();
                ctx.fill();
            }
        }
    }

    private drawRects(): void {
        if(this._numRects === 0) return;

        let ctx = this._context;
        let m = this._matrix;
        let rts = this._rects;

        ctx.globalAlpha = this._alpha;
        ctx.setTransform(m.a, m.b, m.c, m.d, m.tx, m.ty);

        if(this._drawmode === DrawMode.RECT_FILL) {
            for(let i = 0; i < this._numRects; ++i) {
                let r = rts[i];
                ctx.fillStyle = r.c;
                ctx.fillRect(r.x, r.y, r.w, r.h);
            }
        } else {
            for(let i = 0; i < this._numRects; ++i) {
                let r = rts[i];
                ctx.strokeStyle = r.c;
                ctx.strokeRect(r.x, r.y, r.w, r.h);
            }
        }
    }

    private drawImages(): void {
        if(this._numImages === 0) return;

        let e = this._image.getElement();
        let ctx = this._context;
        let m = this._matrix;
        let ga = this._alpha;

        ctx.setTransform(m.a, m.b, m.c, m.d, m.tx, m.ty);

        for(let i = 0; i < this._numImages; ++i) {
            let s = this._images[i];
            ctx.globalAlpha = clamp(ga * s.a, 0, 1);
            ctx.drawImage(e, s.ix, s.iy, s.iw, s.ih, s.x, s.y, s.iw * s.sx, s.ih * s.sy);
        }
    }

    private drawSprites(): void {
        if(this._numSprites === 0) return;

        let e = this._image.getElement();
        let r = this._image.getSampleRect();
        let sx = r.position.x;
        let sy = r.position.y;
        let sw = r.size.x;
        let sh = r.size.y;
        let ctx = this._context;
        let m = this._spritemtx;
        let ga = this._alpha;

        for(let i = 0; i < this._numSprites; ++i) {
            let s = this._sprites[i];
            m.buildSpriteMatrix(s.x, s.y, s.sx, s.sy, s.r);
            m.prepend(this._matrix);

            ctx.globalAlpha = clamp(ga * s.a, 0, 1);
            ctx.setTransform(m.a, m.b, m.c, m.d, m.tx, m.ty);
            ctx.drawImage(e, sx, sy, sw, sh, -s.px, -s.py, sw, sh);
        }
    }

    private drawSpritesAnimated(): void {
        if(this._numSprites === 0) return;

        let a = this._animation;
        let e = this._image.getElement();
        let ctx = this._context;
        let m = this._spritemtx;
        let ga = this._alpha;

        for(let i = 0; i < this._numASprites; ++i) {
            let s = this._sprites_anim[i];
            let f = a.getFrame(s.f);
            let sx = f.x;
            let sy = f.y;
            let sw = f.w;
            let sh = f.h;

            m.buildSpriteMatrix(s.x, s.y, s.sx, s.sy, s.r);
            m.prepend(this._matrix);
            ctx.globalAlpha = clamp(ga * s.a, 0, 1);
            ctx.setTransform(m.a, m.b, m.c, m.d, m.tx, m.ty);
            ctx.drawImage(e, sx, sy, sw, sh, -s.px, -s.py, sw, sh);
        }
    }

    public draw(): void {

        switch(this._drawmode) {

            case DrawMode.LINES: /* no break*/
            case DrawMode.LINE_STRIP: /* no break*/
            case DrawMode.LINE_LOOP:
                this.drawLines();
                break;
            case DrawMode.TRI_OUTLINE:
            case DrawMode.TRI_FILLED:
                this.drawTris();
                break;
            case DrawMode.RECT_OUTLINE: /* no break*/
            case DrawMode.RECT_FILL:
                this.drawRects();
                break;

            case DrawMode.IMAGES:
                this.drawImages();
                break;

            case DrawMode.SPRITES:
                this.drawSprites();
                break;

            case DrawMode.SPRITES_ANIMATED:
                this.drawSpritesAnimated();
                break;

        }

    }

}
