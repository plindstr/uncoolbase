import { createLogger } from "../../core/logging";
import { Surface } from "../surface";
import { BlendMode } from "../blendmode";
import { Color } from "../color";
import { assert, assertFail } from "../../core/assert";
import { isDebug } from "../../core/debug";
import { setDebugVisible } from "../../core/debugcontainer";

//
// Automatically enabled extensions (virtually 100% compatibility):
//
// OES_element_index_uint
// OES_standard_derivatives
// OES_texture_float
// OES_texture_float_linear
// OES_texture_half_float
// OES_texture_half_float_linear
// WEBGL_depth_texture
//
// OES_vertex_array_object
// ANGLE_instanced_arrays
// EXT_texture_filter_anisotropic
//

const logger = createLogger('GL');

/**
 * Access to WebGL constants
 */
export const GL = WebGLRenderingContext;

const INSTANCED_ARRAYS_EXT = 0;
const VERTEX_ARRAY_OBJECT_EXT = 1;
const TEXTURE_FILTER_ANISOTROPIC_EXT = 2;
const TEXTURE_HALF_FLOAT_EXT = 3;

/**
 * Maximum attributes to support on any given buffer
 */
const MAX_ATTRIBS = 16;

const extensions    = new Map<WebGLRenderingContext, Map<string, Object>>();
const extTables     = new Map<WebGLRenderingContext, Object[]>();
const vaoMap        = new Map<WebGLRenderingContext, WebGLVertexArrayObjectOES>();

let gl: WebGLRenderingContext = null;
let extTable: Object[] = null;
let vao: WebGLVertexArrayObjectOES = null;
let desiredState: Array<boolean> = new Array<boolean>(MAX_ATTRIBS);
let view_width: number = 320;
let view_height: number = 320;

let mem_usage: number = 0;

export const gl_createContext = (elem: HTMLCanvasElement): WebGLRenderingContext => {
    const options = {
        alpha: false,
        depth: false,
        powerPreference: 'high-performance',
        preserveDrawingBuffer: true
    };

    let context =
        <WebGLRenderingContext>(
            elem.getContext('webgl', options) ||
            elem.getContext('experimental-webgl', options)
        );

    if(!context) {
        assertFail('Could not create WebGL context!');
    } else {

        if(defaultContext === null) {
            gl_setDefaultContext(context);
        }

        vaoMap.set(context, null);
        gl_makeActive(context);

        gl_enableExtensions(
            "OES_element_index_uint",
            "OES_standard_derivatives",
            "OES_texture_float",
            "OES_texture_float_linear",
            "OES_texture_half_float",
            "OES_texture_half_float_linear",
            "WEBGL_depth_texture",
            "OES_vertex_array_object",
            "ANGLE_instanced_arrays",
            "EXT_texture_filter_anisotropic"
        );
    }

    return context;
}

/**
 * Internal: report allocation of GL memory
 *
 * @param size_bytes
 */
export const gl_reportMemoryUsage = (size_bytes: number) => {
    size_bytes |= 0;
    mem_usage += size_bytes;
}

/**
 * Get currently reported amount of GL memory used, in bytes
 */
export const gl_getMemoryUsage = () => mem_usage;

/**
 * Set to 'true' if user agent string matches a device with
 * known quirky glBufferSubData implementation
 */
export const GL_PARTIAL_BUFFER_QUIRK = ((): boolean => {
    // TODO: replace by some kind of proper runtime detection
    // maintaining a quiry device list may be too unsustainable
    const quirky_list = [
        "iPad",
        "iPhone",
        "SM-G",
        "Mobile"    // XXX: just default to glBufferData on any mobile browser...
    ];

    let ua = navigator.userAgent;
    let quirks = false;
    for(let q of quirky_list) {
        quirks = quirks || !!ua.match(q);
    }

    if(quirks) logger.warn("Partial buffer quirk detected! Using full buffer workaround.");

    return quirks;
})();

let defaultContext: WebGLRenderingContext = null;

export const gl_setDefaultContext = (gl: WebGLRenderingContext) => {
    if(defaultContext === null) {
        gl_makeActive(gl);
    }
    defaultContext = gl;
}

export const __gl_hasDefaultContext = () => {
    return defaultContext !== null;
}

export const gl_getDefaultContext = (): WebGLRenderingContext => {
    if(!defaultContext) {
        logger.debugError("No default GL context!")
        logger.debugError("Call getContextGL() or gl_setDefaultContext() to make the default context available!");
        throw 0;
    }
    return defaultContext;
}

export const gl_makeActive = (context: WebGLRenderingContext): WebGLRenderingContext => {
    if(gl === context) return gl;

    if(gl) {
        vaoMap.set(gl, vao);
    }

    gl = context;
    vao = vaoMap.get(gl);
    if(vao) {
        (<OES_vertex_array_object>extTable[VERTEX_ARRAY_OBJECT_EXT]).bindVertexArrayOES(vao);
    }

    return gl;
}

export const gl_getSupportedExtensions = (): string[] => {
    assert(!!gl, "No active GL context!");

    return gl.getSupportedExtensions();
}

const gl_putExtensionInTable = (ctx: WebGLRenderingContext, idx: number, ext: Object) => {
    if(!extTables.has(ctx)) {
        let table: Object[] = [ null, null, null, null ];
        extTables.set(ctx, table);
        if(extTable === null) {
            extTable = table;
        }
    }
    extTables.get(ctx)[idx] = ext;
}

export const gl_enableExtension = (extName: string): Object => {
    assert(!!gl, "No active GL context!");

    if(!extensions.has(gl)) {
        extensions.set(gl, new Map<string, any>());
    }

    let extmap = extensions.get(gl);
    if(extmap.has(extName)) {
        return extmap.get(extName);
    }

    if(isDebug()) {
        let supported = gl.getSupportedExtensions();
        assert(supported.indexOf(extName) >= 0, `Extension "${extName}" is not supported by GL context; check spelling.`);
    }

    let ext = gl.getExtension(extName);
    if(!ext) {
        logger.warn(`Extension "${extName}" could not be loaded!`);
        if(isDebug()) {
            setDebugVisible(true);
        }
    } else {
        extmap.set(extName, ext);

        switch(extName) {
            case 'OES_vertex_array_object':
                gl_putExtensionInTable(gl, VERTEX_ARRAY_OBJECT_EXT, ext);
            break;
            case 'ANGLE_instanced_arrays':
                gl_putExtensionInTable(gl, INSTANCED_ARRAYS_EXT, ext);
            break;
            case 'EXT_texture_filter_anisotropic':
                gl_putExtensionInTable(gl, TEXTURE_FILTER_ANISOTROPIC_EXT, ext);
            break;
            case 'OES_texture_half_float':
                gl_putExtensionInTable(gl, TEXTURE_HALF_FLOAT_EXT, ext);
            break;
        }

        logger.debugLog(`Enabled extension ${extName}`);
    }

    return ext;
}

export const gl_enableExtensions = (...extNames: string[]): Object[] => {
    let exts = [];
    for(let name of extNames) {
        exts.push(gl_enableExtension(name));
    }
    return exts;
}

export const gl_getExtension = (extName: string): any => {
    assert(!!gl, "No active GL context!");

    if(!extensions.has(gl) ||
       !extensions.get(gl).has(extName)) {
        gl_enableExtension(extName);
    }

    return extensions.get(gl).get(extName);
}

export const gl_getInstancedArraysExt = (): ANGLE_instanced_arrays => {
    assert(!!gl, "No active GL context!");

    return <ANGLE_instanced_arrays>extTable[INSTANCED_ARRAYS_EXT];
}

export const gl_getVertexArrayExt = (): OES_vertex_array_object => {
    assert(!!gl, "No active GL context!");

    return <OES_vertex_array_object>extTable[VERTEX_ARRAY_OBJECT_EXT];
}

export const gl_getAnisotropicFilterExt = (): EXT_texture_filter_anisotropic => {
    assert(!!gl, "No active GL context!");

    return <EXT_texture_filter_anisotropic>extTable[TEXTURE_FILTER_ANISOTROPIC_EXT];
}

export const gl_getHalfFloatExt = (): OES_texture_half_float => {
    assert(!!gl, "No active GL context!");

    return <OES_texture_half_float>extTable[TEXTURE_HALF_FLOAT_EXT];
}

export const gl_init2D = (width: number, height: number): void => {
    gl_setViewport(width, height);
    gl.disable(GL.DEPTH_TEST);
    gl.disable(GL.CULL_FACE);
    gl.enable(GL.BLEND);
    gl.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);
}

export const gl_setEnabledAttribs = (...loc: number[]): void => {
    desiredState.fill(false);

    for(let l of loc) {
        if(l < 0) continue;
        assert(l < MAX_ATTRIBS, "Need more state slots");
        desiredState[l] = true;
    }

    for(let i = 0; i < MAX_ATTRIBS; ++i) {
        if(desiredState[i]) {
            gl.enableVertexAttribArray(i);
        } else {
            gl.disableVertexAttribArray(i);
        }
    }
}

export const gl_enableAttribs = (...loc: number[]): void => {
    assert(!!gl, "No active GL context!");

    for(let l of loc) {
        l |= 0;
        if(l < 0) continue;
        assert(l < MAX_ATTRIBS, "Need more state slots");
        gl.enableVertexAttribArray(l);
    }
}

export const gl_disableAttribs = (...loc: number[]): void => {
    assert(!!gl, "No active GL context!");

    for(let l of loc) {
        l |= 0;
        if(l < 0) continue;
        assert(l < MAX_ATTRIBS, "Need more state slots");
        gl.disableVertexAttribArray(l);
    }
}

export const gl_clear = (color: Color): void => {
    const r = 1.0 / 255.0;
    gl.clearColor(color.getR() * r, color.getG() * r, color.getB() * r, color.getA() * r);
    gl.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT | GL.STENCIL_BUFFER_BIT);
}

export const gl_useBlendMode = (blend: BlendMode) => {
    switch(blend) {
        case BlendMode.ALPHA:
        gl.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);
        break;
        case BlendMode.ADDITIVE:
        gl.blendFunc(GL.SRC_ALPHA, GL.ONE);
        break;
        case BlendMode.MULTIPLY:
        gl.blendFunc(GL.DST_COLOR, GL.ZERO);
        break;
    }
}

export const gl_setViewport = (width: number, height: number, ctx: WebGLRenderingContext = gl) => {
    if(ctx != gl) {
        gl_makeActive(ctx);
    }

    if(view_width != width || view_height != height) {
        gl.viewport(0, 0, width, height);
        view_width = width;
        view_height = height;

        let cvs = gl.canvas;
        if(cvs) {
            let surf = ((<any>cvs)['__ub_surface']) as Surface;
            if(surf) {
                surf.setRenderSize(view_width, view_height);
            }
        }
    }
}

export const gl_getViewportWidth = () => {
    return view_width;
}

export const gl_getViewportHeight = () => {
    return view_height;
}

export const gl_createVertexArray = () => {
    assert(!!gl, "No active GL context!");

    let ext = <OES_vertex_array_object>extTable[VERTEX_ARRAY_OBJECT_EXT];
    let handle = ext.createVertexArrayOES();
    return handle;
}

export const gl_destroyVertexArray  = (handle: WebGLVertexArrayObjectOES) => {
    assert(!!gl, "No active GL context!");

    let ext = <OES_vertex_array_object>extTable[VERTEX_ARRAY_OBJECT_EXT];
    ext.deleteVertexArrayOES(handle);
}

export const gl_bindVertexArray = (handle: WebGLVertexArrayObjectOES) => {
    assert(!!gl, "No active GL context!");

    if(vao != handle) {
        let ext = <OES_vertex_array_object>extTable[VERTEX_ARRAY_OBJECT_EXT];
        ext.bindVertexArrayOES(handle);
        vao = handle;
    }
}
