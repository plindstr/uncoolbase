import { isDebug } from "../../core/debug";
import { debugLog, createLogger } from "../../core/logging";
import { GL, gl_getDefaultContext } from "./glbase";
import { GLUniform } from "./gluniform";
import { GLResource } from "./glresource";

const logger = createLogger('GLShader');
const log10 = Math.log10;

export function logShaderSource(src: string) {
    let source = src.split('\n');
    let pad = log10(source.length); // for applying padding to line numbers...
    for(let i = 0; i < source.length; ++i) {
        let line = '';
        for(let p = pad - ('' + (i + 1)).length + 1; p - 1 >= 0; --p) line += ' ';
        line += `${i + 1}: ${source[i]}`;
        debugLog(line);
    }
}

function compileShader(gl: WebGLRenderingContext, src: string, shader: WebGLShader): boolean {
    gl.shaderSource(shader, src);
    gl.compileShader(shader);
    let success = gl.getShaderParameter(shader, GL.COMPILE_STATUS);
    if(!success) {
        if(isDebug()) {
            let log = gl.getShaderInfoLog(shader).split('\n');
            logger.debugError("Could not compile shader; dumping log");
            for(let i = 0; i < log.length; ++i) {
                debugLog(`${i}: ${log[i]}`);
            }
        }
    }

    return !!success;
}

function linkShader(gl: WebGLRenderingContext, program: WebGLProgram): boolean {
    gl.linkProgram(program);
    let success = gl.getProgramParameter(program, GL.LINK_STATUS);
    if (!success) {
        if(isDebug()) {
            let log = gl.getProgramInfoLog(program).split('\n');
            logger.debugError("Could not link program; dumping log");
            for(let i = 0; i < log.length; ++i) {
                debugLog(`${i}: ${log[i]}`);
            }
        }
    }

    return !!success;
}

let dummyUniform = new GLUniform(true, null, null, 0);

export enum GLShaderPrecision {
    LOW, MEDIUM, HIGH, UNDEFINED
}

/**
 * Shader program for use in GL rendering
 */
export class GLShader extends GLResource {

    private _program: WebGLProgram = null;
    private _uniforms: { [key: string]: GLUniform } = {};
    private _precsision: GLShaderPrecision;

    constructor(gl: WebGLRenderingContext = gl_getDefaultContext(), precision = GLShaderPrecision.HIGH) {
        super(gl);
        this._precsision = precision;
    }

    public destroyGL(): void {
        if(this._program !== null) {
            this._gl.deleteProgram(this._program);
        }
    }

    public compile(vs: string, fs: string): void {
        if(this._program === null) {
            this._program = this._gl.createProgram();
        }

        if(this._precsision != GLShaderPrecision.UNDEFINED) {
            let precisionStr = "";

            let prec: string;
            switch(this._precsision) {
                case GLShaderPrecision.LOW:
                prec = "lowp ";
                break;
                case GLShaderPrecision.MEDIUM:
                prec = "mediump ";
                break;
                case GLShaderPrecision.HIGH:
                prec = "highp ";
                break;
            }
            const prefix = "precision ";
            precisionStr += prefix + prec + "float;\n";
            precisionStr += prefix + prec + "int;\n";
            precisionStr += "\n";

            vs = precisionStr + vs;
            fs = precisionStr + fs;
        }

        let vertShader = this._gl.createShader(GL.VERTEX_SHADER);
        let fragShader = this._gl.createShader(GL.FRAGMENT_SHADER);

        if(!compileShader(this._gl, vs, vertShader)) throw "Vertex shader compile failed";
        if(!compileShader(this._gl, fs, fragShader)) throw "Fragment shader compile failed";

        this._gl.attachShader(this._program, vertShader);
        this._gl.attachShader(this._program, fragShader);
        if(!linkShader(this._gl, this._program)) throw "Shader program link failed";

        this._gl.deleteShader(fragShader);
        this._gl.deleteShader(vertShader);

        // Auto-assign all active uniforms
        this._uniforms = {};
        let ucount = this._gl.getProgramParameter(this._program, GL.ACTIVE_UNIFORMS);
        for(let i = 0; i < ucount; ++i) {
            let u = new GLUniform(false, this._gl, this._program, i);
            let name = this._gl.getActiveUniform(this._program, i).name;
            this._uniforms[name] = u;
        }
    }

    /**
     * Get access to a named shader uniform
     *
     * @param name name of the uniform as defined in the glsl source
     * @return either an active uniform specific to this program or a generic dummy
     */
    public getUniform(name: string): GLUniform {
        let u = this._uniforms[name];
        if(!u) {
            logger.debugWarn(`Requested inactive uniform ${name}; returned dummy`);
            return dummyUniform;
        }
        return this._uniforms[name];
    }

    /**
     * Get the location of a GL vertex attribute
     *
     * @param name name of attribute as defined in shader source
     */
    public getAttribLocation(name: string): number {
        let gl = this._gl;
        return gl.getAttribLocation(this._program, name);
    }

    /**
     * Make this program active in the current context
     */
    public use(): void {
        this._gl.useProgram(this._program);
        this.update();
    }

    /**
     * Update any dirty uniforms. Only call this function if you know
     * the program is already in use (call use() if unsure).
     */
    public update(): void {
        for(let u in this._uniforms) {
            this._uniforms[u].update(this._gl);
        }
    }
}

/**
 * Concatenates sources into one string, renaming
 * the main routine in each with 'pass<N>', if present,
 * then generates a small main routine at the end which
 * calls all the other mains, in sequence, effectively
 * combining several shaders into a single program.
 *
 * Hairy, but it works.
 *
 * @param sources an array of glsl sources as strings
 */
export function combineShaders(sources: string[]): string {
    let idx = 1;
    let src = '';
    for(let s of sources) {
        s = s.trim();
        if(s.indexOf('void main()') >= 0) {
            src += s.replace('void main()', `void pass${idx++}()`);
        } else {
            src += s;
        }
        src += '\n\n';
    }

    src += 'void main() {\n';
    for(let i = 1; i < idx; ++i) {
        src += `pass${i}();\n`;
    }
    src += '}\n';

    return src;
}
