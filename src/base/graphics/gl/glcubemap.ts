import { GLResource } from "./glresource";
import { gl_getDefaultContext } from "./glbase";

/**
 * 2D texture for use in GL rendering
 */
export class GLCubemap extends GLResource {

    private _tex: WebGLTexture;
    private _size: number = 0;

    constructor(gl: WebGLRenderingContext = gl_getDefaultContext()) {
        super(gl);
    }

    public destroyGL(): void {
        this._gl.deleteTexture(this._tex);
    }

    public getSize(): number {
        return this._size;
    }

}
