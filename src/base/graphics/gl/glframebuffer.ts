import { GLResource } from "./glresource";
import { GLTexture } from "./gltexture";
import { gl_getDefaultContext, GL } from "./glbase";
import { Screen, getScreen } from "../../core/screen";
import { debugLogNamed } from "../../core/logging";

/**
 * Wraps WebGL Framebuffer Object
 */
export class GLFramebuffer extends GLResource {

    private _buf: WebGLFramebuffer;
    private _colorTex: GLTexture;
    private _depthTex: GLTexture;
    private _width: number;
    private _height: number;

    constructor(gl: WebGLRenderingContext = gl_getDefaultContext()) {
        super(gl);
        this._buf = gl.createFramebuffer();
        this._width = Screen.WIDTH;
        this._height = Screen.HEIGHT;
    }

    /**
     * Get the current enforced width of this Framebuffer
     */
    public getWidth(): number {
        return this._width;
    }

    /**
     * Get the current enforced height of this Framebuffer
     */
    public getHeight(): number {
        return this._height;
    }

    /**
     * Destroy associated GL resources
     *
     * @param destroyTextures if true, calls destroyGL on any attached texture objects
     */
    public destroyGL(destroyTextures: boolean = false): void {
        this._gl.deleteFramebuffer(this._buf);

        if(destroyTextures) {
            this._colorTex && (this._colorTex.destroyGL());
            this._depthTex && (this._depthTex.destroyGL());
        }
    }

    /**
     * Bind the Framebuffer to the current context
     */
    public bind(setViewport = true): void {
        this._gl.bindFramebuffer(GL.FRAMEBUFFER, this._buf);
        if(setViewport) this._gl.viewport(0, 0, this._width, this._height);
    }

    /**
     * Restore the default framebuffer to the current context
     */
    public unbind(setViewport = true): void {
        this._gl.bindFramebuffer(GL.FRAMEBUFFER, null);
        if(setViewport) this._gl.viewport(0, 0, getScreen().getWidth(), getScreen().getHeight());
    }

    /**
     * Set the size of this framebuffer. The framebuffer is initially set
     * to use the main screen reference resolution. If color and depth textures
     * are added, their data will be reallocated to fit the new framebuffer size.
     */
    public setSize(w: number, h: number): GLFramebuffer {
        w |= 0;
        h |= 0;
        if(this._width !== w || this._height !== h) {
            this._colorTex && (this._colorTex.setPixels(null, w, h, GL.RGBA, GL.RGBA, 0));
            this._depthTex && (this._depthTex.setPixels(null, w, h, GL.DEPTH_COMPONENT, GL.DEPTH_COMPONENT, 0));
            this._width = w;
            this._height = h;
        }
        return this;
    }

    /**
     * Add a color texture to this framebuffer. Be careful about
     * not letting this texture leak. It is suggested to always
     * call Framebuffer.setSize before adding textures.
     */
    public addColorTexture(): GLFramebuffer {
        debugLogNamed(this, `Add color texture of size ${this._width}x${this._height} of type ${GL.RGBA}`);
        let t = new GLTexture(this._gl);
        t.setPixels(null, this._width, this._height, GL.RGBA);
        this.attachColorTexture(t);
        return this;
    }

    /**
     * Attach a color texture object to this framebuffer.
     * If the texture is not same size as this framebuffer, the texture's
     * pixel store will be reallocated as RGBA8. It is suggested to always call
     * Framebuffer.setSize() before attaching textures.
     *
     * @param tex a texture object
     */
    public attachColorTexture(tex: GLTexture): GLFramebuffer {
        this.bind();
        this._colorTex = tex;
        this._gl.framebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, tex.getHandle(), 0);
        if(tex.getWidth() !== this._width || tex.getHeight() !== this._height) {
            tex.setPixels(null, this._width, this._height, GL.RGBA);
        }
        return this;
    }

    /**
     * Get a reference to the current color texture
     */
    public getColorTexture(): GLTexture {
        return this._colorTex;
    }

    /**
     * Create a new depth texture and attach it to this framebuffer
     */
    public addDepthTexture(): GLFramebuffer {
        let t = new GLTexture(this._gl);
        t.setPixels(null, this._width, this._height, GL.DEPTH_COMPONENT, GL.DEPTH_COMPONENT, 0);
        this.attachDepthTexture(t);
        return this;
    }

    /**
     * Attach a depth texture object to this framebuffer
     * If the texture is not same size as this framebuffer, the texture's
     * pixel store will be reallocated. It is suggested to always call
     * Framebuffer.setSize() before attaching textures.
     *
     * @param tex a texture object initialized with GL.DEPTH_ATTACHMENT
     */
    public attachDepthTexture(tex: GLTexture): GLFramebuffer {
        this.bind();
        this._depthTex = tex;
        this._gl.framebufferTexture2D(GL.FRAMEBUFFER, GL.DEPTH_ATTACHMENT, GL.TEXTURE_2D, tex.getHandle(), 0);
        if(tex.getWidth() !== this._width || tex.getHeight() !== this._height) {
            tex.setPixels(null, this._width, this._height, GL.DEPTH_ATTACHMENT, GL.DEPTH_COMPONENT, 0);
        }
        return this;
    }

    /**
     * Get a reference to the current depth texture
     */
    public getDepthTexture(): GLTexture {
        return this._depthTex;
    }

}
