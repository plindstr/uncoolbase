import { GLResource } from "./glresource";
import { GL, gl_getDefaultContext, gl_reportMemoryUsage } from "./glbase";
import { debugWarn } from "../../core/logging";

let useNPOT = true;

function getFormatPixelSize(format: GLenum): number {
    switch(format) {
        case GL.RGB:
        case GL.RGBA:
            return 4;
        case GL.RGBA4:
        case GL.RGB565:
            return 2;
        case GL.DEPTH_COMPONENT:
            return 4;
        case GL.DEPTH_COMPONENT16:
            return 2;
        default:
            debugWarn(`getFormatPixelSize: Unhandled texture format ${format}`);
            return 0;
    }
}

function getTypeSize(type: GLenum): number {
    switch(type) {
        case GL.UNSIGNED_BYTE: return 1;
        case GL.FLOAT: return 4;
        case GL.DEPTH_COMPONENT: return 4;
        case GL.DEPTH_COMPONENT16: return 2;
    }
}

export function setNPOTTextures(enabled: boolean): void {
    useNPOT = !!enabled;
}

export function isNPOTTexturesEnabled(): boolean {
    return useNPOT;
}

/**
 * 2D texture for use in GL rendering
 */
export class GLTexture extends GLResource {

    private _tex: WebGLTexture;
    private _format: GLenum;
    private _type: GLenum;
    private _width: number;
    private _height: number;
    private _mip: boolean;

    constructor(gl: WebGLRenderingContext = gl_getDefaultContext()) {
        super(gl);
        this._tex = gl.createTexture();
        this._width = 0;
        this._height = 0;
        this._format = GL.RGBA;
        this._type = GL.UNSIGNED_BYTE;
        this._mip = false;
    }

    public destroyGL(): void {
        gl_reportMemoryUsage(-this.getSize());
        this._gl.deleteTexture(this._tex);
    }

    /**
     * Get amount of used memory, in bytes
     */
    public getSize(): number {
        return getFormatPixelSize(this._format) * getTypeSize(this._type) * this._width * this._height;
    }

    public getHandle(): WebGLTexture {
        return this._tex;
    }

    public getWidth(): number {
        return this._width;
    }

    public getHeight(): number {
        return this._height;
    }

    public setMipMapping(enabled: boolean) {
        let gl = this._gl;
        this.bind();

        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);

        if(enabled) {
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR_MIPMAP_LINEAR);
            if(this._width + this._height > 0) gl.generateMipmap(GL.TEXTURE_2D);
        } else {
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
        }
        this._mip = enabled;
    }

    public setData(data: HTMLImageElement | HTMLCanvasElement, unit: number = 0): void {
        gl_reportMemoryUsage(-this.getSize());
        let gl = this._gl;
        gl.activeTexture((GL.TEXTURE0 + unit) | 0);
        gl.bindTexture(GL.TEXTURE_2D, this._tex);
        gl.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, false);
        gl.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, data);
        this._format = GL.RGBA;
        this._width = data.width;
        this._height = data.height;

        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);

        if(this._mip) {
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR_MIPMAP_LINEAR);
            gl.generateMipmap(GL.TEXTURE_2D);
        } else {
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
        }
        gl_reportMemoryUsage(this.getSize());
    }

    public setPixels(pixels: ArrayBufferView, width: number, height: number, glformat: number, gltype = GL.UNSIGNED_BYTE, unit: number = 0): void {
        gl_reportMemoryUsage(-this.getSize());
        width |= 0;
        height |= 0;
        let gl = this._gl;
        gl.activeTexture((GL.TEXTURE0 + unit) | 0);
        gl.bindTexture(GL.TEXTURE_2D, this._tex);
        gl.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, false);
        gl.texImage2D(GL.TEXTURE_2D, 0, glformat, width, height, 0, glformat, gltype, pixels);
        this._format = glformat;
        this._type = gltype;
        this._width = width;
        this._height = height;

        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);

        if(this._mip) {
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR_MIPMAP_LINEAR);
            gl.generateMipmap(GL.TEXTURE_2D);
        } else {
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
        }
        gl_reportMemoryUsage(this.getSize());
    }

    /**
     * Copy pixels from current GL_READ_BUFFER (usually the framebuffer you're drawing to) into this
     * texture. This sets texture width and height.
     *
     * @param x X coordinate to start copying from (lower-left of area)
     * @param y Y coordinate to start copying from (lower-left of area)
     * @param width width of area to copy
     * @param height height of area to copy
     * @param glformat [optional] format of resulting texture (defaults to GL.RGBA for screen pixels)
     */
    public copyPixels(x: number, y: number, width: number, height: number, glformat: number = GL.RGBA, unit: number = 0): void {
        let gl = this._gl;

        gl.activeTexture((GL.TEXTURE0 + unit) | 0);
        gl.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, false);
        gl.bindTexture(GL.TEXTURE_2D, this._tex);

        if(width === this._width && height === this._height) {
            gl.copyTexSubImage2D(GL.TEXTURE_2D, 0, 0, 0, x, y, width, height);
        } else {
            gl.copyTexImage2D(GL.TEXTURE_2D, 0, glformat, x, y, width, height, 0);
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
            gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);

            if(this._mip) {
                gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR_MIPMAP_LINEAR);
            } else {
                gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
            }

            this._width = width;
            this._height = height;
        }

        if(this._mip) {
            gl.generateMipmap(GL.TEXTURE_2D);
        }
    }

    public update(data: HTMLImageElement | HTMLCanvasElement, xoffset: number = 0, yoffset: number = 0, unit: number = 0): void {
        let gl = this._gl;
        gl.activeTexture((GL.TEXTURE0 + unit) | 0);
        gl.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, false);
        gl.bindTexture(GL.TEXTURE_2D, this._tex);
        gl.texSubImage2D(GL.TEXTURE_2D,0, xoffset, yoffset, GL.RGBA, GL.UNSIGNED_BYTE, data);
        if(this._mip) {
            gl.generateMipmap(GL.TEXTURE_2D);
        }
    }

    public updatePixels(pixels: ArrayBufferView, xoffset: number, yoffset: number, width: number, height: number, dataformat: number, unit: number = 0): void {
        let gl = this._gl;
        gl.activeTexture((GL.TEXTURE0 + unit) | 0);
        gl.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, false);
        gl.bindTexture(GL.TEXTURE_2D, this._tex);
        gl.texSubImage2D(GL.TEXTURE_2D, 0, xoffset, yoffset, width, height, dataformat, GL.UNSIGNED_BYTE, pixels);
        if(this._mip) {
            gl.generateMipmap(GL.TEXTURE_2D);
        }
    }

    public bind(unit: number = 0): void {
        let gl = this._gl;
        gl.activeTexture((GL.TEXTURE0 + unit) | 0);
        gl.bindTexture(GL.TEXTURE_2D, this._tex);
    }

}
