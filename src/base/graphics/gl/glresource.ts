/**
 * GLResource provides the basis for a WebGL
 * managed resource, and mandates the inclusion
 * of relevant resource management API
 */
export abstract class GLResource {

    /**
     * GL context reference. Handy place to store your GL ref.
     */
    protected _gl: WebGLRenderingContext;

    constructor(gl: WebGLRenderingContext) {
        this._gl = gl;
    }

    /**
     * Get access to the WebGL context associated with this resource
     */
    public getGL(): WebGLRenderingContext {
        return this._gl;
    }

    /**
     * Destroy any and all GL resources associated with this object
     */
    public abstract destroyGL(): void;

}
