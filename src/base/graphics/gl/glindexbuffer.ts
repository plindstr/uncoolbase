import { gl_getDefaultContext, GL, GL_PARTIAL_BUFFER_QUIRK } from "./glbase";
import { GLBuffer } from "./glbuffer";
import { max, nextPow2 } from "../../math/coremath";

/**
 * IndexBuffer composed of 4 byte integer, no particular alignment.
 */
export class GLIndexBuffer extends GLBuffer {

    private _data: Uint16Array;
    private _num: number;
    private _pos: number;

    constructor(gl: WebGLRenderingContext = gl_getDefaultContext()) {
        super(GL.ELEMENT_ARRAY_BUFFER, gl);

        this._data = new Uint16Array(4096);    // 8k per buffer initially
        this._num = 0;
        this._pos = 0;
    }

    public getIndexCount(): number {
        return this._num;
    }

    public setPosition(idx: number): GLIndexBuffer {
        this._pos = idx;
        return this;
    }

    public clear(): GLIndexBuffer {
        let me = this;
        me._num = 0;
        me._pos = 0;
        return this;
    }

    public alloc(num: number): GLIndexBuffer {
        if(num > this._data.length) {
            let next = new Uint16Array(nextPow2(num));
            next.set(this._data, 0);
            this._data = next;
        }
        return this;
    }

    public copy(values: Uint16Array, count: number, offset: number = 0, stride: number = 1) {
        this.alloc(this._pos + count);

        let p = this._pos;
        let d = this._data;

        let sp = offset;
        for(let i = 0; i < count; ++i) {
            d[p++] = values[sp];
            sp += stride;
        }

        return this;
    }

    public write(idx: number): GLIndexBuffer {
        this._data[this._pos++] = idx;
        this._num = max(this._pos, this._num);
        return this;
    }

    public update(usage: number = GL.STREAM_DRAW): GLIndexBuffer {

        let len = this._num;
        let buf = this._data.buffer;

        if(GL_PARTIAL_BUFFER_QUIRK) {
            this.setData(new Uint16Array(buf, 0, len), usage);
        } else {
            if(this.getSize() < len << 1) {
                this.setData(new Uint16Array(buf, 0, len), usage);
            } else {
                this.updateData(new Uint16Array(buf, 0, len), 0);
            }
        }

        return this;
    }

}
