import { GL_PARTIAL_BUFFER_QUIRK, gl_getDefaultContext, gl_reportMemoryUsage } from "./glbase";
import { GLResource } from "./glresource";
import { debugWarn } from "../../core/logging";

export abstract class GLBuffer extends GLResource {

    private _type: number;
    private _size: number;
    private _buf: WebGLBuffer;

    protected constructor(type: number, gl: WebGLRenderingContext = gl_getDefaultContext()) {
        super(gl);

        this._type = type | 0;
        this._buf = gl.createBuffer();
        this._size = 0;
    }

    public destroyGL(): void {
        gl_reportMemoryUsage(-this._size);
        let gl = this._gl;
        gl.deleteBuffer(this._buf);
    }

    public getSize(): number {
        return this._size;
    }

    public getType(): number {
        return this._type;
    }

    public bind(): void {
        let gl = this._gl;
        gl.bindBuffer(this._type, this._buf);
    }

    protected setData(data: ArrayBufferView, usage: number = WebGLRenderingContext.STATIC_DRAW): void {
        gl_reportMemoryUsage(-this._size);
        let gl = this._gl;
        gl.bindBuffer(this._type, this._buf);
        gl.bufferData(this._type, data, usage);
        this._size = data.byteLength;
        gl_reportMemoryUsage(this._size);
    }

    protected updateData(data: ArrayBufferView, offset: number = 0): void {
        if(GL_PARTIAL_BUFFER_QUIRK) {
            debugWarn("[base.gl.Buffer] Do not use updateData() on Apple devices - it is SLOW");
        }

        let gl = this._gl;
        gl.bindBuffer(this._type, this._buf);
        gl.bufferSubData(this._type, offset, data);
    }

}
