import { Vec2, Vec3, Vec4 } from "../../math/vec";
import { Matrix2, Matrix3, Matrix4, Matrix } from "../../math/matrix";
import { GL } from "./glbase";
import { Color } from "../color";
import { assertFail } from "../../core/assert";

export enum GLUniformType {
    DUMMY,          //
    FLOAT,          //
    INT,            //
    SAMPLER_2D,     //
    SAMPLER_CUBE,   //
    VEC2,           //
    IVEC2,          //
    VEC3,           //
    IVEC3,          //
    VEC4,           //
    IVEC4,          //
    MAT2,           //
    MAT3,           //
    MAT4            //
}

function parseType(info: WebGLActiveInfo): GLUniformType {
    switch(info.type) {
        case GL.FLOAT:        return GLUniformType.FLOAT;
        case GL.FLOAT_VEC2:   return GLUniformType.VEC2;
        case GL.FLOAT_VEC3:   return GLUniformType.VEC3;
        case GL.FLOAT_VEC4:   return GLUniformType.VEC4;
        case GL.INT:          return GLUniformType.INT;
        case GL.INT_VEC2:     return GLUniformType.IVEC2;
        case GL.INT_VEC3:     return GLUniformType.IVEC3;
        case GL.INT_VEC4:     return GLUniformType.IVEC4;
        case GL.BOOL:         return GLUniformType.INT;
        case GL.BOOL_VEC2:    return GLUniformType.IVEC2;
        case GL.BOOL_VEC3:    return GLUniformType.IVEC3;
        case GL.BOOL_VEC4:    return GLUniformType.IVEC4;
        case GL.FLOAT_MAT2:   return GLUniformType.MAT2;
        case GL.FLOAT_MAT3:   return GLUniformType.MAT3;
        case GL.FLOAT_MAT4:   return GLUniformType.MAT4;
        case GL.SAMPLER_2D:   return GLUniformType.SAMPLER_2D;
        case GL.SAMPLER_CUBE: return GLUniformType.SAMPLER_CUBE;
    }
    assertFail(`Invalid uniform type ${info.type} for uniform ${info.name} of size ${info.size}`);
}

function getDefaultValue(type: GLUniformType): any {
    switch(type) {
    case GLUniformType.VEC2: return new Vec2();
    case GLUniformType.VEC3: return new Vec3();
    case GLUniformType.VEC4: return new Vec4();
    case GLUniformType.IVEC2: return new Vec2();
    case GLUniformType.IVEC3: return new Vec3();
    case GLUniformType.IVEC4: return new Vec4();
    case GLUniformType.MAT2: return new Matrix2();
    case GLUniformType.MAT3: return new Matrix3();
    case GLUniformType.MAT4: return new Matrix4();
    }
    return 0;
}

function setVec2(gl: WebGLRenderingContext, loc: WebGLUniformLocation, v: any): void {
    gl.uniform2f(loc, <number>(v.x), <number>(v.y));
}

function setVec2i(gl: WebGLRenderingContext, loc: WebGLUniformLocation, v: any): void {
    gl.uniform2i(loc, <number>(v.x) | 0, <number>(v.y) | 0);
}

function setVec3(gl: WebGLRenderingContext, loc: WebGLUniformLocation, v: any): void {
    let x: number, y: number, z: number;
    if(v instanceof Color) {
        const r = 1.0 / 255.0;
        x = v.getR() * r;
        y = v.getG() * r;
        z = v.getB() * r;
    } else {
        x = v.x;
        y = v.y;
        z = v.z;
    }
    gl.uniform3f(loc, x, y, z);
}

function setVec3i(gl: WebGLRenderingContext, loc: WebGLUniformLocation, v: any): void {
    let x: number, y: number, z: number;
    if(v instanceof Color) {
        x = v.getR() | 0;
        y = v.getG() | 0;
        z = v.getB() | 0;
    } else {
        x = v.x | 0;
        y = v.y | 0;
        z = v.z | 0;
    }
    gl.uniform3i(loc, x, y, z);
}

function setVec4(gl: WebGLRenderingContext, loc: WebGLUniformLocation, v: any): void {
    let x: number, y: number, z: number, w: number;
    if(v instanceof Color) {
        const r = 1.0 / 255.0;
        x = v.getR() * r;
        y = v.getG() * r;
        z = v.getB() * r;
        w = v.getA() * r;
    } else {
        x = v.x;
        y = v.y;
        z = v.z;
        w = v.w;
    }
    gl.uniform4f(loc, x, y, z, w);
}

function setVec4i(gl: WebGLRenderingContext, loc: WebGLUniformLocation, v: any): void {
    let x: number, y: number, z: number, w: number;
    if(v instanceof Color) {
        x = v.getR() | 0;
        y = v.getG() | 0;
        z = v.getB() | 0;
        w = v.getA() | 0;
    } else {
        x = v.x | 0;
        y = v.y | 0;
        z = v.z | 0;
        w = v.w | 0;
    }
    gl.uniform4i(loc, x, y, z, w);
}

const f32: Float32Array = new Float32Array(9);
function setMatrix(gl: WebGLRenderingContext, loc: WebGLUniformLocation, v: Matrix): void {
    f32[0] = v.a;
    f32[1] = v.b;
    f32[2] = 0;
    f32[3] = v.c;
    f32[4] = v.d;
    f32[5] = 0;
    f32[6] = v.tx;
    f32[7] = v.ty;
    f32[8] = 1;
    gl.uniformMatrix3fv(loc, false, f32);
}

/**
 * WebGL uniform abstraction
 */
export class GLUniform {

    private _type: GLUniformType;
    private _loc: WebGLUniformLocation;
    private _value: any;
    private _dirty: boolean;

    constructor(dummy: boolean, gl: WebGLRenderingContext, program: WebGLProgram, index: number) {
        if(dummy) {
            this._type = GLUniformType.DUMMY;
            this._loc = null;
            this._value = 0;
            this._dirty = false;
        } else {
            var info = gl.getActiveUniform(program, index | 0);
            this._type = parseType(info);
            this._loc = gl.getUniformLocation(program, info.name);
            this._value = getDefaultValue(this._type);
            this._dirty = true;
        }
    }

    public isDummy(): boolean {
        return this._type === GLUniformType.DUMMY;
    }

    public getType(): GLUniformType {
        return this._type;
    }

    public getLocation(): WebGLUniformLocation {
        return this._loc;
    }

    public setValue(value: any): GLUniform {
        if(!this.isDummy()) {
            this._dirty = true;
            this._value = value;
        }
        return this;
    }

    public getValue(): any {
        return this._value;
    }

    /**
     * Update the value of this uniform. Should only be called
     * when the owning program is bound.
     *
     * @param gl webgl rendering context that owns the program
     */
    public update(gl: WebGLRenderingContext): void {
        if(!this._dirty) return;

        let loc = this._loc;
        let val = this._value;
        let err = 'invalid matrix type';

        switch(this._type) {
            case GLUniformType.FLOAT: gl.uniform1f(loc, <number>val); break;
            case GLUniformType.INT: gl.uniform1i(loc, (<number>val) | 0); break;
            case GLUniformType.SAMPLER_2D: gl.uniform1i(loc, (<number>val) | 0); break;
            case GLUniformType.SAMPLER_CUBE: gl.uniform1i(loc, (<number>val) | 0); break;
            case GLUniformType.VEC2: setVec2(gl, loc, val); break;
            case GLUniformType.IVEC2: setVec2i(gl, loc, val); break;
            case GLUniformType.VEC3: setVec3(gl, loc, val); break;
            case GLUniformType.IVEC3: setVec3i(gl, loc, val); break;
            case GLUniformType.VEC4: setVec4(gl, loc, val); break;
            case GLUniformType.IVEC4: setVec4i(gl, loc, val); break;
            case GLUniformType.MAT2:
                if(val instanceof Matrix2) {
                    gl.uniformMatrix2fv(loc, false, <Float32Array>(val));
                } else {
                    assertFail(err);
                }
            break;
            case GLUniformType.MAT3:
                if(val instanceof Matrix) {
                    setMatrix(gl, loc, val);
                } else if(val instanceof Matrix3) {
                    gl.uniformMatrix3fv(loc, false, <Float32Array>(val));
                } else {
                    assertFail(err);
                }
            break;
            case GLUniformType.MAT4:
                if(val instanceof Matrix4) {
                    gl.uniformMatrix4fv(loc, false, <Float32Array>(val));
                } else {
                    assertFail(err);
                }
            break;
        }

        this._dirty = false;
    }

}
