import { GLVBOField, GLVertexBuffer } from "./glvertexbuffer";
import { GLShader, combineShaders, logShaderSource } from "./glshader";
import { BlendMode } from "../blendmode";
import { GLUniform } from "./gluniform";
import { gl_getDefaultContext, gl_makeActive, GL, gl_useBlendMode, gl_setEnabledAttribs } from "./glbase";
import { debugLog } from "../../core/logging";
import { clamp } from "../../math/coremath";


    const vsSource =
`
precision highp float;
precision highp int;

attribute vec2 a_pos;
attribute vec2 a_uv;

varying vec2 position;
varying vec2 texcoord;

void main() {
    position = a_pos;
    texcoord = a_uv;
    gl_Position = vec4(a_pos, 0.5, 1.0);
}`;

    const fsBase =
`
precision highp float;
precision highp int;

varying vec2 position;
varying vec2 texcoord;
`;

    const fsAlpha =
`
uniform float u_filter_alpha;

void main() {
    gl_FragColor.a *= u_filter_alpha;
}`;

const vboMap = new Map<WebGLRenderingContext, GLVertexBuffer>();

let a_pos_def: GLVBOField = null;
let a_uv_def: GLVBOField = null;

function initVBO(vbo: GLVertexBuffer): void {
    if(!a_pos_def) a_pos_def = vbo.addField('a_pos', 2);
    if(!a_uv_def) a_uv_def = vbo.addField('a_uv', 2);

    vbo.setPosition(0);
    vbo.write(a_pos_def, -1,  1);
    vbo.write(a_pos_def,  1,  1);
    vbo.write(a_pos_def,  1, -1);
    vbo.write(a_pos_def, -1,  1);
    vbo.write(a_pos_def,  1, -1);
    vbo.write(a_pos_def, -1, -1);

    vbo.setPosition(0);
    vbo.write(a_uv_def, 0, 1);
    vbo.write(a_uv_def, 1, 1);
    vbo.write(a_uv_def, 1, 0);
    vbo.write(a_uv_def, 0, 1);
    vbo.write(a_uv_def, 1, 0);
    vbo.write(a_uv_def, 0, 0);

    vbo.update();
}

/**
 * Filter - a convenience class for WebGL which allows
 * painting fullscreen passes with a custom fragment shader.
 */
export class GLFilter extends GLShader {

    private _blendmode: BlendMode = BlendMode.ALPHA;
    private _alpha: number = 1;
    private _pos_loc: number = 0;
    private _uv_loc: number = 1;
    private _u_alpha: GLUniform;

    /**
     * User-overrideable function. Called after draw has been
     * set up on the Filter side, just before the final call
     * to gl.draw(GL.TRIANGLES,...);
     */
    public beforeDraw: () => void = null;

    constructor(fsSource: string[], gl: WebGLRenderingContext = gl_getDefaultContext(), vsExtensions: string[] = null, dump: boolean = false) {
        super(gl);

        let vs = vsSource;
        if(vsExtensions) {
            let src: string[] = [ vsSource ];
            src.concat(vsExtensions);
            vs = combineShaders(src);
        }

        let fssrc = [ fsBase ];
        fssrc = fssrc.concat(fsSource);
        fssrc.push(fsAlpha);

        let fs = combineShaders(fssrc);

        if(dump) {
            debugLog("GLFilter: Created filter");
            logShaderSource(fs);
        }

        this.compile(vs, fs);
        this._pos_loc = this.getAttribLocation('a_pos');
        this._uv_loc = this.getAttribLocation('a_uv');

        this._u_alpha = this.getUniform('u_filter_alpha');
    }

    public setBlendMode(blend: BlendMode): GLFilter {
        this._blendmode = blend;
        return this;
    }

    public getBlendMode(): BlendMode {
        return this._blendmode;
    }

    public setAlpha(a: number): GLFilter {
        this._alpha = clamp(a, 0, 1);
        return this;
    }

    public getAlpha(): number {
        return this._alpha;
    }

    public draw(): void {
        let gl = gl_makeActive(this._gl);

        let vbo = vboMap.get(gl);
        if(!vbo) {
            vbo = new GLVertexBuffer(gl);
            initVBO(vbo);
            vboMap.set(gl, vbo);
        }

        gl.disable(GL.DEPTH_TEST);
        gl.disable(GL.CULL_FACE);
        gl.enable(GL.BLEND);
        gl_useBlendMode(this._blendmode);

        this._u_alpha.setValue(this._alpha);
        this.use();

        vbo.bind();
        vbo.bindField(a_pos_def, this._pos_loc);
        vbo.bindField(a_uv_def, this._uv_loc);

        gl_setEnabledAttribs(this._pos_loc, this._uv_loc);

        if(this.beforeDraw) {
            this.beforeDraw.call(this);
        }

        gl.drawArrays(GL.TRIANGLES, 0, 6);
    }

}
