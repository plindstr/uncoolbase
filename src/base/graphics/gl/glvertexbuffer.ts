import { GLBuffer } from "./glbuffer";
import { gl_getDefaultContext, GL, GL_PARTIAL_BUFFER_QUIRK } from "./glbase";
import { nextPow2, max } from "../../math/coremath";
import { assert } from "../../core/assert";

export interface GLVBOField {
    size: number;
    offs: number;
}

/**
 * VertexBuffer composed of 4 byte (float) values, no particular alignment.
 * Current semantics support up to 4 values per field
 */
export class GLVertexBuffer extends GLBuffer {

    private _data: Float32Array;
    private _num: number;
    private _pos: number;
    private _stride: number;
    private _fields: { [id: string]: GLVBOField };

    constructor(gl: WebGLRenderingContext = gl_getDefaultContext()) {
        super(GL.ARRAY_BUFFER, gl);

        this._data = new Float32Array(4096);    // 16k per buffer initially
        this._num = 0;
        this._pos = 0;
        this._stride = 0;
        this._fields = {};
    }

    public getVertexCount(): number {
        return this._num;
    }

    public setPosition(idx: number): GLVertexBuffer {
        this._pos = idx | 0;
        return this;
    }

    public start(): GLVertexBuffer {
        this._pos = 0;
        return this;
    }

    public clear(): GLVertexBuffer {
        this._num = 0;
        this._pos = 0;
        return this;
    }

    public addField(name: string, numValues: number): GLVBOField {
        assert(!this._fields[name], `Double definition of field ${name}!`);

        let f = this._fields[name] = {
            size: numValues | 0,
            offs: this._stride
        };
        this._stride += numValues | 0;
        return f;
    }

    public clearFields(): GLVertexBuffer {
        this._stride = 0;
        this._fields = {};
        return this;
    }

    public getFieldOffset(name: string): number {
        let f = this._fields[name];
        return f ? f.offs : 0;
    }

    public getFieldSize(name: string): number {
        let f = this._fields[name];
        return f ? f.size : 0;
    }

    public getField(name: string): GLVBOField {
        let f = this._fields[name];
        return f ? f : null;
    }

    public alloc(num: number): GLVertexBuffer {
        if((num * this._stride) > this._data.length) {
            let next = new Float32Array(nextPow2(num) * this._stride);
            next.set(this._data, 0);
            this._data = next;
        }
        return this;
    }

    public copy(field: GLVBOField, values: Float32Array, count: number, offset = 0, stride = field.size) {
        this.alloc(this._pos + count);

        let s = this._stride;
        let p = (this._pos * s) + field.offs;
        let d = this._data;

        let sp = offset;
        for(let i = 0; i < count; ++i) {
            switch(field.size) {
                case 4:
                d[p + 3] = values[sp + 3];
                case 3:
                d[p + 2] = values[sp + 2];
                case 2:
                d[p + 1] = values[sp + 1];
                case 1:
                d[p + 0] = values[sp + 0];
                break;
            }
            p += s;
            sp += stride;
        }

        this._pos += count;
        this._num = max(this._num, this._pos);
        return this;
    }

    public write(field: GLVBOField, x: number, y?: number, z?: number, w?: number): GLVertexBuffer {
        let p = (this._pos * this._stride) + field.offs;
        let d = this._data;
        switch(field.size) {
            case 4:
            d[p + 3] = w;
            case 3:
            d[p + 2] = z;
            case 2:
            d[p + 1] = y;
            case 1:
            d[p + 0] = x;
            break;
            default:
            for(let i = 0; i < field.size; ++i) {
                d[p + i] = arguments[i + 1];
            }
            break;
        }
        this._pos++;
        this._num = max(this._num, this._pos);
        return this;
    }

    public writeRepeat(field: GLVBOField, repeat: number, x: number, y?: number, z?: number, w?: number): GLVertexBuffer {
        let p = (this._pos * this._stride) + field.offs;
        let d = this._data;
        for(let i = 0; i < repeat; ++i) {
            switch(field.size) {
                case 4:
                d[p + 3] = w;
                case 3:
                d[p + 2] = z;
                case 2:
                d[p + 1] = y;
                case 1:
                d[p + 0] = x;
                break;
            }
            p += this._stride;
        }
        this._pos += repeat;
        this._num = max(this._num, this._pos);
        return this;
    }

    public update(usage: number = GL.STREAM_DRAW): void {
        let me = this;
        let len = this._num * this._stride;
        let buf = this._data.buffer;

        if(GL_PARTIAL_BUFFER_QUIRK) {
            // In their eternal quest for being SIMPLE, Apple has,
            // in its wisdom, completely broken the SubData functions
            // so we need to just keep redeclaring the buffer over and over
            this.setData(new Float32Array(buf, 0, len), usage);
        } else {
            if(this.getSize() < len << 2) {
                this.setData(new Float32Array(buf, 0, len), usage);
            } else {
                this.updateData(new Float32Array(buf, 0, len), 0);
            }
        }
    }

    public bindField(field: GLVBOField, loc: number): void {
        this.bind();
        let gl = this._gl;
        if(loc >= 0) {
            gl.vertexAttribPointer(loc, field.size, GL.FLOAT, false, this._stride << 2, field.offs << 2);
        }
    }
}
