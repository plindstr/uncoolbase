import { GLResource } from "./glresource";
import { gl_getDefaultContext, gl_makeActive, gl_createVertexArray, gl_destroyVertexArray, gl_bindVertexArray } from "./glbase";

//
// Vertex Array Object through
// OES_vertex_array_object extension
//

export class GLVertexArray extends GLResource {

    private _handle: WebGLVertexArrayObjectOES = null;

    constructor(gl: WebGLRenderingContext = gl_getDefaultContext()) {
        super(gl);
        gl_makeActive(gl);
        this._handle = gl_createVertexArray();
    }

    public destroyGL(): void {
        gl_makeActive(this._gl);
        gl_destroyVertexArray(this._handle);
        this._handle = null;
    }

    public bind(): void {
        gl_makeActive(this._gl);
        gl_bindVertexArray(this._handle);
    }

    public unbind(): void {
        gl_makeActive(this._gl);
        gl_bindVertexArray(null);
    }

}
