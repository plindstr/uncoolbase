import { clamp } from "../math/coremath";
import { lerp } from "../math/interpolation";

export class Color {

    public static BLACK         = new Color(   0,   0,   0, 255 );
    public static GREY          = new Color(  96,  96,  96, 255 );
    public static WHITE         = new Color( 255, 255, 255, 255 );
    public static TRANSPARENT   = new Color(   0,   0,   0,   0 );
    public static RED           = new Color( 255,   0,   0, 255 );
    public static GREEN         = new Color(   0, 255,   0, 255 );
    public static BLUE          = new Color(   0,   0, 255, 255 );
    public static CYAN          = new Color(   0, 255, 255, 255 );
    public static MAGENTA       = new Color( 255,   0, 255, 255 );
    public static YELLOW        = new Color( 255, 255,   0, 255 );

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    private _r: number;
    private _g: number;
    private _b: number;
    private _a: number;

    private _rnorm: number;
    private _gnorm: number;
    private _bnorm: number;
    private _anorm: number;

    private _string: string;
    private _dirty: boolean;

    constructor(
        r: number = 255,
        g: number = 255,
        b: number = 255,
        a: number = 255) {

        const i = 1.0 / 255;
        r = this._r = clamp(r, 0, 255);
        g = this._g = clamp(g, 0, 255);
        b = this._b = clamp(b, 0, 255);
        a = this._a = clamp(a, 0, 255);
        this._rnorm = r * i;
        this._gnorm = g * i;
        this._bnorm = b * i;
        this._anorm = a * i;
        this._dirty = true;
        this.asString();
    }

    public setR(r: number): Color {
        const i = 1.0 / 255;
        r = this._r = clamp(r, 0, 255);
        this._rnorm = r * i;
        this._dirty = true;
        return this;
    }

    public setRNorm(r: number): Color {
        r = this._rnorm = clamp(r, 0, 1);
        this._r = r * 255.0;
        this._dirty = true;
        return this;
    }

    public setG(g: number): Color {
        const i = 1.0 / 255;
        g = this._g = clamp(g, 0, 255);
        this._gnorm = g * i;
        this._dirty = true;
        return this;
    }

    public setGNorm(g: number): Color {
        g = this._gnorm = clamp(g, 0, 1);
        this._g = g * 255.0;
        this._dirty = true;
        return this;
    }

    public setB(b: number): Color {
        const i = 1.0 / 255;
        b = this._b = clamp(b, 0, 255);
        this._bnorm = b * i;
        this._dirty = true;
        return this;
    }

    public setBNorm(b: number): Color {
        b = this._bnorm = clamp(b, 0, 1);
        this._b = b * 255.0;
        this._dirty = true;
        return this;
    }

    public setA(a: number): Color {
        const i = 1.0 / 255;
        a = this._a = clamp(a, 0, 255);
        this._anorm = a * i;
        this._dirty = true;
        return this;
    }

    public setANorm(a: number): Color {
        a = this._anorm = clamp(a, 0, 1);
        this._a = a * 255.0;
        this._dirty = true;
        return this;
    }

    public set(c: Color): Color {
        const i = 1.0 / 255.0;
        let r = this._r = c._r;
        let g = this._g = c._g;
        let b = this._b = c._b;
        let a = this._a = c._a;
        this._rnorm = r * i;
        this._gnorm = g * i;
        this._bnorm = b * i;
        this._anorm = a * i;
        if(c._dirty) {
            this._dirty = true;
        } else {
            this._string = c._string;
            this._dirty = false;
        }
        return this;
    }

    public setRGBA(r: number, g: number, b: number, a: number): Color {
        const i = 1.0 / 255.0;
        r = this._r = clamp(r, 0, 255);
        g = this._g = clamp(g, 0, 255);
        b = this._b = clamp(b, 0, 255);
        a = this._a = clamp(a, 0, 255);
        this._rnorm = r * i;
        this._gnorm = g * i;
        this._bnorm = b * i;
        this._anorm = a * i;
        this._dirty = true;
        return this;
    }

    public setRGBANorm(r: number, g: number, b: number, a: number): Color {
        r = this._rnorm = clamp(r, 0, 1);
        g = this._gnorm = clamp(g, 0, 1);
        b = this._bnorm = clamp(b, 0, 1);
        a = this._anorm = clamp(a, 0, 1);
        this._r = r * 255;
        this._g = g * 255;
        this._b = b * 255;
        this._a = a * 255;
        this._dirty = true;
        return this;
    }

    public blend(c: Color, bias: number): Color {
        const i = 1.0 / 255.0;
        bias = clamp(bias, 0, 1);

        let r = this._r = lerp(bias, this._r, c._r);
        let g = this._g = lerp(bias, this._g, c._g);
        let b = this._b = lerp(bias, this._b, c._b);
        let a = this._a = lerp(bias, this._a, c._a);
        this._rnorm = r * i;
        this._gnorm = g * i;
        this._bnorm = b * i;
        this._anorm = a * i;
        this._dirty = true;

        return this;
    }

    public getR(): number {
        return this._r;
    }

    public getRNorm(): number {
        return this._rnorm;
    }

    public getG(): number {
        return this._g;
    }

    public getGNorm(): number {
        return this._gnorm;
    }

    public getB(): number {
        return this._b;
    }

    public getBNorm(): number {
        return this._bnorm;
    }

    public getA(): number {
        return this._a;
    }

    public getANorm(): number {
        return this._anorm;
    }

    public asString(): string {
        if(this._dirty) {
            this._string = `rgba(${this._r | 0},${this._g | 0},${this._b | 0},${this._anorm})`;
            this._dirty = false;
        }
        return this._string;
    }

    public equals(c: Color): boolean {
        return this._r === c._r
            && this._g === c._g
            && this._b === c._b
            && this._a === c._a;
    }

}
