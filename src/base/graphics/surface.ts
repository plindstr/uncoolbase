import { debugLogNamed } from "../core/logging";
import { Rect } from "../math/rect";
import { ImageSource } from "./imagesource";
import { Renderer2D } from "./renderer2d";
import { CanvasRenderer2D } from "./canvasrenderer2d";
import { WebGLRenderer2D } from "./webglrenderer2d";
import { linkSurfaceToCanvas } from "./surfaceutil";
import { gl_createContext } from "./gl/glbase";
import { assertFail } from "../core/assert";
import { TextureCache } from "./texturecache";

export enum ContextType {
    CANVAS2D,
    WEBGL
}

/**
 * Wrapper around a HTMLCanvasElement
 * Can be used as an image source when drawing.
 * Intended to be used as either a rendertarget
 * for a Stage or for generating dynamic graphics.
 */
export class Surface implements ImageSource {

    private _sur_element: HTMLCanvasElement;
    private _sur_context: { ctx: CanvasRenderingContext2D | WebGLRenderingContext, type: ContextType };
    private _sur_width: number;
    private _sur_height: number;
    private _sur_rect: Rect;
    private _sur_renderer: Renderer2D;
    private _sur_volatile: boolean;
    private _sur_resident: boolean;
    private _sur_render_width: number;
    private _sur_render_height: number;
    private _sur_texture_cache: TextureCache;

    public static create(width: number, height: number): Surface {
        let s = new Surface();
        s.setSize(width, height);
        return s;
    }

    constructor(elem: HTMLCanvasElement = null) {
        this._sur_element = elem === null ? window.document.createElement('canvas') : elem;
        this._sur_width = this._sur_element.width;
        this._sur_height = this._sur_element.height;
        this._sur_render_width = this._sur_width;
        this._sur_render_height = this._sur_height;
        this._sur_rect = new Rect(0,0,this._sur_width,this._sur_height);
        this._sur_renderer = null;
        this._sur_context = null;
        this._sur_volatile = false;
        this._sur_resident = false;
        this._sur_texture_cache = null;
        linkSurfaceToCanvas(this._sur_element, this);
    }

    public isVolatile(): boolean {
        return this._sur_volatile;
    }

    public setVolatile(b: boolean): void {
        this._sur_volatile = !!b;
    }

    public getElement(): HTMLCanvasElement {
        return this._sur_element;
    }

    public getElementWidth(): number {
        return this._sur_width;
    }

    public getElementHeight(): number {
        return this._sur_height;
    }

    public createImageData(width: number = this._sur_width, height: number = this._sur_height): ImageData {
        return this.getContext2D().createImageData(width | 0, height | 0);
    }

    public putImageData(data: ImageData, xoffset: number = 0, yoffset: number = 0): Surface {
        this.getContext2D().putImageData(data, xoffset, yoffset);
        return this;
    }

    public getTextureCache(): TextureCache {
        if(!this._sur_texture_cache) {
            this._sur_texture_cache = new TextureCache(this.getContextGL());
        }
        return this._sur_texture_cache;
    }

    public getX(): number {
        return 0;
    }

    public getY(): number {
        return 0;
    }

    public getWidth(): number {
        return this._sur_width;
    }

    public getHeight(): number {
        return this._sur_height;
    }

    public getAspect(): number {
        return this._sur_width / this._sur_height;
    }

    public setSize(w: number, h: number): void {
        w |= 0;
        h |= 0;
        this._sur_element.style.width = `${w}px`;
        this._sur_element.style.height = `${h}px`;
        this._sur_element.width = w;
        this._sur_element.height = h;
        this._sur_width = w;
        this._sur_height = h;
        this._sur_rect.position.setXY(0,0);
        this._sur_rect.size.setXY(w,h);
    }

    /**
     * Get access to a Renderer object local to this Surface.
     */
    public getRenderer(preferredType: ContextType = ContextType.CANVAS2D): Renderer2D {
        if(this._sur_context === null) {
            if(preferredType === ContextType.WEBGL) {
                this.getContextGL();
            } else {
                this.getContext2D();
            }
        }

        if(this._sur_renderer == null) {
            if(this._sur_context.type === ContextType.CANVAS2D) {
                this._sur_renderer = new CanvasRenderer2D(this);
            } else {
                this._sur_renderer = new WebGLRenderer2D(this);
            }
        }
        return this._sur_renderer;
    }

    public getContext2D(): CanvasRenderingContext2D {
        if(this._sur_context === null || this._sur_context.ctx == null) {
            debugLogNamed(this, "Initializing 2D context");
            this._sur_context = {
                ctx: this._sur_element.getContext('2d'),
                type: ContextType.CANVAS2D
            };
        } else if(this._sur_context.type !== ContextType.CANVAS2D) {
            assertFail("Requested 2D context for canvas with WebGL context");
        }
        return <CanvasRenderingContext2D>(this._sur_context.ctx);
    }

    public getContextGL(): WebGLRenderingContext {
        if(this._sur_context === null || this._sur_context.ctx == null) {
            debugLogNamed(this, "Initializing WebGL context");
            this._sur_context = {
                ctx: gl_createContext(this._sur_element),
                type: ContextType.WEBGL
            };
        } else if(this._sur_context.type !== ContextType.WEBGL) {
            assertFail("Requested WebGL context for canvas with 2D context");
        }
        return <WebGLRenderingContext>(this._sur_context.ctx);
    }

    public setRenderSize(width: number, height: number): Surface {
        this._sur_render_width = width;
        this._sur_render_height = height;
        return this;
    }

    public getRenderWidth(): number {
        return this._sur_render_width;
    }

    public getRenderHeight(): number {
        return this._sur_render_height;
    }

    public getSampleRect(): Rect {
        return this._sur_rect;
    }

    public setSampleRect(x: number, y: number, w: number, h: number): Surface {
        this._sur_rect.position.setXY(x,y);
        this._sur_rect.size.setXY(w,h);
        return this;
    }

    public setResident(enable: boolean): void {
        this._sur_resident = !!enable;
    }

    public isResident(): boolean {
        return this._sur_resident;
    }

}
