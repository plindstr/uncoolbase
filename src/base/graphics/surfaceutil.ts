//
// Hacky indirection to break the surface->gl->surface
// circular dependency.
//
// However, this actually breaks the chain of definition.
// Replace this with something better in the future - perhaps
// GLTextureManager does not need to be in gl at all?
//

const fieldname = '__surface';

export function linkSurfaceToCanvas(canvas: HTMLCanvasElement, surface: any) {
    (<any>canvas)[fieldname] = surface;
}

export function isCanvasVolatile(canvas: HTMLCanvasElement): boolean {
    let surface = (<any>canvas)[fieldname];
    if(!!surface) {
        return surface.isVolatile();
    }
    return false;
}
