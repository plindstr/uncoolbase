//
// WebGL shaders for UncoolBase 2D scene graph
//
export const vertex2d = {
        common:
`
attribute vec2 a_vertex;
uniform mat3 u_view;
uniform mat3 u_matrix;

void main() {
vec3 p = u_view * u_matrix * vec3(a_vertex, 1.0);
gl_Position = vec4(p.x, p.y, 0.5, 1.0);
}
`,

        color:
`
attribute vec4 a_color;
varying vec4 v_color;

uniform float u_alpha;

void main() {
v_color.rgb = a_color.rgb;
v_color.a = a_color.a * u_alpha;
}
`,

        image:
`
attribute vec2 a_texcoord;
varying vec2 v_texcoord;

void main() {
v_texcoord = vec2(a_texcoord.x, a_texcoord.y);
}
`,

        sprite:
`
attribute float a_rot;
attribute vec2 a_position;
attribute vec2 a_scale;
attribute vec2 a_pivot;

void main() {
float j = cos(a_rot);
float k = sin(a_rot);
float a = a_scale.x * j;
float b = -a_scale.y * k;
float c = a_scale.x * k;
float d = a_scale.y * j;
float e = -a_pivot.x;
float f = -a_pivot.y;
float g = a_position.x + (a*e + c*f);
float h = a_position.y + (b*e + d*f);
mat3 i = mat3(a,b,0.0,c,d,0.0,g,h,1.0);
gl_Position = vec4(u_view * u_matrix * i * vec3(a_vertex, 1.0), 1.0);
}
`,
    };

export const fragment2d = {
        color:
`
varying vec4 v_color;

void main() {
gl_FragColor = v_color;
}
`,

        image:
`
uniform sampler2D u_texture;

varying vec2 v_texcoord;
varying vec4 v_color;

vec3 rgb2hsv(vec3 c) {
vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

float d = q.x - min(q.w, q.y);
float e = 1.0e-10;
return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 modhsv(in vec3 rgb, in vec3 hsv) {
vec3 hsv_in = rgb2hsv(rgb);
return hsv2rgb(vec3(hsv_in.x + hsv.x, hsv_in.y + hsv.y, hsv_in.z * hsv.z));
}

void main() {
vec4 cin = texture2D(u_texture, v_texcoord);
vec3 cout = modhsv(cin.rgb, v_color.rgb);
gl_FragColor = vec4(cout, cin.a * v_color.w);
}
`
    };
