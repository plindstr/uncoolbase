import { ArrayList } from "../mem/arraylist";

export enum FileType {
    RAW,
    JSON,
    XML,
    IMAGE,
    AUDIO,
    WEBFONT,
}

export class AssetFile {

    private _url: String      = null;
    private _type: FileType   = null;
    private _data: Blob       = null;
    private _loaded: boolean  = false;
    private _refcount: number = 0;

    public AssetFile(url: String, type: FileType = null) {
        this._url = url;

        if(!type) {
            // Guess type

        }

        this._type = type;
    }

    public getUrl(): String {
        return this._url;
    }

    public getType(): FileType {
        return this._type;
    }

    public getData(): any {
        return this._data;
    }

    public isLoaded(): boolean {
        return this._loaded;
    }

    protected async load(): Promise<Blob> {
        const response = await fetch('' + this._url, {
            method: 'GET',
            credentials: 'include',
            cache: 'default',
            mode: 'same-origin',
            headers: {
                responseType: 'blob'
            }
        });

        if(!response.ok) {
            throw 'LOAD_FAIL ' + this._url;
        }

        return await response.blob();
    }

}

interface Asset {

}

interface ImageAsset extends Asset {

}

abstract class AbstractAsset implements Asset {

    private _asset_id: String;

    constructor(id: String) {
        this._asset_id = id;
    }

    public getId(): String {
        return this._asset_id;
    }

    public abstract getFileURLs(): String[];

}

class ImageFileAsset extends AbstractAsset implements ImageAsset {

    constructor(id: String, url: String) {
        super(id);

    }

    public getFileURLs(): String[] {
        throw new Error("Method not implemented.");
    }

}

class AtlasAsset extends AbstractAsset {

    constructor(id: String, url: String) {
        super(id);

    }

    public getFileURLs(): String[] {
        throw new Error("Method not implemented.");
    }

}

class AtlasImageAsset extends AbstractAsset implements ImageAsset {

    constructor(id: String, atlas: AtlasAsset) {
        super(id);
    }

    public getFileURLs(): String[] {
        throw new Error("Method not implemented.");
    }

}

class AssetList {

    private _assets = new Map<String, Asset>();

    constructor() {

    }

    public add(asset: AbstractAsset) {

    }

    public getAsset<T extends Asset>(id: String): T {
        return <T>(this._assets.get(id));
    }

    public getAtlas(id: String): AtlasAsset {
        return this.getAsset<AtlasAsset>(id);
    }

    public getImage(id: String): ImageAsset {
        return this.getAsset<ImageAsset>(id);
    }


}

let _instance: MasterLoader = null;
export class MasterLoader {

    private _lists = new ArrayList<AssetList>();
    private _files = new Map<String, AssetFile>();
    private _loadedSize: number = 0;
    private _totalSize: number = 0;

    public static get(): MasterLoader {
        if(!_instance) {
            _instance = new MasterLoader();
        }
        return _instance;
    }

    constructor() {

    }

    /**
     * Get cumulative size of currently loaded files
     */
    public getLoadedSize(): number {
        return this._loadedSize;
    }

    /**
     * Get the total size of all currently known files
     * (note that this may change unexpectedly)
     */
    public getTotalSize(): number {
        return this._totalSize;
    }

    /**
     * Start downloading all data in an asset list,
     * if that data is not already loaded.
     *
     * @param list
     */
    public load(list: AssetList) {
        if(this._lists.addUnique(list)) {
            // TODO: queue items
        }
    }

    /**
     * Unload all data requested by an asset list,
     * if that data is not being referenced by some
     * other list.
     *
     * @param list
     */
    public unload(list: AssetList) {
        if(this._lists.remove(list)) {
            // TODO: remove items
        }
    }
}


