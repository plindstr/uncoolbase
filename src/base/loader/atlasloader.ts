import { Image } from "../graphics/image";
import { Atlas } from "../graphics/atlas";
import { assert } from "../core/assert";
import { debugError } from "../core/logging";


/**
 * Create an atlas from data as exported by
 * Free Texture Packer, using "JSON (hash)" data
 * format.
 *
 * http://free-tex-packer.com/
 *
 * @param image image to use as source for atlas
 * @param json data as exported by Free Texture Packer (as string)
 */
const createAtlasFromFTP = (image: Image, data: any): Atlas => {

    let a = new Atlas(image);

    // XXX: the string-form access syntax is to keep Terser from mangling JSON prop names

    try {
        let frames = data['frames'];
        let meta = data['meta'];
        let size = meta['size'];

        assert(image.getElementWidth() == size['w'],  `Image width differs from meta (${image.getElementWidth()} vs ${meta.size.w})`);
        assert(image.getElementHeight() == size['h'], `Image height differs from meta (${image.getElementHeight()} vs ${meta.size.h})`);

        for(let f in frames) {
            if(!frames.hasOwnProperty(f)) continue;

            let ff = frames[f];
            let frame = ff['frame'];
            let id = f;
            let x  = frame['x'];
            let y  = frame['y'];
            let w  = frame['w'];
            let h  = frame['h'];

            let ss = ff['spriteSourceSize'];
            let xoffset = ss['x'];
            let yoffset = ss['y'];
            ss = ff['sourceSize'];
            let real_w  = ss['w'];
            let real_h  = ss['h'];

            a.addImage(id, x, y, w, h, xoffset, yoffset, real_w, real_h);
        }
    } catch(error) {
        debugError("Error parsing Free Texture Packer atlas data: " + error);
    }

    return a;

}

export const loadAtlas = (image: Image, json: string): Atlas => {
    try {
        let data = JSON.parse(json);
        if(data['meta']['app'].search(/free-tex-packer/)) {
            return createAtlasFromFTP(image, data);
        }
    } catch(error) {
        debugError("Error parsing JSON");
    }

    debugError("Cannot load atlas; unknown data format");
    return null;
}
