import { debugError, debugErrorNamed } from "../core/logging";
import { Image } from "../graphics/image";
import { Sound } from "../sound/sound";
import { Music } from "../sound/music";
import { Font } from "../graphics/font";
import { getUrl, createXHR } from "../util/xhr";
import { SoundBuffer } from "../sound/soundbuffer";
import { AudioStream } from "../sound/audiostream";
import { debug } from "../core/debug";
import { AssetList } from "./assetlist";
import { Atlas } from "../graphics/atlas";
import { SubImage } from "../graphics/subimage";
import { loadAtlas } from "./atlasloader";

const ERR_NOTFOUND = (type: string, name: string) =>
    debugError(`Loader does not contain ${type} named "${name}"!`);

class QueueItem {
    public resource: HTMLImageElement | any;
    public type: string;
    public id: string;
    public url: string;
}

class AudioQueueItem {
    public onCompleteCallbacks: Function[] = [];
    public onErrorCallbacks: Function[] = [];
}

/**
 * Simple asset loader for images and sounds.
 */
export class Loader {

    public onComplete: () => void = () => {};
    public onProgress: (message: string) => void = () => {};
    public onError: (message: string) => void = () => {};

    private _xhrs: XMLHttpRequest[] = [];
    private _failed: boolean = false;

    private _atlases: { [name: string]: Atlas } = {};
    private _images:  { [name: string]: Image } = {};
    private _audio:   { [name: string]: AudioQueueItem } = {};
    private _sounds:  { [name: string]: { sound: Sound, url: string } } = {};
    private _musics:  { [name: string]: { music: Music, url: string } } = {};
    private _fonts:   { [name: string]: Font }  = {};
    private _queue:   QueueItem[] = [];
    private _queueSizeTotal: number = 0; // Size of all queued items
    private _loadedItems: number = 0; // Number of loaded items

    public hasFailed(): boolean {
        return this._failed;
    }

    public getAtlas(id: string): Atlas {
        let atl = this._atlases[id];
        if(!atl) {
            ERR_NOTFOUND("atlas", id);
            return null;
        }
        return atl;
    }

    public getImage(id: string): Image | SubImage {
        // Always search atlases for image first
        for(let a in this._atlases) {
            if(!this._atlases.hasOwnProperty(a)) continue;

            let img = this._atlases[a].getImage(id);
            if(!!img) return img;
        }

        let img = this._images[id];
        if(!img) {
            ERR_NOTFOUND("image", id);
            return null;
        }
        return img;
    }

    public getSound(id: string): Sound {
        let snd = this._sounds[id];
        if(!snd) {
            ERR_NOTFOUND("sound", id);
            return null;
        }
        return snd.sound.clone();
    }

    public getMusic(id: string): Music {
        let mus = this._musics[id];
        if(!mus) {
            ERR_NOTFOUND("music", id);
            return null;
        }
        return mus.music.clone();
    }

    public getFont(id: string): Font {
        let fnt = this._fonts[id];
        if(!fnt) {
            ERR_NOTFOUND("font", id);
            return null;
        }
        return fnt;
    }

    private _queueAtlas(id: string, dataUrl: string, imageUrl: string) {
        let item = new QueueItem();

        let res: { data: string, image: HTMLImageElement, imageloaded: boolean } = {
            data: null,
            image: document.createElement('img'),
            imageloaded: false
        };

        item.type = 'atlas';
        item.id = id;
        item.url = dataUrl;
        item.resource = res;
        this._queueItem(item);

        let imageComplete = () => {
            res.imageloaded = true;
            res.image.onload = undefined;
            res.image.onerror = undefined;
            if(res.data != null) {
                this._itemComplete(item);
            }
        };

        let dataComplete = (json: string) => {
            res.data = json;
            if(res.imageloaded) {
                this._itemComplete(item);
            }
        };

        res.image.onload = imageComplete;
        res.image.onerror = () => {
            this._itemFailed(item);
        };

        res.image.src = getUrl(imageUrl);
        this._xhrs.push(
            createXHR(
                dataUrl,
                (xhr) => dataComplete(xhr.responseText),
                () => this._itemFailed(item),
                'text'
            )
        );
    }

    private _queueImage(id: string, url: string) {
        let item = new QueueItem();
        let img = document.createElement('img');
        img.onload = () => {
            this._itemComplete(item);
        };
        img.onerror = () => {
            this._itemFailed(item);
        };
        item.resource = img;
        item.type = 'image';
        item.id = id;
        item.url = url;
        this._queueItem(item);

        // Start loading of image
        img.src = getUrl(url);
    }

    private _queueAudio(url: string, onComplete: (buf: SoundBuffer) => void, onError: () => void) {
        let item: AudioQueueItem = null;
        if(!this._audio[url]) {
            item = new AudioQueueItem();
            this._xhrs.push(createXHR(url, (xhr) => {
                if(/Edge/.test(navigator.userAgent)) {
                    // Edge is special. Edge doesn't handle fonts OR audio correctly.
                    // Edge does not get audio.
                    for(let cb of item.onCompleteCallbacks) {
                        cb.call(this, null);
                    }
                } else {
                    let buf = new SoundBuffer(xhr.response, () => {
                        for(let cb of item.onCompleteCallbacks) {
                            cb.call(this, buf);
                        }
                    });
                }
            }, () => {
                for(let cb of item.onErrorCallbacks) {
                    cb.call(this);
                }
            }, 'arraybuffer'));
            this._audio[url] = item;
        } else {
            item = this._audio[url];
        }

        item.onCompleteCallbacks.push(onComplete);
        item.onErrorCallbacks.push(onError);
    }

    private _queueSound(id: string, url: string) {
        let item = new QueueItem();
        item.resource = null;
        item.type = 'sound';
        item.id = id;
        item.url = url;
        this._queueAudio(url, (buf) => {
            item.resource = new Sound(buf);
            this._itemComplete(item);
        }, () => {
            this._itemFailed(item);
        });
        this._queueItem(item);
    }

    private _queueMusic(id: string, url: string) {
        let item = new QueueItem();
        item.resource = null;
        item.type = 'music';
        item.id = id;
        item.url = url;
        AudioStream.createFromURL(url).then((stream) => {
            item.resource = new Music(stream);
            this._itemComplete(item);
        }).catch((msg) => {
            debugError("Failed to load music", msg);
            this._itemFailed(item);
        });
        this._queueItem(item);
    }

    private _queueFont(id: string, dataurl: string, imageurl: string) {
        let item = new QueueItem();
        let res: { data: Document, image: HTMLImageElement, imageloaded: boolean } = {
            data: null,
            image: document.createElement('img'),
            imageloaded: false
        };
        item.resource = res;
        item.type = 'font';
        item.url = dataurl;
        item.id = id;
        this._queueItem(item);

        let imageComplete = () => {
            res.imageloaded = true;
            if(res.data != null) {
                this._itemComplete(item);
            }
        };

        let dataComplete = (xml: Document) => {
            res.data = xml;
            if(res.imageloaded) {
                this._itemComplete(item);
            }
        };

        // Start load
        res.image.onload = imageComplete;
        res.image.src = getUrl(imageurl);

        this._xhrs.push(createXHR(dataurl, (xhr) => {
            let parser = new DOMParser();
            let xml = parser.parseFromString(xhr.responseText, 'text/xml');
            dataComplete(xml);
        }, () => this._itemFailed(item), 'text'));
    }

    private _queueWebFont(id: string, urls: string[]) {
        if(!document.fonts) {
            return;
        }

        let item = new QueueItem();
        item.id = id;
        item.type = 'webfont';

        let urlstring = '';
        for(let url of urls) {
            if(urlstring.length > 0) urlstring += ', ';
            urlstring += 'url(' + getUrl(url) + ')';
        }

        item.url = urlstring;
        this._queueItem(item);

        let ff = new FontFace(id, urlstring);
        document.fonts.add(ff);
        document.fonts.load('64px ' + id).then(() => {
            this._itemComplete(item);
        }, () => {
            this._itemFailed(item);
        });
    }

    private _queueItem(item: QueueItem): void {
        this._queue.push(item);
        this._queueSizeTotal++;
    }

    private _dequeueItem(item: QueueItem): void {
        let idx = this._queue.indexOf(item);
        if(idx >= 0) {
            this._queue.splice(idx,1);
        } else {
            debugErrorNamed(this, "Did not find item " + item + " in queue!");
        }
        this._loadedItems++;
    }

    private _itemComplete(item: QueueItem): void {
        this._dequeueItem(item);
        let msg = "[" + this._loadedItems + "/" + this._queueSizeTotal + "] ";

        switch(item.type) {
            case 'atlas':
                this._atlases[item.id] = loadAtlas(new Image(item.resource.image), item.resource.data);
                debug(() => { msg += `Atlas "${item.id}" loaded from ${item.url}` });
                break;
            case 'image':
                this._images[item.id] = new Image(item.resource);
                item.resource.onload = undefined;
                item.resource.onerror = undefined;
                debug(() => { msg += `Image "${item.id}" loaded from ${item.url}` });
                break;
            case 'sound':
                this._sounds[item.id] = { sound: item.resource, url: item.url };
                debug(() => { msg += `Sound "${item.id}" loaded from ${item.url}` });
                break;
            case 'music':
                this._musics[item.id] = { music: item.resource, url: item.url };
                debug(() => { msg += `Music "${item.id}" loaded from ${item.url}` });
                break;
            case 'font':
                let f = this._fonts[item.id] = new Font(new Image(item.resource.image));
                let result = f.initFromXML(<XMLDocument>(item.resource.data));

                if(!result) {
                    debug(() => { msg += `Font "${item.id}" from "${item.url}" failed to load` });
                } else {
                    debug(() => { msg += `Font "${item.id}" loaded from ${item.url}` });
                }
                break;
            case 'webfont':
                debug(() => { msg += `Webfont "${item.id}" loaded` });
                break;
            default:
                msg += "ERROR";
                debug(() => { msg += `: failed to determine type of item ${item}` });
                break;
        }

        this.onProgress(msg);

        if(this._queue.length == 0) {
            this._loadComplete();
        }
    }

    private _itemFailed(item: QueueItem): void {
        this._failed = true;
        this._dequeueItem(item);
        let msg = `[${this._loadedItems}/${this._queueSizeTotal}] `;

        switch(item.type) {
            case 'atlas':
                debug(() => { msg += "Atlas " });
                break;
            case 'image':
                item.resource.onload = undefined;
                item.resource.onerror = undefined;
                debug(() => { msg += "Image " });
                break;
            case 'sound':
                debug(() => { msg += "Sound " });
                break;
            case 'music':
                debug(() => { msg += "Music " });
                break;
            case 'font':
                debug(() => { msg += "Font " });
                break;
            case 'webfont':
                debug(() => { msg += "Webfont" });
                break;
            default:
                debug(() => { msg += "ERROR: failed to determine type of item " + item });
                break;
        }
        msg += `"${item.id}" failed to load from ${item.url}`;
        !!this.onError && this.onError(msg);
    }

    private _loadComplete(): void {
        if(this.onComplete) {
            this.onComplete.call(this);
        }
    }

    /**
     * Return progress of the loading process as an integer percentage
     * (0..100), with 100 being complete.
     */
    public getLoadProgress(): number {
        if(this._queue.length === 0) return 100;
        return ((this._loadedItems / this._queueSizeTotal) * 100) | 0;
    }

    public load(assets: AssetList): void {

        if(assets.size() === 0) {
            this.onComplete();
            return;
        }

        this._failed = false;
        let atlases = assets.getAtlases();
        let images = assets.getImages();
        let sounds = assets.getSounds();
        let musics = assets.getMusics();
        let fonts = assets.getFonts();
        let webfonts = assets.getWebFonts();

        for(let i = 0, l = atlases.length; i < l; ++i) {
            let item = atlases[i];
            this._queueAtlas(item.id, item.dataurl, item.imageurl);
        }

        for(let i = 0, l = images.length; i < l; ++i) {
            let item = images[i];
            this._queueImage(item.id, item.url);
        }

        for(let i = 0, l = sounds.length; i < l; ++i) {
            let item = sounds[i];
            this._queueSound(item.id, item.url);
        }

        for(let i = 0, l = musics.length; i < l; ++i) {
            let item = musics[i];
            this._queueMusic(item.id, item.url);
        }

        for(let i = 0, l = fonts.length; i < l; ++i) {
            let f = fonts[i];
            this._queueFont(f.id, f.dataurl, f.imageurl);
        }

        for(let i = 0, l = webfonts.length; i < l; ++i) {
            let f = webfonts[i];
            this._queueWebFont(f.id, f.urls);
        }

        for(let i = 0; i < this._xhrs.length; ++i) {
            this._xhrs[i].send();
        }
        this._xhrs = [];

        // Loading should start automagically

    }

}
