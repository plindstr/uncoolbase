// Replace a file extension (.something) with either nothing (for id) or with another ext (e.g.: .png)
const replaceExt = (url: string, replacement: string = '') => url.replace(/\.....?.?$/, replacement)

/**
 * List used by Loader
 */
export class AssetList {

    private _atlases:  { id: string, dataurl: string, imageurl: string }[] = [];
    private _fonts:    { id: string, dataurl: string, imageurl: string }[] = [];
    private _webfonts: { id: string, urls: string[]                    }[] = [];
    private _images:   { id: string, url:  string                      }[] = [];
    private _sounds:   { id: string, url:  string                      }[] = [];
    private _musics:   { id: string, url:  string                      }[] = [];

    private _imageBaseUrl:   string = "";
    private _musicBaseUrl:   string = "";
    private _soundBaseUrl:   string = "";
    private _fontBaseUrl:    string = "";
    private _webfontBaseUrl: string = "";

    public size(): number {
        return this._atlases.length +
            this._images.length +
            this._sounds.length +
            this._musics.length +
            this._fonts.length  +
            this._webfonts.length;
    }

    public setImageBaseURL(url: string): AssetList {
        this._imageBaseUrl = !url ? "" : url;
        return this;
    }

    public setSoundBaseURL(url: string): AssetList  {
        this._soundBaseUrl = !url ? "" : url;
        return this;
    }

    public setMusicBaseURL(url: string): AssetList {
        this._musicBaseUrl = !url ? "" : url;
        return this;
    }

    public setFontBaseURL(url: string): AssetList {
        this._fontBaseUrl = !url ? "" : url;
        return this;
    }

    public setWebfontBaseURL(url: string): AssetList {
        this._webfontBaseUrl = !url ? "" : url;
        return this;
    }

    public addAtlas(dataurl: string, imageurl: string = replaceExt(dataurl, '.png'), id: string = replaceExt(dataurl)) {
        dataurl = this._imageBaseUrl + dataurl;
        imageurl = this._imageBaseUrl + imageurl;
        this._atlases.push({id: id, dataurl: dataurl, imageurl: imageurl});
        return this;
    }

    public addImage(url: string, id: string = replaceExt(url)): AssetList {
        this._images.push({id: id, url: this._imageBaseUrl + url});
        return this;
    }

    public addSound(url: string, id: string = replaceExt(url)): AssetList {
        this._sounds.push({id: id, url: this._soundBaseUrl + url});
        return this;
    }

    public addMusic(url: string, id: string = replaceExt(url)): AssetList {
        this._musics.push({id: id, url: this._musicBaseUrl + url});
        return this;
    }

    public addFont(dataurl: string, imageurl: string = replaceExt(dataurl, '.png'), id: string = replaceExt(dataurl)): AssetList {
        dataurl = this._fontBaseUrl + dataurl;
        imageurl = this._fontBaseUrl + imageurl;
        this._fonts.push({id: id, dataurl: dataurl, imageurl: imageurl});
        return this;
    }

    public addWebFont(mainurl: string, ... otherurls: string[]): AssetList {
        let ref = {
            id: replaceExt(mainurl),
            urls: [ this._webfontBaseUrl + mainurl ]
        };
        if(otherurls) {
            for(let url of otherurls) {
                ref.urls.push(this._webfontBaseUrl + url);
            }
        }
        this._webfonts.push(ref);
        return this;
    }

    public getAtlases() {
        return this._atlases;
    }

    public getImages() {
        return this._images;
    }

    public getSounds() {
        return this._sounds;
    }

    public getMusics() {
        return this._musics;
    }

    public getFonts() {
        return this._fonts;
    }

    public getWebFonts() {
        return this._webfonts;
    }
}
