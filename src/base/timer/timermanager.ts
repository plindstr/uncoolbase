import { SafeList } from "../mem";
import { Timer } from "./timer";
import { DelayTimer } from "./delaytimer";

//
// Code for managing timers
//

const repatedTimers = new SafeList<Timer>();
const delayTimers = new SafeList<DelayTimer>();

export const registerTimer = (timer: Timer) => repatedTimers.add(timer);

export const unregisterTimer = (timer: Timer) => repatedTimers.remove(timer);

export const registerDelayTimer = (timer: DelayTimer) => delayTimers.add(timer);

export const unregisterDelayTimer = (timer: DelayTimer) => delayTimers.remove(timer);

export const updateTimers = (delta: number) => {
    delta *= 1000;
    repatedTimers.forEach((t) => t.update(delta));
    delayTimers.forEach((t) => t.update(delta));
}
