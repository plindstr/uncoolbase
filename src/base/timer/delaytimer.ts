import { unregisterDelayTimer, registerDelayTimer } from "./timermanager";

/**
 * Timer that waits for a certain amount of time
 * and then executes a callback, destroying itself
 * once it's done.
 */
export class DelayTimer {

    private _callback: () => void;
    private _delay: number;
    private _elapsed: number;

    /**
     * Create a timer that runs a callback after a certain number of
     * milliseconds and then self-destructs.
     *
     * @param callback function to run when timer is complete
     * @param delay number of milliseconds to wait before triggering callback
     */
    constructor(callback: () => void, delay: number = 1000) {
        this._callback = callback;
        this._delay = delay;
        this._elapsed = 0;
    }

    public getDelay(): number {
        return this._delay;
    }

    public setDelay(msec: number): DelayTimer {
        this._delay = Math.max(1, msec);
        return this;
    }

    public reset(): DelayTimer {
        this._elapsed = 0;
        return this;
    }

    /**
     * Shorthand for base.addTimer(mytimer)
     */
    public start(): DelayTimer {
        registerDelayTimer(this);
        return this;
    }

    /**
     * Shorthand for base.removeTimer(mytimer)
     */
    public stop(): DelayTimer {
        unregisterDelayTimer(this);
        return this;
    }

    /**
     * Update timer by a certain number of milliseconds
     */
    public update(elapsed_msec: number): DelayTimer {
        var e = this._elapsed + elapsed_msec;
        if(e >= this._delay) {
            this._callback();
            this.stop();
        }
        this._elapsed = e;
        return this;
    }

}

export function delay(time_millis: number, callback: () => void): void {
    new DelayTimer(callback, time_millis).start();
}

/**
 * Create a DelayTimer with the specific delay and start it, wait for
 * it to complete.
 *
 * @param delay_millis time to wait, in milliseconds.
 */
export async function delayAsync(delay_millis: number): Promise<void> {
    return new Promise<void>((res, rej) => {
        let t = new DelayTimer(res, delay_millis);
        t.start();
    });
}
