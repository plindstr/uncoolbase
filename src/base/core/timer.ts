import { registerInitRoutine } from "../init";
import { debugLog } from "./logging";
import { CallbackList } from "../mem/callbacklist";

let time_current: number = 0;
let time_last: number = 0;
let time_delta_millis: number = 0;
let time_delta: number = 0;
let time_fps_update: number = 0;
let framerate: number = 60;
let measured_fps: number = 0;
let fpsUpdateHandlers = new CallbackList<(fps: number) => void>();

export function getTimestamp(): number {
    return performance.now();
}

/**
 * Return current timestamp in milliseconds
 */
export function getTimeCurrent(): number {
    return time_current;
}

/**
 * Return the timestamp of the previous frame in milliseconds
 */
export function getTimeLast(): number {
    return time_last;
}

/**
 * Return the time in seconds between the current and the previous frame
 */
export function getTimeDelta(): number {
    return time_delta;
}

/**
 * Return time in milliseconds between the current and previous frame
 * (you're almost certainly looking for the version that returns time in seconds)
 */
export function getTimeDeltaMillis(): number {
    return time_delta_millis;
}

/**
 * Return the measured frame rate, updated at 1 second intervals
 */
export function getFrameRate(): number {
    return framerate;
}

export function addFPSUpdateHandler(handler: (fps: number) => void): void {
    fpsUpdateHandlers.add(handler);
}

export function removeFPSUpdateHandler(handler: (fps: number) => void): void {
    fpsUpdateHandlers.remove(handler);
}

export function clearFPSUpdateHandlers() {
    fpsUpdateHandlers.clear();
}

export function updateTime(timestamp: number): void {
    time_delta_millis = timestamp - time_current;
    time_last = time_current;
    time_current = timestamp;
    if(time_delta_millis > 667) {
        time_delta_millis = 0;
    }
    time_delta = time_delta_millis * 0.001;

    // FPS counter
    measured_fps++;
    time_fps_update += time_delta_millis;
    if(time_fps_update >= 1000) {
        framerate = measured_fps;
        measured_fps = 0;
        time_fps_update -= 1000;
        fpsUpdateHandlers.run(framerate);
    }
}

//
// Initialize timer
//

registerInitRoutine(() => {
    debugLog("Init core timing loop");
    requestAnimationFrame(updateTime)
}, 200);

