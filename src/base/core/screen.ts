//
// Code for handling the main render output
//

import { debugLog, debugError } from "./logging";
import { getContainer } from "./container";
import { registerInitRoutine } from "../init";
import { inputOffset, setInputSizeRatio } from "../input/coreinput";
import { Vec2 } from "../math/vec";
import { Rect } from "../math/rect";
import { Surface } from "../graphics/surface";
import { gl_setDefaultContext, gl_getDefaultContext, __gl_hasDefaultContext } from "../graphics/gl/glbase";
import { CallbackList } from "../mem/callbacklist";
import * as screenfull from "../../lib/screenfull"
import { assertFail } from "./assert";

const canvas = document.createElement('canvas');
canvas.style.left = "50%";
canvas.style.top = "50%";
canvas.style.transform = "translate(-50%,-50%)";
canvas.style.position = "absolute";

let screen = new Surface(canvas);
let triggerFullscreen = false;
let triggerMouseCapture = false;
let sizeratio = 1.0;
let sizescaling = 1.0;
let aspectScaling = false;
let onResizeRun = false;

let screen_w = 800;
let screen_h = 600;

function requestFullscreenActual(): boolean {
    triggerFullscreen = false;
    try {
        screenfull.request(getContainer());
        return true;
    } catch(err) {
        debugError("Failed to request fullscreen mode; error", err);
    }
    resize();
    return false;
}

function requestMouseCaptureActual(): boolean {
    triggerMouseCapture = false;
    (<any>canvas).requestPointerLock();
    return true;
}

const resize = () => {
    if(!onResizeRun) {
        resizehandlers.run(getOutputWidth(), getOutputHeight());
        onResizeRun = true;
    }

    let width = getOutputWidth()
    let height = getOutputHeight();
    let cvswidth: number;
    let cvsheight: number;
    if(aspectScaling) {
        let wratio = width / screen_w;
        let hratio = height / screen_h;
        sizeratio = Math.min(wratio,hratio) / window.devicePixelRatio;
    } else {
        sizeratio = 1.0;
    }
    cvswidth = Math.round(screen_w * sizeratio) | 0;
    cvsheight = Math.round(screen_h * sizeratio) | 0;
    canvas.style.width = cvswidth + 'px';
    canvas.style.height = cvsheight + 'px';
    onResizeRun = false;

    let r = canvas.getBoundingClientRect();
    inputOffset.x = r.left;
    inputOffset.y = r.top;
    setInputSizeRatio(sizeratio);

    debugLog("Output resized: virtual " + Screen.WIDTH + "x" + Screen.HEIGHT + ", context " + screen_w + "x" + screen_h + ", element " + cvswidth + "x" + cvsheight + ", ratio " + sizeratio);
};

const resizehandlers = new CallbackList<(w: number, h: number)=>void>();

/**
 * Add a function to call when the output window gets resized. This function gets
 * run once when the display has been resized.
 *
 * @param handler a function that takes the new width and height as parameters.
 */
export function addResizeHandler(handler: (w: number, h: number) => void): void {
    resizehandlers.add(handler);
}

/**
 * Remove a previously added resize handler
 *
 * @param handler handler function
 */
export function removeResizeHandler(handler: (w: number, h : number) => void): void {
    resizehandlers.remove(handler);
}

/**
 * Clear all previously added resize handlers
 */
export function clearResizeHandlers(): void {
    resizehandlers.clear();
}

export function simulateResize(): void {
    resize();
}

export function setAspectRatioScaling(enabled: boolean): void {
    aspectScaling = !!enabled;
}

/**
 * Get the width of the native display
 */
export function getNativeWidth(): number {
    try {
        return window.screen.width;
    } catch(e) {
        return 0;
    }
}

/**
 * Get the height of the native display
 */
export function getNativeHeight(): number {
    try {
        return window.screen.height;
    } catch(e) {
        return 0;
    }
}

/**
 * Get the width of the displayable region of the current graphics output, in pixels
 */
export function getOutputWidth(): number {
    return window.innerWidth * window.devicePixelRatio;
}

/**
 * Get the height of the displayable region of the current graphics output, in pixels
 */
export function getOutputHeight(): number {
    return window.innerHeight * window.devicePixelRatio;
}

/**
 * Get access to the primary render surface.
 */
export function getScreen(): Surface {
    return screen;
}

/**
 * Set game window size. Do not call getScreen().setSize(),
 * since that will not update the container div for the canvas,
 * but just the canvas itself.
 */
export function setScreenSize(w: number, h: number): void {
    let real_w = w |= 0;
    let real_h = h |= 0;
    screen_w = (real_w * sizescaling) | 0;
    screen_h = (real_h * sizescaling) | 0;
    Screen.TOP_LEFT.setXY(screen_w / -2, screen_h / -2);
    Screen.TOP_CENTER.setXY(0, screen_h / -2);
    Screen.TOP_RIGHT.setXY(screen_w / 2, screen_h / -2);
    Screen.CENTER_LEFT.setXY(screen_w / -2, 0);
    Screen.CENTER.setXY(0, 0);
    Screen.CENTER_RIGHT.setXY(screen_w / 2, 0);
    Screen.BOTTOM_LEFT.setXY(screen_w / -2, screen_h / 2);
    Screen.BOTTOM_CENTER.setXY(0, screen_h / 2);
    Screen.BOTTOM_RIGHT.setXY(screen_w / 2, screen_h / 2);
    Screen.WIDTH = screen_w;
    Screen.HEIGHT = screen_h,
    Screen.HALF_WIDTH = screen_w / 2;
    Screen.HALF_HEIGHT = screen_h / 2;
    Screen.AREA.position.setXY(0, 0);
    Screen.AREA.size.setXY(screen_w, screen_h)
    screen.setSize(real_w, real_h);
    debugLog("Set screen size to " + w + "x" + h + " with scaling " + sizescaling + "; real " + real_w + "x" + real_h);

    resize();
}

/**
 * Set the size of the main output canvas, along with internal output
 * state values, to a scaled version of a reference resolution.
 * For example, a reference resolution of 1920x1080 with a scaling of 0.5
 * would result in an actual output of 960x540, but the framework would
 * still behave as if the application was running in FullHD (e.g. the 2D
 * scene graph would still display a 1920x1080 point area).
 *
 * This function exists in order to facilitate easy down- and upscaling
 * of a fixed aspect ratio game.
 *
 * @param reference_width an integer value representing desired horizontal resolution
 * @param reference_height an integer value representing desired vertical resolution
 * @param scaling a floating point value by which the reference width and height are multiplied
 */
export function setScreenSizeScaled(reference_width: number, reference_height: number, scaling: number): void {
    reference_width |= 0;
    reference_height |= 0;
    sizescaling = 1.0 / scaling;
    setScreenSize(reference_width * scaling, reference_height * scaling);
}

export function setScreenScaling(factor: number): void {
    sizescaling = factor;
}

export function getScreenScaling(): number {
    return sizescaling;
}

/**
 * Convenient access to current main output size values.
 * Please note, that these values will change to reflect
 * the current screen size.
 */
export const Screen = {
    TOP_LEFT: new Vec2(),
    TOP_CENTER: new Vec2(),
    TOP_RIGHT: new Vec2(),
    CENTER_LEFT: new Vec2(),
    CENTER: new Vec2(),
    CENTER_RIGHT: new Vec2(),
    BOTTOM_LEFT: new Vec2(),
    BOTTOM_CENTER: new Vec2(),
    BOTTOM_RIGHT: new Vec2(),
    SIZE: new Vec2(),
    AREA: new Rect(),
    WIDTH: 0,
    HEIGHT: 0,
    HALF_WIDTH: 0,
    HALF_HEIGHT: 0
};

/**
 * Access the screen's 2D rendering context.
 *
 * Note: if this function is called _AFTER_ the screen has been initialized
 * in WebGL mode, an error will be thrown.
 */
export function getContext2D(): CanvasRenderingContext2D {
    try {
        return screen.getContext2D();
    } catch(e) {
        assertFail("Cannot get 2D context; screen may have been initialized with WebGL context");
        throw 0;
    }
}

/**
 * Access the screen's WebGL rendering context.
 *
 * Note: if this function is called _AFTER_ the screen has been initialized in
 * Canvas 2D mode, an error will be thrown.
 */
export function getContextGL(): WebGLRenderingContext {
    try {
        let context = screen.getContextGL();
        if(!__gl_hasDefaultContext()) {
            gl_setDefaultContext(context);
        }
        return context;
    } catch(e) {
        assertFail("Cannot get GL context; screen may have been initialized with Canvas2D context");
        throw 0;
    }
}

export function requestMouseCapture(): void {
    triggerMouseCapture = true;
}

export function exitMouseCapture(): void {
    try {
        (<any>document).exitPointerLock();
    } catch(ignore) {
        debugLog("Cannot exit pointer lock");
    }
}

export function setMouseCursorVisible(visible: boolean): void {
    if(visible) {
        screen.getElement().style.cursor = 'default';
    } else {
        screen.getElement().style.cursor = 'none';
    }
}

export function requestFullscreen(): void {
    triggerFullscreen = true;
}

export function exitFullscreen(): boolean {
    try {
        screenfull.exit();
        return true;
    } catch(ignore) {
        debugError("Failed to exit fullscreen mode");
    }
    return false;
}

registerInitRoutine(() => {

    debugLog("Init display");

    // Add output canvas
    getContainer().appendChild(canvas);

    let updateTriggers = () => {
        if(triggerFullscreen) {
            requestFullscreenActual();
        }
        if(triggerMouseCapture) {
            requestMouseCaptureActual();
        }
    };

    document.addEventListener('keyup', () => {
        updateTriggers();
    });

    document.addEventListener('mouseup', () => {
        updateTriggers();
    });

    document.addEventListener('touchend', () => {
        updateTriggers();
    });

    window.onresize = resize;
    resize();
});
