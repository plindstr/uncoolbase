import { isDebug } from "./debug";

//
// Assertion facilities
//
// This file needs to be kept import-free since
// pretty much everything depends on it!
//

const DEFAULT_ERROR = "unknown error";
const assertFailureCallbacks: Function[] = [];

/**
 * Throw error if condiditon is not met. Stripped out by
 * UncoolPack during stripping of debug symbols.
 *
 * @param condition a boolean expression
 * @param errorMessage message to print on failure
 */
export function assert(condition: boolean, errorMessage: string = DEFAULT_ERROR): void {
    if(!condition) {
        assertFail(errorMessage);
    }
}

/**
 * Immediately report assertion failure and throw.
 * Stripped out by UncoolPack during stripping of 
 * debug symbols.
 * 
 * @param errorMessage message to print on failure
 */
export function assertFail(errorMessage: string = DEFAULT_ERROR): void {
    console.error("*** ASSERTION FAILED ***")
    if(isDebug()) console.error("Message: " + errorMessage);

    for(let fn of assertFailureCallbacks) {
        fn.call(null);
    }
    
    if(isDebug()) console.error("Stack trace in console");
    
    console.trace();
    debugger;
    throw 0;
}

/**
 * Register a callback to run when an assertion fails.
 * 
 * @param callback function to call
 */
export function onAssertFailure(callback: Function): void {
    assertFailureCallbacks.push(callback);
}
