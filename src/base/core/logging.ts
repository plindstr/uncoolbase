import { isDebug, debug } from "./debug";
import { Ringbuffer } from "../mem/ringbuffer";

const logbuffer = new Ringbuffer<string>(16);

export const nameof = (cls: Object): string => `${cls.constructor.name}:`;

/**
 * Print a message to console.
 *
 * @param msg
 */
export const log = (...msg: any[]): void => {
    let str = msg.join(' ');
    console.log.call(console, `[INFO] ${str}`);
    logbuffer.add(str);
}

export const warn = (...msg: any[]): void => {
    let str = msg.join(' ');
    console.warn.call(console, `[WARN] ${str}`);
    logbuffer.add(str);
}

export const error = (...msg: any[]): void => {
    let str = msg.join(' ');
    console.log.call(console, `[ERROR] ${str}`);
    logbuffer.add(str);
}

/**
 * Print a log message to console, but only if in debug mode
 * This call will be removed by UncoolPack if the -s switch is provided.
 *
 * @param msg string to print
 */
export const debugLog = (...msg: any[]): void => {
    if(isDebug()) log(msg);
}

/**
 * Print a log message to console, but only if in debug mode
 * This call will be removed by UncoolPack if the -s switch is provided.
 *
 * @param cls class name of sender
 * @param msg string to print
 */
export const debugLogNamed = (cls: Object, ...msg: any[]): void => {
    if(isDebug()) log(nameof(cls), msg);
}

/**
 * Print a warning message to console, but only if in debug mode
 * This call will be removed by UncoolPack if the -s switch is provided.
 *
 * @param msg string to print
 */
export const debugWarn = (...msg: any[]): void => {
    if(isDebug()) warn(msg);
}

/**
 * Print a warning message to console, but only if in debug mode
 * This call will be removed by UncoolPack if the -s switch is provided.
 *
 * @param cls class name of sender
 * @param msg string to print
 */
export const debugWarnNamed = (cls: Object, ...msg: any[]): void => {
    if(isDebug()) warn(nameof(cls), msg);
}

/**
 * Print an error message to console, but only if in debug mode.
 * This call will be removed by UncoolPack if the -s switch is provided.
 *
 * @param msg string to print
 */
export const debugError = (...msg: any[]): void => {
    if(isDebug()) error(msg);
}

/**
 * Print an error message to console, but only if in debug mode.
 * This call will be removed by UncoolPack if the -s switch is provided.
 *
 * @param cls class name of sender
 * @param msg string to print
 */
export const debugErrorNamed = (cls: Object, ...msg: any[]): void => {
    if(isDebug()) error(nameof(cls), msg);
}

export const createLogger = (debugName: string): Logger => {
    let l = null;
    debug(() => l = new Logger(debugName));
    if(!l) l = new Logger('');
    return l;
}

/**
 * Named logger class.
 * May get some additional functionality in the future
 */
export class Logger {

    private _prefix: string;

    constructor(name: string) {
        this._prefix = (name != '' ? `${name}:` : '');
    }

    public log(...msg: any[]) {
        log.apply(null, this._prefix === '' ? msg : [ this._prefix ].concat(msg));
    }

    public warn(...msg: any[]) {
        warn.apply(null, this._prefix === '' ? msg : [ this._prefix ].concat(msg));
    }

    public error(...msg: any[]) {
        error.apply(null, this._prefix === '' ? msg : [ this._prefix ].concat(msg));
    }

    public debugLog(...msg: any[]) {
        if(isDebug()) log.apply(null, this._prefix === '' ? msg : [ this._prefix ].concat(msg));
    }

    public debugWarn(...msg: any[]) {
        if(isDebug()) warn.apply(null, this._prefix === '' ? msg : [ this._prefix ].concat(msg));
    }

    public debugError(...msg: any[]) {
        if(isDebug()) error.apply(null, this._prefix === '' ? msg : [ this._prefix ].concat(msg));
    }

}
