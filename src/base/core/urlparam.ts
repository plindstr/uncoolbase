//
// URL param handling
//

export function hasUrlParam(param: string): boolean {
    let url = new URL(window.location.href);
    return url.searchParams.get(param) != null;
}

export function getUrlParam(param: string): string {
    let url = new URL(window.location.href);
    return url.searchParams.get(param);
}

export function getUrlDomain(): string {
    return window.location.hostname;
}
