import { hasUrlParam } from "./urlparam";

let _debug: boolean = null;
export function isDebug(): boolean {
    if(_debug === null) {
        _debug = !hasUrlParam('release');
    }
    return _debug;
}

/**
 * Force debug mode, unless "?release" is added to url
 */
export function forceDebug(): void {
    _debug = !hasUrlParam('release');
}

/**
 * Call a function only if in debug mode. This call will be
 * removed by UncoolPack if the -s switch is provided.
 *
 * @param fn code to run
 */
export function debug(fn: () => void): void {
    if(isDebug()) {
        fn.call(null);
    }
}

