import { debug } from "./debug";
import { assert, onAssertFailure } from "./assert";
import { registerInitRoutine } from "../init";
import { debugLog } from "./logging";

const maxElements = 100;

let debugdiv: HTMLDivElement = null;

/**
 * Creates a debug output container
 *
 * @param id it to use for new container
 */
const createDebugContainer = (id: string): HTMLDivElement => {
    let container = document.getElementById(id) as HTMLDivElement;
    if(!container) {
        container = document.createElement('div');
        container.id = id;
    }
    assert(container.tagName === 'DIV', "'debug' element must be a div");

    // Set container style
    let s = container.style;
    s.position = 'absolute';
    s.visibility = 'hidden';
    s.overflow = 'hidden';
    s.color = '#fff';
    s.background = 'rgba(20, 20, 20, 0.7)';
    s.width = '100%';
    s.height = '100%';
    s.display = 'block';
    s.pointerEvents = 'none';
    s.zIndex = '999';
    s.fontFamily = 'monospace, monospace';

    // Create watermark thingy
    let watermark = document.createElement('div');
    watermark.innerHTML = '<h1>UncoolBase</h1><h2>Debug Mode</h2>'
    s = watermark.style;
    s.position = 'absolute';
    s.color = 'rgba(233,255,212,0.24)';
    s.display = 'block';
    s.zIndex = '-1';
    s.right = '15px';
    s.bottom = '15px';
    container.appendChild(watermark);

    // Create content
    let content = document.createElement('div');
    s = content.style;
    s.position = 'absolute';
    s.overflow = 'hidden';
    s.color = '#fff';
    s.width = '100%';
    s.height = '100%';
    s.display = 'block';
    s.pointerEvents = 'none';
    container.appendChild(content);

    // Allow toggling debug output with tilde
    window.addEventListener('keyup', (e) => {

        // TODO: make key configurable
        if(e.keyCode === 220) {
            setDebugVisible(!isDebugVisible());
        }
    });

    // Rebind logging to output to debug div
    const nativelog = {
        log: console.log,
        warn: console.warn,
        error: console.error
    };

    // Hook into console output functions
    for(let prop of [ 'log', 'warn', 'error' ]) {
        (<any>console)[prop] = function() {

            let line = "";
            for(let a of arguments) {
                line += a + " ";
            }

            // Remove oldest child
            if(content.childElementCount > maxElements) {
                content.removeChild(content.children[0]);
            }

            let color = '';
            switch(prop) {
                case 'log':
                color = '#fff';
                break;
                case 'warn':
                color = '#ff6';
                break;
                case 'error':
                color = '#f66';
                break;
            }

            let child = document.createElement('pre');
            child.style.color = color;
            child.innerText = line;
            content.appendChild(child);
            child.scrollIntoView();

            (<any>nativelog)[prop].apply(window.console, arguments);
        };
    }

    document.body.appendChild(container);
    return container;
}

/**
 * Set visibility of the debug overlay
 *
 * @param visible true to show, false to hide
 */
export function setDebugVisible(visible: boolean): void {
    if(!!debugdiv) {
        debugdiv.style.visibility = visible ? 'visible' : 'hidden';
    }
}

/**
 * Check if debug functionality is currently visible
 */
export function isDebugVisible(): boolean {
    if(!!debugdiv) {
        return debugdiv.style.visibility === 'visible';
    }
    return false;
}

export const getDebugContainer = () => debugdiv;

registerInitRoutine(() => {
    debugLog("Init debug overlay");

    debugdiv = ((): HTMLDivElement => {
        let divname = 'debug';  // TODO: allow customizable target name
        let elem = null;
        debug(() => {
            elem = createDebugContainer(divname);
        });
        return elem;
    })();

    onAssertFailure(() => setDebugVisible(true));
}, 50);
