import { registerInitRoutine } from "../init";
import { debugLog } from "./logging";

//
// Handles the div element that contains the primary render canvas (Screen)
//

const createContainer = (id: string) => {
    let elem = document.createElement('div');
    elem.id = id;
    document.body.appendChild(elem);
    return elem;
}

const formatContainer = (elem: HTMLDivElement) => {
    let s = elem.style;
    s.position = 'absolute';
    s.top = '50%';
    s.left = '50%';
    s.transform = 'translate(-50%,-50%)';
    s.overflow = 'hidden';
    s.backgroundColor = '#000';
    s.color = '#fff';
    s.textAlign = 'center';
    s.fontSize = '12pt';
    s.visibility = 'hidden';
}

let gamediv: HTMLDivElement = null;

export const getContainer = () => gamediv;

export const getContainerWidth = (): number => gamediv.clientWidth;

export const getContainerHeight = (): number => gamediv.clientHeight;

export const setContainerSize = (w: string, h: string) => {
    gamediv.style.width = w;
    gamediv.style.height = h;
}

export const setContainerSizeXY = (w: number, h: number) => {
    setContainerSize((w | 0) + 'px', (h | 0) + 'px');
}

export const setContainerColor = (rgb: string) => {
    gamediv.style.backgroundColor = rgb;
}

export const setCursorVisible = (enable: boolean) => {
    gamediv.style.cursor = enable ? 'default' : 'none';
}

registerInitRoutine(() => {
    debugLog("Init app container");

    gamediv = (() => {
        let divname = 'game'; // TODO: allow setting different target div name
        let elem = document.getElementById(divname) as HTMLDivElement;
        if(!elem) {
            elem = createContainer(divname);
        }
        formatContainer(elem);
        return elem;
    })();
    setContainerSize('100%', '100%');
}, 100);
