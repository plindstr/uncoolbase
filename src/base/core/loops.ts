import { SafeList } from "../mem/safelist";
import { getTimeCurrent } from "./timer";
import { setDebugVisible } from "./debugcontainer";
import { isDebug } from "./debug";
import { debugError } from "./logging";

class FixedLoop {
    public maxRepeat: number = 2;
    public interval: number = 0;
    public jitter: number = 0;
    public lastUpdate: number = 0;
    public callback: Function = null;
}

let loops      = new SafeList<(delta: number) => void>();
let loopsFixed = new SafeList<FixedLoop>();

export function addLoop(loop: (delta: number) => void): void {
    loops.add(loop);
}

/**
 * Add a loop that is called a fixed amount of times per second
 *
 * @param loop callback function for each frame
 * @param fps frames per second target
 * @param jitter percentage of loop time that the update routine is allowed to undershoot. Set to 0 to enforce accurate fps
 * @param maxRepeat maximum number of repeats of the loop
 */
export function addLoopFixed(loop: Function, fps: number, jitter: number = 0.15, maxRepeat: number = 4): void {
    let l = new FixedLoop();
    l.callback = loop;
    l.maxRepeat = Math.max(maxRepeat | 0, 0);
    l.interval = 1000.0 / fps;      // interval time is in milliseconds
    l.jitter = l.interval * jitter;   // allow undershooting time by this amount
    l.lastUpdate = getTimeCurrent();
    loopsFixed.add(l)
}

export function removeLoop(loop: (delta: number) => void): void {
    loops.remove(loop);
}

export function removeLoopFixed(loop: (delta: number) => void): void {
    loopsFixed.forEach((item) => {
        if(item.callback == loop) {
            loopsFixed.remove(item);
        }
    });
}

function updateFixedLoops(): void {
    let tm = getTimeCurrent();
    loopsFixed.forEach((item) => {

        if(tm - item.lastUpdate >= item.interval * item.maxRepeat) {
            // Do not update more than once if we're past the maximum number of repeats
            // This way we can avoid a runaway feedback effect
            item.lastUpdate = tm;
            item.callback(item.interval * 0.001);
        } else {

            // Update less than maxRepeats times while taking jitter into account
            let repeats = 0;
            let elapsedTotal = 0;
            let lastUpdate = item.lastUpdate;
            let interval = item.interval;
            let jitter = item.jitter;

            while(tm - lastUpdate >= interval - jitter) {
                elapsedTotal += Math.min(interval, tm - lastUpdate);
                lastUpdate += elapsedTotal;
                ++repeats;
            }

            for(let i = 0, c = repeats; i < c; ++i) {
                item.callback(interval * 0.001);
            }

            item.lastUpdate += elapsedTotal;
        }

    });

}

export function updateLoops(delta: number): void {
    try {
        updateFixedLoops();
    } catch(e) {
        debugError(`Uncaught ${e} during fixed loop update`);
        if(isDebug()) {
            setDebugVisible(true);
        }
        throw e;
    }

    try {
        loops.forEach(loop => loop(delta))
    } catch(e) {
        debugError(`Uncaught ${e} during frame loop update`);
        if(isDebug()) {
            setDebugVisible(true);
        }
        throw e;
    }
}
