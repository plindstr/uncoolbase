import { array_add_unique, array_remove, array_clear } from "./array"

export class CallbackList<T> {
    private _callbacks: T[] = [];

    public add(fn: T): void {
        array_add_unique(this._callbacks, fn);
    }

    public remove(fn: T): void {
        array_remove(this._callbacks, fn);
    }

    public clear(): void {
        array_clear(this._callbacks);
    }

    public run(...args: any[]) {
        for(let fn of this._callbacks) {
            (fn as any).call(null, args);
        }
    }
}
