import { assert } from "../core/assert";

/**
 * List that retains and returns instances of
 * a specified class or structure. Useful in
 * things like retained display list records, etc.
 *
 * This list will allocate objects as needed.
 * The objects will be reused once the list is cleared.
 */
export class StaticPool<T> {

    private _data: Array<T>;
    private _pointer: number;

    private _fn_create: () => T;
    private _fn_init: (obj:T) => void;

    constructor(
        create: () => T,
        init: (obj:T) => void = () => {},
        initialSize: number = 16) {

        let sz = Math.max(4,initialSize) | 0;
        this._pointer = 0;
        this._fn_create = create;
        this._fn_init = init;

        this._data = new Array<T>(sz);
        for(let i = 0; i < sz; ++i) {
            this._data[i] = create();
        }
    }

    private _grow() {
        let data = this._data;
        let sz = data.length;
        let next = new Array<T>(sz << 1);
        let create = this._fn_create;
        for(let i = 0; i < sz; ++i) {
            next[i] = data[i];
            next[i + sz] = create();
        }
        this._data = next;
    }

    public alloc(): T {
        if(this._pointer == this._data.length) {
            this._grow();
        }

        let obj = this._data[this._pointer++];
        this._fn_init(obj);
        return obj;
    }

    public get(idx: number): T {
        assert(idx >= 0 && idx < this._pointer, "Index not allocated");
        return this._data[idx];
    }

    public size(): number {
        return this._pointer;
    }

    public clear(): void {
        this._pointer = 0;
    }
}
