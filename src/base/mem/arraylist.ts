import { assert } from "../core/assert";
import { max } from "../math/coremath";
import { debugLog } from "../core/logging";

/**
 * An ArrayList class, maintains a steady-sized array
 */
export class ArrayList<T> {

    private _data: T[] = null;
    private _size: number = 0;

    private _grow(): void {
        let data = this._data;
        let next = new Array<T>(this._data.length << 1);
        let sz = this._size;
        for(let i = 0; i < sz; ++i) next[i] = data[i];
        this._data = next;
    }

    constructor(size: number = 16) {
        size = max(size, 1);
        this._data = new Array<T>(size);
    }

    /**
     * Get the number of items added to this list
     */
    public size(): number {
        return this._size;
    }

    /**
     * Get access to the data array
     */
    public getData(): T[] {
        return this._data;
    }

    /**
     * Check if list contains an item
     * 
     * @param item an object of type T
     */
    public contains(item: T): boolean {
        let data = this._data;
        let sz = this._size;
        for(let i = 0; i < sz; ++i) {
            if(data[i] === item) return true;
        }
        return false;
    }

    /**
     * Find the index of an item, or -1 if it does not exist
     * 
     * @param item an object of type T
     */
    public indexOf(item: T): number {
        let data = this._data;
        let sz = this._size;
        for(let i = 0; i < sz; ++i) {
            if(data[i] === item) return i;
        }
        return -1;
    }

    /**
     * Add an object to the end of the list.
     *
     * @param item an object of type T
     */
    public add(item: T): void {
        let idx = this._size;
        if(idx === this._data.length) this._grow();
        this._data[idx] = item;
        ++this._size;
    }

    /**
     * Add an object to the array if it doesn't
     * already exist in the array.
     *
     * @param item an object of type T
     */
    public addUnique(item: T): boolean {
        if(this._data.indexOf(item) === -1) {
            this.add(item);
            return true;
        }
        return false;
    }

    /**
     * Remove an object from the list. Not stable.
     *
     * @param item reference to an item in the list
     */
    public remove(item: T): boolean {
        let data = this._data;
        let idx = data.indexOf(item);

        if(idx >= 0) {
            let last = --this._size;
            data[idx] = data[last];
            data[last] = null;
            return true;
        }
        return false;
    }

    /**
     * Remove an object from the list by its index. Not stable.
     *
     * @param idx index of an object in the list
     */
    public removeAt(idx: number): void {
        let data = this._data;
        assert(idx >= 0 && idx < this._size, "Invalid index");

        let last = --this._size;
        data[idx] = data[last];
        data[last] = null;
    }

    /**
     * Get an item at a specific point in the array
     * 
     * @param idx index of item to get
     */
    public get(idx: number): T {
        assert(idx >= 0 && idx < this._size, "Invalid index");
        return this._data[idx];
    }

    /**
     * Get the first item in the list
     */
    public getFirst(): T {
        assert(this._size > 0, "List is empty");
        return this._data[0];
    }

    /**
     * Get the last item in the list
     */
    public getLast(): T {
        assert(this._size > 0, "List is empty");
        return this._data[this._size - 1];
    }

    /**
     * Swap items at two indices with each other
     * 
     * @param idx0 index of first item
     * @param idx1 index of second item
     */
    public swapIndex(idx0: number, idx1: number) {
        assert(idx0 >= 0 && idx0 < this._size, "Invalid index");
        assert(idx1 >= 0 && idx1 < this._size, "Invalid index");
        let data = this._data;
        let t = data[idx0];
        data[idx0] = data[idx1];
        data[idx1] = t;
    }

    /**
     * Swap positions of the first item and the item at the specified index
     * 
     * @param idx index of the item
     */
    public swapIndexFirst(idx: number) {
        assert(idx >= 0 && idx < this._size, "Invalid index");
        let data = this._data;
        let t = data[idx];
        data[idx] = data[0];
        data[0] = t;
    }

    /**
     * Swap positions of the last item and the item at the specified index
     * 
     * @param idx index of the item
     */
    public swapIndexLast(idx: number) {
        assert(idx >= 0 && idx < this._size, "Invalid index");
        let data = this._data;
        let last = this._size - 1;
        let t = data[idx];
        data[idx] = data[last];
        data[last] = t;
    }

    /**
     * Clear the list, set all slots to 0
     */
    public clear(): void {
        let data = this._data;
        let used = this._size;
        for(let i = 0; i < used; ++i) {
            data[i] = null;
        }
        this._size = 0;
    }
}
