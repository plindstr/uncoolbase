import { assert } from "../core/assert";

/**
 * Creates a new signed 8 bit integer typed array that is larger than the size of
 * the input array, copies over values from the old array to the new array, and returns
 * the new array.
 * In debug mode, asserts that the size of the new array is larger than the size of the original
 * array.
 *
 * @param array an Int8Array
 * @param newSize [optional] size of the new array; by default, double the size of the original.
 */
export function array_grow_i8(array: Int8Array, newSize: number = array.length << 1): Int8Array {
    assert(newSize > array.length, "New size must be larger than old size (new: " + newSize + ", old: " + array.length);

    let newarray = new Int8Array(newSize);
    newarray.set(array, 0);
    return newarray;
}

/**
 * Creates a new unsigned 8 bit integer typed array that is larger than the size of
 * the input array, copies over values from the old array to the new array, and returns
 * the new array.
 * In debug mode, asserts that the size of the new array is larger than the size of the original
 * array.
 *
 * @param array a Uint8Array
 * @param newSize [optional] size of the new array; by default, double the size of the original.
 */
export function array_grow_u8(array: Uint8Array, newSize: number = array.length << 1): Uint8Array {
    assert(newSize > array.length, "New size must be larger than old size (new: " + newSize + ", old: " + array.length);

    let newarray = new Uint8Array(newSize);
    newarray.set(array, 0);
    return newarray;
}

/**
 * Creates a new signed 16 bit integer typed array that is larger than the size of
 * the input array, copies over values from the old array to the new array, and returns
 * the new array.
 * In debug mode, asserts that the size of the new array is larger than the size of the original
 * array.
 *
 * @param array an Int!6Array
 * @param newSize [optional] size of the new array; by default, double the size of the original.
 */
export function array_grow_i16(array: Int16Array, newSize: number = array.length << 1): Int16Array {
    assert(newSize > array.length, "New size must be larger than old size (new: " + newSize + ", old: " + array.length);

    let newarray = new Int16Array(newSize);
    newarray.set(array, 0);
    return newarray;
}

/**
 * Creates a new unsigned 16 bit integer typed array that is larger than the size of
 * the input array, copies over values from the old array to the new array, and returns
 * the new array.
 * In debug mode, asserts that the size of the new array is larger than the size of the original
 * array.
 *
 * @param array a Uint!6Array
 * @param newSize [optional] size of the new array; by default, double the size of the original.
 */
export function array_grow_u16(array: Uint16Array, newSize: number = array.length << 1): Uint16Array {
    assert(newSize > array.length, "New size must be larger than old size (new: " + newSize + ", old: " + array.length);

    let newarray = new Uint16Array(newSize);
    newarray.set(array, 0);
    return newarray;
}

/**
 * Creates a new signed 32 bit integer typed array that is larger than the size of
 * the input array, copies over values from the old array to the new array, and returns
 * the new array.
 * In debug mode, asserts that the size of the new array is larger than the size of the original
 * array.
 *
 * @param array an Int32Array
 * @param newSize [optional] size of the new array; by default, double the size of the original.
 */
export function array_grow_i32(array: Int32Array, newSize: number = array.length << 1): Int32Array {
    assert(newSize > array.length, "New size must be larger than old size (new: " + newSize + ", old: " + array.length);

    let newarray = new Int32Array(newSize);
    newarray.set(array, 0);
    return newarray;
}

/**
 * Creates a new unsigned 32 bit integer typed array that is larger than the size of
 * the input array, copies over values from the old array to the new array, and returns
 * the new array.
 * In debug mode, asserts that the size of the new array is larger than the size of the original
 * array.
 *
 * @param array an Uint32Array
 * @param newSize [optional] size of the new array; by default, double the size of the original.
 */
export function array_grow_u32(array: Uint32Array, newSize: number = array.length << 1): Uint32Array {
    assert(newSize > array.length, "New size must be larger than old size (new: " + newSize + ", old: " + array.length);

    let newarray = new Uint32Array(newSize);
    newarray.set(array, 0);
    return newarray;
}

/**
 * Creates a new 32 bit floating point typed array that is larger than the size of
 * the input array, copies over values from the old array to the new array, and returns
 * the new array.
 * In debug mode, asserts that the size of the new array is larger than the size of the original
 * array.
 *
 * @param array a Float32Array
 * @param newSize [optional] size of the new array; by default, double the size of the original.
 */
export function array_grow_f32(array: Float32Array, newSize: number = array.length << 1): Float32Array {
    assert(newSize > array.length, "New size must be larger than old size (new: " + newSize + ", old: " + array.length);

    let newarray = new Float32Array(newSize);
    newarray.set(array, 0);
    return newarray;
}

/**
 * Creates a new 64 bit floating point typed array that is larger than the size of
 * the input array, copies over values from the old array to the new array, and returns
 * the new array.
 * In debug mode, asserts that the size of the new array is larger than the size of the original
 * array.
 *
 * @param array a Float64Array
 * @param newSize [optional] size of the new array; by default, double the size of the original.
 */
export function array_grow_f64(array: Float64Array, newSize: number = array.length << 1): Float64Array {
    assert(newSize > array.length, "New size must be larger than old size (new: " + newSize + ", old: " + array.length);

    let newarray = new Float64Array(newSize);
    newarray.set(array, 0);
    return newarray;
}

/**
 * Safely access an array. Checks bounds using assert().
 *
 * @param array An generic JS array
 * @param index index value (auto-converted to integer)
 */
export function array_access<T>(array: T[], index: number): T {
    index |= 0;
    assert(!!array, "array_access: array is null");
    assert(index >= 0 && index < array.length, "array_access: invalid index " + index + "(valid range [0.." + array.length + "])");
    return array[index];
}

/**
 * Remove an object from a JavaScript array using array.splice().
 * Returns 'true' on successful removal, false if object was not found in array
 *
 * @param array array to modify
 * @param object object to remove
 */
export function array_remove<T>(array: T[], object: T): boolean {
    assert(!!array, "array_remove: array is null");
    assert(!!object, "array_remove: object is null");

    let idx = array.indexOf(object);
    if(idx >= 0) {
        array.splice(idx, 1);
        return true;
    }
    return false;
}

/**
 * Remove an object at a specific index from an array using array.splice(). Returns
 * removed object on success. Asserts that index is valid in debug mode.
 * Removing an object from an invalid index in release mode returns undefined.
 *
 * @param array array to modify
 * @param index index of object to remove
 */
export function array_removeAt<T>(array: T[], index: number): T {
    assert(!!array, "array_removeAt: array is null");
    assert(index >= 0 && index < array.length, "array_removeAt: Invalid array index " + index);

    return array.splice(index | 0, 1)[0];
}

/**
 * Add an object to an array, unconditionally, using array.push().
 * In debug mode, asserts that array and object are not null.
 *
 * @param array array to modify
 * @param object object to add
 */
export function array_add<T>(array: T[], object: T): void {
    assert(!!array, "array_add: array is null");
    assert(!!object, "array_add: object is null");

    array.push(object);
}

/**
 * Add an object to an array only if the object doesn't already exist
 * in the array.
 *
 * @param array array to modify
 * @param object object to add
 */
export function array_add_unique<T>(array: T[], object: T): void {
    assert(!!array, "array_add_unique: array is null");
    assert(!!object, "array_add_unique: object is null");

    let idx = array.indexOf(object);
    if(idx < 0) {
        array.push(object);
    }
}

/**
 * Remove all objects from an array using array.splice().
 * Returns true if any objects were removed.
 *
 * @param array array to modify
 */
export function array_clear<T>(array: T[]): boolean {
    assert(!!array, "array_clear: array is null");

    return array.splice(0, array.length).length > 0;
}

/**
 * Randomize order of objets in an array using the Fisher-Yates shuffle.
 * Asserts that array is not null in debug mode. In release mode, shuffling
 * a null array results in a null reference exception.
 *
 * @param array array to modify
 */
export function array_shuffle<T>(array: T[]): void {
    assert(!!array, "array_shuffle: array is null");

    let i: number, j: number, x: T;
    for (i = array.length - 1; i > 0; i--) {
        j = (Math.random() * (i + 1)) | 0;
        x = array[i];
        array[i] = array[j];
        array[j] = x;
    }
}

/**
 * Rotate the objects in an array by one, using array.push(array.shift());
 * This is a potentially slow operation, and should not be used heavily in a
 * game loop.
 *
 * Asserts that array is not null in debug mode. In release mode, shuffling a null
 * array results in a null reference exception.
 *
 * @param array array to modify
 */
export function array_rotate<T>(array: T[]): void {
    assert(!!array, "array_rotate: array is null");

    array.push(array.shift());
}

/**
 * Pick a random object from a javascript array.
 * Asserts that array is not null in debug mode.
 * If array is empty, returns null.
 *
 * @param array array to pick from
 */
export function array_pickRandom<T>(array: T[]): T {
    assert(!!array, "array_pickRandom: array is null");

    let sz = array.length;
    let idx = (Math.random() * sz) | 0;
    return array[idx] || null;
}

/**
 * Generates an array of integers.
 *
 * @param from start index
 * @param to end index (exclusive)
 */
export function gen_int_range(from: number, to: number): number[] {
    from |= 0;
    to |= 0;
    let n = new Array<number>(to - from);
    for(let i = from, p = 0; i < to; ++i, ++p) {
        n[p] = i;
    }
    return n;
}
