import { debugError } from "../core/logging";

//
// Object manipulation utilities
//

/**
 * Create a deep clone. Slow.
 *
 * @param obj object to clone
 */
export function cloneObject(obj: any): any {
    let target: any = {};
    for(let prop in obj) {
        let f = obj[prop];
        if(obj.hasOwnProperty(prop)) {
            switch(typeof(f)) {
                case 'undefined':
                    target[prop] = undefined;
                break;
                case 'function':
                case 'symbol':
                case 'boolean':
                case 'number':
                case 'string':
                    target[prop] = f;
                break;
                case 'object':
                if(f === null) {
                    target[prop] = null;
                } else {
                    target[prop] = cloneObject(f);
                }
                break;
            }
        }
    }
    return target;
}

/**
 * Extend object in a shallow manner, without cloning objects
 * @param dst object to extend
 * @param props properties to add to dst
 */
export function extendObject(dst: any, props: any): void {
    if(dst === null || dst === undefined) {
        debugError("[Base]", "Cannot extend object; destination object is null or undefined");
        throw 0;
    }
    for(let prop in props) {
        if(props.hasOwnProperty(prop)) {
            dst[prop] = props[prop];
        }
    }
}

/**
 * Extend object in a deep manner, by cloning fields recursively
 * @param dst object to extend
 * @param props properties to add to dst
 */
export function extendObjectDeep(dst: any, props: any): void {
    for(let prop in props) {
        if(props.hasOwnProperty(prop)) {
            dst[prop] = cloneObject(props[prop]);
        }
    }
}

/**
 * Iterate over properties in an object in a legacy compatible manner
 *
 * @param obj object to iterate over
 * @param handler function to call for each property
 */
export function forEachProperty(obj: any, handler: (key: string, value: any) => void): void {
    for(let prop in obj) {
        if(obj.hasOwnProperty(prop)) {
            handler.call(null, prop, obj[prop]);
        }
    }
}
