/**
 * Statically sized ringbuffer container.
 * Will overwrite the oldest items once 
 * capacity is reached.
 * 
 * For a somewhat faster container that
 * will grow to support more data, use
 * Queue.
 */
export class Ringbuffer<T> {
    private _data: Array<T>;
    private _size: number;
    private _items: number;
    private _pos: number;

    constructor(maxsize: number = 16) {
        this._data = new Array<T>(maxsize);
        this._size = maxsize;
        this._items = 0;
        this._pos = 0;
    }

    public size(): number {
        return this._items;
    }

    public add(item: T): void {
        const sz = this._size;
        const items = this._items;
        const pos = this._pos;

        this._data[(pos + items) % sz] = item;

        if(items === sz - 1) {
            this._pos = (pos + 1) % sz;
        } else {
            ++this._items;
        }
    }

    public clear(): void {
        for(let i = 0; i < this._size; ++i) {
            this._data[i] = null;
        }
        this._items = 0;
        this._pos = 0;
    }

    public get(idx: number): T {
        let p = (this._pos + (idx % this._items)) % this._size;
        return this._data[p];
    }
}
