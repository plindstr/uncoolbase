import { nextPow2 } from "../math/coremath";
import { assert } from "../core/assert";

/**
 * More efficient queue than JavaScripts's array
 * with push()/shift() semantics.
 */
export class Queue<T> {

    private _head: number = 0;
    private _size: number = 0;
    private _data: Array<T>;

    constructor(sz = 64) {
        sz = nextPow2(sz);
        this._data = new Array<T>(sz);
    }

    /**
     * Called when there's no more space for new
     * objects, and data storage area needs to grow.
     */
    private _grow(): void {
        let data = this._data;
        let n = data.length;

        // Allocate more space
        let next = new Array<T>(n << 1);

        // Move data to beginning of new array
        let p = this._head;
        let nn = n - 1;
        for(let i = 0; i < n; ++i) {
            next[i] = data[p & nn];
            ++p;
        }

        // Reset pointers
        this._data = next;
        this._head = 0;
    }

    public getItem(idx: number): T {
        assert(idx < this._size, "Index out of bounds");
        let data = this._data;
        return data[(this._head + idx) & (data.length - 1)];
    }

    public isEmpty(): boolean {
        return this._size === 0;
    }

    public size(): number {
        return this._size;
    }

    public clear(): void {
        let data = this._data;
        let from = this._head;
        let to = (this._head + (this._size - 1)) & (data.length - 1);
        if(from > to) {
            let t = from;
            from = to;
            to = t;
        }
        for(let i = from; i <= to; ++i) {
            data[i] = null;
        }
        this._size = 0;
        this._head = 0;
    }

    public push(object: T): void {
        let data = this._data;
        let size = this._size;

        if(size === data.length) {
            this._grow();
        }

        this._size = size + 1;
        let idx = (this._head + size) & (data.length - 1);
        data[idx] = object;
    }

    public pop(): T {
        assert(this._size > 0, "Cannot pop an empty queue");
        
        let data = this._data;
        let head = this._head;
        let obj = data[head];
        data[head] = null;
        
        this._head = (--this._size) === 0 ? 0 : (head + 1) & (data.length - 1);
        return obj;
    }

}
