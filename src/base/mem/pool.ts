import { assert } from "../core/assert";
import { ArrayList } from "./arraylist";

/**
 * A minimalistic object pool class
 */
export class Pool<T> {

    private _create: () => T;
    private _reset: (obj: T) => void;
    private _data = new ArrayList<T>();
    private _used: number = 0;

    constructor(initialSize: number, create: () => T, reset: (obj: T) => void = null) {
        let i = initialSize < 1 ? 1 : initialSize | 0;
        
        assert(create != null, "Create function can not be null");
        this._create = create;
        this._reset = reset;

        while(i--) this._data.add(create());
    }

    /**
     * Allocate an object from the pool
     */
    public alloc(): T {
        let data = this._data;
        let used = this._used++;
        if(used === data.size()) {
            data.add(this._create())
        }
        return data.get(used);
    }

    /**
     * Free item by reference
     *
     * @param item an object that belongs to this Pool
     */
    public free(item: T): void {
        let data = this._data;
        data.swapIndex(data.indexOf(item), --this._used);
        let rst = this._reset;
        rst && rst(item);
    }

    /**
     * Free item by index in data array
     *
     * @param idx the index of used object in this Pool
     */
    public freeAt(idx: number): void {
        assert(idx < this._used, "Invalid idex");

        let data = this._data;
        data.swapIndex(idx, --this._used);
        let rst = this._reset;
        rst && rst(data.get(this._used));
    }

    /**
     * Clear the pool
     */
    public clear(): void {
        let u = this._used;
        if(!u) return;

        let rst = this._reset;
        if(rst) {
            let data = this._data.getData();
            while(u--) {
                rst(data[u]);
            }
        }
        this._used = 0;
    }

    /**
     * Get access to the data array of this Pool
     */
    public getData(): T[] {
        return this._data.getData();
    }

    /**
     * Get the number of allocated objects in this Pool
     */
    public size(): number {
        return this._used;
    }

}
