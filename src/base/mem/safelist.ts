import { QList } from "./qlist";
import { ArrayList } from "./arraylist";

const OP_ADD    = 1;
const OP_REMOVE = 2;
const OP_CLEAR  = 3;

/**
 * Internal container structure used for lists that can be
 * modified during iteration.
 */
export class SafeList<T> {

    private _data      = new ArrayList<T>();
    private _opQueue   = new QList<number>();
    private _dataQueue = new QList<T>();
    private _locked    = false;

    constructor() {}

    public size(): number {
        return this._data.size();
    }

    public add(item: T): void {
        if(this._locked) {
            this._opQueue.add(OP_ADD);
            this._dataQueue.add(item);
        } else {
            this._data.add(item);
        }
    }

    public remove(item: T): void {
        if(this._locked) {
            this._opQueue.add(OP_REMOVE);
            this._dataQueue.add(item);
        } else {
            this._data.remove(item);
        }
    }

    public forEach(fn: (item: T) => void): void {
        this._locked = true;
        let data = this._data.getData();
        let sz = this._data.size();
        for(let i = 0; i < sz; ++i) {
            let item = data[i];
            fn(item);
        }
        this._locked = false;

        // If we have no ops queued, we're done
        if(this._opQueue.isEmpty()) return;
        
        // Process queued ops
        let ops = this._opQueue.getData();
        let opcount = this._opQueue.size();
        let opdata = this._dataQueue.getData();

        for(let i = 0; i < opcount; ++i) {
            switch(ops[i]) {
                case OP_ADD:
                    this._data.add(opdata[i]);
                break;
                case OP_REMOVE:
                    this._data.remove(opdata[i]);
                break;
                case OP_CLEAR:
                    this._data.clear();
                break;
            }
        }

        // Clear temp queue
        this._opQueue.purge();
        this._dataQueue.purge();
    }

    public clear(): void {
        if(this._locked) {
            this._opQueue.add(OP_CLEAR);
            this._dataQueue.add(null);
        } else {
            this._data.clear();
        }
    }
}
