/**
 * Bi-directional map structure.
 * Requires one-to-one unique mapping.
 */
export class BiMap<KEY, VALUE> {

    private _fwd = new Map<KEY, VALUE>();
    private _rev = new Map<VALUE, KEY>();

    public clear(): BiMap<KEY, VALUE> {
        this._fwd.clear();
        this._rev.clear();
        return this;
    }

    public keys(): IterableIterator<KEY> {
        return this._fwd.keys();
    }

    public values(): IterableIterator<VALUE> {
        return this._rev.keys();
    }

    public set(key: KEY, value: VALUE): BiMap<KEY, VALUE> {
        // Ensure uniqueness
        if(this._fwd.has(key)) {
            this._rev.delete(this._fwd.get(key));
        }

        this._fwd.set(key, value);
        this._rev.set(value, key);

        return this;
    }

    public deleteByKey(key: KEY): BiMap<KEY, VALUE> {
        if(key) {
            let v = this._fwd.get(key);
            if(v) {
                this._rev.delete(v);
                this._fwd.delete(key);
            }
        }
        return this;
    }

    public deleteByValue(value: VALUE): BiMap<KEY, VALUE> {
        if(value) {
            let k = this._rev.get(value);
            if(k) {
                this._fwd.delete(k);
                this._rev.delete(value);
            }
        }
        return this;
    }

    public get(key: KEY): VALUE {
        return this._fwd.get(key);
    }

    public getKey(value: VALUE): KEY {
        return this._rev.get(value);
    }

    public getValue(key: KEY): VALUE {
        return this._fwd.get(key);
    }

    public hasKey(key: KEY): boolean {
        return this._fwd.has(key);
    }

    public hasValue(value: VALUE): boolean {
        return this._rev.has(value);
    }

}
