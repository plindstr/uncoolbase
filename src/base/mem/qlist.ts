import { assert } from "../core/assert";

/**
 * QList - Quick List for stable temporary object storage
 * 
 * Used in order to cut down on potential block reallocs
 * in the general case.
 */
export class QList<T> {
    private _data: T[];
    private _size: number;

    constructor(initialSize = 16) {
        this._data = new Array<T>(16 < initialSize ? 16 : initialSize);
        this._size = 0;
    }

    public isEmpty(): boolean {
        return this._size === 0;
    }

    public size(): number {
        return this._size;
    }

    public getData(): T[] {
        return this._data;
    }

    private _grow(): void {
        let next = new Array<T>(this._data.length << 1);
        const sz = this._size;
        for(let i = 0; i < sz; ++i) next[i] = this._data[i];
        this._data = next;
    }

    public add(item: T): void {
        if(this._data.length == this._size) this._grow();
        this._data[this._size++] = item;
    }

    public addUnique(item: T): void {
        let data = this._data;
        let sz = this._size;
        for(let i = 0; i < sz; ++i) {
            if(data[i] === item) return;
        }
        this.add(item);
    }

    public get(idx: number): T {
        idx |= 0;
        assert(idx > -1 && idx < this._size, "Invalid index");
        return this._data[idx];
    }

    public clear(): void {
        this._size = 0;
    }

    public purge(): void {
        let sz = this._size;
        let data = this._data;
        while(sz) data[--sz] = null;
        this._size = 0;
    }
}
