import { isDebug } from "../core";
import { randInt } from "../math";

export function getUrl(url: string): string {
    return isDebug() ? url + '?' + randInt(0, Math.pow(2, 29)) : url;
}

export function createXHR(url: string, onComplete: (xhr: XMLHttpRequest) => void, onError: () => void, type: XMLHttpRequestResponseType = undefined): XMLHttpRequest {
    let req = new XMLHttpRequest();
    req.onerror = onError;
    if(type) {
        req.responseType = type;
    }
    req.onload = (e) => {
        onComplete(req);
    };
    req.open('GET', getUrl(url), true);
    return req;
}
