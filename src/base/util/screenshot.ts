import { Surface } from "../graphics/surface";
import { Screen, getScreen } from "../core/screen";

const mmax = (a: number, b: number) => a > b ? a : b;

let tsurf = new Surface();

/**
 * Takes a screenshot of the current screen (possibly rescaled
 * into some size or by some percentage) and return a data url.
 */
export function getScreenshot(
    dscale: number = 1.0,
    type: 'png' | 'jpeg' = 'png', quality = 1.0,
    x: number = 0, y: number = 0,
    width: number = Screen.WIDTH,
    height: number = Screen.HEIGHT): string {

    let screen = getScreen();
    let wscale = screen.getWidth() / Screen.WIDTH;
    let hscale = screen.getHeight() / Screen.HEIGHT;

    let sx = ((x * wscale) + 0.5) | 0;
    let sy = ((y * hscale) + 0.5) | 0;
    let sw = ((width * wscale) + 0.5) | 0;
    let sh = ((height * hscale) + 0.5) | 0;

    dscale = mmax(dscale, 0.01);
    let dw = ((dscale * width) + 0.5)  | 0;
    let dh = ((dscale * height) + 0.5) | 0;
    tsurf.setSize(dw, dh);
    tsurf.getContext2D().drawImage(screen.getElement(), sx, sy, sw, sh, 0, 0, dw, dh);
    let dataurl = tsurf.getElement().toDataURL('image/' + type, quality);

    return dataurl;
}
