export function exportStringAsFile(str: string, filename: string = "data", fileext: string = "txt"): void {
    var content = "data:text/plain," + encodeURIComponent(str);
    var elem = document.createElement('a');
    elem.setAttribute('href', content);
    elem.setAttribute('download', filename + "." + fileext);
    elem.click();
}
