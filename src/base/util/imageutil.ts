import { ImageSource, Color, Surface } from "../graphics";
import { clamp } from "../math";

/**
 * Basic image manipulation routines
 */
export class ImageUtil {

    /**
     * Create an offscreen canvas with a tinted version of the
     * input image source.
     *
     * @param img some ImageSource
     * @param tint tint color definition
     * @param strength strength of the tint
     */
    public static makeTinted(img: ImageSource, tint: Color, strength: number): Surface {

        var w = img.getWidth();
        var h = img.getHeight();
        var surf = new Surface();
        surf.setSize(w, h);
        var ctx = surf.getContext2D();

        ctx.fillStyle = tint.asString();
        ctx.globalAlpha = clamp(strength, 0, 1);
        ctx.globalCompositeOperation = 'source-over';
        ctx.fillRect(0, 0, w, h);
        ctx.globalAlpha = 1.0;
        ctx.globalCompositeOperation = 'destination-atop';
        ctx.drawImage(img.getElement(), 0, 0);

        return surf;

    }

}
