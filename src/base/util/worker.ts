//
// Worker - short utility to spawn
// web workers by providing them with
// a function.
//

/**
 * Spawn a Web Worker by supplying a function to it.
 *
 * @param code a Function object, which is converted to a string and wrapped in a self-invoking wrapper
 * @return a Worker object
 */
export function spawnWorker(code: Function): Worker {
    let fn = '(' + code.toString() + ')()';
    let blob = new Blob([ fn ]);
    let opts: WorkerOptions = {
        'type': 'classic',
        'credentials': 'same-origin'
    };
    return new Worker(URL.createObjectURL(blob), opts);
}
