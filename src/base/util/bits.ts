/**
 * Get the value for the Nth bit.
 *
 * @param index a value between 0 (for first bit) and 30 (for last bit)
 */
export const bit = (index: number) => 1 << index;

/**
 * Set bits in a bit field. Returns the modified field value.
 * To use a 'clean slate', use '0' as the field value.
 *
 * @param field Field containing the bits to manipulate
 * @param bitMask Bit or bits (correctly shifted) to manipulate
 * @param enable true to set, false to unset
 */
export const setBit = (field: number, bitMask: number, enable: boolean): number =>
    enable ? (field | bitMask) : (field & (~bitMask));

/**
 * Test if all bits in a bitmask are set in a bit field.
 * Does (field & bitMask) === bitMask.
 *
 * @param field Field containing the bits to test
 * @param bitMask Bit or bits to check
 */
export const testBit = (field: number, bitMask: number): boolean =>
    (field & bitMask) === bitMask;
