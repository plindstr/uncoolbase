import { EventHub } from "./eventhub";
import { assert } from "../core/assert";
import { array_clear } from "../mem/array";

//
// TODO: write a more performant queue implementation!
// TODO: use more performant queue implementation, as one exists now
//

interface Event {
    type: string;
    data: Object;
}

/**
 * Provides an event queue system on top of EventHub
 */
export class EventQueue {

    private _hub: EventHub = null;
    private _queue: Event[] = [];
    private _overflow: Event[] = [];
    private _locked: boolean = false;

    /**
     * Create a new EventQueue instance. This Queue will use the
     * eventhub instance provided to send events.
     *
     * @param hub an EventHub instance.
     */
    constructor(hub: EventHub) {
        this._hub = hub;
    }

    /**
     * Get access to the associated EventHub instance.
     */
    public getEventHub(): EventHub {
        return this._hub;
    }

    /**
     * Return true if there are no events queued.
     */
    public isEmpty(): boolean {
        return this._queue.length === 0;
    }

    /**
     * Add an event to the queue. Can take an optional object
     * as parameter; it is given to the event handlers as an
     * argument.
     *
     * @param event name of event
     * @param data [optional] an object to pass as argument
     */
    public add(event: string, data: Object = null): void {
        let ev = {
            type: event,
            data: data
        };
        let q = this._locked ? this._overflow : this._queue;
        q.push(ev);
    }

    /**
     * Dispatch the first event in the queue.
     * Return 'true' if there are still events in the queue,
     * 'false' otherwise.
     *
     * Note: you shouldn't need to use this function in a normal
     * use case; go for dispatchAll as it's more performant.
     */
    public dispatch(): boolean {
        assert(!this._locked, "Cannot dispatch event while locked; possible loop detected!");

        if(this._queue.length === 0) return false;

        this._locked = true;
        let ev = this._queue.shift();
        this._hub.sendEvent(ev.type, ev.data);
        this._locked = false;

        this._queue.concat(this._overflow);
        array_clear(this._overflow);

        return this._queue.length > 0;
    }

    /**
     * Dispatch all events in the queue. Any events that were
     * added during the dispatch aren't processed (to prevent
     * infinite loops). Returns 'true' if there are new events
     * in the queue, or 'false' if the queue is empty.
     */
    public dispatchAll(): boolean {
        assert(!this._locked, "Cannot dispatch event while locked; possible loop detected!");

        if(this._queue.length === 0) return false;

        this._locked = true;
        do {
            let ev = this._queue.shift();
            this._hub.sendEvent(ev.type, ev.data);
        } while(this._queue.length > 0);

        // Swap queue and overflow
        let q = this._queue;
        this._queue = this._overflow;
        this._overflow = q;

        this._locked = false;

        return this._queue.length > 0;
    }

}
