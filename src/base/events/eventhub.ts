import { array_add_unique, array_remove } from "../mem";
import { assert } from "../core/assert";

/**
 * Simple event hub system. Maintains a mapping
 * from string to array of functions. When an event
 * is 'sent', calls all functions associated with that
 * event name immediately.
 */
export class EventHub {
    private _map = new Map<string, ((event: string, ...args: any) => void)[]>();
    private _callstack: string[] = [];

    /**
     * Remove all event listeners from all events in this EventHub.
     */
    public clear(): void {
        this._map.clear();
    }

    /**
     * Remove all event listners from a specific event in the EventHub.
     *
     * @param evname name of event
     */
    public clearEvent(evname: string): boolean {
        return this._map.delete(evname);
    }

    /**
     * Send an event. Immediately calls all registered event handlers.
     * Can result in an infinite loop, if the handler logic results in
     * an event of the same type to be sent.
     * Checks for circular event dependencies in debug mode using assert().
     *
     * @param evname name of event
     * @param data arguments to pass to event
     */
    public sendEvent(evname: string, ...data: any): number {
        assert(this._callstack.indexOf(evname) === -1, "Event dependency loop detected!");

        let list = this._map.get(evname), i = 0;
        if(list) {
            this._callstack.push(evname);
            for(let fn of list) {
                fn.call(null, evname, data);
                ++i;
            }
            this._callstack.pop();
        }
        return i;
    }

    /**
     * Add an event listener. Event listeners are functions that take
     * in the name of the event as the first parameter, and then any number
     * of optional arguments of any type.
     *
     * @param evname name of event
     * @param handler handler function
     */
    public addHandler(evname: string, handler: (event: string, ...args: any) => void): void {
        let list = this._map.get(evname);
        if(list) {
            array_add_unique(list, handler);
        } else {
            this._map.set(evname, [ handler ]);
        }
    }

    /**
     * Remove a previously added event listener. Returns true if the event
     * listener was removed, and false if either no such event or no such
     * listener existed (and thus the listener map was not modified).
     *
     * @param evname name of event
     * @param handler handler function to remove
     */
    public removeHandler(evname: string, handler: (event: string, ...args: any) => void): boolean {
        let list = this._map.get(evname);
        if(list) {
            return array_remove(list, handler);
        }
        return false;
    }
}
