import { Loader } from "./loader/loader";
import { debugLog } from "./core/logging";

export * from "./loader/assetlist"
export * from "./loader/loader"

let loader      = new Loader();

loader.onProgress = (msg: string) => {
    debugLog(msg);
};

loader.onError = (msg: string) => {
    console.error(msg);
};

/**
 * Get access to the default loader
 */
export function getLoader(): Loader {
    return loader;
}
