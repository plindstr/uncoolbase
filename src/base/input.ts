import { Keyboard } from "./input/keyboard";
import { Mouse } from "./input/mouse";
import { TouchTracker } from "./input/touch";
import { registerInitRoutine } from "./init";
import { debugLog } from "./core";

export * from "./input/coreinput"
export * from "./input/keyboard"
export * from "./input/mouse"
export * from "./input/touch"

const keyboard = new Keyboard();
const mouse = new Mouse();
const touch = new TouchTracker();

registerInitRoutine(() => {

    debugLog("Initialize input");

    // iOS 10 Safari zoom prevention
    document.addEventListener('gesturestart', function (e) {
        e.stopPropagation();
        e.preventDefault();
        e.cancelBubble = true;
        e.preventDefault();
    });

    // Disable scrolling
    document.addEventListener('scroll', (e) => {
        e.stopPropagation();
        e.preventDefault();
        e.cancelBubble = true;
        e.preventDefault();
    });

});

export function getKeyboard(): Keyboard {
    return keyboard;
}

export function getMouse(): Mouse {
    return mouse;
}

export function getTouch(): TouchTracker {
    return touch;
}
