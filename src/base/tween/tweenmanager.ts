//
// TweenManager - functions for managing a global tween list
//

import { Tween } from "./tween";
import { SafeList } from "../mem";

const tweens = new SafeList<Tween>();

export const registerTween = (t: Tween) => tweens.add(t);

export const unregisterTween = (t: Tween) => tweens.remove(t);

export const clearTweens = () => tweens.clear();

export const updateTweens = (delta: number) => {
    delta *= 1000;
    tweens.forEach((t) => t.update(delta));
}

