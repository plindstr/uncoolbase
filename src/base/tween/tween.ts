import { registerTween, unregisterTween } from "./tweenmanager";

/**
 * Simple disposable Tween object. Provides in-between animation
 * functionality, usually used for moving UI elements, fading the
 * screen between scenes, etc.
 */
export class Tween {
    private _onComplete: (tween?: Tween) => void;
    private _onProgress: (bias: number, tween?: Tween) => void;
    private _ease: (k:number, tween?: Tween) => number;

    private _duration: number;
    private _elapsed: number;
    private _bias: number;

    /**
     * Create a new Tween.
     *
     * @param duration_msec number of milliseconds to run this Tween
     * @param ease function which translates normalized temporal progress to normalized animation progress; see base.Easing.* functions
     * @param onProgress user-provided function to call on each tick, receives the calculated animation bias a parameter
     * @param onComplete [optional] function to call once when the animation completes
     */
    constructor(duration_msec: number,
        ease: (k:number, tween?: Tween) => number,
        onProgress: (bias: number, tween?: Tween) => void,
        onComplete: (tween?: Tween) => void = () => {}) {

        this._bias = 0;
        this._duration = duration_msec;
        this._elapsed = 0;
        this._ease = ease;
        this._onProgress = onProgress;
        this._onComplete = onComplete;
    }

    /**
     * Start automatic execution of this Tween. The Tween is added
     * to an internal list in base, which is iterated over once per
     * frame, and the update() routine is called.
     *
     * @param delay number of milliseconds to delay before starting the Tween
     */
    public start(delay: number = 0): Tween {
        this._bias = 0;
        this.startManual(delay);
        registerTween(this);
        return this;
    }

    /**
     * Get the current calculated bias of this Tween
     */
    public getBias(): number {
        return this._bias;
    }

    /**
     * Same as 'start' but does not add Tween to base tween list,
     * allowing for manual update
     *
     * @param delay number of milliseconds to wait before starting Tween
     *
     * @returns a reference to self
     */
    public startManual(delay: number = 0): Tween {
        this._bias = 0;
        this._elapsed = -delay;
        if(this._elapsed >= 0) {
            this._onProgress(this._ease(this._bias = this._elapsed / this._duration, this));
        }
        return this;
    }

    /**
     * Stop execution of this tween by removing it from the base tween list,
     * and call the onProgress handler with ease 1, then call the onComplete handler.
     * The 'tween' parameter will be set to 'null' in the onProgress handler in order
     * to stop a stack overflow error.
     */
    public end(): Tween {
        unregisterTween(this);
        this._onProgress(this._ease(this._bias = 1, this), null);
        this._elapsed = this._duration;
        this._onComplete(this);
        return this;
    }

    /**
     * Stop execution of this Tween by removing it from the base tween list.
     *
     * @returns a reference to self
     */
    public stop(): Tween {
        unregisterTween(this);
        return this;
    }

    /**
     * Stop execution of this Tween by removing it from the base tween list
     * and execute the progress callback with a calculated bias of 0. This
     * effectively resets the animation to its starting position.
     * The 'tween' parameter will be set to 'null' in the onProgress handler in order
     * to stop a stack overflow error.
     *
     * @returns a reference to self
     */
    public cancel(): Tween {
        unregisterTween(this);
        this._onProgress(this._ease(this._bias = 0, this), null);
        this._elapsed = 0;
        return this;
    }

    /**
     * Update the Tween's progress. This calculates the current state of the
     * animation and calls the onProgress and onComplete callbacks as appropriate.
     * If the Tween completes, it is removed from the base auto-execution list.
     *
     * Unless you started the Tween through the startManual() function, you do
     * not need to call this.
     *
     * @param delta_millis number of milliseconds elapsed since last frame
     */
    public update(delta_millis: number): void {
        let e = this._elapsed + delta_millis;
        if(e > this._duration) {
            this._onProgress(this._ease(1), this);
            this._elapsed = 0;
            this._bias = 1;
            unregisterTween(this);
            if(!!this._onComplete) {
                this._onComplete(this);
            }
        } else {
            if(e >= 0) {
                this._onProgress(this._ease(this._bias = (e / this._duration), this), this);
            } else {
                this._bias = 0;
            }
        }
        this._elapsed = e;
    }
}

/**
 * Create and start a new Tween with the provided arguments. Convenience function.
 *
 * @param duration_msec duration of tween
 * @param ease easing function
 * @param onProgress 'tick' function; bias parameter is calculated and passed in by easing function
 * @param onComplete [optional] function to run when complete
 * @param delay_millis [optional] number of milliseconds to wait before starting
 *
 * @returns the created Tween object
 */
export function tween(duration_msec: number, ease: (bias: number, tween?: Tween) => number, onProgress: (bias: number, tween?: Tween) => void, onComplete: (tween?: Tween) => void = () => {}, delay_millis: number = 0): Tween {
    return new Tween(duration_msec, ease, onProgress, onComplete).start(delay_millis);
}

/**
 * Create and process a new Tween asynchronously. Convenience function.
 *
 * @param duration_msec duration of tween
 * @param ease easing function
 * @param onProgress 'tick' function; bias parameter is calculated and passed in by easing function
 * @param delay_millis [optional] number of milliseconds to wait before starting
 *
 * @returns a Promise that resolves when the Tween completes
 */
export function tweenAsync(duration_msec: number, ease: (bias: number, tween?: Tween) => number, onProgress: (bias: number, tween?: Tween) => void, delay_millis: number = 0): Promise<void> {
    return new Promise(res => {
        new Tween(duration_msec, ease, onProgress, () => res()).start(delay_millis);
    });
}
