import { PlatformProvider } from "../platformprovider";
import { DeviceOrientation } from "../deviceorientation";
import { DeviceType } from "../devicetype";
import { Size } from "../../math/size";
import { CallbackList } from "../../mem/callbacklist";

/**
 * A system for getting information about the current runtime
 * environment.
 */
export abstract class RuntimeProvider extends PlatformProvider {

    //
    // Default handler lists
    //

    private _runtime_suspendHandlers = new CallbackList<() => void>();
    private _runtime_resumeHandlers = new CallbackList<() => void>();
    private _runtime_exitHandlers = new CallbackList<() => void>();

    /**
     * Run all currently added suspend handlers
     */
    protected _runSuspendHandlers(): void {
        this._runtime_suspendHandlers.run();
    }

    /**
     * Run all currently added resume handlers
     */
    protected _runResumeHandlers(): void {
        this._runtime_resumeHandlers.run();
    }

    /**
     * Run all currently added exit handlers
     */
    protected _runExitHandlers(): void {
        this._runtime_exitHandlers.run();
    }

    /**
     * Get the current device type (or a best guess). The default
     * implementation will just return DeviceType.UNKNOWN.
     */
    public abstract getDeviceType(): DeviceType;

    /**
     * Get the orientation of the device. Desktops are usually in
     * LANDSCAPE, mobiles are usually in PORTRAIT.
     */
    public abstract getDeviceOrientation(): DeviceOrientation;

    /**
     * Find the native resolution of the current device. The default
     * implementation will return window.screen.width/height
     */
    public abstract getNativeResolution(): Size;

    /**
     * Find the device display pixel ratio (which will tell if there
     * is pixel doubling or something going on).
     */
    public abstract getPixelRatio(): number;

    /**
     * Return true if this game is currently in a suspended state
     * (i.e. has lost focus or such).
     */
    public abstract isSuspended(): boolean;

    /**
     * Add a handler that runs when the game loses focus or is otherwise
     * suspended in some fashion.
     *
     * @param handler function to run
     */
    public addSuspendHandler(handler: () => void): void {
        this._runtime_suspendHandlers.add(handler);
    }

    /**
     * Remove a previously added suspend handler.
     *
     * @param handler function to remove
     */
    public removeSuspendHandler(handler: () => void): void {
        this._runtime_suspendHandlers.remove(handler);
    }

    /**
     * Add a handler that runs when the game returns from a suspended state.
     * Be aware that there is a good chance that the game will never resume
     * from a suspended state...
     *
     * @param handler function to run
     */
    public addResumeHandler(handler: () => void): void {
        this._runtime_resumeHandlers.add(handler);
    }

    /**
     * Remove a previously added resume handler.
     *
     * @param handler function to remove
     */
    public removeResumeHandler(handler: () => void): void {
        this._runtime_resumeHandlers.remove(handler);
    }

    /**
     * Add a handler that runs when exit() is _successfully_ called, but
     * before application exits.
     *
     * @param handler function to run
     */
    public addExitHandler(handler: () => void): void {
        this._runtime_exitHandlers.add(handler);
    }

    /**
     * Remove a previously added exit handler.
     *
     * @param handler function to remove
     */
    public removeExitHandler(handler: () => void): void {
        this._runtime_exitHandlers.remove(handler);
    }

    /**
     * Return true if we can exit the game programmatically
     */
    public abstract canExit(): boolean;

    /**
     * Try to exit the application, if at all possible.
     *
     * The default implementation calls window.close(), if available.
     */
    public abstract exit(): void;

}
