import { RuntimeProvider } from "./runtimeprovider";
import { DeviceType } from "../devicetype";
import { Size } from "../../math/size";
import { debugWarn } from "../../core/logging";
import { DeviceOrientation } from "../deviceorientation";
import { hasUrlParam } from "../../core/urlparam";

let suspended = false;
let mobile = false;

export class DefaultRuntimeProvider extends RuntimeProvider {

    public isAvailable(): boolean {
        return true;
    }

    public initialize(): Promise<void> {
        return new Promise(res => {

            window.addEventListener('focus', () => {
                suspended = false;
                this._runResumeHandlers();
            });

            window.addEventListener('blur', () => {
                this._runSuspendHandlers();
                suspended = true;
            });

            let ua = navigator.userAgent;
            mobile = ua.search(/Android|iPhone|iPad|Mobile/i) >= 0;

            res();
        });
    }

    public getDeviceType(): DeviceType {
        if(mobile) {
            return DeviceType.MOBILE;
        }
        return DeviceType.DESKTOP;
    }

    public getDeviceOrientation(): DeviceOrientation {
        if(hasUrlParam('portrait')) {
            return DeviceOrientation.PORTRAIT;
        }
        return DeviceOrientation.LANDSCAPE;
    }

    public getNativeResolution(): Size {
        return new Size(
            window.screen.width,
            window.screen.height
        );
    }

    public getPixelRatio(): number {
        return window.devicePixelRatio;
    }

    public isSuspended(): boolean {
        return suspended;
    }

    public canExit(): boolean {
        return !!window.opener;
    }

    public exit(): void {
        if(!!window.opener) {
            this._runExitHandlers();
            window.close();
        } else {
            debugWarn("Window has no opener; cannot exit!");
        }
    }
}
