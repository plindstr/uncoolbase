import { PlatformProvider } from "../platformprovider";

export abstract class AdProvider extends PlatformProvider {

    /**
     * Prepare an insterstitial ad. Should return a Promise
     * that resolves as 'true' if an ad could be prepared
     * and loaded, or 'false' if an ad could not be prepared
     * for whatever reason.
     */
    public abstract async prepareInterstitial(): Promise<boolean>;

    public abstract async showInterstitial(): Promise<void>;

    public abstract canShowInterstitial(): boolean;

    /**
     * Prepare an insterstitial rewarded video ad. Should return
     * a Promise that resolves as 'true' if an ad could be prepared
     * and loaded, or 'false' if an ad could not be prepared
     * for whatever reason.
     */
    public abstract async prepareRewardVideo(): Promise<boolean>;

    public abstract async showRewardVideo(): Promise<void>;

    public abstract canShowRewardVideo(): boolean;

}
