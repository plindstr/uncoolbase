import { AdProvider } from "./adprovider";

/**
 * Default ad provider provides does not exist.
 */
export class DefaultAdProvider extends AdProvider {

    public isAvailable(): boolean {
        return false;
    }

    public initialize(): Promise<void> {
        return new Promise(res => res());
    }

    public prepareInterstitial(): Promise<boolean> {
        return new Promise((res, rej) => rej());
    }

    public showInterstitial(): Promise<void> {
        return new Promise((res, rej) => rej());
    }

    public canShowInterstitial(): boolean {
        return false;
    }

    public prepareRewardVideo(): Promise<boolean> {
        return new Promise((res, rej) => rej());
    }

    public showRewardVideo(): Promise<void> {
        return new Promise((res, rej) => rej());
    }

    public canShowRewardVideo(): boolean {
        return false;
    }

}
