import { PlatformProvider } from "../platformprovider";
import { IHighscoreTableEntry, HighscoreData } from "./highscoredata";

export abstract class HighscoreProvider extends PlatformProvider {

    /**
     * Get the (unique?) ID of the player for the current session.
     * The default LocalStorage implementation just returns 'player'; other platforms
     * may return something else uniquely identifiable.
     *
     * This function returns immediately; its value should be prepared during initialization.
     */
    public abstract getPlayerId(): string;

    /**
     * Get the default or assumed name of the current player.
     *
     * This function returns immediately, its value should be prepared during initialization.
     */
    public abstract getPlayerName(): string;

    /**
     * Get the score of the current player for a given highscore table.
     * Returns an object conforming to IHighscoreTableEntry through a promise
     * when the request completes.
     *
     * @param table_id ID string of the table to fetch data from
     */
    public abstract async getPlayerScore(table_id: string): Promise<IHighscoreTableEntry>;

    /**
     * Submit the player's score. Returns an object conforming to IHighscoreTableEntry
     * through a promise when the submission is complete. The object represents the
     * player's new ranking (if any). This value should be compared with a previously
     * fetched player score.
     *
     * @param table_id ID string of the table to fetch data from
     */
    public abstract async submitPlayerScore(table_id: string, score: number, extra: string): Promise<IHighscoreTableEntry>;

    /**
     * Fetch high score data from the provider and return a highscoredata object
     * on success.
     *
     * @param table_id ID string of the table to fetch data from
     * @param from Index of the first entry (usually 0)
     * @param maxcount Maximum number of scores to fetch at once (usually 10 or so, depending on your highscore table)
     */
    public abstract async getEntries(table_id: string, from: number, maxcount: number): Promise<HighscoreData>;

}
