import { HighscoreProvider } from "./highscoreprovider";
import { IHighscoreTableEntry, HighscoreData } from "./highscoredata";
import { delayAsync } from "../../timer";

/**
 * Default highscore data provider: localStorage.
 * Provides a single highscore table entry for the player.
 */
export class DefaultHighscoreProvider extends HighscoreProvider {

    public isAvailable(): boolean {
        return true;
    }

    public async initialize(): Promise<void> {
    }

    public getPlayerId(): string {
        return "player";
    }

    public getPlayerName(): string {
        return "Player McPlayerface";
    }

    public async getPlayerScore(table_id: string): Promise<IHighscoreTableEntry> {
        await delayAsync(30);   // simulate network lag
        let item = localStorage.getItem('highscore.' + table_id);
        let data: any;
        if(item) {
            data = JSON.parse(item);
            let entry: IHighscoreTableEntry = {
                name: data.name,
                id: data.id,
                extra: data.extra,
                rank: data.rank,
                score: data.score
            };
            return entry;
        }

        return {
            name: this.getPlayerName(),
            id: this.getPlayerId(),
            extra: '{}',
            rank: -1,
            score: 0
        };
    }

    public async submitPlayerScore(table_id: string, score: number, extra: string): Promise<IHighscoreTableEntry> {
        await delayAsync(30);    // simulate network lag
        let old = await this.getPlayerScore(table_id);
        if(old.score < score) {
            let entry: IHighscoreTableEntry = {
                name: this.getPlayerName(),
                id: this.getPlayerId(),
                extra: extra,
                rank: -1,
                score: score
            };
            localStorage.setItem('highscore.' + table_id, JSON.stringify(entry));
            return entry;
        }
        return old;
    }

    public async getEntries(table_id: string, from: number, maxcount: number): Promise<HighscoreData> {
        await delayAsync(30);    // simulate network lag
        let entry = await this.getPlayerScore(table_id);
        let data = new HighscoreData();
        if(entry.score > 0) {
            data.addEntry(entry);
        }
        return data;
    }

}
