export interface IHighscoreTableEntry {
    id: string;
    name: string;
    score: number;
    rank: number;
    extra: string;
}

/**
 * Highscore table data - platforms that support
 * highscore tables should populate this structure
 * to provide a uniform interface for use in games.
 *
 * Can be used to provide the basic backing data
 * structure for an actual high score table.
 */
export class HighscoreData {

    public entries: IHighscoreTableEntry[] = [];

    constructor() {}

    /**
     * Remove all entries
     */
    public clear(): void {
        this.entries = [];
    }

    /**
     * Sort all entries according to highest score.
     * If any entry is
     */
    public sort(): void {
        this.entries = this.entries.sort((a, b) => b.score - a.score);

        // Update rank
        for(let i = 0; i < this.entries.length; ++i) {
            let e = this.entries[i];
            if(e.rank === -1) {
                e.rank = i + 1;
            }
        }
    }

    /**
     * Go through all entries and invalidate all ranking.
     * The next call to addEntry, merge or sort will cause
     * all entries to be automatically (re-)ranked.
     */
    public invalidateRanks(): void {
        for(let e of this.entries) {
            e.rank = -1;
        }
    }

    /**
     * See if the list of highscore table entries contains an entry
     * with the specified unique id
     *
     * @param id a unique id string
     */
    public hasEntryWithId(id: string): boolean {
        for(let e of this.entries) {
            if(e.id === id) return true;
        }
        return false;
    }

    /**
     * Add a high score entry.
     *
     * @param uid a unique ID, or null to disable entry matching
     * @param name
     * @param score
     * @param rank
     * @param extra
     */
    public addEntry(entry: IHighscoreTableEntry): void {

        let old = false;
        for(let e of this.entries) {
            if(entry.id && (e.id === entry.id)){
                // Update entry
                e.name = entry.name;
                e.score = entry.score;
                e.rank = entry.rank;
                e.extra = entry.extra;

                old = true;
                break;
            }
        }

        if(!old) {
            this.entries.push({
                id: entry.id,
                name: entry.name,
                score: entry.score,
                rank: entry.rank,
                extra: entry.extra
            });
        }
    }

    /**
     * Sort the scores and remove the last (lowest) entry.
     */
    public removeLast(): void {
        if(this.entries.length) {
            this.sort();
            this.entries.splice(this.entries.length - 1, 1);
        }
    }

    /**
     * Merge data from an other HighscoreData object into this one.
     *
     * @param other another HighscoreData object
     */
    public merge(other: HighscoreData): void {
        const findEntry = (uid: string) => {
            if(!!uid) {
                for(let e of this.entries) {
                    if(e.id === uid) return e;
                }
            }
            return null;
        };

        for(let e of other.entries) {
            let orig = findEntry(e.id);
            if(orig) {
                orig.name = e.name;
                orig.rank = e.rank;
                orig.score = e.score;
                orig.extra = e.extra;
            } else {
                this.entries.push(e);
            }
        }

        this.sort();
    }
}
