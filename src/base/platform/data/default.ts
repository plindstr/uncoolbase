import { DataProvider } from "./dataprovider";

export class DefaultDataProvider extends DataProvider {

    public async setUserData(key: string, data: string): Promise<void> {
        localStorage.setItem(key, data);
    }

    public async getUserData(key: string): Promise<string> {
        return localStorage.getItem(key);
    }

    public isAvailable(): boolean {
        return true;
    }

    public async initialize(): Promise<void> {
        // Nop
    }

}
