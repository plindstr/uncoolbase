import { PlatformProvider } from "../platformprovider";

/**
 * Abstract base API for a deployment platform module
 */
export abstract class DataProvider extends PlatformProvider {

    /**
     * Unconditionally store user data by a key. The same data
     * must then be retrievable with the same key. If data with
     * the specified key already exists, the old data shall
     * be overwritten.
     *
     * @param key Identifying key for the data
     * @param data Data to store, in string form (use e.g. base64 encode)
     */
    public abstract async setUserData(key: string, data: string): Promise<void>;

    /**
     * Retrieve previously stored user data. If data with the
     * specified key does not exist, return an empty string.
     *
     * @param key Identifying key for the data to get
     */
    public abstract async getUserData(key: string): Promise<string>;

}
