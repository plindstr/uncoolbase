import { IAPProvider } from "./iapprovider";

export class DefaultIAPProvider extends IAPProvider {

    public isAvailable(): boolean {
        return false;
    }

    public initialize(): Promise<void> {
        return new Promise(res => res());
    }

    public playerHasPurchase(product_id: string): Promise<boolean> {
        return new Promise((res, rej) => rej());
    }

    public requestPurchase(product_id: string): Promise<boolean> {
        return new Promise((res, rej) => rej());
    }

}
