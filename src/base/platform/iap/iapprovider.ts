import { PlatformProvider } from "../platformprovider";

/**
 * In-app Purchase Provider
 *
 * Provides a generic API into an in-app purchase system
 */
export abstract class IAPProvider extends PlatformProvider  {

    /**
     * Check if a player has purchased a product
     * Returns a boolean promise indicating whether or not the player
     * is indeed in posession of the product
     *
     * @param product_id ID of product to check (platform specific)
     */
    public abstract async playerHasPurchase(product_id: string): Promise<boolean>;

    /**
     * Request that the player purchase a product.
     * Returns a boolean promise indicating whether or not the player
     * went through with the purchase.
     *
     * @param product_id ID of product to purchase
     */
    public abstract async requestPurchase(product_id: string): Promise<boolean>;

}
