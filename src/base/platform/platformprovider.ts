/**
 * Base type for all platform feature providers.
 */
export abstract class PlatformProvider {

    /**
     * Return true if the features provided by an abstract platform
     * provider can be used.
     */
    public abstract isAvailable(): boolean;

    /**
     * Run initialization code for the platform.
     *
     * This function is called automatically by base.start() -
     * setting of deployment-specific platform providers should
     * be performed before then.
     *
     * This should return a promise when it completes. Platforms
     * are initialized asynchronously in order.
     */
    public abstract async initialize(): Promise<void>;

}
