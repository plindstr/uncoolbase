import { DeploymentProvider } from "./deployment/deploymentprovider";
import { DefaultDeploymentProvider } from "./deployment/default";
import { RuntimeProvider } from "./runtime/runtimeprovider";
import { DefaultRuntimeProvider } from "./runtime/default";
import { IAPProvider } from "./iap/iapprovider";
import { DefaultIAPProvider } from "./iap/default";
import { AdProvider } from "./ads/adprovider";
import { DefaultAdProvider } from "./ads/default";
import { HighscoreProvider } from "./highscore/highscoreprovider";
import { DefaultHighscoreProvider } from "./highscore/default";
import { DataProvider } from "./data/dataprovider";
import { DefaultDataProvider } from "./data/default";
import { assert } from "../core/assert";

export * from "./data/dataprovider"
export * from "./ads/adprovider"
export * from "./deployment/deploymentprovider"
export * from "./highscore/highscoredata"
export * from "./highscore/highscoreprovider"
export * from "./iap/iapprovider"
export * from "./runtime/runtimeprovider"
export * from "./devicetype"
export * from "./deviceorientation"

let dataProvider: DataProvider = new DefaultDataProvider();

export const setDataProvider = (provider: DataProvider): void => {
    assert(!!provider, "Data provider can not be null");

    dataProvider = provider;
}

export const getDataProvider = (): DataProvider => {
    return dataProvider;
}

let deploymentProvider: DeploymentProvider = new DefaultDeploymentProvider();

export const setDeploymentProvider = (provider: DeploymentProvider): void => {
    assert(!!provider, "Deployment provider can not be null");

    deploymentProvider = provider;
}

export const getDeploymentProvider = (): DeploymentProvider => {
    return deploymentProvider;
}

let runtimeProvider: RuntimeProvider = new DefaultRuntimeProvider();

export const setRuntimeProvider = (provider: RuntimeProvider): void => {
    assert(!!provider, "Runtime provider can not be null");

    runtimeProvider = provider;
}

export const getRuntimeProvider = (): RuntimeProvider => {
    return runtimeProvider;
}

let iapProvider: IAPProvider = new DefaultIAPProvider();

export const setIAPProvider = (provider: IAPProvider): void => {
    assert(!!provider, "IAP provider can not be null");

    iapProvider = provider;
}

export const getIAPProvider = (): IAPProvider => {
    return iapProvider;
}

let adProvider: AdProvider = new DefaultAdProvider();

export const setAdProvider = (provider: AdProvider): void => {
    assert(!!provider, "Ad provider can not be null");

    adProvider = provider;
}

export const getAdProvider = (): AdProvider => {
    return adProvider;
}

let highscoreProvider: HighscoreProvider = new DefaultHighscoreProvider();

export const setHighscoreProvider = (provider: HighscoreProvider): void => {
    assert(!!provider, "Highscore provider can not be null");

    highscoreProvider = provider;
}

export const getHighscoreProvider = (): HighscoreProvider => {
    return highscoreProvider;
}

export async function initializePlatformProviders(): Promise<void> {
    await deploymentProvider.initialize();
    await runtimeProvider.initialize();
    await dataProvider.initialize();
    await highscoreProvider.initialize();
    await adProvider.initialize();
    await iapProvider.initialize();
}
