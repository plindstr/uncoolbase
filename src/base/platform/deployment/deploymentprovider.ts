import { PlatformProvider } from "../platformprovider";
import { AssetList } from "../../loader";

/**
 * Abstract base API for a deployment platform module
 */
export abstract class DeploymentProvider extends PlatformProvider {

    /**
     * Begin loading of the application. Returns a promise that resolves
     * when all assets have loaded.
     *
     * @param assetList initial asset list required for main program start
     */
    public abstract async loadApplication(assetList: AssetList): Promise<void>;

    /**
     * Start the application main loop.
     *
     * The promise resolves after the platform has acknowledged enginge startup.
     */
    public abstract async startApplication(): Promise<void>;

}
