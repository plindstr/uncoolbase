import { AssetList, getLoader } from "../../loader";
import { DeploymentProvider } from "./deploymentprovider";

export class DefaultDeploymentProvider extends DeploymentProvider {

    public isAvailable(): boolean {
        return true;
    }

    public async initialize(): Promise<void> {
        return new Promise(res => res());
    }

    public async loadApplication(assetList: AssetList): Promise<void> {
        return new Promise((res, rej) => {
            let loader = getLoader();
            loader.onComplete = () => {
                loader.onComplete = null;
                loader.onError = null;
                res();
            };
            loader.onError = (err: string) => {
                loader.onComplete = null;
                loader.onError = null;
                rej(new Error(err));
            };
            getLoader().load(assetList);
        });
    }

    public async startApplication(): Promise<void> {
        return new Promise(res => res());
    }

}
