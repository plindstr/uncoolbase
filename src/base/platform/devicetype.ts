/**
 * Enumeration of possible device types
 */
export enum DeviceType {

    /**
     * A desktop PC
     */
    DESKTOP,

    /**
     * A mobile device, such as a phone or tablet
     */
    MOBILE,

    /**
     * Something else, or we're not sure which
     */
    UNKNOWN

};
