
export enum DeviceOrientation {
    LANDSCAPE,
    PORTRAIT
}
