import { Music } from "./music";
import { clamp, abs } from "../math/coremath";
import { Sound } from "./sound";
import { SourceNodePool } from "./sourcenodepool";
import { debugError, warn } from "../core/logging";
import { getAudioContext } from "./context";

let available = false;

interface MixerChannel {

    setSourceNode(node: AudioNode): void;

    getSourceNode(): AudioNode;

    setDetune(amount: number): void;

    getDetune(): number;

    connect(dest: AudioNode): void;

    disconnect(): void;

    mute(): void;

    unmute(): void;

    isMuted(): boolean;

    setVolume(vol: number): void;

    getVoume(): number;

    setLooping(enabled: boolean): void;

    isLooping(): boolean;

    setStereoBias(bias: number): void;

    getStereoBias(): number;

    startPlayback(timeOffset?: number): void;

    stopPlayback(): void;

    isPlaying(): boolean;

    getPosition(): number;

}

export abstract class AbstractChannel implements MixerChannel {

    protected _context: AudioContext = null;
    private _sourceNode: AudioNode = null;
    protected _stereoNode: PannerNode = null;
    protected _volumeNode: GainNode = null;
    protected _playing: boolean = false;
    protected _looping: boolean = false;
    private _volume: number = 1.0;
    private _stereo: number = 0.0;
    private _detune: number = 0.0;
    private _mute: boolean = false;

    constructor() {
        let ctx = this._context = getAudioContext();
        available = !!ctx;

        this._stereoNode = ctx.createPanner();
        this._stereoNode.panningModel = 'equalpower';
        this._volumeNode = ctx.createGain();
        this._stereoNode.connect(this._volumeNode);
    }

    public connect(dest: AudioNode): void {
        if(!available) return;

        this._volumeNode.connect(dest);
    }

    public disconnect(): void {
        if(!available) return;

        this._volumeNode.disconnect();
    }

    public setSourceNode(node: AudioNode): void {
        if(!available) return;

        this.stopPlayback();

        if(this._sourceNode) {
            this._sourceNode.disconnect();
            if((<any>this._sourceNode).onended) {
                (<any>this._sourceNode).onended = false;
            }
        }

        this._sourceNode = node;
        if(this._sourceNode) {
            this._sourceNode.connect(this._stereoNode);
        }
    }

    public getSourceNode(): AudioNode {
        return this._sourceNode;
    }

    public setDetune(amount: number): void {
        if(!available) return;

        if(this._sourceNode) {
            if((<any>this._sourceNode).detune) {
                (<any>this._sourceNode).detune.value = amount;
            } else if((<any>this._sourceNode).playbackRate) {
                (<any>this._sourceNode).playbackRate.value = amount;
            }
        }

        this._detune = amount;
    }

    public getDetune(): number {
        return this._detune;
    }

    public mute(): void {
        this._mute = true;

        if(this.isPlaying()) {
            this._volumeNode.gain.value = 0;
        }
    }

    public unmute(): void {
        this._mute = false;

        if(this.isPlaying()) {
            this._volumeNode.gain.value = this._volume;
        }
    }

    public isMuted(): boolean {
        return this._mute;
    }

    public setVolume(vol: number): void {
        this._volume = clamp(vol, 0, 1);

        if(!available) return;

        if(!this._mute) {
            this._volumeNode.gain.value = this._volume;
        }
    }

    public getVoume(): number {
        return this._volume;
    }

    public setLooping(enabled: boolean): void {
        this._looping = !!enabled;
    }

    public isLooping(): boolean {
        return this._looping;
    }

    public setStereoBias(bias: number): void {
        bias = clamp(bias, -1, 1);
        this._stereo = bias;

        if(!available) return;

        let q = 1 - abs(bias);
        this._stereoNode.setPosition(bias, 0, q);
    }

    public getStereoBias(): number {
        return this._stereo;
    }

    protected initPlayback(): void {
        let bias = this._stereo;
        let q = 1 - abs(bias);
        this._stereoNode.setPosition(bias, 0, q);

        if(this._mute) {
            this._volumeNode.gain.value = 0;
        } else {
            this._volumeNode.gain.value = this._volume;
        }

        this.setDetune(this._detune);
    }

    public isPlaying(): boolean {
        return this._playing;
    }

    public abstract startPlayback(timeOffset?: number): void;

    public abstract stopPlayback(): void;

    public abstract getPosition(): number;

}

export class SoundChannel extends AbstractChannel {

    private _sound: Sound = null;
    private _timeStarted: number = 0;

    public setSound(sound: Sound): void {
        this._sound = sound;
        sound['__internal_setChannel'](this);
    }

    public getSound(): Sound {
        return this._sound;
    }

    public setLooping(enable: boolean): void {
        super.setLooping(!!enable);

        if(this.isPlaying()) {
            (this.getSourceNode() as AudioBufferSourceNode).loop = !!enable;
        }
    }

    public startPlayback(timeOffset: number = 0): void {
        if(!available) return;

        this.stopPlayback();

        this._timeStarted = this._context.currentTime;
        let srcnode = SourceNodePool.get().alloc();
        srcnode.buffer = this._sound.getBuffer().getData();
        this.setSourceNode(srcnode);
        this.setVolume(this._sound.getVolume());
        this.setStereoBias(this._sound.getStereoBias());
        this.setDetune(this._sound.getDetune());
        srcnode.loop = this._sound.isLooping();
        srcnode.onended = () => {
            this._playing = false;
        };
        this.initPlayback();
        srcnode.start(0, timeOffset);
        this._playing = true;
    }

    public stopPlayback(): void {
        if(!available) return;

        if(this.isPlaying()) {
            let node = this.getSourceNode() as AudioBufferSourceNode;
            if(node) {
                node.stop();
                node.disconnect();
            } else {
                debugError("[base.SoundChannel]","Invalid state during stopPlayback(): sound channel playing without source node?!");
            }
        }
        this._playing = false;
        this._timeStarted = 0;
    }

    public getPosition(): number {
        if(this.isPlaying()) {
            return this._context.currentTime - this._timeStarted;
        }
        return 0;
    }

}

//
// NOTE: the music channel may need to have an alternative API, using
// MP3 chunking for certain older mobile browsers.
//

export class MusicChannel extends AbstractChannel {

    private _music: Music = null;

    public setMusic(music: Music): void {
        this._music = music;
    }

    public getMusic(): Music {
        return this._music;
    }

    public setLooping(enabled: boolean): void {
        super.setLooping(enabled);

        if(!available || !this._music) return;

        this._music.getStream().setLooping(enabled);
    }

    public startPlayback(timeOffset: number = 0): void {
        if(!available || !this._music) return;

        if(this.isPlaying()) {
            this.stopPlayback();
        }

        this.setSourceNode(this._music.getStream().getSourceNode());
        this.setVolume(this._music.getVolume());
        this.setLooping(this._music.isLooping());

        this.initPlayback();

        let s = this._music.getStream();
        s.setPosition(timeOffset);
        s.play();

        this._playing = true;
    }

    public stopPlayback(): void {
        if(!available || !this._music) return;

        if(this.isPlaying()) {
            this._music.getStream().stop();
            this._playing = false;
        }
    }

    public getPosition(): number {
        if(this.isPlaying()) {
            return this._music.getStream().getPosition();
        }
        return 0;
    }

}
