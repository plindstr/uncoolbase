import { getMixer } from "./mixer";
import { AudioStream } from "./audiostream";
import { Tween } from "../tween";

let notAvailable = () => !getMixer().isAvailable();
let suspended = () => getMixer().isSuspended();

export class Music {

    private _stream: AudioStream;
    private _volume: number;
    private _pausePosition: number;
    private _looping: boolean;
    private _tween: Tween;

    constructor(stream: AudioStream) {
        this._stream = stream;
        this._volume = 1.0;
        this._pausePosition = 0;
        this._looping = true;
    }

    public getStream(): AudioStream {
        return this._stream;
    }

    public isLooping(): boolean {
        return this._looping;
    }

    public setLooping(enable: boolean): Music {
        this._looping = !!enable;

        if(this.isPlaying()) {
            getMixer().getMusicChannel().setLooping(false);
        }

        return this;
    }

    public isPlaying(): boolean {
        if(notAvailable() || suspended()) return false;

        let chan = getMixer().getMusicChannel();
        return chan.getMusic() == this && chan.isPlaying();
    }

    public isPaused(): boolean {
        return this._pausePosition != 0;
    }

    public getPausePosition(): number {
        return this._pausePosition;
    }

    public play(): Music {
        let chan = getMixer().getMusicChannel();

        if(notAvailable() || suspended()) {
            this._pausePosition = 0;
            chan.setMusic(this);
            return this;
        }

        this._pausePosition = 0;
        chan.stopPlayback();
        chan.setMusic(this);
        chan.startPlayback();
        return this;
    }

    public pause(): Music {
        if(notAvailable() || suspended() || !this.isPlaying()) return this;

        let chan = getMixer().getMusicChannel();
        this._pausePosition = chan.getPosition();
        chan.stopPlayback();
        return this;
    }

    public pauseWithFadeOut(onComplete?: () => void, duration: number = 350, power: number = 0.8): Music {
        if(notAvailable() || suspended() || !this.isPlaying()) {
            if(onComplete) {
                onComplete.call(this);
            }

            return this;
        }

        let chan = getMixer().getMusicChannel();

        if(this._tween) {
            this._tween.end();
        }

        this._tween = new Tween(duration, (k: number) => Math.pow(k, power), (vol) => {
            vol = 1.0 - vol;
            chan.setVolume(vol * this._volume);
        }, () => {
            this._pausePosition = chan.getPosition();
            chan.stopPlayback();
            this._tween = null;

            if(onComplete) {
                onComplete.call(this);
            }
        }).start();

        return this;
    }

    public resume(): Music {
        if(notAvailable() || suspended()) return this;

        let chan = getMixer().getMusicChannel();
        chan.setMusic(this);
        chan.startPlayback(this._pausePosition);
        return this;
    }

    public resumeWithFadeIn(onComplete?: () => void, duration: number = 350, power: number = 0.8): Music {
        if(notAvailable() || suspended()) {
            if(onComplete) {
                onComplete.call(this);
            }
            return this;
        }

        if(this._tween) {
            this._tween.end();
        }

        let chan = getMixer().getMusicChannel();
        chan.setMusic(this);
        chan.startPlayback(this._pausePosition);
        chan.setVolume(0);

        this._tween = new Tween(duration, (k: number) => Math.pow(k, power), (vol) => {
            chan.setVolume(vol * this._volume);
        }, () => {
            this._tween = null;

            if(onComplete) {
                onComplete.call(this);
            }
        }).start();

        return this;
    }

    public playWithFadeIn(onComplete?: () => void, duration: number = 1000, power: number = 0.8): Music {
        let chan = getMixer().getMusicChannel()

        if(notAvailable() || suspended()) {
            chan.setMusic(this);
            if(onComplete) {
                onComplete.call(this);
            }
            return this;
        }

        chan.setMusic(this);
        chan.startPlayback();

        this._tween = new Tween(duration, (k: number) => Math.pow(k, power), (vol) => {
            chan.setVolume(vol * this._volume);
        }, () => {
            this._tween = null;

            if(onComplete) {
                onComplete.call(this);
            }
        }).start();

        return this;
    }

    public stop(): Music {
        let chan = getMixer().getMusicChannel()
        this._pausePosition = 0;

        if(notAvailable() || suspended() || !this.isPlaying()) {
            if(chan.getMusic() == this) {
                chan.setMusic(null);
            }
            return this;
        }

        chan.stopPlayback();
        chan.setMusic(null);

        return this;
    }

    public stopWithFadeOut(onComplete?: () => void, duration: number = 1000, power: number = 0.8): Music {
        let chan = getMixer().getMusicChannel()
        this._pausePosition = 0;

        if(notAvailable() || suspended() || !this.isPlaying()) {
            if(onComplete) {
                onComplete.call(this);
            }
            return this;
        }

        if(this._tween) {
            this._tween.end();
        }

        this._tween = new Tween(duration, (k: number) => Math.pow(k, power), (vol) => {
            vol = 1.0 - vol;
            chan.setVolume(vol * this._volume);
        }, () => {
            chan.stopPlayback();
            this._tween = null;

            if(onComplete) {
                onComplete.call(this);
            }
        }).start();
        return this;
    }

    public getVolume(): number {
        return this._volume;
    }

    public setVolume(vol: number): Music {
        this._volume = vol;
        let chan = getMixer().getMusicChannel()
        if(chan.getMusic() == this) {
            chan.setVolume(this._volume);
        }
        return this;
    }

    public clone(): Music {
        let mus = new Music(this._stream);
        mus._volume = this._volume;
        return mus;
    }

}
