import { getMixer } from "./mixer";
import { SoundChannel } from "./channels";
import { SoundBuffer } from "./soundbuffer";
import { clamp } from "../math";

let notAvailable = () => !getMixer().isAvailable();

/**
 * Sound control object.
 */
export class Sound {

    private _channel: SoundChannel;
    private _buffer: SoundBuffer;
    private _volume: number;
    private _stereo: number;
    private _detune: number;
    private _looping: boolean;

    constructor(buffer: SoundBuffer) {
        this._buffer = buffer;
        this._looping = false;
        this._volume = 1.0;
        this._stereo = 0.0;
        this._detune = 0.0;
    }

    protected __internal_setChannel(channel: SoundChannel): void {
        this._channel = channel;
    }

    protected __internal_getChannel(): SoundChannel {
        return this._channel;
    }

    public play(): Sound {
        if(notAvailable() || !this._buffer) {
            return this;
        }

        if(this._channel) {
            getMixer().stopSound(this);
        }

        getMixer().playSound(this);
        return this;
    }

    public stop(): Sound {
        if(notAvailable() || !this._buffer) {
            return this;
        }

        getMixer().stopSound(this);
        return this;
    }

    public isPlaying(): boolean {
        return this._channel != null;
    }

    public getBuffer(): SoundBuffer {
        return this._buffer;
    }

    public setLooping(enable: boolean): Sound {
        this._looping = !!enable;
        if(this._channel) {
            this._channel.setLooping(!!enable);
        }
        return this;
    }

    public isLooping(): boolean {
        return this._looping;
    }

    /**
     * Return volume between 0 and 1
     */
    public getVolume(): number {
        return this._volume;
    }

    /**
     * Set volume (float value between 0 and 1)
     */
    public setVolume(vol: number): Sound {
        this._volume = clamp(vol, 0, 1);
        if(this._channel) {
            this._channel.setVolume(vol);
        }

        return this;
    }

    /**
     * Control detune of current channel
     *
     * @param amount amount to detune (see docs)
     */
    public setDetune(amount: number): Sound {
        this._detune = amount;
        if(this._channel) {
            this._channel.setDetune(amount);
        }
        return this;
    }

    /**
     * Return the amount of detune
     */
    public getDetune(): number {
        return this._detune;
    }

    /**
     * Get stereo bias (-1: left, 0: center, 1: right)
     * TODO: implement this feature!
     */
    public getStereoBias(): number {
        return this._stereo;
    }

    /**
     * Set stereo bias (-1: left, 0: center, 1: right)
     * TODO: implement this feature!
     */
    public setStereoBias(bias: number = 0): Sound {
        this._stereo = clamp(bias, -1, 1);
        if(this._channel) {
            this._channel.setStereoBias(this._stereo);
        }

        return this;
    }

    public clone(): Sound {
        var snd = new Sound(this._buffer);
        snd._volume = this._volume;
        snd._stereo = this._stereo;
        return snd;
    }

}
