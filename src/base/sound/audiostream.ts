import { debugWarnNamed } from "../core/logging";
import { createXHR } from "../util/xhr";

import { SoundBuffer } from "./soundbuffer";
import { SourceNodePool } from "./sourcenodepool";
import { getMixer } from "./mixer";


/**
 * XXX: AudioStream is curerntly based on MediaElementAudioSourceNode.
 * HOWEVER, iOS hates the <audio> element, and as such has made it necessary
 * to eventually rewrite this entire thing as a proper chunker (currently
 * ONLY mp3 is supported for that).
 *
 * The current music system is written so that by satisfying the API of this
 * one object, a chunk streaming system will plug into the existing codebase.
 */
export abstract class AudioStream {

    public static createFromURL(url: string): Promise<AudioStream> {
        if(navigator.userAgent.search(/iPhone|iPad|iOS|Safari/i)) {
            return BufferNodeAudioStream.create(url);
        }
        return MediaElementAudioStream.create(url);
    }

    public abstract getSourceNode(): AudioNode;

    public abstract play(): void;

    public abstract stop(): void;

    public abstract setLooping(enabled: boolean): void;

    public abstract setPosition(time: number): void;

    public abstract getPosition(): number;

    public abstract getDuration(): number;

}

class BufferNodeAudioStream extends AudioStream {

    private _buffer: SoundBuffer = null;
    private _node: AudioBufferSourceNode = null;
    private _position: number = 0;

    public static create(url: string): Promise<BufferNodeAudioStream> {
        return new Promise<BufferNodeAudioStream>((res, rej) => {
            createXHR(url, (xhr) => {
                let buf = new SoundBuffer(xhr.response, () => {
                    res(new BufferNodeAudioStream(buf));
                });
            }, rej, 'arraybuffer').send();
        });
    }

    constructor(buffer: SoundBuffer) {
        super();
        this._buffer = buffer;
    }

    public getSourceNode(): AudioNode {
        if(!this._node) {
            this._node = SourceNodePool.get().alloc();
            this._node.buffer = this._buffer.getData();
        }
        return this._node;
    }

    public play(): void {
        this.getSourceNode();
        this._node.start(0, this._position);
    }

    public stop(): void {
        if(this._node) {
            this._node.stop();
        }
        this._node = null;
    }

    public setLooping(enabled: boolean): void {
        this.getSourceNode();
        this._node.loop = !!enabled;
    }

    public setPosition(time: number): void {
        this._position = time;
    }

    public getPosition(): number {
        return 0;
    }

    public getDuration(): number {
        return this._buffer.getDuration();
    }

}

class MediaElementAudioStream extends AudioStream {
    private _element: HTMLAudioElement = null;
    private _node: MediaElementAudioSourceNode = null;

    public static create(url: string): Promise<MediaElementAudioStream> {
        return new Promise<MediaElementAudioStream>((res, rej) => {
            let e = document.createElement('audio');
            e.src = url;
            e.preload = 'auto';

            var removeEventListeners = () => {
                try { e.removeEventListener('loadedmetadata', onload); } catch(err) {}
                try { e.removeEventListener('canplay', onload); } catch(err) {}
                try { e.removeEventListener('error', onerror); } catch(err) {}
            };

            var onload = () => {
                try { clearTimeout(failsafe); } catch(ignore) {}
                removeEventListeners();
                res(new MediaElementAudioStream(e));
            };

            var onerror = () => {
                removeEventListeners();
                rej("AudioStream load failed");
            };

            if(navigator.userAgent.match(/Safari/i)) {
                e.addEventListener('loadedmetadata', onload);
                var failsafe = setTimeout(() => {
                    onload();
                }, 1000);
            }
            e.addEventListener('canplay', onload);
            e.addEventListener('error', onerror);
            e.load();
        });
    }

    constructor(element: HTMLAudioElement) {
        super();

        this._element = element;
        let mixer = getMixer();
        mixer.whenAvailable(() => {
            this._node = mixer.getContext().createMediaElementSource(this._element);
        });
    }

    public getSourceNode(): AudioNode {
        return this._node;
    }

    public play(): void {
        this._element.play().catch((err) => {
            debugWarnNamed(this, err);
        });
    }

    public stop(): void {
        this._element.pause();
    }

    public setLooping(enabled: boolean): void {
        this._element.loop = !!enabled;
    }

    public setPosition(time: number): void {
        this._element.currentTime = time;
    }

    public getPosition(): number {
        return this._element.currentTime;
    }

    public getDuration(): number {
        return this._element.duration;
    }
}
