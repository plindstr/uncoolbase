import { debugError } from "../core/logging";
import { getMixer } from "./mixer";

export class SoundBuffer {
    private _sourcebuffer: ArrayBuffer = null;
    private _data: AudioBuffer = null;

    constructor(data: ArrayBuffer, onAvailable: () => void) {
        let mixer = getMixer();

        if(!mixer.isAvailable()) {
            this._sourcebuffer = null;
            this._data = null;
            mixer.decodeWhenAvailable(this);
            onAvailable && (onAvailable.call(this));
        } else {
            try {
                mixer.getContext().decodeAudioData(data, (buffer: AudioBuffer) => {
                    this._data = buffer;
                    onAvailable && (onAvailable.call(this));
                }, (err: any) => {
                    debugError("Failed to decode audio buffer, source data: ", data, " error: ", err);
                    onAvailable && (onAvailable.call(this));
                });
            } catch(err) {
                debugError("Failed to create audio buffer, reason:", err);
                this._data = null;
                onAvailable && (onAvailable.call(this));
            }
        }
    }

    public async decode(): Promise<void> {
        if(this._sourcebuffer) {
            let mixer = getMixer();
            return new Promise((res, rej) => {
                mixer.getContext().decodeAudioData(this._sourcebuffer, (data: AudioBuffer) => {
                    this._data = data;
                    this._sourcebuffer = null;
                    res();
                }, (err: any) => {
                    debugError("Failed to decode audio buffer, source data: ", this._sourcebuffer);
                    this._sourcebuffer = null;
                    rej(err);
                });
            });
        }
    }

    /**
     * Get access to the underlying AudioBuffer object
     */
    public getData(): AudioBuffer {
        return this._data;
    }

    /**
     * Get duration of this sound buffer, in seconds
     */
    public getDuration(): number {
        if(this._data) {
            return this._data.duration;
        }
        return 0;
    }

}
