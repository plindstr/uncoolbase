import { debugError, debugLog } from "../core/logging";

let defaultContext: AudioContext = null;

export function setAudioContext(ctx: AudioContext): void {
    defaultContext = ctx;
}

export function getAudioContext(): AudioContext {
    if(!defaultContext) {
        debugError("Default audio context is not available!");
    }
    return defaultContext;
}
