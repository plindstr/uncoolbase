import { getAudioContext } from "./context";

const POOL_SIZE = 8;
let pool: SourceNodePool = null;

export class SourceNodePool {
    private _context: AudioContext;
    private _nodes: AudioBufferSourceNode[] = [];
    private _frame: number = 0;

    constructor() {
        this._context = getAudioContext();
    }

    public alloc(): AudioBufferSourceNode {
        if(this._nodes.length) {
            return this._nodes.pop();
        }

        // Create a buffer source node immediately
        return this._context.createBufferSource();
    }

    public update(): void {
        if(this._nodes.length < POOL_SIZE) {
            // Create a new source node only every three frames
            if(this._frame > 2) {
                this._nodes.unshift(this._context.createBufferSource());
                this._frame = 0;
            }
            this._frame++;
        }
    }

    public static get(): SourceNodePool {
        if(!pool) {
            pool = new SourceNodePool();
        }
        return pool;
    }
}
