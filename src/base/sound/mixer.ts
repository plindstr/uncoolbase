import { SoundChannel, MusicChannel } from "./channels";
import { SoundBuffer } from "./soundbuffer";
import { Sound } from "./sound";
import { debugError, debugLog } from "../core";
import { array_clear, array_add_unique } from "../mem";
import { clamp } from "../math";
import { Music } from "./music";
import { SourceNodePool } from "./sourcenodepool";
import { setAudioContext } from "./context";
import { error } from "../core/logging";

const MAX_QUEUED: number = 5;       // number of sounds that can be queued per frame
const NUM_CHANNELS: number = 11;    // number of channels available for sound playback

let available = false;

/**
 * WebAudio based sound playback system.
 */
export class Mixer {

    private _context: AudioContext;
    private _channels: SoundChannel[] = [];
    private _musicChannel: MusicChannel;
    private _masterVolumeNode: GainNode;
    private _musicVolumeNode: GainNode;
    private _soundVolumeNode: GainNode;

    private _decodeQueue: SoundBuffer[] = [];
    private _onAvailableQueue: (() => void)[] = [];
    private _soundQueue: Sound[] = [];
    private _queuedBuffers: SoundBuffer[] = [];
    private _mainVolume: number = 0.9;
    private _soundVolume: number = 0.8;
    private _musicVolume: number = 0.8;

    constructor(document: Document) {

        //
        // Attempt to create context
        //

        var options: AudioContextOptions = {
            latencyHint: 'interactive'
        };

        try {
            (<any>window).AudioContext = (<any>window).AudioContext || (<any>window).webkitAudioContext;
            this._context = new AudioContext(options);
            if(!this._context) {
                this._context = undefined;
            }
        } catch(e) {
            error("WebAudio not available on platform");
            this._context = undefined;
        }

        let ctx = this._context;
        setAudioContext(ctx);
        available = !!this._context;
        if(!available) return;

        // Set spatial reference
        if(ctx.listener) {
            ctx.listener.forwardX && (ctx.listener.forwardX.value = 0);
            ctx.listener.forwardY && (ctx.listener.forwardY.value = 0);
            ctx.listener.forwardZ && (ctx.listener.forwardZ.value = -1);
            ctx.listener.upX && (ctx.listener.upX.value = 0);
            ctx.listener.upY && (ctx.listener.upY.value = 1);
            ctx.listener.upZ && (ctx.listener.upZ.value = 1);
        }

        // Create master volume node
        this._masterVolumeNode = ctx.createGain();
        this._masterVolumeNode.gain.value = this._mainVolume;
        this._masterVolumeNode.connect(ctx.destination);

        // Create sound and music volume nodes
        this._musicVolumeNode = ctx.createGain();
        this._musicVolumeNode.gain.value = this._musicVolume;
        this._musicVolumeNode.connect(this._masterVolumeNode);

        this._soundVolumeNode = ctx.createGain();
        this._soundVolumeNode.gain.value = this._soundVolume;
        this._soundVolumeNode.connect(this._masterVolumeNode);

        // Set up channels
        this._musicChannel = new MusicChannel();
        this._musicChannel.setVolume(1.0);
        this._musicChannel.setStereoBias(0.0);
        this._musicChannel.connect(this._musicVolumeNode);

        for(var i = 0; i < NUM_CHANNELS; ++i) {
            var chan = new SoundChannel();
            chan.setVolume(1.0);
            chan.setStereoBias(0.0);
            chan.connect(this._soundVolumeNode);
            this._channels.push(chan);
        }

        if(ctx.state !== 'running') {
            let unlock = (e: any) => {
                ctx.resume().then(() => {
                    document.removeEventListener('touchend', unlock);
                    document.removeEventListener('mouseup', unlock);
                    document.removeEventListener('keyup', unlock);

                    this._decodeAudio().then(() => {
                    }).catch(err => {
                        debugError("[base.Mixer] Error decoding audio buffers, reason:", err);
                    });

                    for(let fn of this._onAvailableQueue) {
                        fn.call(null);
                    }
                    array_clear(this._onAvailableQueue);

                    if(this._musicChannel.getMusic() !== null) {
                        this._musicChannel.startPlayback();
                    }
                });
            };
            document.addEventListener('touchend', unlock);
            document.addEventListener('mouseup', unlock);
            document.addEventListener('keyup', unlock);
        }
    }

    private async _decodeAudio(): Promise<void> {
        let decode = this._decodeQueue.length > 0;
        while(this._decodeQueue.length > 0) {
            let buf = this._decodeQueue.pop();
            try {
                await buf.decode();
            } catch(err) {
                debugError("[base.Mixer] Error decoding buffer", buf, "reason:", err);
            }
        }

        if(decode) {
            debugLog("[base.Mixer] Deferred audio decode OK");
        }
    }

    public decodeWhenAvailable(buffer: SoundBuffer): void {
        array_add_unique(this._decodeQueue, buffer);
    }

    public whenAvailable(fn: () => void): void {
        if(this.isAvailable && !this.isSuspended()) {
            fn.call(null);
        } else {
            array_add_unique(this._onAvailableQueue, fn);
        }
    }

    public isAvailable(): boolean {
        return available;
    }

    public suspend(): void {
        if(!available) return;
        this._context.suspend();
    }

    public resume(): void {
        if(!available) return;
        this._context.resume();
    }

    public isSuspended(): boolean {
        if(!available) return true;
        return this._context.state === 'suspended';
    }

    public getContext(): AudioContext {
        if(!available) return null;
        return this._context;
    }

    public getMasterVolumeNode(): GainNode {
        return this._masterVolumeNode;
    }

    public getVolume(): number {
        return this._mainVolume;
    }

    /**
     * Set master volume
     *
     * @param vol a value between 0 (silent) and 1 (max)
     */
    public setVolume(vol: number): Mixer {
        this._mainVolume = clamp(vol, 0, 1);

        if(this._context) {
            this._masterVolumeNode.gain.value = this._mainVolume;
        }

        return this;
    }

    public getSoundVolume(): number {
        return this._soundVolume;
    }

    /**
     * Set master sound volume
     *
     * @param vol a value between 0 (silent) and 1 (max)
     */
    public setSoundVolume(vol: number): Mixer {
        this._soundVolume = clamp(vol, 0, 1);

        if(this._context) {
            this._soundVolumeNode.gain.value = this._soundVolume;
        }

        return this;
    }

    /**
     * Set master music volume
     *
     * @param vol a value between 0 (silent) and 1 (max)
     */
    public setMusicVolume(vol: number): Mixer {
        this._musicVolume = clamp(vol, 0, 1);

        if(this._context) {
            this._musicVolumeNode.gain.value = this._musicVolume;
        }

        return this;
    }

    public getMusicVolume(): number {
        return this._musicVolume;
    }

    public stopAllSounds(): void {
        if(!available) return;
        if(!this._context) return;

        for(var i = 0; i < NUM_CHANNELS; ++i) {
            this._channels[i].stopPlayback();
        }
    }

    public mute(): void {
        if(!available) return;
        this._masterVolumeNode.gain.value = 0;
    }

    public unmute(): void {
        if(!available) return;
        this._masterVolumeNode.gain.value = this._mainVolume;
    }

    public playSound(sound: Sound): void {
        if(!available) return;
        if(!this._context) return;
        if(this.isSuspended()) return;

        // Make sure we're not playing the same sample too soon
        if(this._queuedBuffers.indexOf(sound.getBuffer()) >= 0) return;

        if(this._soundQueue.indexOf(sound) === -1) {
            this._soundQueue.push(sound);
            this._queuedBuffers.push(sound.getBuffer());
        }

        // Discard sounds bigger than queue
        while(this._soundQueue.length > MAX_QUEUED) {
            this._soundQueue.shift();
        }
    }

    public stopMusic(): void {
        if(!available) return;
        if(!this._context) return;

        this._musicChannel.stopPlayback();
    }

    public getCurrentMusic(): Music {
        if(!available) return;
        if(!this._context) return;

        return this._musicChannel.getMusic();
    }

    public getMusicChannel(): MusicChannel {
        return this._musicChannel;
    }

    public stopSound(sound: Sound): void {
        if(!available) return;
        if(!this._context) return;

        var idx = this._soundQueue.indexOf(sound);
        if(idx >= 0) {
            // Sound is already queued; do nothing
            return;
        } else {
            // Make sure we're not trying to start the same sound twice
            for(var i = 0; i < this._soundQueue.length; ++i) {
                if(this._soundQueue[i].getBuffer() == sound.getBuffer()) {
                    return;
                }
            }

            // Sound may be playing; cancel it
            for(var i = 0; i < NUM_CHANNELS; ++i) {
                if(this._channels[i].getSound() == sound) {
                    this._channels[i].stopPlayback();
                }
            }
        }
    }

    public update(): void {
        if(!available) return;
        if(!this._context) return;

        SourceNodePool.get().update();

        this._masterVolumeNode.gain.value = this._mainVolume;
        this._musicVolumeNode.gain.value = this._musicVolume;
        this._soundVolumeNode.gain.value = this._soundVolume;

        this._queuedBuffers.splice(0, this._queuedBuffers.length);

        // Empty out queue, attempt to assign to available channels,
        // cancel oldest
        while(this._soundQueue.length) {
            let sound = this._soundQueue.pop();

            for(let i = 0; i < NUM_CHANNELS; ++i) {

                let chan = this._channels[i];

                if(chan.getSound() == sound || !chan.isPlaying()) {
                    chan.setSound(sound);
                    chan.startPlayback();
                    break;
                }

                // ..just drop the sound? maybe
            }

        }

    }

}

let instance: Mixer = null;

export const getMixer = (): Mixer => {
    if(!instance) {
        instance = new Mixer(document);
    }
    return instance;
}
