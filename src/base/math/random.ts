const mrandom = Math.random;
const mpow = Math.pow;
const mlog = Math.log;
const round = (a: number) => (a + 0.5) | 0;
const nearestPow2 = (num: number): number => mpow(2, round(mlog(num) / mlog(2)));

/**
 * Rotating float random number pool.
 * Provides faster random values than Math.random()
 * at the cost of being predictable and attackable.
 */
export class Random {

    private _data: Float32Array;
    private _size: number;
    private _position: number;

    constructor(size: number = 65536) {
        this._size = nearestPow2(size);
        this._data = new Float32Array(size);
        this._position = 0;
        this.init();
    }

    public size(): number {
        return this._size;
    }

    public init(): Random {
        for(let i = 0; i < this._size; ++i) {
            this._data[i] = mrandom();
        }
        return this;
    }

    public next(): number {
        let p = this._position;
        let v = this._data[p];
        this._data[p] = 1.0 - v;
        this._position = (p + 1) & (this._size - 1);
        return v;
    }

    public nextInRange(min: number, max: number): number {
        let delta = max - min;
        let v = this.next();
        return min + v * delta;
    }
}

let randpool = new Random(32768);

export function init_fast_rand(size = randpool.size()) {
    if(randpool.size() != size) {
        randpool = new Random(size);
    } else {
        randpool.init();
    }
}

export function fast_rand(min: number, max: number): number {
    return randpool.nextInRange(min, max);
}

export function fast_randInt(min: number, max: number): number {
    return randpool.nextInRange(min | 0, max | 0) | 0;
}
