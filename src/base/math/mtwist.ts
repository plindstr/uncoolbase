///
/// Mersenne Twister implementation
/// Adapted from https://gist.github.com/banksean/300494
///

const M = 624;
const N = 397;
const MATRIX_A   = 0x9908b0df;
const UPPER_MASK = 0x80000000;
const LOWER_MASK = 0x7fffffff;

/**
 * Mersenne twister implementation based on gist by
 * Sean McCullough (banksean@gmail.com) and rewritten
 * and cleaned up for use in UncoolBase.
 *
 * Original algorithm by Takuji Nishimura and Makoto Matsumoto.
 */
export class MTwist {

    private _entropy: number[];
    private _index: number;

    /**
     * Create a new mersenne twister instance
     *
     * @param seed [optional] seed value to use, defaults to Date.now()
     */
    constructor(seed: number = Date.now()) {
        let me = this;
        me._entropy = new Array<number>(N);
        me._index = N + 1;
        me.init(seed);
    }

    /**
     * Initialize the entropy pool. This also sets index to N + 1 as a
     * side effect so as to cause the next call to random() or random_iN()
     * to call randomize(). This function is automatically called from the
     * constructor.
     *
     * @param seed seed value to use.
     */
    public init(seed: number): MTwist {
        let me = this;
        me._entropy[0] = seed >>> 0;
        for (me._index = 1; me._index < N; me._index++) {
            let s = me._entropy[me._index - 1] ^ (me._entropy[me._index - 1] >>> 30);
            me._entropy[me._index] =
                (((((s & 0xffff0000) >>> 16) * 1812433253) << 16) +
                (s & 0x0000ffff) * 1812433253) +
                me._index;

            // See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier.
            // In the previous versions, MSBs of the seed affect
            // only MSBs of the array mt[].
            // 2002/01/09 modified by Makoto Matsumoto

            me._entropy[me._index] >>>= 0;
            // for >32 bit machines
        }

        me._index = N + 1;
        return me;
    }

    /**
     * Randomize the entropy pool. This function is called every time the
     * maximum number of generated random values (397) has been reached.
     */
    public randomize(): MTwist {
        let me = this;
        let y: number;
        let kk: number;

        for(kk = 0; kk < N - M; kk++) {
            y = (me._entropy[kk] & UPPER_MASK) | (me._entropy[kk + 1] & LOWER_MASK);
            me._entropy[kk] = me._entropy[kk + M] ^ (y >>> 1) ^ ((y & 0x1) * MATRIX_A);
        }

        for(; kk < N - 1; kk++) {
            y = (me._entropy[kk] & UPPER_MASK) | (me._entropy[kk + 1] & LOWER_MASK);
            me._entropy[kk] = me._entropy[kk + (M - N)] ^ (y >>> 1) ^ ((y & 0x1) * MATRIX_A);
        }

        y = (me._entropy[N - 1] & UPPER_MASK) | (me._entropy[0] & LOWER_MASK);
        me._entropy[N - 1] = me._entropy[M - 1] ^ (y >>> 1) ^ ((y & 0x1) * MATRIX_A);

        me._index = 0;

        return me;
    }

    /**
     * Return a random 32 bit integer. Note, that unless explicitly bit-shifted
     * down, this value will be interpreted as signed 32 bit, meaning you get a
     * value between -2^31 and 2^31-1.
     */
    public random_i32(): number {
        let me = this;
        if (me._index >= N) { // generate N words at one time
            me.randomize();
        }

        let y = me._entropy[me._index++];

        // Tempering
        y ^= (y >>> 11);
        y ^= (y <<   7) & 0x9d2c5680;
        y ^= (y <<  15) & 0xefc60000;
        y ^= (y >>> 18);

        return y >>> 0;
    }

    /**
     * Return a random 31 bit integer, i.e. a value between 1 and 2^31 - 1
     */
    public random_i31(): number {
        return this.random_i32() >>> 1;
    }

    /**
     * Return a random floating point value between 0 and 1, exclusive,
     */
    public random(): number {
        return this.random_i31() * (1.0 / 2147483647.0);
    }

    /**
     * Return a random floating-point number from a specific range
     *
     * @param min minumum value
     * @param max maxumum value
     */
    public rand(min: number, max: number): number {
        return min + ((this.random_i31() * (1.0 / 2147483647.0)) * (max - min));
    }

    /**
     * Return a random integer from a specific range
     *
     * @param min minumum value (truncated to integer)
     * @param max maxumum value (truncated to integer)
     */
    public randInt(min: number, max: number): number {
        min |= 0;
        max |= 0;
        return min + (((this.random_i31() * (1.0 / 2147483647.0)) * (max - min)) | 0);
    }
}
