import { Vec2 } from "./vec";
import { MTwist } from "./mtwist";
import { clamp, rand } from "./coremath";

/**
 * 2D rectangle object consisiting of a position and size vector.
 * Provided to make it simpler to control coordinates as they relate
 * to rectangular ares.
 * Similar in concept but different in intended function to AABB
 */
export class Rect {

    public position: Vec2;
    public size: Vec2;

    constructor(x: number = 0, y: number = 0, w: number = 0, h: number = 0) {
        this.position = new Vec2(x,y);
        this.size = new Vec2(w,h);
    }

    /**
     * Test if a point is inside this rectangle
     *
     * @param v a Vec2 instance
     * @returns true if the x and y coordinates of v are inside the range of [position, position + size]
     */
    public containsPoint(v: Vec2): boolean {
        return this.containsPointXY(v.x,v.y);
    }

    /**
     * Test if a point is inside this rectangle
     *
     * @param x X coordinate
     * @param y Y coordinate
     * @returns true if the x and y coordinates are inside the range of [position, position + size]
     */
    public containsPointXY(x: number, y: number): boolean {
        let x0 = this.position.x;
        let x1 = x0 + this.size.x;
        let y0 = this.position.y;
        let y1 = y0 + this.size.y;
        if(x0 > x1) { let t = x0; x0 = x1; x1 = t; }
        if(y0 > y1) { let t = y0; y0 = y1; y1 = t; }

        return x >= x0 && x < x1 && y >= y0 && y < y1;
    }

    public containsCircle(v: Vec2, radius: number): boolean {
        let x0 = this.position.x;
        let x1 = x0 + this.size.x;
        let y0 = this.position.y;
        let y1 = y0 + this.size.y;
        if(x0 > x1) { let t = x0; x0 = x1; x1 = t; }
        if(y0 > y1) { let t = y0; y0 = y1; y1 = t; }

        let x = v.x;
        let y = v.y;
        return x + radius >= x0 && x - radius < x1 && y + radius >= y0 && y - radius < y1;
    }


    public containsCircleXY(x: number, y: number, radius: number): boolean {
        let x0 = this.position.x;
        let x1 = x0 + this.size.x;
        let y0 = this.position.y;
        let y1 = y0 + this.size.y;
        if(x0 > x1) { let t = x0; x0 = x1; x1 = t; }
        if(y0 > y1) { let t = y0; y0 = y1; y1 = t; }

        return x + radius >= x0 && x - radius < x1 && y + radius >= y0 && y - radius < y1;
    }

    /**
     * Test if this rectangle intersects another rectangle
     *
     * @param r another Rect instance
     * @returns true if there is overlap between this rect and r
     */
    public intersectsRect(r: Rect): boolean {
        let x0 = this.position.x;
        let x1 = x0 + this.size.x;
        let y0 = this.position.y;
        let y1 = y0 + this.size.y;
        if(x0 > x1) { let t = x0; x0 = x1; x1 = t; }
        if(y0 > y1) { let t = y0; y0 = y1; y1 = t; }

        let x2 = r.position.x;
        let y2 = r.position.y;
        let x3 = x2 + r.size.x;
        let y3 = y2 + r.size.y;
        if(x2 > x3) { let t = x2; x2 = x3; x3 = t; }
        if(y2 > y3) { let t = y2; y2 = y3; y3 = t; }

        let xtest: boolean = !(x0 > x3 || x1 < x2);
        let ytest: boolean = !(y0 > y3 || y1 < y2);

        return xtest && ytest;
    }

    /**
     * Get a random point from inside the area of this Rect
     *
     * @param v [optional] vector to store result in
     * @returns the vector instance the coordinates were written to
     */
    public getRandomPoint(v = new Vec2()): Vec2 {
        v.x = this.position.x + rand(0, this.size.x);
        v.y = this.position.y + rand(0, this.size.y);
        return v;
    }

    /**
     * Get a random point inside the rect, using a Mersenne Twister
     * RNG instance for predictable results.
     *
     * @param mt a Mersenne Twister RNG instance - random() gets called twice
     * @param v [optional] vector to store result in
     * @returns the vector instance the coordinates were written to
     */
    public getRandomPointMT(mt: MTwist, v = new Vec2()): Vec2 {
        v.x = this.position.x + mt.random() * this.size.x;
        v.y = this.position.y + mt.random() * this.size.y;
        return v;
    }

    /**
     * Modify a Rect object so that it's contained inside this Rect.
     *
     * @returns true if parameter Rect was modified
     */
    public confine(r: Rect): boolean {
        let cx = this.confineX(r);
        return this.confineY(r) || cx;
    }

    /**
     * Modify a Rect object's X positionc omponent so that it's contained inside this Rect.
     *
     * @returns true if parameter Rect was modified
     */
    public confineX(r: Rect): boolean {
        let x = r.position.x;
        r.position.x = clamp(x,this.position.x,this.position.x + this.size.x - r.size.x);
        return x != r.position.x;
    }

    /**
     * Modify a Rect object's Y position component so that it's contained inside this Rect.
     *
     * @returns true if parameter Rect was modified
     */
    public confineY(r: Rect): boolean {
        let y = r.position.y;
        r.position.y = clamp(y,this.position.y,this.position.y + this.size.y - r.size.y);
        return y != r.position.y;
    }

}
