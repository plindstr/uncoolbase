import { fast_sin } from "./triglut";
import { getTimeCurrent } from "../core/timer";

const PI = Math.PI;
const pow = Math.pow;

const mabs = (a: number) => a < 0 ? -a : a;
const msign = (x: number) => x ? (x < 0 ? -1 : 1) : 0;
const mfrac = (x: number) => x - (x | 0);

/**
 * Calculate a sine wave - by default this function is connected to
 * the running time counter and, as such, provides a per-frame varying
 * waveform.
 *
 * @param low Low point of wave
 * @param high High point of wave
 * @param freq Frequency of wave (i.e. how many times per second it goes from low to high and back)
 * @param timeOffset [optional] how many milliseconds from the base time reference to offset the wave
 * @param timeBase [optional] reference time value in milliseconds
 */
export const sineWave = (low: number, high: number, freq: number, timeOffset: number = 0, timeBase: number = getTimeCurrent()): number => {
    let tm = (timeBase + timeOffset) * (PI * 0.002);
    let phase = fast_sin(tm * freq);
    let range = (high - low) * .5;
    return phase * range + range + low;
}

/**
 * Same as sine wave, but with a power function applied to the phase
 *
 * @param low Low point of wave
 * @param high High point of wave
 * @param freq Frequency of wave (i.e. how many times per second it goes from low to high and back)
 * @param power Power to apply to phase (phase^power)
 * @param timeOffset [optional] how many milliseconds from the base time reference to offset the wave
 * @param timeBase [optional] reference time value in milliseconds
 */
export const sineWavePow = (low: number, high: number, freq: number, power: number, timeOffset: number = 0, timeBase: number = getTimeCurrent()): number => {
    let tm = (timeBase + timeOffset) * (PI * 0.002);
    let phase = fast_sin(tm * freq);
    let range = (high - low) * .5;
    phase = pow((phase + 1.0) * .5, power) * 2 - 1.0;
    return phase * range + range + low;
}

/**
 * Calculate a triangle wave - by default this function is connected to
 * the running time counter and, as such, provides a per-frame varying
 * waveform.
 *
 * @param low Low point of wave
 * @param high High point of wave
 * @param freq Frequency of wave (i.e. how many times per second it goes from low to high and back)
 * @param timeOffset [optional] how many milliseconds from the base time reference to offset the wave
 * @param timeBase [optional] reference time value in milliseconds
 */
export const triWave = (low: number, high: number, freq: number, timeOffset: number = 0, timeBase: number = getTimeCurrent()): number => {
    let tm = (timeBase * freq + timeOffset) * 0.001;
    let range = (high - low) * .5;
    let inner = tm * .5 + .25;
    let outer = .5 - mfrac(inner);
    let phase = (1 - 4 * mabs(outer));
    return phase * range + range + low;
}

/**
 * Same as triWave, but with a power function applied to the phase
 *
 * @param low Low point of wave
 * @param high High point of wave
 * @param freq Frequency of wave (i.e. how many times per second it goes from low to high and back)
 * @param power Power to apply to phase (phase^power)
 * @param timeOffset [optional] how many milliseconds from the base time reference to offset the wave
 * @param timeBase [optional] reference time value in milliseconds
 */
export const triWavePow = (low: number, high: number, freq: number, power: number, timeOffset: number = 0, timeBase: number = getTimeCurrent()): number => {
    let tm = (timeBase * freq + timeOffset) * 0.001;
    let range = (high - low) * .5;
    let inner = tm * .5 + .25;
    let outer = .5 - mfrac(inner);
    let phase = (1 - 4 * mabs(outer));
    return pow(phase, power) * range + range + low;
}

/**
 * Calculate a sawtooth wave - by default this function is connected to
 * the running time counter and, as such, provides a per-frame varying
 * waveform.
 *
 * @param low Low point of wave
 * @param high High point of wave
 * @param freq Frequency of wave (i.e. how many times per second it goes from low to high and back)
 * @param timeOffset [optional] how many milliseconds from the base time reference to offset the wave
 * @param timeBase [optional] reference time value in milliseconds
 */
export const sawtoothWave = (low: number, high: number, freq: number, timeOffset: number = 0, timeBase: number = getTimeCurrent()): number => {
    let tm = (timeBase * freq + timeOffset) * 0.001;
    let range = high - low;
    return range * mfrac(tm) + low;
}

/**
 * Same as sawtooth wave, but with a power function applied to the phase
 *
 * @param low Low point of wave
 * @param high High point of wave
 * @param freq Frequency of wave (i.e. how many times per second it goes from low to high and back)
 * @param power Power to apply to phase (phase^power)
 * @param timeOffset [optional] how many milliseconds from the base time reference to offset the wave
 * @param timeBase [optional] reference time value in milliseconds
 */
export const sawtoothWavePow = (low: number, high: number, freq: number, power: number, timeOffset: number = 0, timeBase: number = getTimeCurrent()): number => {
    let tm = (timeBase * freq + timeOffset) * 0.001;
    let range = high - low;
    return range * pow(mfrac(tm), power) + low;
}

/**
 * Calculate a sqare wave - by default this function is connected to
 * the running time counter and, as such, provides a per-frame varying
 * waveform.
 *
 * @param low Low point of wave
 * @param high High point of wave
 * @param freq Frequency of wave (i.e. how many times per second it goes from low to high and back)
 * @param timeOffset [optional] how many milliseconds from the base time reference to offset the wave
 * @param timeBase [optional] reference time value in milliseconds
 */
export const squareWave = (low: number, high: number, freq: number, timeOffset: number = 0, timeBase: number = getTimeCurrent()): number => {
    let tm = (timeBase + timeOffset) * (PI * 0.002);
    let phase = fast_sin(tm * freq);
    let range = (high - low) * .5;
    return msign(phase) * range + range + low;
}
