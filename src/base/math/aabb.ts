import { Vec2 } from "./vec";
import { min, max, sqrt } from "./coremath"

const mmin = min;
const mmax = max;

/**
 * Axis-aligned bounding box functionality
 */
export class AABB {
    public min: Vec2 = new Vec2();
    public max: Vec2 = new Vec2();

    public set(p: Vec2): AABB {
        let min = this.min;
        let max = this.max;
        min.x = max.x = p.x;
        min.y = max.y = p.y;
        return this;
    }

    public setXY(x: number, y: number): AABB {
        let min = this.min;
        let max = this.max;
        min.x = max.x = x;
        min.y = max.y = y;
        return this;
    }

    public add(p: Vec2): AABB {
        let min = this.min;
        let max = this.max;
        min.x = mmin(p.x, min.x);
        min.y = mmin(p.y, min.y);
        max.x = mmax(p.x, max.x);
        max.y = mmax(p.y, max.y);
        return this;
    }

    public addXY(x: number, y: number): AABB {
        let min = this.min;
        let max = this.max;
        min.x = mmin(x, min.x);
        min.y = mmin(y, min.y);
        max.x = mmax(x, max.x);
        max.y = mmax(y, max.y);
        return this;
    }

    public copy(bb: AABB): AABB {
        let min = this.min;
        let max = this.max;
        min.x = bb.min.x;
        min.y = bb.min.y;
        max.x = bb.max.x;
        max.y = bb.max.y;
        return this;
    }

    public merge(bb: AABB): AABB {
        let min = this.min;
        let max = this.max;

        min.x = mmin(bb.min.x, min.x);
        min.y = mmin(bb.min.y, min.y);
        max.x = mmax(bb.max.x, max.x);
        max.y = mmax(bb.max.y, max.y);

        return this;
    }

    public getX0(): number {
        return this.min.x;
    }

    public getX1(): number {
        return this.max.x;
    }

    public getY0(): number {
        return this.min.y;
    }

    public getY1(): number {
        return this.max.y;
    }

    public getCenterX(): number {
        return (this.min.x + this.max.x) * .5;
    }

    public getCenterY(): number {
        return (this.min.y + this.max.y) * .5;
    }

    public getWidth(): number {
        return this.max.x - this.min.x;
    }

    public getHeight(): number {
        return this.max.y - this.min.y;
    }

    public getRadius(): number {
        let dx = (this.max.x - this.min.x) * .5;
        let dy = (this.max.y - this.min.y) * .5;
        return sqrt(dx * dx + dy * dy);
    }

    public contains(p: Vec2): boolean {
        let min = this.min;
        let max = this.max;

        let lr = p.x >= min.x && p.x < max.x;
        let ud = p.y >= min.y && p.y < max.y;

        return lr && ud;
    }

    public containsXY(px: number, py: number): boolean {
        let min = this.min;
        let max = this.max;

        let lr = px >= min.x && px < max.x;
        let ud = py >= min.y && py < max.y;

        return lr && ud;
    }

    public containsXorY(px: number, py: number): boolean {
        let min = this.min;
        let max = this.max;

        let lr = px >= min.x && px < max.x;
        let ud = py >= min.y && py < max.y;

        return lr || ud;
    }

    public isTouching(other: AABB): boolean {
        let min = this.min;
        let max = this.max;
        let min2 = other.min;
        let max2 = other.max;
        let lr = max.x < min2.x || min.x > max2.x;
        let ud = max.y < min2.y || min.y > max2.y;
        return !(lr || ud);
    }

    public isInside(other: AABB): boolean {
        let min = this.min;
        let max = this.max;
        let min2 = other.min;
        let max2 = other.max;
        let lr = min.x >= min2.x && max.x <= max2.x;
        let ud = min.y >= min2.y && max.y <= max2.y;
        return lr && ud;
    }

    public isOutside(other: AABB): boolean {
        let min = this.min;
        let max = this.max;
        let min2 = other.min;
        let max2 = other.max;
        let lr = max.x < min2.x || min.x > max2.x;
        let ud = max.y < min2.y || min.y > max2.y;
        return lr || ud;
    }

    public toString(): string {
        let min = this.min;
        let max = this.max;
        return `AABB (${min.x}, ${min.y}) -> (${max.x}, ${max.y})`;
    }
}
