import { fast_cos, fast_sin } from "./triglut";

const mabs = Math.abs;
const msqrt = Math.sqrt;
const matan2 = Math.atan2;

export class Vec2 {

    public static ZERO: Vec2 = new Vec2(0,0);

    public x: number;
    public y: number;

    constructor(x: number = 0, y: number = x) {
        let t = this;
        t.x = x;
        t.y = y;
    }

    public equals(v: Vec2) {
        let t = this;
        let bx = mabs(v.x - t.x) <= 0.00001;
        let by = mabs(v.y - t.y) <= 0.00001;
        return bx && by;
    }

    public length(): number {
        let dx = this.x * this.x;
        let dy = this.y * this.y;
        return msqrt(dx + dy);
    }

    public normalize(): Vec2 {
        let t = this;
        let l = 1.0 / this.length();
        t.x *= l;
        t.y *= l;
        return t;
    }

    /**
     * Get angle through atan2
     */
    public getAngle(): number {
        let t = this;
        let l = 1.0 / this.length();
        let x = t.x * l;
        let y = t.y * l;
        return matan2(-y, x);
    }

    /**
     * Invert components of this vector
     *
     * @returns a reference to self
     */
    public invert(): Vec2 {
        let t = this;
        t.x = -t.x;
        t.y = -t.y;
        return t;
    }

    /**
     * Invert this vector's X component
     *
     * @returns a reference to self
     */
    public invertX(): Vec2 {
        let t = this;
        t.x = -t.x;
        return t;
    }

    /**
     * Invert this vector's Y component
     *
     * @returns a reference to self
     */
    public invertY(): Vec2 {
        let t = this;
        t.y = -t.y;
        return t;
    }

    /**
     * Set this vector's components identical to that of another vector
     *
     * @param v another vector object
     */
    public set(v: Vec2): Vec2 {
        let t = this;
        t.x = v.x;
        t.y = v.y;
        return t;
    }

    /**
     * Set this vector's components from an angle and a length.
     *
     * @param angle angle in radians
     * @param length [optional] length of desired vector, defaults to 1
     * @returns a reference to self
     */
    public setA(angle: number, length: number = 1.0): Vec2 {
        this.x = fast_cos(angle) * length;
        this.y = fast_sin(angle) * length;
        return this;
    }

    /**
     * Set this vector's X and Y components
     *
     * @param x X coordinate value
     * @param y [optional] Y coordinate value, defaults to X value
     * @returns a reference to self
     */
    public setXY(x: number, y: number = x): Vec2 {
        let t = this;
        t.x = x;
        t.y = y;
        return t;
    }

    /**
     * Clamp the components of this vector to a value range defined by the
     * component values of two vectors
     *
     * @param min vector with minimum x and y values
     * @param max vector with maximum x and y values
     * @returns a reference to self
     */
    public clamp(min: Vec2, max: Vec2): Vec2 {
        return this.clampXY(min.x,max.x,min.y,max.y);
    }

    /**
     * Clamp this vector's X component to a range
     *
     * @param xmin minimum x value
     * @param xmax maximum x value
     * @returns a reference to self
     */
    public clampX(xmin: number, xmax: number): Vec2 {
        let t = this;
        t.x = t.x < xmin ? xmin : t.x > xmax ? xmax : t.x;
        return t;
    }

    /**
     * Clamp this vector's Y component to a range
     *
     * @param ymin minimum x value
     * @param ymax maximum x value
     * @returns a reference to self
     */
    public clampY(ymin: number, ymax: number): Vec2 {
        let t = this;
        t.y = t.y < ymin ? ymin : t.y > ymax ? ymax : t.y;
        return t;
    }

    /**
     * Clamp this vector's components to a range
     *
     * @param xmin minimum x component value
     * @param xmax maximum x component value
     * @param ymin [optional] minimum y component value (defaults to xmin)
     * @param ymax [optional] maximum y component value (defaults to xmax)
     * @returns a reference to self
     */
    public clampXY(xmin: number, xmax: number, ymin: number = xmin, ymax: number = xmax): Vec2 {
        let t = this;
        t.clampX(xmin,xmax);
        t.clampY(ymin,ymax);
        return t;
    }

    public add(v: Vec2): Vec2 {
        let t = this;
        t.x += v.x;
        t.y += v.y;
        return t;
    }

    public addXY(x: number, y: number = x): Vec2 {
        let t = this;
        t.x += x;
        t.y += y;
        return t;
    }

    public subtract(v: Vec2): Vec2 {
        let t = this;
        t.x -= v.x;
        t.y -= v.y;
        return t;
    }

    public subtractXY(x: number, y: number = x): Vec2 {
        let t = this;
        t.x -= x;
        t.y -= y;
        return t;
    }

    public multiply(v: Vec2): Vec2 {
        let t = this;
        t.x *= v.x;
        t.y *= v.y;
        return t;
    }

    public multiplyXY(x: number, y: number = x): Vec2 {
        let t = this;
        t.x *= x;
        t.y *= y;
        return t;
    }

    public divide(v: Vec2): Vec2 {
        let t = this;
        t.x /= v.x;
        t.y /= v.y;
        return this;
    }

    public divideXY(x: number, y: number = x): Vec2 {
        let t = this;
        t.x /= x;
        t.y /= y;
        return this;
    }

    /**
     * Create a copy of this vector
     *
     * @returns a new vector object with the same component values as this one
     */
    public clone(): Vec2 {
        let t = this;
        return new Vec2(t.x,t.y);
    }
}

export class Vec3 {

    public static ZERO: Vec3 = new Vec3(0,0,0);

    public x: number;
    public y: number;
    public z: number;

    constructor(x: number = 0, y: number = x, z: number = y) {
        let t = this;
        t.x = x;
        t.y = y;
        t.z = z;
    }

    public equals(v: Vec3) {
        let t = this;
        let bx = mabs(v.x - t.x) <= 0.00001;
        let by = mabs(v.y - t.y) <= 0.00001;
        let bz = mabs(v.z - t.z) <= 0.00001;
        return bx && by && bz;
    }

    public length(): number {
        let t = this;
        let dx = t.x * t.x;
        let dy = t.y * t.y;
        let dz = t.z * t.z;
        return msqrt(dx + dy + dz);
    }

    public normalize(): Vec3 {
        let t = this;
        let l = 1.0 / t.length();
        t.x *= l;
        t.y *= l;
        t.z *= l;
        return t;
    }

    public invert(): Vec3 {
        let t = this;
        t.x = -t.x;
        t.y = -t.y;
        t.z = -t.z;
        return t;
    }

    public invertX(): Vec3 {
        let t = this;
        t.x = -t.x;
        return t;
    }

    public invertY(): Vec3 {
        let t = this;
        t.y = -t.y;
        return t;
    }

    public invertZ(): Vec3 {
        let t = this;
        t.z = -t.z;
        return t;
    }

    public set(v: Vec3): Vec3 {
        let t = this;
        t.x = v.x;
        t.y = v.y;
        t.z = v.z;
        return t;
    }

    public setXYZ(x: number, y: number = x, z: number = y): Vec3 {
        let t = this;
        t.x = x;
        t.y = y;
        t.z = z;
        return t;
    }

    public clamp(min: Vec3, max: Vec3): Vec3 {
        return this.clampXYZ(min.x,max.x,min.y,max.y,min.z,max.z);
    }

    public clampX(xmin: number, xmax: number): Vec3 {
        let t = this;
        if(t.x < xmin) t.x = xmin;
        if(t.x > xmax) t.x = xmax;
        return t;
    }

    public clampY(ymin: number, ymax: number): Vec3 {
        let t = this;
        if(t.y < ymin) t.y = ymin;
        if(t.y > ymax) t.y = ymax;
        return t;
    }

    public clampZ(zmin: number, zmax: number): Vec3 {
        let t = this;
        if(t.z < zmin) t.z = zmin;
        if(t.z > zmax) t.z = zmax;
        return t;
    }

    public clampXYZ(xmin: number, xmax: number, ymin: number = xmin, ymax: number = xmax, zmin: number = ymin, zmax: number = ymax): Vec3 {
        let t = this;
        t.clampX(xmin,xmax);
        t.clampY(ymin,ymax);
        t.clampZ(zmin,zmax);
        return t;
    }

    public add(v: Vec3): Vec3 {
        let t = this;
        t.x += v.x;
        t.y += v.y;
        t.z += v.z;
        return t;
    }

    public addXYZ(x: number, y: number = x, z: number = y): Vec3 {
        let t = this;
        t.x += x;
        t.y += y;
        t.z += z;
        return t;
    }

    public subtract(v: Vec3): Vec3 {
        let t = this;
        t.x -= v.x;
        t.y -= v.y;
        t.z -= v.z;
        return t;
    }

    public subtractXYZ(x: number, y: number = x, z: number = y): Vec3 {
        let t = this;
        t.x -= x;
        t.y -= y;
        t.z -= z;
        return t;
    }

    public multiply(v: Vec3): Vec3 {
        let t = this;
        t.x *= v.x;
        t.y *= v.y;
        t.z *= v.z;
        return t;
    }

    public multiplyXYZ(x: number, y: number = x, z: number = y): Vec3 {
        let t = this;
        t.x *= x;
        t.y *= y;
        t.z *= z;
        return t;
    }

    public divide(v: Vec3): Vec3 {
        let t = this;
        t.x /= v.x;
        t.y /= v.y;
        t.z /= v.z;
        return this;
    }

    public divideXYZ(x: number, y: number = x, z: number = y): Vec3 {
        let t = this;
        t.x /= x;
        t.y /= y;
        t.z /= z;
        return this;
    }

    public clone(): Vec3 {
        let t = this;
        return new Vec3(t.x,t.y,t.z);
    }
}

export class Vec4 {

    public static ZERO: Vec4 = new Vec4(0,0,0,0);

    public x: number;
    public y: number;
    public z: number;
    public w: number;

    constructor(x: number = 0, y: number = x, z: number = y, w: number = z) {
        let t = this;
        t.x = x;
        t.y = y;
        t.z = z;
        t.w = w;
    }

    public equals(v: Vec4) {
        let t = this;
        let bx = mabs(v.x - t.x) <= 0.00001;
        let by = mabs(v.y - t.y) <= 0.00001;
        let bz = mabs(v.z - t.z) <= 0.00001;
        let bw = mabs(v.w - t.w) <= 0.00001;
        return bx && by && bz && bw;
    }

    public length(): number {
        let t = this;
        let dx = t.x * t.x;
        let dy = t.y * t.y;
        let dz = t.z * t.z;
        let dw = t.w * t.w;
        return msqrt(dx + dy + dz + dw);
    }

    public normalize(): Vec4 {
        let t = this;
        let l = 1.0 / this.length();
        t.x *= l;
        t.y *= l;
        t.z *= l;
        t.w *= l;
        return t;
    }

    public invert(): Vec4 {
        let t = this;
        t.x = -t.x;
        t.y = -t.y;
        t.z = -t.z;
        t.w = -t.w;
        return t;
    }

    public invertX(): Vec4 {
        let t = this;
        t.x = -t.x;
        return t;
    }

    public invertY(): Vec4 {
        let t = this;
        t.y = -t.y;
        return t;
    }

    public invertZ(): Vec4 {
        let t = this;
        t.z = -t.z;
        return t;
    }

    public invertW(): Vec4 {
        let t = this;
        t.w = -t.w;
        return t;
    }

    public set(v: Vec4): Vec4 {
        let t = this;
        t.x = v.x;
        t.y = v.y;
        t.z = v.z;
        t.w = v.w;
        return t;
    }

    public setXYZW(x: number, y: number = x, z: number = y, w: number = z): Vec4 {
        let t = this;
        t.x = x;
        t.y = y;
        t.z = z;
        t.w = w;
        return t;
    }

    public clamp(min: Vec4, max: Vec4): Vec4 {
        return this.clampXYZ(min.x,max.x,min.y,max.y,min.z,max.z,min.w,max.w);
    }

    public clampX(xmin: number, xmax: number): Vec4 {
        let t = this;
        if(t.x < xmin) t.x = xmin;
        if(t.x > xmax) t.x = xmax;
        return t;
    }

    public clampY(ymin: number, ymax: number): Vec4 {
        let t = this;
        if(t.y < ymin) t.y = ymin;
        if(t.y > ymax) t.y = ymax;
        return t;
    }

    public clampZ(zmin: number, zmax: number): Vec4 {
        let t = this;
        if(t.z < zmin) t.z = zmin;
        if(t.z > zmax) t.z = zmax;
        return t;
    }

    public clampW(wmin: number, wmax: number): Vec4 {
        let t = this;
        if(t.w < wmin) t.w = wmin;
        if(t.w > wmax) t.w = wmax;
        return t;
    }

    public clampXYZ(xmin: number, xmax: number, ymin: number = xmin, ymax: number = xmax, zmin: number = ymin, zmax: number = ymax, wmin: number = zmin, wmax: number = zmax): Vec4 {
        let t = this;
        t.clampX(xmin,xmax);
        t.clampY(ymin,ymax);
        t.clampZ(zmin,zmax);
        t.clampW(wmin,wmax);
        return t;
    }

    public add(v: Vec4): Vec4 {
        let t = this;
        t.x += v.x;
        t.y += v.y;
        t.z += v.z;
        t.w += v.w;
        return t;
    }

    public addXYZW(x: number, y: number = x, z: number = y, w: number = z): Vec4 {
        let t = this;
        t.x += x;
        t.y += y;
        t.z += z;
        t.w += w;
        return t;
    }

    public subtract(v: Vec4): Vec4 {
        let t = this;
        t.x -= v.x;
        t.y -= v.y;
        t.z -= v.z;
        t.w -= v.w;
        return t;
    }

    public subtractXYZW(x: number, y: number = x, z: number = y, w: number = z): Vec4 {
        let t = this;
        t.x -= x;
        t.y -= y;
        t.z -= z;
        t.w -= w;
        return t;
    }

    public multiply(v: Vec4): Vec4 {
        let t = this;
        t.x *= v.x;
        t.y *= v.y;
        t.z *= v.z;
        t.w *= v.w;
        return t;
    }

    public multiplyXYZW(x: number, y: number = x, z: number = y, w: number = z): Vec4 {
        let t = this;
        t.x *= x;
        t.y *= y;
        t.z *= z;
        t.w *= w;
        return t;
    }

    public divide(v: Vec4): Vec4 {
        let t = this;
        t.x /= v.x;
        t.y /= v.y;
        t.z /= v.z;
        t.w /= v.w;
        return this;
    }

    public divideXYZW(x: number, y: number = x, z: number = y, w: number = z): Vec4 {
        let t = this;
        t.x /= x;
        t.y /= y;
        t.z /= z;
        t.w /= w;
        return this;
    }

    public clone(): Vec4 {
        let t = this;
        return new Vec4(t.x,t.y,t.z,t.w);
    }
}
