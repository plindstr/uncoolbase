import { fast_rand, fast_randInt } from "./random";

const mmin = (a: number, b: number) => a < b ? a : b;
const mmax = (a: number, b: number) => a > b ? a : b;
const mabs = (a: number) => a < 0 ? -a : a;

/**
 * Convenience class for handling a contiguous value range
 */
export class Range {
    private _from: number;
    private _to: number;

    constructor(from = 0, to = 1) {
        this._from = from;
        this._to = to;
    }

    public isEmpty(epsilon = 0.0001): boolean {
        return mabs(this._to - this._from) <= epsilon;
    }

    public getMin(): number {
        return mmin(this._from, this._to);
    }

    public getMax(): number {
        return mmax(this._from, this._to);
    }

    public setFrom(from: number): Range {
        this._from = +from;
        return this;
    }

    public setTo(to: number): Range {
        this._to = +to;
        return this;
    }

    public addFrom(delta: number): Range {
        this._from += delta;
        return this;
    }

    public addTo(delta: number): Range {
        this._to += delta;
        return this;
    }

    public set(from: number, to: number): Range {
        this._from = +from;
        this._to = +to;
        return this;
    }

    public contains(value: number): boolean {
        return this.getMin() >= value && this.getMax() <= value;
    }

    public random(): number {
        return fast_rand(this.getMin(), this.getMax());
    }

    public randomInt(): number {
        return fast_randInt(this.getMin(), this.getMax());
    }

}
