import { wrap_pi2_norm } from "./wrap";

const pi = Math.PI;
const pi2 = pi * 2;
const pow = Math.pow;
const log = Math.log;
const round = (a: number) => (a + 0.5) | 0;

class TrigTable {

    private _fn: Function;
    private _sz: number = 0;
    private _data: number[];

    constructor(fn: Function) {
        this._fn = fn;
    }

    public recalc(size: number): void {
        size |= 0;
        this._sz = size;
        this._data = [];
        const isize = (1.0 / size) * pi2;

        for(var i = 0; i <= size; ++i) {
            this._data[i] = this._fn(i * isize);
        }
    }

    public find(value: number): number {
        var norm = wrap_pi2_norm(value);
        return this._data[(norm * this._sz) | 0];
    }

}

let sinTable = new TrigTable(Math.sin);
let cosTable = new TrigTable(Math.cos);
let tanTable = new TrigTable(Math.tan);

export function initFastTrig(size: number = 8192): void {
    // Set size to nearest power of two
    size = pow(2, round(log(size) / log(2)));

    sinTable.recalc(size);
    cosTable.recalc(size);
    tanTable.recalc(size);
}

initFastTrig();

export function fast_sin(value: number): number {
    return sinTable.find(value);
}

export function fast_cos(value: number): number {
    return cosTable.find(value);
}

export function fast_tan(value: number): number {
    return tanTable.find(value);
}
