export const pow = Math.pow;
export const sqrt = Math.sqrt;

const log = Math.log;
const mrandom = Math.random;

/**
 * Return the absolute value of input (i.e. make a number positive if it's negative)
 *
 * @param a a number
 */
export const abs = (a: number): number => a < 0 ? -a : a;

/**
 * Return the smaller of two numbers
 *
 * @param a a number
 * @param b a number
 */
export const min = (a: number, b: number) => a < b ? a : b;

/**
 * Return the greater of two numbers
 *
 * @param a a number
 * @param b a number
 */
export const max = (a: number, b: number) => a > b ? a : b;

/**
 * If a value is positive, return 1. If it is negative, return -1.
 * If it is 0, return 0.
 *
 * @param x a number
 */
export const sign = (x: number): number => x ? (x < 0 ? -1 : 1) : 0;

/**
 * Return the fractional part of a number.
 *
 * @param x a (floating point) number
 */
export const frac = (x: number): number => x - (x | 0);

/**
 * Round a number to the nearest integer.
 *
 * @param a a (floating point) number
 */
export const round = (a: number): number => (a + 0.5) | 0;

/**
 * Linearily move towards zero from value by amount
 */
export function toZero(value: number, amount: number): number {
    if((abs(value) - amount) < 0) return 0;
    if(value > 0) value -= amount;
    else value += amount;
    return value;
}

/**
 * Return a normalized progress value (between 0 and 1) over time.
 * All values must be positive.
 *
 * @param elapsed elapsed time (or similar)
 * @param limit target time (or similar)
 */
export function progress(elapsed: number, limit: number): number {
    return 1.0 - (max(limit - elapsed, 0) / limit);
}

/**
 * Confine a value between two extremes. Returns value
 * clamped to the range [min, max];
 *
 * @param value value to confine
 * @param min minimum value
 * @param max maximum value
 */
export function clamp(value: number, min: number, max: number): number {
    return value < min ? min : (value > max ? max : value);
}

/**
 * Convert an angle in degrees to radians
 *
 * @param deg angle in degrees
 */
export function toRadian(deg: number): number {
    return deg * 0.01745329251994329;
}

/**
 * Convert an angle in radians to degrees
 *
 * @param rad angle in degrees
 */
export function toDegree(rad: number): number {
    return rad * 57.2957795130832892;
}

/**
 * Find nearest power of two to a value
 *
 * @param num a number
 */
export function nearestPow2(num: number): number {
    return pow(2, round(log(num) / log(2)));
}

/**
 * Find the next power of two
 *
 * @param v
 */
export function nextPow2(v: number): number {
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    return v;
}

/**
 * Check if a number is power of two
 * @param v
 */
export function isPow2(v: number): boolean {
    return (v & (v - 1)) === 0;
}

/**
 * Get a platform-provided random value in an interval
 *
 * @param min lower bound (inclusive)
 * @param max upper bound (exclusive)
 */
export function rand(min: number, max: number): number {
    return min + (mrandom() * (max - min));
}

/**
 * Return random integer inside a value range.
 *
 * @param min minimum value (rounded down to int)
 * @param max maximum value (rounded down to int, exclusive)
 * @return an integer value between min and (max - 1)
 */
export function randInt(min: number, max: number): number {
    return rand(min | 0, max | 0) | 0;
}

/**
 * Use the platform-provided random function to decide the outcome
 * of a coin toss. True for heads, false for tails. Or vice versa,
 * if you prefer...
 */
export function flipCoin(): boolean {
    return mrandom() >= 0.5;
}
