/**
 * Wrap a value to an arbitrary value range.
 *
 * @param value value to wrap
 * @param min
 * @param max
 */
export function wrap(value: number, min: number, max: number): number {
    if (min === max) {
        return min;
    }
    let v0 = value - min;
    let d = max - min;
    let v1 = v0 - (((v0 / d) | 0) * d);
    return min + v1 + (v1 < .0 ? d : .0);
}

/**
 * Wrap a value to the range [-PI, PI]
 *
 * @param value value to wrap
 */
export function wrap_pi(value: number): number {
    const p = Math.PI;
    const d = p * 2.0;
    let v0 = value - p
    let v1 = v0 - (((v0 / d) | 0) * d);
    return v1 + (v1 < .0 ? d : .0) - p;
}

/**
 * Wrap a value to the range [0, 2 * PI]
 *
 * @param value value to wrap
 */
export function wrap_pi2(value: number): number {
    const p = Math.PI;
    const d = p * 2.0;
    let v1 = value - (((value / d) | 0) * d);
    return v1 + (v1 < .0 ? d : .0);
}

/**
 * Wrap a value to the range [0, 2 * PI] and normalize to the range [0, 1]
 *
 * @param value value to wrap
 */
export function wrap_pi2_norm(value: number): number {
    const p = Math.PI;
    const d = p * 2.0;
    const di = 1.0 / d;
    let v1 = value - (((value * di) | 0) * d);
    return (v1 + (v1 < .0 ? d : .0)) * di;
}
