import { Vec2, Vec3, Vec4 } from "./vec";
import { fast_cos, fast_sin } from "./triglut";
import { Quaternion } from "./quaternion";

const mcos  = Math.cos;
const msin  = Math.sin;
const msqrt = Math.sqrt;

/**
 * Clipped 3x3 matrix for 2D affine transforms
 */
export class Matrix {

    public static IDENTITY: Matrix = new Matrix();

    public a: number;
    public b: number;
    public c: number;
    public d: number;
    public tx: number;
    public ty: number;

    constructor(a = 1, b = 0, c = 0, d = 1, tx = 0, ty = 0) {
        this.a  = a;
        this.b  = b;
        this.c  = c;
        this.d  = d;
        this.tx = tx;
        this.ty = ty;
    }

    public set(m: Matrix): Matrix {
        this.a  = m.a;
        this.b  = m.b;
        this.c  = m.c;
        this.d  = m.d;
        this.tx = m.tx;
        this.ty = m.ty;
        return this;
    }

    public clone(): Matrix {
        return new Matrix(this.a,this.b,this.c,this.d,this.tx,this.ty);
    }

    /**
     * Mutliply this matrix by another (other x this),
     * then assign the result back to this matrix.
     *
     * @param m a reference to self
     */
    public prepend(m: Matrix): Matrix {
        let a  = m.a * this.a  + m.c * this.b;
        let b  = m.b * this.a  + m.d * this.b;
        let c  = m.a * this.c  + m.c * this.d;
        let d  = m.b * this.c  + m.d * this.d;
        let tx = m.a * this.tx + m.c * this.ty + m.tx;
        let ty = m.b * this.tx + m.d * this.ty + m.ty;

        this.a  = a;
        this.b  = b;
        this.c  = c;
        this.d  = d;
        this.tx = tx;
        this.ty = ty;

        return this;
    }

    /**
     * Multiply this matrix by another (this x other),
     * then assign the result back to this matrix.
     *
     * @param m a reference to self
     */
    public multiply(m: Matrix): Matrix {
        let a  = this.a * m.a  + this.c * m.b;
        let b  = this.b * m.a  + this.d * m.b;
        let c  = this.a * m.c  + this.c * m.d;
        let d  = this.b * m.c  + this.d * m.d;
        let tx = this.a * m.tx + this.c * m.ty + this.tx;
        let ty = this.b * m.tx + this.d * m.ty + this.ty;

        this.a  = a;
        this.b  = b;
        this.c  = c;
        this.d  = d;
        this.tx = tx;
        this.ty = ty;

        return this;
    }

    public scale(s: Vec2): Matrix {
        return this.scaleXY(s.x,s.y);
    }

    public scaleXY(sx: number, sy: number): Matrix {
        this.a *= sx;
        this.b *= sx;
        this.c *= sy;
        this.d *= sy;
        return this;
    }

    /**
     * Rotate matrix around Z axis by z radians
     */
    public rotate(z: number): Matrix {

        let ca = fast_cos(z);
        let sa = fast_sin(z);

        let ta = this.a;
        let tb = this.b;
        let tc = this.c;
        let td = this.d;

        this.a = (ta * ca) - (tc * sa);
        this.b = (tb * ca) - (td * sa);
        this.c = (ta * sa) + (tc * ca);
        this.d = (tb * sa) + (td * ca);

        return this;
    }

    public rotate_accurate(z: number): Matrix {

        let ca = mcos(z);
        let sa = msin(z);

        let ta = this.a;
        let tb = this.b;
        let tc = this.c;
        let td = this.d;

        this.a = (ta * ca) - (tc * sa);
        this.b = (tb * ca) - (td * sa);
        this.c = (ta * sa) + (tc * ca);
        this.d = (tb * sa) + (td * ca);

        return this;
    }

    public translate(d: Vec2): Matrix {
        return this.translateXY(d.x,d.y);
    }

    public translateXY(dx: number, dy: number): Matrix {
        let x   = this.tx + this.a * dx + this.c * dy;
        let y   = this.ty + this.b * dx + this.d * dy;
        this.tx = x;
        this.ty = y;
        return this;
    }

    public identity(): Matrix {
        this.a  = 1;
        this.b  = 0;
        this.c  = 0;
        this.d  = 1;
        this.tx = 0;
        this.ty = 0;
        return this;
    }

    public determinant(): number {
        let xx = this.a;
        let xy = this.b;
        let xz = .0;

        let yx = this.c;
        let yy = this.d;
        let yz = .0;

        let zx = this.tx;
        let zy = this.ty;
        let zz = 1.0;

        let determinant = + xx * (yy * zz - zy * yz)
                          - xy * (yx * zz - yz * zx)
                          + xz * (yx * zy - yy * zx);

        return determinant;
    }

    public normalize(): Matrix {

        let lx = 1.0 / msqrt(this.a  * this.a  + this.b  * this.b);
        let ly = 1.0 / msqrt(this.c  * this.c  + this.d  * this.d);
        let lt = 1.0 / msqrt(this.tx * this.tx + this.ty * this.ty);

        this.a  *= lx;
        this.b  *= lx;
        this.c  *= ly;
        this.d  *= ly;
        this.tx *= lt;
        this.ty *= lt;

        return this;
    }

    public invert(): Matrix {
        let a1  = this.a;
        let b1  = this.b;
        let c1  = this.c;
        let d1  = this.d;
        let tx1 = this.tx;
        let ty1 = this.ty;
        let n   = 1.0 / (a1 * d1 - b1 * c1);

        this.a  =   d1 * n;
        this.b  =  -b1 * n;
        this.c  =  -c1 * n;
        this.d  =   a1 * n;
        this.tx =  (c1 * ty1 - d1 * tx1) * n;
        this.ty = -(a1 * ty1 - b1 * tx1) * n;

        return this;
    }

    public projectX(x: number, y: number): number {
        return x * this.a + y * this.c + this.tx;
    }

    public projectY(x: number, y: number): number {
        return x * this.b + y * this.d + this.ty;
    }

    public projectXY(x: number, y: number, target: Vec2 = new Vec2()): Vec2 {
        target.x = x * this.a +
                   y * this.c +
                       this.tx;
        target.y = x * this.b +
                   y * this.d +
                       this.ty;
        return target;
    }

    public project(p: Vec2, target: Vec2 = new Vec2()): Vec2 {
        let tx = p.x * this.a +
                 p.y * this.c +
                       this.tx;
        let ty = p.x * this.b +
                 p.y * this.d +
                       this.ty;
        target.x = tx;
        target.y = ty;
        return target;
    }

    public equals(m: Matrix): boolean {
        return this.a  === m.a
            && this.b  === m.b
            && this.c  === m.c
            && this.d  === m.d
            && this.tx === m.tx
            && this.ty === m.ty;
    }

    /**
     * Optimized version of matrix build function
     * used by Node and Camera...
     */

     /**
      * Optimized version of matrix build function. Uses lookup
      * tables for sine/cosine calculation. Less accurate but
      * measurably faster.
      *
      * @param pos position vector
      * @param scale scale vector
      * @param rot rotation in radians
      */
    public buildNodeMatrix(pos: Vec2, scale: Vec2, rot: number): Matrix {
        let ca: number = fast_cos(rot);
        let sa: number = fast_sin(rot);

        this.a  =  (scale.x * ca);
        this.b  = -(scale.y * sa);
        this.c  =  (scale.x * sa);
        this.d  =  (scale.y * ca);
        this.tx =   pos.x;
        this.ty =   pos.y;

        return this;
    }

    /**
     * Build a matrix for a node using accurate cos/sin instead
     * of lookup table.
     *
     * @param pos position vector
     * @param scale scale vector
     * @param rot rotation in radians
     */
    public buildNodeMatrix_accurate(pos: Vec2, scale: Vec2, rot: number): Matrix {
        let ca: number = mcos(rot);
        let sa: number = msin(rot);

        this.a  =  (scale.x * ca);
        this.b  = -(scale.y * sa);
        this.c  =  (scale.x * sa);
        this.d  =  (scale.y * ca);
        this.tx =   pos.x;
        this.ty =   pos.y;

        return this;
    }

    /**
     * Build a matrix for a sprite. Uses lookup tables for sine/cos.
     *
     * @param x parent-relative X coordinate
     * @param y parent-relative Y coordinate
     * @param sx X scaling factor
     * @param sy Y scaling factor
     * @param r rotation in radians
     */
    public buildSpriteMatrix(x: number, y: number, sx: number, sy: number, r: number): Matrix {
        let ca: number = fast_cos(r);
        let sa: number = fast_sin(r);

        this.a  =  (sx * ca);
        this.b  = -(sy * sa);
        this.c  =  (sx * sa);
        this.d  =  (sy * ca);
        this.tx =   x;
        this.ty =   y;

        return this;
    }

    /**
     * Build a matrix for a sprite with a variable pivot. More complex than
     * buildSpriteMatrix by a few multiplications. Uses lookup table for sin/cos.
     *
     * @param x parent-relative X coordinate
     * @param y parent-relative Y coordinate
     * @param px pivot X coordinate
     * @param py pivot Y coordinate
     * @param sx X scaling factor
     * @param sy Y scaling factor
     * @param r rotation in radians
     */
    public buildSpriteMatrix2(x: number, y: number, px: number, py: number, sx: number, sy: number, r: number): Matrix {
        let ca: number = fast_cos(r);
        let sa: number = fast_sin(r);

        let a   = this.a =  (sx * ca);
        let b   = this.b = -(sy * sa);
        let c   = this.c =  (sx * sa);
        let d   = this.d =  (sy * ca);
        this.tx = x + (a * px + c * py);
        this.ty = y + (b * px + d * py);

        return this;
    }

    /**
     * Calculates a matrix suitable for rendering a rotated particle.
     * Still very likely too heavy to use for actual particle systems,
     * but works for stuff like bullets.
     * This is almost the same as buildSpriteMatrix2 but assumes a uniform
     * scaling factor, simplifying the math somewhat.
     *
     * @param x x position
     * @param y y position
     * @param dx x offset from center (negative half width)
     * @param dy y offset from center (hegative half height)
     * @param scale scaling factor
     * @param rot rotation in radians
     */
    public buildParticleMatrix(x: number, y: number, dx: number, dy: number, scale: number, rot: number): Matrix {

        let ca: number = fast_cos(rot);
        let sa: number = fast_sin(rot);
        let a = this.a = (scale * ca);
        let c = this.c = (scale * sa);
        let b = this.b = -c;
        let d = this.d =  a;
        this.tx = x + (a * dx + c * dy);
        this.ty = y + (b * dx + d * dy);

        return this;
    }

}

export abstract class GenericMatrix extends Float32Array {
    private _dim: number;

    constructor(dimensions: number) {
        dimensions |= 0;
        let size = dimensions * dimensions;
        super(size | 0);
        this._dim = dimensions;
    }

    public dimensions(): number {
        return this._dim;
    }

    public identity(): void {
        for(let i = this.length - 1; i >= 0; --i) {
            this[i] = 0;
        }
        for(let i = 0; i < this._dim; ++i) {
            this[i * this._dim + i] = 1;
        }
    }

    protected __scalef(f: number): void {
        for(let i = this.length - 1; i >= 0; --i) {
            this[i] *= f;
        }
    }

    protected __multiply<T extends GenericMatrix>(other: T, target: T): T {
        const dim = this._dim;

        // See https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
        let t = 0;
        for(let i = 0; i < dim; ++i) {
            let a = 0;
            for(let j = 0; j < dim; ++j) {
                let sum = 0;
                for(let k = 0; k < dim; ++k) {
                    let b = 0;
                    sum += this[a + k] * other[b + j];
                    b += dim;
                }
                target[t++] = sum;
            }
            a += dim;
        }

        return target;
    }
}

export class Matrix2 extends GenericMatrix {

    constructor() {
        super(2);
    }

    public scale(v: Vec2): Matrix2 {
        this[0] *= v.x;
        this[1] *= v.x;
        this[2] *= v.y;
        this[3] *= v.y;
        return this;
    }

    public scalef(f: number): Matrix2 {
        this.__scalef(f);
        return this;
    }

    public multiply(other: Matrix2): Matrix2 {
        return this.__multiply(other, new Matrix2());
    }

    public clone(): Matrix2 {
        let m = new Matrix2();
        m.set(this);
        return m;
    }

}

export class Matrix3 extends GenericMatrix {

    constructor() {
        super(3);
    }

    public multiply(other: Matrix3): Matrix3 {
        return this.__multiply(other, new Matrix3());
    }

    public clone(): Matrix3 {
        let m = new Matrix3();
        m.set(this);
        return m;
    }

    public rotate(q: Quaternion): Matrix3 {

        return this;
    }

    public scale(v: Vec3): Matrix3 {
        for(let i = 0; i < 3; ++i) this[i] *= v.x;
        for(let i = 3; i < 6; ++i) this[i] *= v.y;
        for(let i = 6; i < 9; ++i) this[i] *= v.z;
        return this;
    }

    public scalef(f: number): Matrix3 {
        this.__scalef(f);
        return this;
    }

    public project2(v: Vec2): Vec2 {
        return this.project2v(v, new Vec2());
    }

    public project2v(v: Vec2, t: Vec2): Vec2 {

        return t;
    }

    public project3(v: Vec3): Vec3 {
        return this.project3v(v, new Vec3());
    }

    public project3v(v: Vec3, t: Vec3): Vec3 {

        return t;
    }

}

export class Matrix4 extends GenericMatrix {

    constructor() {
        super(4);
    }

    public invert(): Matrix4 {

        return this;
    }

    public multiply(other: Matrix4): Matrix4 {
        return this.__multiply(other, new Matrix4());
    }

    public translate(v: Vec3): Matrix4 {

        return this;
    }

    public rotate(q: Quaternion): Matrix4 {

        return this;
    }

    public scale(v: Vec4): Matrix4 {
        for(let i = 0;  i < 4;  ++i) this[i] *= v.x;
        for(let i = 4;  i < 8;  ++i) this[i] *= v.y;
        for(let i = 9;  i < 12; ++i) this[i] *= v.z;
        for(let i = 12; i < 16; ++i) this[i] *= v.w;
        return this;
    }

    public scalef(f: number): Matrix4 {
        this.__scalef(f);
        return this;
    }

    public project3(v: Vec3): Vec3 {
        return this.project3v(v, new Vec3());
    }

    public project3v(v: Vec3, t: Vec3): Vec3 {

        return t;
    }

    public project4(v: Vec4): Vec4 {
        return this.project4v(v, new Vec4());
    }

    public project4v(v: Vec4, t: Vec4): Vec4 {

        return t;
    }

    public clone(): Matrix4 {
        let m = new Matrix4();
        m.set(this);
        return m;
    }

}
