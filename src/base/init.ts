import { array_add_unique } from "./mem/array";
import { debugLog } from "./core/logging";

let initRoutines: ({
    fn: () => void,
    prio: number
})[] = [];

export const registerInitRoutine = (fn: () => void, priority = 0) => {
    array_add_unique(initRoutines, {
        fn: fn,
        prio: priority
    });
}

export const initialize = () => {
    if(!initRoutines) return;

    initRoutines = initRoutines.sort((a, b) => b.prio - a.prio);

    debugLog("Perform base init");
    for(let i of initRoutines) {
        i.fn();
    }
    initRoutines = null;
}
