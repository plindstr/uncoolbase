import { Pool } from "../mem";
import { AABB, Matrix } from "../math";
import { Screen, debugWarn } from "../core";
import { Node2D } from "../scene2d";
import { Renderer2D, Color } from "../graphics";
import { debugWarnNamed } from "../core/logging";

//
// Quadtree.ts
//
// Class for testing collisions between objects
//

let maxdepth: number = 4;
let activePool: Pool<QuadTreeNode<any>> = null;
const pool_alloc = (minX: number, minY: number, maxX: number, maxY: number) => {
    let a = activePool.alloc();
    a.clear();
    let b = a.bounds;
    let min = b.min;
    let max = b.max;
    min.x = minX;
    min.y = minY;
    max.x = maxX;
    max.y = maxY;
    return a;
}

interface QuadTreeData<T> {
    aabb: AABB;
    data: T;
}

class QuadTreeNode<T> {

    public bounds: AABB;
    public data: QuadTreeData<T>[];
    public NW: QuadTreeNode<T>;
    public NE: QuadTreeNode<T>;
    public SW: QuadTreeNode<T>;
    public SE: QuadTreeNode<T>;

    constructor() {
        let me = this;
        me.bounds = new AABB();
        me.data = [];
        me.NW = null;
        me.NE = null;
        me.SW = null;
        me.SE = null;
    }

    public clear(): void {
        let me = this;
        me.data.splice(0, me.data.length);
        me.NW = null;
        me.NE = null;
        me.SW = null;
        me.SE = null;
    }

    public setBounds(minX: number, minY: number, maxX: number, maxY: number): void {
        let me = this;
        me.bounds.setXY(minX, minY);
        me.bounds.addXY(maxX, maxY);
    }

    public insert(aabb: AABB, data: T, depth: number): void {
        let me = this;
        let b = me.bounds;
        if(!aabb.isTouching(b)) return;

        // If we're here we know we're inside the root node
        let bmin = b.min;
        let bmax = b.max;
        let x0 = bmin.x;
        let y0 = bmin.y;
        let x1 = bmax.x;
        let y1 = bmax.y;
        let cx = (x0 + x1) * .5;
        let cy = (y0 + y1) * .5;

        // if the object overlaps our center point, it shall be a
        // child of this node, as it can not be fully contained by
        // a sub node
        if(depth === maxdepth || aabb.containsXorY(cx,cy)) {
            this.data.push({ aabb, data });
            return;
        }

        // Otherwise we add insert the object into a child node
        let lr = aabb.getCenterX() > cx;  // left/right
        let ud = aabb.getCenterY() > cy;  // up/down
        let quad = (ud ? 2 : 0) + (lr ? 1 : 0);
        let sub: QuadTreeNode<T> = null;

        switch(quad) {
            case 0: // left && up -> NW
            sub = me.NW;
            if(!sub) sub = me.NW = pool_alloc(x0, y0, cx, cy);
            break;
            case 1: // right && up -> NE
            sub = me.NE;
            if(!sub) sub = me.NE = pool_alloc(cx, y0, x1, cy);
            break;
            case 2: // left && down -> SW
            sub = me.SW;
            if(!sub) sub = me.SW = pool_alloc(x0, cy, cx, y1);
            break;
            case 3: // right && down -> SE
            sub = me.SE;
            if(!sub) sub = me.SE = pool_alloc(cx, cy, x1, y1);
            break;
        }

        sub.insert(aabb, data, depth + 1);
    }

    public testHits(ref: AABB, onHit: (collider: T) => void, firstHit: boolean): boolean {
        let me = this;
        if(!ref.isTouching(me.bounds)) return true;   // keep testing

        for(let d of me.data) {
            if(ref.isTouching(d.aabb)) {
                onHit.call(ref, d.data);
                if(firstHit) return false;  // stop testing
            }
        }

        if(firstHit) {
            let keepGoing: boolean = true;
            keepGoing && (me.NW && (keepGoing = me.NW.testHits(ref, onHit, true)));
            keepGoing && (me.NE && (keepGoing = me.NE.testHits(ref, onHit, true)));
            keepGoing && (me.SE && (keepGoing = me.SE.testHits(ref, onHit, true)));
            keepGoing && (me.SW && (keepGoing = me.SW.testHits(ref, onHit, true)));
            return keepGoing;
        } else {
            me.NW && (me.NW.testHits(ref, onHit, false));
            me.NE && (me.NE.testHits(ref, onHit, false));
            me.SE && (me.SE.testHits(ref, onHit, false));
            me.SW && (me.SW.testHits(ref, onHit, false));
        }

        return true;    // keep testing

    }

}

/**
 * Quadtree structure for doing AABB tests against the least amount of objects
 *
 * Automatically goes for max depth instead of dealing with rebalancing of the tree.
 * Defaults to screen size as interpreted by Scene2D - i.e. from negative half width
 * to positive half width and negative half height to positive half height.
 */
export class QuadTree<T> {

    private _qt_pool: Pool<QuadTreeNode<T>>;
    private _qt_root: QuadTreeNode<T>;
    private _qt_nodes: QuadTreeData<T>[];
    private _qt_maxdepth: number = 6;      // maximum depth before just adding straight to a container (6 subdivisions is a lot)

    constructor() {
        let me = this;
        me._qt_pool = new Pool<QuadTreeNode<T>>(64, () => new QuadTreeNode<T>());
        me._qt_root = new QuadTreeNode();
        me._qt_root.setBounds(-Screen.HALF_WIDTH, -Screen.HALF_HEIGHT, Screen.HALF_WIDTH, Screen.HALF_HEIGHT);
        me._qt_nodes = [];
    }

    public getNodeCount(): number {
        return this._qt_pool.size();
    }

    public getRoot(): QuadTreeNode<T> {
        return this._qt_root;
    }

    public setBounds(minX: number, minY: number, maxX: number, maxY: number): void {
        let me = this;
        let b = me._qt_root.bounds;

        let oldx0 = b.min.x;
        let oldy0 = b.min.y;
        let oldx1 = b.max.x;
        let oldy1 = b.max.y;

        b.setXY(minX, minY);
        b.addXY(maxX, maxY);

        if(me._qt_nodes.length) {
            if(oldx0 !== minX || oldy0 !== minY || oldx1 !== maxX || oldy1 !== maxY) {
                me.rebalance();
            }
        }
    }

    /**
     * Set size limits for this Tree
     *
     * @param treeDepth maximum number of subdivisions before just dumping object into lowest level
     */
    public setDepthLimit(treeDepth: number): void {
        this._qt_maxdepth = Math.min(1, treeDepth | 0);
    }

    /**
     * Find the width of the smallest container node. You want this to be larger
     * than your smallest object - maybe twice the size or so.
     * Can also be found by calculator - punch in <area width> divided by 2 to the power of <depthLimit>
     */
    public getMinFeatureWidth(): number {
        let me = this;
        return me._qt_root.bounds.getWidth() / (Math.pow(2, me._qt_maxdepth));
    }

    /**
     * Find the height of the smallest container node. You want this to be larger
     * than your smallest object - maybe twice the size or so.
     * Can also be found by calculator - punch in <area height> divided by 2 to the power of <depthLimit>
     */
    public getMinFeatureHeight(): number {
        let me = this;
        return me._qt_root.bounds.getHeight() / (Math.pow(2, me._qt_maxdepth));
    }

    public clear(): void {
        let me = this;
        me._qt_nodes.splice(0, me._qt_nodes.length);
        me._qt_pool.clear();
        me._qt_root.clear();
    }

    public rebalance(): void {
        let me = this;
        let nodes = me._qt_nodes.splice(0, me._qt_nodes.length);
        me._qt_pool.clear();
        me._qt_root.clear();
        for(let n of nodes) {
            me.add(n.aabb, n.data);
        }
    }

    public add(aabb: AABB, data: T): void {
        let me = this;
        if(!aabb.isTouching(me._qt_root.bounds)) {
            debugWarnNamed(this, "Discarded object ", data, " outside quadtree BB ", me);
            return;
        }

        maxdepth = me._qt_maxdepth;
        activePool = me._qt_pool;
        me._qt_root.insert(aabb, data, 0);
    }

    /**
     * Test against ALL collisions
     *
     * @param ref
     * @param onHit
     */
    public testCollisions(ref: AABB, onHit: (collider: T) => void): void {
        this._qt_root.testHits(ref, onHit, false);
    }

    public testFirstHit(ref: AABB, onHit: (collider: T) => void): void {
        this._qt_root.testHits(ref, onHit, true);
    }

}

const red = Color.RED;
const green = Color.GREEN;

const recursePainter = (r: Renderer2D, n: QuadTreeNode<any>) =>{
    let b = n.bounds;
    r.addRect(b.min.x, b.min.y, b.getWidth(), b.getHeight(), red);

    for(let d of n.data) {
        let bb = d.aabb;
        r.addRect(bb.min.x, bb.min.y, bb.getWidth(), bb.getHeight(), green);
    }

    if(n.NE) recursePainter(r, n.NE);
    if(n.NW) recursePainter(r, n.NW);
    if(n.SE) recursePainter(r, n.SE);
    if(n.SW) recursePainter(r, n.SW);
}

export class QuadtreeVisualizer extends Node2D {

    private _tree: QuadTree<any>;

    constructor(tree: QuadTree<any>) {
        super();
        this.markAsDrawable();
        this.markAsSticky();

        this._tree = tree;
    }

    public draw(r: Renderer2D, mtx: Matrix, alpha: number): void {

        let root = this._tree.getRoot();

        r.setMatrix(mtx);
        r.setAlpha(alpha);
        r.beginRectsOutlined(this._tree.getNodeCount());
        recursePainter(r, root);
        r.draw();

    }

}
