import { InputMethod, setLastInputMethod } from "./coreinput";

let reserved: number[] = [ 13, 116, 122, 123 ];

export const reserveKey = (code: number) => {
    if(reserved.indexOf(code) < 0) {
        reserved.push(code);
    }
};

export const isReservedKey = (code: number) => {
    return reserved.indexOf(code) >= 0;
};

export enum KeySym {
    LEFT = 37,
    RIGHT = 39,
    UP = 38,
    DOWN = 40,

    SPACE = 32,
    SHIFT = 16,
    CTRL = 17,
    ENTER = 13,
    ESC = 27,

    A = 65,
    B = 66,
    C = 67,
    D = 68,
    E = 69,
    F = 70,
    G = 71,
    H = 72,
    I = 73,
    J = 74,
    K = 75,
    L = 76,
    M = 77,
    N = 78,
    O = 79,
    P = 80,
    Q = 81,
    R = 82,
    S = 83,
    T = 84,
    U = 85,
    V = 86,
    W = 87,
    X = 88,
    Y = 89,
    Z = 90,

    NUM1 = 49,
    NUM2 = 50,
    NUM3 = 51,
    NUM4 = 52,
    NUM5 = 53,
    NUM6 = 54,
    NUM7 = 55,
    NUM8 = 56,
    NUM9 = 57,
    NUM0 = 48,

    F1 = 112,
    F2 = 113,
    F3 = 114,
    F4 = 115,
    F5 = 116,
    F6 = 117,
    F7 = 118,
    F8 = 119,
    F9 = 120,
    F10 = 121,
    F11 = 122,
    F12 = 123
}

let keystate: boolean[] = [];
for(let i = 0; i < 386; ++i) {
    keystate.push(false);
}

export class Keyboard {

    private _current: boolean[] = [];
    private _last: boolean[] = [];
    private _nativeHandlers: Function[] = [];
    private _anyDown: boolean = false;
    private _anyPressed: boolean = false;
    private _anyReleased: boolean = false;

    private _keydown = (e: KeyboardEvent) => {
        setLastInputMethod(InputMethod.KEYBOARD);

        keystate[e.keyCode] = true;

        for(let h of this._nativeHandlers) {
            h.call(null, e);
        }

        if(!isReservedKey(e.keyCode)) {
            e.preventDefault();
        }

        return false;
    };

    private _keyup = (e: KeyboardEvent) => {
        setLastInputMethod(InputMethod.KEYBOARD);

        keystate[e.keyCode] = false;

        for(let h of this._nativeHandlers) {
            h.call(null, e);
        }

        if(!isReservedKey(e.keyCode)) {
            e.preventDefault();
        }

        return false;
    };

    constructor() {
        for(let i = 0; i < 386; ++i) {
            this._current.push(keystate[i]);
            this._last.push(keystate[i]);
        }

        window.addEventListener('keyup', this._keyup, false);
        window.addEventListener('keydown', this._keydown, false);
        window.addEventListener('keypress', () => { return false; }, false);
    }

    /**
     * Add a native event handler, that gets called for both
     * keyup and keydown events.
     *
     * @param fn a native keyboard event handler
     */
    public addNativeHandler(fn: (e: KeyboardEvent) => void) {
        this._nativeHandlers.push(fn);
    }

    public isAnyKeyDown(): boolean {
        return this._anyDown;
    }

    public isAnyKeyPressed(): boolean {
        return this._anyPressed;
    }

    public isAnyKeyReleased(): boolean {
        return this._anyReleased;
    }

    public isKeyDown(sym: number): boolean {
        return this._current[sym];
    }

    public isKeyPressed(sym: number): boolean {
        return this._current[sym] && !this._last[sym];
    }

    public isKeyReleased(sym: number): boolean {
        return !this._current[sym] && this._last[sym];
    }

    public isKeyHeld(sym: number): boolean {
        return this._current[sym] && this._last[sym];
    }

    public update(): void {
        this._anyDown = false;
        this._anyPressed = false;
        this._anyReleased = false;
        for(let i: number = 0; i < 386; ++i) {
            this._last[i] = this._current[i];
            this._current[i] = keystate[i];
            this._anyDown = this._anyDown || keystate[i];
            this._anyPressed = this._anyPressed || (this._current[i] && !this._last[i]);
            this._anyReleased = this._anyPressed || (!this._current[i] && this._last[i]);
        }
    }
}
