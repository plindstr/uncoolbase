import { Vec2 } from "../math";
import { scaleInput, inputPosition, wasInputTouchedThisFrame, setLastInputMethod, InputMethod } from "./coreinput";

let buttons: boolean[] = [];
let mouseX: number = 0;
let mouseY: number = 0;

let updatePosition = (e: MouseEvent) => {
    scaleInput(e.clientX, e.clientY);
    mouseX = inputPosition.x;
    mouseY = inputPosition.y;
};

let buttonUp = (e: MouseEvent) => {
    buttons[e.button] = false;
};

let buttonDown = (e: MouseEvent) => {
    buttons[e.button] = true;
};

let cancel = (e: MouseEvent) => {
    e.preventDefault();
}

let clearWheel = () => {
    // Clear mouse wheel
    buttons[MouseButton.WHEEL_UP] = false;
    buttons[MouseButton.WHEEL_DOWN] = false;
}

export enum MouseButton {
    LEFT = 0,
    MIDDLE = 1,
    RIGHT = 2,
    WHEEL_UP = 4,
    WHEEL_DOWN = 5
}

export class Mouse {

    private _position_current: Vec2 = new Vec2(-500,-500);
    private _position_last: Vec2 = new Vec2(-500,-500);
    private _position_delta: Vec2 = new Vec2();

    private _buttons_current: boolean[] = [];
    private _buttons_last: boolean[] = [];

    constructor() {
        for(var i = 0; i < 6; ++i) {
            buttons.push(false);
            this._buttons_current.push(false);
            this._buttons_last.push(false);
        }

        window.addEventListener('mouseup', (e) => {
            if(wasInputTouchedThisFrame()) return;

            setLastInputMethod(InputMethod.MOUSE);

            buttonUp(e);
            updatePosition(e);
            cancel(e);
        });

        window.addEventListener('mousedown', (e) => {
            if(wasInputTouchedThisFrame()) return;

            setLastInputMethod(InputMethod.MOUSE);

            buttonDown(e);
            updatePosition(e);
            cancel(e);
        });

        window.addEventListener('mousemove', (e) => {
            if(!wasInputTouchedThisFrame()) {
                setLastInputMethod(InputMethod.MOUSE);
            }

            updatePosition(e);
            cancel(e);
        });

        window.addEventListener('contextmenu', (e) => {
            if(!wasInputTouchedThisFrame()) {
                setLastInputMethod(InputMethod.MOUSE);
            }

            updatePosition(e);
            cancel(e);
        });

        window.addEventListener('wheel', (e) => {
            if(!wasInputTouchedThisFrame()) {
                setLastInputMethod(InputMethod.MOUSE);
            }

            if(e.deltaY > 0) buttons[MouseButton.WHEEL_DOWN] = true;
            else buttons[MouseButton.WHEEL_UP] = true;

            updatePosition(<MouseEvent>e);
        });
    }

    public getX(): number {
        return this._position_current.x;
    }

    public getY(): number {
        return this._position_current.y;
    }

    public getPosition(): Vec2 {
        return this._position_current;
    }

    public getLastX(): number {
        return this._position_last.x;
    }

    public getLastY(): number {
        return this._position_last.y;
    }

    public getLastPosition(): Vec2 {
        return this._position_last;
    }

    public getDeltaX(): number {
        return this._position_delta.x;
    }

    public getDeltaY(): number {
        return this._position_delta.y;
    }

    public getDelta(): Vec2 {
        return this._position_delta;
    }

    public isButtonDown(idx: number): boolean {
        return this._buttons_current[idx];
    }

    public isButtonPressed(idx: number): boolean {
        return this._buttons_current[idx] && !this._buttons_last[idx];
    }

    public isButtonReleased(idx: number): boolean {
        return !this._buttons_current[idx] && this._buttons_last[idx];
    }

    public isButtonHeld(idx: number): boolean {
        return this._buttons_current[idx] && this._buttons_last[idx];
    }

    public update(): void {
        this._position_last.set(this._position_current);
        this._position_current.setXY(mouseX, mouseY);
        this._position_delta.set(this._position_current).subtract(this._position_last);

        for(var i = 0; i < 6; ++i) {
            this._buttons_last[i] = this._buttons_current[i];
            this._buttons_current[i] = buttons[i];
        }

        clearWheel();
    }

}
