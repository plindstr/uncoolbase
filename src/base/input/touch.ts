import { scaleInput, inputPosition, setLastInputMethod, InputMethod, markInputTouchedThisFrame } from "./coreinput";
import { Vec2, Rect } from "../math";
import { array_clear } from "../mem";
import { debugError, Screen, getTimeCurrent } from "../core";

const mmax = (a: number, b: number) => a > b ? a : b;

export class TouchState {

    _id: number = -1;
    _pos_x: number = 0.0;
    _pos_y: number = 0.0;
    _delta_x: number = 0.0;
    _delta_y: number = 0.0;
    _start_x: number = 0.0;
    _start_y: number = 0.0;
    _radius: number = 1.0;
    _t_started: number = 0.0;
    _moved: boolean = false;
    _down: boolean = false;
    _cancel: boolean = false;

    constructor(t: Touch | TouchState) {
        if(t instanceof TouchState) {
            this.copy(t);
        } else {
            this._id = t.identifier;
            scaleInput(t.clientX, t.clientY);
            this._pos_x = this._start_x = inputPosition.x;
            this._pos_y = this._start_y = inputPosition.y;
            this._radius = mmax(t.radiusX, t.radiusY) || 1.0;
            this._t_started = getTimeCurrent();
            this._down = true;
        }
    }

    public getID(): number {
        return this._id;
    }

    public getPosition(): Vec2 {
        return new Vec2(this._pos_x, this._pos_y);
    }

    public getDelta(): Vec2 {
        return new Vec2(this._delta_x, this._delta_y);
    }

    public getStartPosition(): Vec2 {
        return new Vec2(this._start_x, this._start_y);
    }

    public getRadius(): number {
        return this._radius;
    }

    public isDown(): boolean {
        return this._down;
    }

    public isMoved(): boolean {
        return this._moved;
    }

    public getTimeStarted(): number {
        return this._t_started;
    }

    /**
     * Cancel this touch state, so that it won't be included
     * results for subsequent calls to TouchTracker.getTouches
     */
    public cancel(): TouchState {
        this._cancel = true;
        return this;
    }

    copy(t: TouchState): TouchState {
        this._id = t._id;
        this._pos_x = t._pos_x;
        this._pos_y = t._pos_y;
        this._delta_x = t._delta_x;
        this._delta_y = t._delta_y;
        this._start_x = t._start_x;
        this._start_y = t._start_y;
        this._moved = t._moved;
        this._down = t._down;
        this._cancel = t._cancel;
        this._radius = t._radius;
        this._t_started = t._t_started;
        return this;
    }
}

const touch_map      = new Map<number, TouchState>();
const started_touches: TouchState[] = [];
const moved_touches:   TouchState[] = [];
const ended_touches:   TouchState[] = [];
const active_touches:  TouchState[] = [];

let num_started = 0;
let num_moved   = 0;
let num_ended   = 0;
let num_active  = 0;

/// Find collective center of all touches
const getTouchCenter = (touches: TouchState[]) => {
    let x = 0, y = 0, n = 0;
    for(let t of touches) {
        if(t._down && !t._cancel) {
            x += t._pos_x;
            y += t._pos_y;
            n++;
        }
    }
    if(n) {
        n = 1.0 / n;
        x *= n;
        y *= n;
    }
    return new Vec2(x,y);
}

/// Recreate the active touches array based on the touches list of the touch event
const setActiveTouches = (e: TouchEvent) => {
    array_clear(active_touches);
    num_active = 0;
    for(let i = 0, l = e.touches.length; i < l; ++i) {
        let t = e.touches[i];
        let ts = touch_map.get(t.identifier);
        if(!ts) {
            debugError("[base.TouchState] Touch state with identifier " + t.identifier + " not found!");
        } else {
            active_touches.push(ts);
            ++num_active;
        }
    }
}

/// Start a new touch
const beginTouch = (e: TouchEvent) => {
    setLastInputMethod(InputMethod.TOUCH);
    markInputTouchedThisFrame();

    for(let i = 0, l = e.changedTouches.length; i < l; ++i) {
        let t = e.changedTouches[i];
        let ts = new TouchState(t);
        touch_map.set(ts._id, ts);
        started_touches.push(ts);
        ++num_started;
    }

    setActiveTouches(e);
    e.preventDefault();
}

const moveTouch = (e: TouchEvent) => {
    setLastInputMethod(InputMethod.TOUCH);
    markInputTouchedThisFrame();

    for(let i = 0, l = e.changedTouches.length; i < l; ++i) {
        let t = e.changedTouches[i];
        let ts = touch_map.get(t.identifier);
        scaleInput(t.clientX, t.clientY)
        ts._delta_x = inputPosition.x - ts._pos_x;
        ts._delta_y = inputPosition.y - ts._pos_y;
        ts._pos_x = inputPosition.x;
        ts._pos_y = inputPosition.y;
        moved_touches.push(ts);
        ++num_moved;
    }

    setActiveTouches(e);
    e.preventDefault();
}

const endTouch = (e: TouchEvent) => {
    setLastInputMethod(InputMethod.TOUCH);
    markInputTouchedThisFrame();

    for(let i = 0, l = e.changedTouches.length; i < l; ++i) {
        let t = e.changedTouches[i];
        let ts = touch_map.get(t.identifier);
        touch_map.delete(ts._id);
        scaleInput(t.clientX, t.clientY);
        ts._delta_x = inputPosition.x - ts._pos_x;
        ts._delta_y = inputPosition.y - ts._pos_y;
        ts._pos_x = inputPosition.x;
        ts._pos_y = inputPosition.y;
        ended_touches.push(ts);
        ++num_ended;
        ts._down = false;
    }

    setActiveTouches(e);
    e.preventDefault();
}

const update = () => {
    num_started = 0;
    num_moved = 0;
    num_ended = 0;
    array_clear(started_touches);
    array_clear(moved_touches);
    array_clear(ended_touches);
}

/**
 * Multi-touch tracker
 */
export class TouchTracker {

    constructor() {
        window.addEventListener('touchstart',  beginTouch);
        window.addEventListener('touchmove',   moveTouch);
        window.addEventListener('touchend',    endTouch);
        window.addEventListener('touchcancel', endTouch);

        // ios 9+ prevent pinch
        window.addEventListener('gesturechange', (e) => {
            e.preventDefault();
        })
    }

    public getActiveTouchCount(): number {
        return num_active;
    }

    public getStartedTouchCount(): number {
        return num_started;
    }

    public getMovedTouchCount(): number {
        return num_moved;
    }

    public getEndedTouchCount(): number {
        return num_ended;
    }

    public getCommonCenter(): Vec2 {
        return getTouchCenter(active_touches);
    }

    public getStartedTouches(area: Rect = Screen.AREA): TouchState[] {
        let a: TouchState[] = [];
        for(let ts of started_touches) {
            if(!ts._cancel && area.containsCircleXY(ts._pos_x, ts._pos_y, ts._radius)) {
                a.push(new TouchState(ts));
            }
        }
        return a;
    }

    public isTouchStartedInside(area: Rect = Screen.AREA): boolean {
        for(let ts of started_touches) {
            if(!ts._cancel && area.containsCircleXY(ts._pos_x, ts._pos_y, ts._radius)) {
                return true;
            }
        }
        return false;
    }

    public getMovedTouches(area: Rect = Screen.AREA): TouchState[] {
        let a: TouchState[] = [];
        for(let ts of moved_touches) {
            if(!ts._cancel && area.containsCircleXY(ts._pos_x, ts._pos_y, ts._radius)) {
                a.push(new TouchState(ts));
            }
        }
        return a;
    }

    public isTouchMovedInside(area: Rect = Screen.AREA): boolean {
        for(let ts of moved_touches) {
            if(!ts._cancel && area.containsCircleXY(ts._pos_x, ts._pos_y, ts._radius)) {
                return true;
            }
        }
        return false;
    }

    public getEndedTouches(area: Rect = Screen.AREA): TouchState[] {
        let a: TouchState[] = [];
        for(let ts of ended_touches) {
            if(!ts._cancel && area.containsCircleXY(ts._pos_x, ts._pos_y, ts._radius)) {
                a.push(new TouchState(ts));
            }
        }
        return a;
    }

    public isTouchEndedInside(area: Rect = Screen.AREA): boolean {
        for(let ts of ended_touches) {
            if(!ts._cancel && area.containsCircleXY(ts._pos_x, ts._pos_y, ts._radius)) {
                return true;
            }
        }
        return false;
    }

    public getActiveTouches(area: Rect = Screen.AREA): TouchState[] {
        let a: TouchState[] = [];
        for(let ts of active_touches) {
            if(!ts._cancel && area.containsCircleXY(ts._pos_x, ts._pos_y, ts._radius)) {
                a.push(new TouchState(ts));
            }
        }
        return a;
    }

    public isTouchActiveInside(area: Rect = Screen.AREA): boolean {
        for(let ts of active_touches) {
            if(!ts._cancel && area.containsCircleXY(ts._pos_x, ts._pos_y, ts._radius)) {
                return true;
            }
        }
        return false;
    }

    public matchTouches(condition: (x: number, y: number, r: number) => boolean): boolean {
        for(let ts of active_touches) {
            if(condition(ts._pos_x, ts._pos_y, ts._radius)) {
                return true;
            }
        }
        return false;
    }

    public update(): void {
        update();
    }

}
