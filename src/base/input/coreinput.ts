import { Vec2 } from "../math";

export enum InputMethod {
    NONE,
    KEYBOARD,
    MOUSE,
    TOUCH
}

let sizeratio: number = 1.0;
let lastMethod = InputMethod.NONE;
let touchedThisFrame = false;

export const inputOffset = new Vec2();

export const inputPosition = new Vec2();

export const scaleInput = (screenX: number, screenY: number): void => {
    // Calculate unscaled canvas-specific input coordinates
    let adjustedX = screenX - inputOffset.x;
    let adjustedY = screenY - inputOffset.y;

    // Canvas is scaled by CSS, sizeratio calculates that
    let sz = 1.0 / sizeratio;
    inputPosition.x = adjustedX * sz;
    inputPosition.y = adjustedY * sz;
};

export const getInputSizeRatio = () => sizeratio;

export const setInputSizeRatio = (ratio: number) => sizeratio = ratio;

export const getLastInputMethod = (): InputMethod => lastMethod;

export const setLastInputMethod = (method: InputMethod) => lastMethod = method;

export const resetInputTouchedThisFrame = () => touchedThisFrame = false;

export const markInputTouchedThisFrame = () => touchedThisFrame = true;

export const wasInputTouchedThisFrame = () => touchedThisFrame;
