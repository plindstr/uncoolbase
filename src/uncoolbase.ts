import * as vibes from "./lib/uncoolvibes"
export { vibes }

import { injectDAT } from "./dat.gui/dat.gui";

// Try to inject dat.GUI
injectDAT();

// Export UncoolBase as the 'base' module
import * as base from "./base/base";
export { base }
