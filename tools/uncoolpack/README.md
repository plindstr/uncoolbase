# UNCOOLPACK

Use `liblzg` to compress a javascript file,
convert it into a base64 string and pack it
into a new javascript file together with the
liblzg javascript decompressor to create a
self-deploying compressed javascript source
file. This file can then be used at deployment
time to make it slightly more difficult to
cheat at games on platform such as Facebook,
as well as making the source a bit less
immediately obvious.

Any programmer worth his salt will be able
to easily get at the source, though; don't
rely on this for any sort of real security.

End users can never be trusted with secrets.

## Requirements

Have `closure-compiler` and `lzg` on your `PATH`.
Have `js-beautify` available to `node`.

## Compiling

Run `tsc`.

## Installation

Run `npm install -g` after compiling.

## Usage

Run `uncoopack <input> <output>`. Running `uncoolpack`
without any arguments prints a help screen.

