#!/usr/bin/env node

/**
 * UncoolPack - pack your minified JavaScript
 * with lzg and create a self-deploying js file.
 */

/// <reference path="decrunch.ts" />
/// <reference path="obfuscate.ts" />

const fs = require('fs');
const { execSync } = require('child_process');
const beautify = require('js-beautify').js;
const uglify = require('terser').minify;
const babel = require('@babel/core');
const babel_env = require('@babel/preset-env');
const plugin_strip = require('babel-plugin-strip-function-call');

let inputSize: number = 0;
let tempSize: number = 0;
let outputSize: number = 0;

/**
 * Read the input source file, return contents as UTF8
 *
 * @param fin input source file
 */
function readInputFile(fin: string): string {
    let input = fs.readFileSync(fin, function(err) {
        if(err) {
            console.error("Error reading input file");
            console.error(err);
            process.exit(7);
        }
    }).toString('utf8');

    inputSize = input.length;
    return input;
}


/**
 * Run UglifyJS on the source
 *
 * @param input uglified source code
 */
function uglifyInput(input: string): string {
    return uglify(input, {
        /*
        parse: {
        },

        compress: {
            drop_console: true,
            passes: 4,
            hoist_funs: true,
            toplevel: true,
            unsafe: true,
        },

        mangle: {
            toplevel: true,
            properties: true,
            keep_quoted: true
        },
        */

        /*
        compress: {
            toplevel: true,
            hoist_funs: true
        },
        */
        mangle: {
            toplevel: true,
            //properties: true,
            keep_quoted: true
        },

        output: {
            ecma: 6
        }

    }).code;
}

/**
 * Beautify the source. Can be good with debug stuff.
 *
 * @param input input source code
 */
function beautifyInput(input: string): string {
    if(!input || input.length == 0) {
        console.error("Beautifier received null or empty string!");
        console.error("input: ", input);
        process.exit(11);
    }

    let output = beautify(input, {
        "indent_size": 1,
        "indent_char": " ",
        "indent_with_tabs": false,
        "editorconfig": false,
        "eol": "\n",
        "end_with_newline": false,
        "indent_level": 0,
        "preserve_newlines": false,
        "max_preserve_newlines": 1,
        "space_in_paren": false,
        "space_in_empty_paren": false,
        "jslint_happy": true,
        "space_after_anon_function": false,
        "space_after_named_function": false,
        "brace_style": "collapse",
        "unindent_chained_methods": false,
        "break_chained_methods": false,
        "keep_array_indentation": false,
        "unescape_strings": true,
        "wrap_line_length": 80,
        "e4x": false,
        "comma_first": false,
        "operator_position": "after-newline"
    });
    tempSize = output.length;
    return output;
}

/**
 * Run LZG on source string
 *
 * @param source source string
 * @param ftemp output file name
 */
function runLZGSource(source: string, ftemp: string): string {
    // Write escaped input to temp file
    fs.writeFileSync(ftemp, source);
    let lzg = runLZG(ftemp);
    fs.unlinkSync(ftemp);
    return lzg;
}

/**
 * Run LZG on an input file and return the file data as a base64 encoded string
 * @param fin
 */
function runLZG(fin: string): string {
    let compressed: string;
    try {
        compressed = execSync('lzg -s -9 ' + fin).toString('base64');
    } catch(e) {
        console.error("Failed to run lzg");
        console.error(e);
        process.exit(6);
    }
    return compressed;
}

/**
 * Create the wrapper script
 *
 * @param b64conf
 */
function makeWrapper(source: string): string {

    // Create wrapper header
    let wrapper: string = '';

    if(opt_debug) {
        wrapper += 'try {';
    }

    wrapper += 'new Function('  // create function
    wrapper += '(function() {'  // inner closure

    // Add decrunch code method
    wrapper += decrunch.toString();

    // Call decrunch
    wrapper += 'let source = decrunch(';
    wrapper += 'atob(\'' + source + '\')';
    wrapper += ');';

    if(opt_debug) {
        wrapper += 'console.log(source);';
    }

    // Return source
    wrapper += 'return source'

    // Add wrapper end
    wrapper += '})()';            // call inner closure to generate source
    wrapper += ').call(window);'  // Invoke the function to begin program load

    if(opt_debug) {
        wrapper += '} catch(e) {';
        wrapper += 'console.error(e);';
        wrapper += 'debugger;';
        wrapper += '}';
    }

    return wrapper;
}

function toOpts(map: any): string {
    let s = ' ';
    for(let prop in map) {
        if(!map.hasOwnProperty(prop)) continue;

        s += '--' + prop;
        if(map[prop]) {
            s += ' ' + map[prop];
        }
        s += ' ';
    }
    return s;
}

function runClosure(source: string): string {
    let comp;
    let compiler_command = 'google-closure-compiler';
    let compiler_opts = {
        language_in: 'ECMASCRIPT_2017',
        language_out: 'ECMASCRIPT_2017',
        assume_function_wrapper: '',
    };

    let cmd = compiler_command + toOpts(compiler_opts) + '2>/dev/null';
    try {
        comp = execSync(cmd, {
            input: source
        });
    } catch(e) {
        console.error("Error running closure compiler");
        console.error(e);
        process.exit(9);
    }
    return comp.toString('utf8');
}

function bark(...args: any[]) {
    if(!opt_quiet) {
        console.log.apply(console, arguments);
    }
}

function writeOutput(fout: string, source: string): void {

    // Write output file
    fs.writeFile(fout, source, function(err) {
        if(err) {
            console.error("Error writing file " + fout);
            console.error(err);
            process.exit(10);
        }

        //
        // Print stats
        //

        bark("Wrote " + fout);

        bark("Input source size: " + inputSize + " bytes");
        if(opt_beauty_input) {
            bark("Beautified source size: " + tempSize + " bytes");
        }
        bark("Output file size: " + outputSize + " bytes (" +
            (((outputSize * 1000 / inputSize) | 0) / 10) + "% of input)"
        );

        // Exit happy
        process.exit(0);
    });

}

function pack(fin: string, fout: string): void {

    let source = readInputFile(fin);

    if(opt_strip_debug) {
        bark("Stripping debug calls from input...");

        source = babel.transformSync(source, {
            plugins: [
                [
                    plugin_strip, {
                        strip: [
                            "assert",
                            "base.assert",
                            "console.log",
                            "console.log.apply",
                            "console.log.call",
                            "console.warn",
                            "console.warn.apply",
                            "console.warn.call",
                            // Do not remove console.error calls by default
                            //"console.error",
                            //"console.error.apply",
                            //"console.error.call",
                            "debugLog",
                            "debugLog.apply",
                            "debugLog.call",
                            "debugWarn",
                            "debugWarn.apply",
                            "debugWarn.call",
                            "debugError",
                            "debugError.apply",
                            "debugError.call",
                            "debugLogNamed",
                            "debugLogNamed.apply",
                            "debugLogNamed.call",
                            "debugWarnNamed",
                            "debugWarnNamed.apply",
                            "debugWarnNamed.call",
                            "debugErrorNamed",
                            "debugErrorNamed.apply",
                            "debugErrorNamed.call",
                            "debug",
                            "debug.apply",
                            "debug.call",
                            "base.assert",
                            "base.debugLog",
                            "base.debugLog.apply",
                            "base.debugLog.call",
                            "base.debugWarn",
                            "base.debugWarn.apply",
                            "base.debugWarn.call",
                            "base.debugError",
                            "base.debugError.apply",
                            "base.debugError.call",
                            "base.debug",
                            "base.debug.apply",
                            "base.debug.call",
                            "base.debugLogNamed",
                            "base.debugLogNamed.apply",
                            "base.debugLogNamed.call",
                            "base.debugWarnNamed",
                            "base.debugWarnNamed.apply",
                            "base.debugWarnNamed.call",
                            "base.debugErrorNamed",
                            "base.debugErrorNamed.apply",
                            "base.debugErrorNamed.call",
                            "debugger"
                        ]
                    }
                ]
            ]
        }).code;
    }

    if(opt_closure_input) {
        bark("Running Closure Compiler on input...");
        source = runClosure(source);
    }

    if(opt_obfuscate_input) {
        bark("Obfuscating properties of input...");
        source = obfuscator.obfuscate(source);
    }

    if(opt_ugly_input) {
        bark("Running UglifyJS on input...");
        let l = source.length;
        source = uglifyInput(source);
        bark("Length before: " + l + ", length after: " + source.length);
    }

    if(opt_beauty_input) {
        bark("Running JS-Beautify on input...");
        source = beautifyInput(source);
    }

    if(opt_keep_source) {
        bark("Writing modified source as " + (fout + ".in") + "...");
        writeOutput(fout + '.in', source);
    }

    let output = source;
    if(opt_wrapper) {
        bark("Running LGZ compressor...");
        let compressed = runLZGSource(source, fin + '.tmp');

        bark("Creating wrapper...");
        let wrapper = makeWrapper(compressed);

        // Run closure compiler on wrapper
        if(opt_wrapper) {
            bark("Running Closure Compiler on wrapper...");
            wrapper = runClosure(wrapper);
        }
        outputSize = wrapper.length;
    } else {
        outputSize = source.length;
    }

    bark("Writing output...");
    writeOutput(fout, output);
    bark("Done.");
}

var opt_force: boolean = false;
var opt_strip_debug = false;
var opt_keep_source = false;
var opt_beauty_input: boolean = false;
var opt_ugly_input: boolean = false;
var opt_obfuscate_input: boolean = false;
var opt_closure_input: boolean = false;
var opt_wrapper: boolean = false;
var opt_debug: boolean = false;
var opt_quiet: boolean = false;

function handleOptions() {

    function help() {
        console.log("UncoolPack 2.0");
        console.log("Usage: uncoolpack [options] <INPUT> <OUTPUT>");
        console.log("Options:");
        console.log("   -f     : overwrite output file if it exists");
        console.log("   -s     : strip debug and console.* calls from output");
        console.log("   -k     : keep modified source (before wrapping)");
        console.log("   -d     : generate debug routines in wrapper");
        console.log("   -b     : beautify output file");
        console.log("   -o     : obfuscate properties on input");
        console.log("   -c     : run closure compiler on input");
        console.log("   -w     : generate wrapper");
        console.log("   -u     : run uglifyjs on input (after closure compiler if -c is specified)");
        console.log("   -q     : run in quiet mode (do not print stats to console)")
        process.exit(1);
    }

    let argc = process.argv.length;

    if(argc < 4) {
        help();
    }

    for(let i = 2; i < argc; ++i) {
        switch(process.argv[i]) {
            case '-f':
                opt_force = true;
            break;
            case '-s':
                opt_strip_debug = true;
            break;
            case '-k':
                opt_keep_source = true;
            break;
            case '-b':
                opt_beauty_input = true;
            break;
            case '-o':
                opt_obfuscate_input = true;
            break;
            case '-c':
                opt_closure_input = true;
            break;
            case '-w':
                opt_wrapper = true;
            break;
            case '-d':
                opt_debug = true;
            break;
            case '-u':
                opt_ugly_input = true;
            break;
            case '-q':
                opt_quiet = true;
            break;
        }
    }
}

(function main() {

    // Handle options first
    handleOptions();

    // Handle input and output files
    let argc = process.argv.length;
    let inputFilename = process.argv[argc - 2];
    let outputFilename = process.argv[argc - 1];

    if(inputFilename == outputFilename) {
        console.error("Error: input file name is same as output file name");
        process.exit(2);
    }

    if(!fs.existsSync(inputFilename)) {
        console.error("Error: input file does not exist");
        process.exit(3);
    }

    if(fs.existsSync(outputFilename) && !opt_force) {
        console.error("Error: output file name exists; use -f option to overwrite");
        process.exit(4);
    }

    pack(inputFilename, outputFilename);

})();
