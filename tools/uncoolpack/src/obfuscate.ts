//
// Tries to obfuscate private fields
//

namespace obfuscator {

    let repl = 1;
    let replacements: { [symbol: string]: string } = {};

    function findReplacements(src: string): void {
        repl = 1;
        replacements = {};

        let regex = /(?:[\s\.\!\^\~\-\(\)\{\}\"\'\+\,\;\:])(_[\w_]+)/g;

        // Match properties and variables of the form "_word"
        let matches: RegExpMatchArray = src.match(regex);
        if(!!matches) {
            for(let i = 0; i < matches.length; ++i) {
                let sym = matches[i].trim();
                if(sym.startsWith('.')) {
                    sym = sym.slice(1);
                }

                if(!replacements[sym]) {
                    let s = repl.toString(16);
                    if(s.length % 2) s = '0' + s;
                    s = '$0x' + s;
                    replacements[sym] = s;
                    repl++;
                }
            }
        }

        console.log("Found " + (repl - 1) + " eligible symbols");
    }

    function applyReplacements(src: string): string {

        let regex = /(?:[\s\.\!\^\~\-\(\)\{\}\"\'\+\,\;\:])(_[\w_]+)/;
        let counter = 0;

        do {
            let idx = src.search(regex);
            if(idx < 0) break;
            let sym = src.match(regex)[1];
            let r = replacements[sym];

            src = src.substr(0, idx) + src.substr(idx).replace(sym, r);
            ++counter;
        } while(true);

        console.log(counter + " replacements made");

        return src;
    }

    export function obfuscate(src: string): string {
        findReplacements(src);
        return applyReplacements(src);
    }

}
