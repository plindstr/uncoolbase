INFO
----

This directory _used_ to contain various implementations of the
decompressor. However, it now only contains the javascript file
that we're interested in.

FILES
-----

lzgmini.js
    A JavaScript implementation of the LZG decoder.
